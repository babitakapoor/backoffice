<?php
/**
 * PHP version 5.

 * @category Include

 * @package Footer_Content

 * @author Shanetha Tech <info@shanethatech.com>

 * @license movesmart.company http://movesmart.company

 * @link http://movesmart.company/admin/

 * @description content which need to stay on the footer.
 */
?>
            <!--  footer block-->
            <div class="footer">
                <p>&copy; <?php echo date('Y').' '.MODULE_VERSION.'-'.MODULE_VERSION_DATE;?> All Rights Reserved</p>
            </div>
        </div>
        <!--//-->
    </body>
</html>
<?php
/* Set site flash message begins */
echo $this->common->showFlashMessage();
/* Set site flash message ends */

/* To calculate page load time */
if (DEBUG_MODE == 1) {
    global $pageStarttime;
    $endPageLoad = date('h-i-s');
    echo $pageStarttime.' <==>'.$endPageLoad;
}
?>

