<?php
/**
 * PHP version 5.
 
 * @category Include
 
 * @package Menu
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description content which is used to show the menu.
 */
if (isset($_SESSION['user']['user_id'])) {
    $userId = $_SESSION['user']['user_id'];
    $first_name = $_SESSION['user']['first_name'];
    $last_name = $_SESSION['user']['last_name'];
    $parmsmenu['user_id'] = $userId;
    $getMembers = $this->members->getListEditMembers($parmsmenu);
    $profileImage = $getMembers['userimage'];
    $imgPath = '../images'.DS.'uploads'.DS.'movesmart'.DS.'profile'.DS.$profileImage;
    if (($profileImage) && (file_exists($imgPath))) {
        $userImage = 'uploads'.DS.'movesmart'.DS.'profile'.DS.$profileImage;
    } else {
        $userImage = 'user-up-photo.jpg';
    }
} else {
    $userId = '';
    $first_name = '';
    $last_name = '';
    $userImage = 'user-up-photo.jpg';
}
    // for displaying User Profile Image End.
?>
<!--  Menu block-->
    <div class="head-right">
      <ul class="nav-link clearfix">
    <?php
        /* Dynamic menu display begins */
        $sidebarMenu = '';

    if (isset($menuListDynamic) and (count($menuListDynamic)>0)) {
        foreach ($menuListDynamic as $mnRow) {
            if ($mnRow['parent_id'] == 0 && $mnRow['position'] == 'top') {

                //To set menu href link
                $mHref = '#';
                if ($mnRow['file_name'] != '#') {
                    $mHref = '../admin/index.php?p='.$mnRow['file_name'];
                }
                $sidebarMenu .= <<<SIDEBAR_MENU
                                <li>
                                    <a href="{$mHref}">
                                        <span data-hover="{$mnRow['name']}">{$mnRow['name']}</span>
                                    </a>
                                </li>
SIDEBAR_MENU;
            }
        }
    }
        echo $sidebarMenu;
        /* Dynamic menu display ends */
        global $LANG;
        ?>

        <li class="user-name">
          <h3><?php echo $LANG['welcome']; ?></h3>
          <h4><?php echo $first_name.' '.$last_name; ?></h4>
        </li>
        <li class="user-set-pointer">
          <div class="user-photo">
              <img src="<?php echo IMG_PATH.DS.$userImage; ?>" id="shortImg" width="43" height="35" alt="User Photo" />
          </div>
          <a href="#" class="user-lang">
    <?php 
    if (isset($_SESSION['language'])) {
        if ($_SESSION['language'] == 'EN') {
            echo 'EN';
        }
        if ($_SESSION['language'] == 'FR') {
            echo 'FR';
        }
        if ($_SESSION['language'] == 'NL') {
            echo 'NL';
        }
    } else {
        echo 'EN';
    }
            ?></a>
           <div class="user-set-holder">
               <div class="row-sec">
                   <div class="col6 left"><p><?php echo $LANG['selectLanguage']; ?></p></div>
                   <div class="col6 right">
                       <div class="lang-chose">
                           <a href="javascript:;" <?php
                                echo ($_SESSION['language'] == 'NL') ? "class='cur-lang'" : '';
                            ?>>NL</a>
                           <a href="javascript:;" <?php
                                echo ($_SESSION['language'] == 'FR') ? "class='cur-lang'" : '';
                           ?>>FR</a>
                           <a href="javascript:;" <?php
                                echo ($_SESSION['language'] == 'EN') ? "class='cur-lang'" : '';
                           ?>>EN</a>
                       </div>
                   </div>
               </div>
               <div class="row-sec">
                   <a href="javascript:;" onclick="showUserEditPage();" class="your-profile">
                       <?php echo $LANG['yourProfile']; ?><span class="fr icon icon-set-white"></span>
                   </a>
               </div>
               <?php
               if (
                   (isset($_SESSION['showChangeClub']))
                   && (isset($_SESSION['user']['usertype_id']))
                   && ($_SESSION['showChangeClub'] == 1)
                   && ($_SESSION['user']['usertype_id'] != 4)
               ) { ?>
                   <div class="row-sec">
                       <a href="javascript:;" class="your-profile" onclick="showChangeClubPop()">
                           <?php echo $LANG['changeClub'];?>
                           <span class="fr icon icon-set-white"></span>
                       </a>
                   </div>
                   <?php } ?>
               <div class="row-sec">
                   <a href="../ajax/index.php?p=login&method=logout" class="logout">
                       <?php echo $LANG['logout']; ?>
                       <span class="fr icon icon-logout"></span>
                   </a>
               </div>
           </div>
        </li>
    </ul>
    </div>