<?php
/*
 *Author        :   Jeyaram.A
 *Date          :   08-Aug-2014
 *Modified      :   08-Aug-2014
 *Modified By   :   Jeyaram.A
 *Description   :   Class to connect DB
*/

class db
{
    //Variables to store instances of this class
    public $dbcon;

    //The Data Access constructor.
    public function __construct()
    {
        $this->dbcon = &ADONewConnection(DB_TYPE);
        $this->dbcon->debug = false;

        @$this->dbcon->PConnect(DB_SERVER_NAME, DB_USER_NAME, DB_PASSWORD, DB_NAME);
        $errorno = $this->dbcon->ErrorNo();
        if (!empty($errorno)) {
            switch ($errorno) {
            case 1049 :
                echo $this->dbcon->ErrorMsg();
                    break;
            case 1045 :
                echo $this->dbcon->ErrorMsg();
                    break;
            }
        }
        $this->dbcon->SetFetchMode(ADODB_FETCH_ASSOC);
    }

    //returns the DB connection
    public function getConnection()
    {
        return $this->dbcon;
    }
}
