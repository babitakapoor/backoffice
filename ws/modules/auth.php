<?php
/**
 * PHP version 5.
 
 * @category Modules
 
 * @package Login
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description DB access functions to handle login related actions.
 */
require_once SQL_PATH.DS.'login.php';
 /**
 * Class for functions to handle login related actions.
 
 * @category Modules
 
 * @package Login
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/
 */
class authModel
{
    public $dbcon;
    public $status;

    /**
     * Class constructor.
     * @param ADOConnection|array $dbcon connection arguments
     */
    public function __construct(ADOConnection $dbcon)
    {
        $this->dbcon = $dbcon;

        $this->status = array(
            'status' => 'error',
            'status_code' => 0,
            'status_message' => 'No result found',
        );
    }

    /**
    * Authenticate & Returns the Login User Information(On Success)
    *
    * @param array $params service parameter, Keys:Name,Password & Role, LoginType
    *
    * @return array object
    */
    public function authenticate($params)
    {
        $username = $params['username'];
        $password = $params['password'];
        $company_id = $params['company_id'];
        $sql = "select * from t_users where (email = '{$username}'  AND password = '{$password}') and associated_company_id = {$company_id} and r_usertype_id = 3";
		$rsobj = $this->dbcon->Execute($sql);
        if ($rsobj->RecordCount()) 
        {
			$email = $rsobj->fields['email'];
			$userid = $rsobj->fields['user_id'];
			$company_id = $rsobj->fields['r_company_id'];
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'data' => array('user_id'=>$userid,'email'=>$email,'company_id'=>$company_id)
            );
        }
        else
        {
			$chksql = "select * from t_users where email = '{$username}' and associated_company_id = {$company_id} and r_usertype_id = 3";
			$rsobjChk = $this->dbcon->Execute($chksql);
			if ($rsobjChk->RecordCount()) 
			{
				$this->status = array(
					'status' => 'failure',
					'status_code' => 103,
				);
			}
			else
			{
				$this->status = array(
					'status' => 'failure',
					'status_code' => 101,
				);
			}
			
		}
		return $this->status;
    }
    
    public function checkUser($params)
    {
		$sql = "select * from t_users where user_id = {$params['user_id']}  and (email = '{$params['email']}' or username = '{$params['email']}')  and r_company_id = {$params['company_id']}";
		$rsobj = $this->dbcon->Execute($sql);
        if ($rsobj->RecordCount()) 
        {
			$this->status = array(
                'status' => 'success',
                'status_code' => 200,
            );
        }
        else
        {
			$this->status = array(
					'status' => 'failure',
					'status_code' => 101,
				);
			
		}
		return $this->status;
	}
	
	public function getUserData($params)
	{
		$this->status = array(
							'status_code' => 102,
						);
		if(!empty($params))
		{
			$query = "select company.company_id, company.company_name,company.company_name_slug, users.associated_company_id, users.user_id,users.first_name,users.last_name FROM t_users AS users inner join t_associated_companies AS company ON ( users.associated_company_id = company.company_id ) where  users.user_id = {$params['user_id']} limit 1";
			$companyObj = $this->dbcon->Execute($query);
			if ($companyObj->RecordCount()) 
			{
				$data['user_name'] = $companyObj->fields['first_name']." ".$companyObj->fields['last_name'];
				$data['associate_company_name_slug'] = $companyObj->fields['company_name'];
				$data['associate_company_id'] = $companyObj->fields['company_id'];
				$data['associate_company_name'] = $companyObj->fields['company_name'];
				$data['associate_company_name_slug'] = $companyObj->fields['company_name'];
				$this->status = array(
					'status_code' => 200,
					'data' => $data
				);
			}
			else
			{
				$this->status = array(
					'status_code' => 201,
					'data' => $data
				);
			}
		}
		return $this->status;
	}
	
    public function saveSiteCoreData($params)
    {
		$this->status = array(
						'status_code' => 101,
						'status_message' => 'Problem in adding data',
					);
		if(!empty($params))
		{
			$checkRecord = "select * from t_user_sitecore_data where user_id = {$params['user_id']}";
			$getData = $this->dbcon->Execute($checkRecord);
			
			if($getData->RecordCount())
			{
				$mode = 'update';
				$modified = date('Y-m-d H:i');
				$sql = "update t_user_sitecore_data set company_id = {$params['company_id']},modified = '{$modified}' where user_id = {$params['user_id']}";
			}
			else
			{
				$mode = 'save';
				$created = date('Y-m-d H:i');
				$modified = date('Y-m-d H:i');
				$sql = "insert t_user_sitecore_data (user_id,user_sitecore_id,company_id,created,modified)  values ({$params['user_id']},'{$params['user_sitecore_id']}',{$params['company_id']},'{$created}','{$modified}' )";
			}
			
			if($this->dbcon->Execute($sql))
			{
				$getLastRecord = "select sitecore_data.user_id as sitecore_user_id,sitecore_data.user_sitecore_id as sitecore_site_user_id,sitecore_data.company_id as sitecore_company_id,companies.company_id as companies_company_id,companies.company_name as companies_company_name,companies.company_name_slug as companies_company_slug,users.user_id as users_user_id,users.first_name as user_first_name,users.last_name as user_last_name from t_user_sitecore_data as sitecore_data inner join t_associated_companies as companies on (sitecore_data.company_id = companies.company_id) inner join t_users as users on (sitecore_data.user_id = users.user_id) where sitecore_data.user_id = {$params['user_id']} limit 1";
				
				
				$getData = $this->dbcon->Execute($getLastRecord);
				$result = $getData->fields;
				
				$this->status = array(
						'status_code' => 200,
						'mode' => $mode,
						'data' => $result
					);
			}
			else
			{
				$this->status = array(
						'status_code' => 101,
					);
			}
		}
		return $this->status;
		die;
	}
	
	 /**
    * Add/Register Members on Signup New Member From Web
    *
    */
    public function registerWebUser($params)
    {
		$this->status = array(
					'status' => 'error',
					'status_code' => 101,
					'status_message' => 'Please add values',
				);
		if(!empty($params))
		{
			
			foreach($params as $key => $prm)
			{
				if($prm == '')
				{
					unset($params[$key]);
				}
			}
			
			$paramsPersonalinfo['phone'] = $params['phone'];
			$paramsPersonalinfo['userimage'] = $params['userimage'];
			$paramsPersonalinfo['dob'] = date('Y-m-d',strtotime($params['dob']));
			
			unset($params['userimage']);
			unset($params['dob']);
			unset($params['mod']);
			unset($params['register_process']);
			unset($params['method']);
			unset($params['confirm_password']);
			unset($params['register_params']);
			
			$params['created_by'] =  1;
			$params['r_usertype_id'] =  3;
			$params['r_status_id'] =  13;
			$params['created_date'] =  date('Y-m-d H:m:s');
			$params['modified_date'] =  date('Y-m-d H:m:s');
			
			
			
			
			$keys =  array_keys($params);
			$values =  array_values($params);
			$implodeKeys = implode(",",$keys);
			$implodeValues = implode("','",$values);
			
			$sql = "insert into t_users (".$implodeKeys.") values ('".$implodeValues."')";
			//echo $sql;die;
			if($this->dbcon->Execute($sql))
			{
				$lastInsertId = $this->dbcon->Insert_ID();
				$paramsPersonalinfo['r_user_id'] = $lastInsertId;
				$pkeys =  array_keys($paramsPersonalinfo);
				$pvalues =  array_values($paramsPersonalinfo);
				$pimplodeKeys = implode(",",$pkeys);
				$pimplodeValues = implode("','",$pvalues);
				
				$psql = "insert into t_personalinfo (".$pimplodeKeys.") values ('".$pimplodeValues."')";
				
				$this->dbcon->Execute($psql);
				$this->status = array(
					'status' => 'success',
					'status_code' => 200,
					'status_message' => 'Registered Successfully',
				);
			}
			else
			{
				$this->status = array(
					'status' => 'error',
					'status_code' => 101,
					'status_message' => 'Probelm in registeration! please try again later',
				);
			}
			
		}	
        return $this->status;
    }
    
    public function forgotpassword($params)
    {
		$this->status = array(
					'status' => 'error',
					'status_code' => 101,
					'status_message' => 'Please add values',
				);
		if(!empty($params))
		{
			$sql = "update t_users set password ='{$params['password']}' where email ='{$params['email']}' and r_usertype_id = 3";
			
			if($this->dbcon->Execute($sql))
			{
				$this->status = array(
					'status' => 'success',
					'status_code' => 200,
					'status_message' => 'Password Chnaged Successfully',
				);
			}
			else
			{
				$this->status = array(
					'status' => 'error',
					'status_code' => 101,
					'status_message' => 'Probelm in changing password! please try again later',
				);
			}
			
		}	
        return $this->status;
    }
    
	public function checkValidEmail($params)
	{
		$this->status = array(
					'status' => 'error',
					'status_code' => 101,
					'status_message' => 'Please add values',
				);
		if(!empty($params))
		{
			$find = "select * from t_users where email ='{$params['email']}' and associated_company_id = {$params['company_id']} and r_usertype_id = 3";
			$data = $this->dbcon->Execute($find);
			if($data->RecordCount())
			{
				$this->status = array(
						'status' => 'success',
						'status_code' => 200,
					);
			}
			else
			{
				$this->status = array(
					'status' => 'error',
					'status_code' => 101,
					);
			}
			
		}	
        return $this->status;
	}
}
