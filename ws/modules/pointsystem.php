<?php
/**
 * PHP version 5.
 
 * @category Modules
 
 * @package PointSystem
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description DB access functions to handle point system related actions.
 */
require_once SQL_PATH.DS.'pointsystem.php'; 
 /**
 * Class to handle Point system related functions.
 
 * @category Modules
 
 * @package Admin
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/
 */ 
class pointsystemModel
{
    public $dbcon;
    public $status;

    /**
     * Class constructor.
     * @param array $dbcon connection arguments  
     */
    public function __construct($dbcon)
    {
        $this->dbcon = $dbcon;
        $this->status = array(
            'status' => 'error',
            'status_code' => '0',
            'status_message' => 'An error occurred',
        );
    }
    
    /**
    * Returns an array obj of insert Sport Specific for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array obj
    */
    public function insertSportSpecific($params)
    {
        $rsobj = $this->dbcon->Execute(GET_USER_TEST_EXISTS_FOR_SPORT_SPECIFIC, array($params['userId'], $params['userTestId']));

        $data = array(
                'sportspecific_data' => $params['sportSpecificData'],
        );

        if ($rsobj->RecordCount()) {
            $rsUpdates = $this->dbcon->GetUpdateSql($rsobj, $data);
            $this->dbcon->Execute($rsUpdates);

            //Set the status message
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Insert sport specific data updated success',
            );
        } else {

            //Set the status message
            $data['r_user_id'] = $params['userId'];
            $data['r_user_test_id'] = $params['userTestId'];
            $rsInserts = $this->dbcon->GetInsertSql($rsobj, $data);
            $this->dbcon->Execute($rsInserts);

            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Insert sport specific data added success',
            );
        }

        //Return the result array
        return $this->status;
    }
    
    /**
    * Returns an array obj of insert Heart rate Zone Activity for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array obj
    */
    public function insertHeartrateZoneActivity($params)
    {
        foreach ($params['heartrateData'] as $row) {
            $rsobj = $this->dbcon->Execute(GET_HEARTRATE_ZONE_ACTIVITY, array($params['userId'], $params['userTestId'], $row['activity_id']));

            $data = array(
                    'r_user_id' => $params['userId'],
                    'r_user_test_id' => $params['userTestId'],
                    'fitness_level' => $params['fitnessLevel'],
                    'activity_id' => $row['activity_id'],
                    'activity_name' => $row['activity_name'],
                    'hr_activity' => $row['hr_activity'],
                    'hr_zone_a' => $row['hr_zone_a'],
                    'hr_zone_b' => $row['hr_zone_b'],
                    'hr_zone_c' => $row['hr_zone_c'],
                    'hr_zone_d' => $row['hr_zone_d'],
                    'points_rate' => $row['pointsrate'],
                    'activity_used' => (isset($row['activity_used']) ? 1 : 0), //activity if checked values stored one
            );

            if ($rsobj->RecordCount()) {
                $rsUpdates = $this->dbcon->GetUpdateSql($rsobj, $data);
                $this->dbcon->Execute($rsUpdates);
                //Set the status message
                $this->status = array(
                    'status' => 'success',
                    'status_code' => 200,
                    'status_message' => 'Successfully Heartrate Zone activity Updated',
                );
            } else {

                /* Get training device id begins */
                $rsTriningDevice = $this->dbcon->Execute(GET_TRAINING_DEVICE_QUERY, array($row['activity_id']));
                if ($rsTriningDevice->RecordCount()) {
                    $res = $rsTriningDevice->GetRows();
                    if (isset($res['0']['r_training_device_id'])) {
                        $deviceId = $res['0']['r_training_device_id'];
                        $data['r_training_device_id'] = $deviceId;
                    }
                    $activityId = $row['activity_id'];
                } else {
                    //Insert new activity if not exist
                    $data1 = array('activity_id' => $row['activity_id'], 'activity_name' => $row['activity_name'], 'activity_used' => $row['activity_used'], 'created_by' => 1, 'created_date' => date('Y-m-d H:i:s'));
                    $rsInserts1 = $this->dbcon->GetInsertSql($rsTriningDevice, $data1);
                    $this->dbcon->Execute($rsInserts1);
                    $activityId = $this->dbcon->Insert_ID();
                }
                $data['activity_id'] = $activityId;
                /* Get training device id ends */
                $rsInserts = $this->dbcon->GetInsertSql($rsobj, $data);
                $this->dbcon->Execute($rsInserts);
                //Set the status message
                $this->status = array(
                    'status' => 'success',
                    'status_code' => 200,
                    'status_message' => 'Successfully Heartrate Zone activity Added',
                );
            }
        }

        //Return the result array
        return $this->status;
    }
    
    /**
    * Returns an array obj of insert Points To achieve for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array obj
    */
    public function insertPointsToachieve($params)
    {
        foreach ($params['weekPointsAchieve'] as $key => $row) {
            //$pointsAchieveId = isset($params['points_achieve_id']) ? $params['points_achieve_id'] : '';

            $rsobj = $this->dbcon->Execute(GET_POINTS_ACHIEVE_EXIST, array($params['userId'], $params['userTestId'], $key));

            $data = array(
                    'r_user_id' => $params['userId'],
                    'r_user_test_id' => $params['userTestId'],
                    'fitness_level' => $params['fitnessLevel'],
                    'age' => $params['age'],
                    'week' => $key,
                    'target_points' => $row['target_points'],
                    'points_a' => $row['points_a'],
                    'points_b' => $row['points_b'],
                    'points_c' => $row['points_c'],
                    'points_d' => $row['points_d'],
                    'week_start_date' => $params['weekDays'][$key]['start_date'],
                    'week_end_date' => $params['weekDays'][$key]['end_date'],
            );

            if ($rsobj->RecordCount()) {
                $rsUpdates = $this->dbcon->GetUpdateSql($rsobj, $data);
                $this->dbcon->Execute($rsUpdates);
                //Set the status message
                $this->status = array(
                    'status' => 'success',
                    'status_code' => 200,
                    'status_message' => 'Successfully Points Achieve Updated',
                );
            } else {
                $rsInserts = $this->dbcon->GetInsertSql($rsobj, $data);
                $this->dbcon->Execute($rsInserts);
                //Set the status message
                $this->status = array(
                    'status' => 'success',
                    'status_code' => 200,
                    'status_message' => 'Successfully Points Achieve Added',
                );
            }
        }

        //Return the result array
        return $this->status;
    }

    /**
    * Returns an array obj of get User Info For Point System for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array obj
    */
    public function getUserInfoForPointSystem($params)
    {
        $rsobj = $this->dbcon->Execute(GET_USER_INFO_POINT_SYSTEM, array($params['userId'], $params['userId']));

        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get User Basic Information For Point System',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'userDetails' => $userInfo,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'User Basic Information Not Found',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
            );
        }
        //Return the result array    
        return $this->status;
    }
    
    /**
    * Returns an array obj of get Points External Activity for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array obj
    */
    public function getPointsExternalActivity($params)
    {
        $rsobj = $this->dbcon->Execute(GET_USER_POINTS_ACTIVITY, array($params['userId'], $params['testId'], $params['deviceId']));
        if (isset($params['curDate'])) {
            $rsobj = $this->dbcon->Execute(GET_USER_POINTS_ACTIVITY_CUR, array($params['userId'], $params['testId'], $params['deviceId']));
        }
        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $activitesInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Activity Information For Point System',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'pointsActivity' => $activitesInfo,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Activity Information Not Found',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }
    
    /**
    * Returns an array obj of get Collected Points For Each Training Devices for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array obj
    */
    public function getCollectedPointsForEachTrainingDevices($params)
    {
        $queryString = GET_COLLECTEDPOINT_DEVICE;
        if ($params['action'] == 'activity') {
            $groupByDate = (isset($params['week']) && $params['week'] != '') ? " AND tp.week='".$params['week']."'" : '';
            $queryString = GET_COLLECTEDPOINT_ACTIVITY.$groupByDate;//." GROUP BY tp.activity_id,tp.points_collected_date";
        } else {
            $getWeek = (isset($params['week']) && $params['week'] != '') ? " AND tp.week='".$params['week']."'" : '';
            $groupByDate = ' GROUP BY td.training_device_id';
            $queryString = GET_COLLECTEDPOINT_DEVICE.$getWeek.$groupByDate;
        }
        $rsobj = $this->dbcon->Execute($queryString, array($params['userId'], $params['userTestId']));

        $collectedDevices = array();
        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $collectedDevices[] = $rsobj->fields;
                $rsobj->MoveNext();
            }

            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Get Activity For Each Training Devices retrieved',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'collectedDevices' => $collectedDevices,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => ' Activity For Each Training Devices retrieved Not Found',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'collectedDevices' => '',
            );
        }

        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }
} //Class End.
;
