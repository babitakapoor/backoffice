<?php
/**
 * PHP version 5.
 
 * @category Modules
 
 * @package Company
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description DB access functions to handle company related actions.
 */
require_once SQL_PATH.DS.'company.php';
/**
 * Class to handle Company Related Functions.
 
 * @category Modules
 
 * @package Admin
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/
 */
class companyModel
{
    public $dbcon;
    public $status;

    /**
     * Class constructor.
     * @param array $dbcon connection arguments  
     */
    public function __construct($dbcon)
    {
        $this->dbcon = $dbcon;

        $this->status = array(
            'status' => 'success',
            'status_code' => 200,
            'status_message' => 'Success',
        );
    }
    /**
    * Get & Returns the Records of Company List
    *
    * @param array $params service parameter, Keys:Status(optional)
    *
    * @return Array
    */
    public function getCompanyList($params)
    {
        //$categories = array();
        $category = array();
        $status = array();
        $status = isset($params['status']) ? $params['status'] : -1;

        $rsobj = $this->dbcon->Execute(GET_COMPANY_QUERY, array($status));
        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $category[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
        }
        //Set the status message
        $this->status = array(
            'status' => 'success',
            'status_code' => 200,
            'status_message' => 'Categories successfully retrieved',
        );
        //Return the result array
        return array(
            'clublist' => array(
                'club' => $category,
            ),
        );
    }

    /**
    * Add/Update Company 
    *
    * @param array $params service parameter, Keys:UserId(user_id)
    *
    * @return Array
    */
    public function getCompanyAddOrUpdate($params)
    {
        //$categories = array();
        //$category = array();
        $companyId = array();
        $companyId = isset($params['company_id']) ? $params['company_id'] : -1;
        $companyName = isset($params['company_name']) ? $params['company_name'] : -1;
        $rsobj = $this->dbcon->Execute(
            GET_COMPANY_ADDOREDITQUERY, array($companyId, $companyName)
        );
        $dateTime = date('Y-m-d H:i:s');
        if ($rsobj->RecordCount()) {
            $data = array(
                'company_id' => $companyId,
                'company_name' => $companyName,
                'status' => 1,
                'is_deleted' => 0,
                'updation_date' => $dateTime,
                'created_by' => 1,
                'created_date' => $dateTime,
                'modified_by' => 1,
                'modified_date' => $dateTime,
            );

            $rsUpdates = $this->dbcon->GetUpdateSql($rsobj, $data);
            $this->dbcon->Execute($rsUpdates);
        } else {
            $data = array(
                'company_id' => $companyId,
                'company_name' => $companyName,
                'status' => 1,
                'is_deleted' => 0,
                'updation_date' => $dateTime,
                'created_by' => 1,
                'created_date' => $dateTime,
                'modified_by' => 1,
                'modified_date' => $dateTime,
            );
            $rsInserts = $this->dbcon->GetInsertSql($rsobj, $data);
            $this->dbcon->Execute($rsInserts);
        }

        //Set the status message
        $status = array(
            'status' => 'success',
            'status_code' => 200,
            'status_message' => 'Categories successfully retrieved',
        );

        return $status;
    }
}
