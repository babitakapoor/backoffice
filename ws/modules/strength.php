<?php
/**
 * PHP version 5.
 
 * @category Modules
 
 * @package Strength
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description DB access functions to handle strength related actions.
 */
 require_once SQL_PATH.DS.'strength.php';
 /**
 * Class to handle strength related functions.
 
 * @category Modules
 
 * @package Admin
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/
 */
class strengthModel
{
    public $dbcon;
    public $status;
    public $error_general;

    /**
     * Class constructor.
     * @param ADOConnection|array $dbcon connection arguments
     */
    public function __construct(ADOConnection $dbcon)
    {
        $this->dbcon = $dbcon;
        $this->status = array(
            'status' => 'success',
            'status_code' => 200,
            'status_message' => 'Success',
        );
        $this->error_general = array(
            'response' => array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Some error occured. Please try later.',
            ),
        );
    }
     
    /**
    * Returns an json obj of insert Update User Test  for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function insertUpdateUserTest($params)
    {
        $strengthUserTestId = (isset($params['strength_user_test_id'])) ? $params['strength_user_test_id'] : -1;

        $params['allowToEdit'] = isset($params['allowToEdit']) ? $params['allowToEdit'] : 0;

        $rsobj = $this->dbcon->Execute(GET_STRENGTH_USER_TEST_EXIST, array($strengthUserTestId));
        $dateTime = date('Y-m-d H:i:s');
        $data = array(
                'r_user_id' => $params['hiddenUserId'],
                'coach_id' => $params['coach_id'],
                'user_test_status' => $params['test_status'],
                'r_strength_program_id' => $params['strengthProgramId'],
                'strength_auto_or_manual_calculation' => $params['testValuesCalculation'],
                'allow_to_edit' => $params['allowToEdit'],
                'height' => $params['height'],
                'weight' => $params['weight'],
                'test_option' => $params['testOption'],
                //"user_test_type" => $params['r_strength_user_test_type'],
                'r_strength_user_test_type' => $params['r_strength_user_test_type'],
        );
        if (isset($params['clubID'])) {
            $data['clubid'] = $params['clubID'];
        }
        if ($rsobj->RecordCount()) {
            $data['modified_by'] = $params['loggedUserId'];
            $data['modified_date'] = $dateTime;
            $rsUpdates = $this->dbcon->GetUpdateSQL($rsobj, $data);
            $this->dbcon->Execute($rsUpdates);
            //Set the status message
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Update strength user test success',
            );
        } else {
            $data['created_by'] = $params['coach_id'];
            $data['created_date'] = $dateTime;
            $rsInserts = $this->dbcon->GetInsertSQL($rsobj, $data);
            $this->dbcon->Execute($rsInserts);
            $insertId = $this->dbcon->Insert_ID();

            //Set the status message
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Strength test created successfully',
                'strengthUserTestId' => $params['strengthProgramId'],
                'insertid' => $insertId,
            );
        }

        //Return the result array
        return $this->status;
    }
     
    /**
    * Returns an json obj of insert User Test activity by addProgram  for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function insertUserTestactivitybyaddProgram($params)
    {
        $activities = $params['strength_machine_data'];

        foreach ($activities as $key => $value) {
            $rsobj = $this->dbcon->Execute(
                GET_STRENGTH_USER_TEST_ACTIVITY_EXIST,
                array($params['strength_user_test_id'], $key)
            );
            //$activity['strength_machine_id']));

            $data = array(
                    'r_strength_user_test_id' => $params['strength_user_test_id'],
                    //"r_strength_machine_id" => $activity['strength_machine_id'],
                    'r_strength_machine_id' => $key,
                    //"strength" => $activity['strength']
                    'strength' => $value,
            );

            //if($activity['strength_machine_id'] > 0) {
            if ($key > 0) {
                if ($rsobj->RecordCount()) {
                    $rsUpdates = $this->dbcon->GetUpdateSQL($rsobj, $data);
                    $this->dbcon->Execute($rsUpdates);
                } else {
                    $rsInserts = $this->dbcon->GetInsertSQL($rsobj, $data);
                    $this->dbcon->Execute($rsInserts);
                }
            }
        }

        $this->status = array(
            'status' => 'success',
            'status_code' => 200,
            'status_message' => 'Activities created successfully',
        );

        //Return the result array
        return $this->status;
    }
    /**
    * Returns an json obj of insert User Test activity  for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function insertUpdateUserTestActivity($params)
    {

        //$activities = $params['strength_machine_data'];
        $activities = json_decode($params['strength_machine_data']);
        foreach ($activities as $eact) {
            $item = (array) $eact;
            $rsobj = $this->dbcon->Execute(
                GET_STRENGTH_USER_TEST_ACTIVITY_EXIST,
                array($params['strength_user_test_id'], $item['checkedval'])
            );//$activity['strength_machine_id']));

            $data = array(
                    'r_strength_user_test_id' => $params['strength_user_test_id'],
                    //"r_strength_machine_id" => $activity['strength_machine_id'],
                    'r_strength_machine_id' => $item['checkedval'],
                    //"strength" => $activity['strength']
                    'strength' => $item['value'],
            );

            //if($activity['strength_machine_id'] > 0) {
            if ($item['checkedval'] > 0) {
                if ($rsobj->RecordCount()) {
                    $rsUpdates = $this->dbcon->GetUpdateSQL($rsobj, $data);
                    $this->dbcon->Execute($rsUpdates);
                } else {
                    $rsInserts = $this->dbcon->GetInsertSQL($rsobj, $data);
                    $this->dbcon->Execute($rsInserts);
                }
            }
        }

        $this->status = array(
            'status' => 'success',
            'status_code' => 200,
            'status_message' => 'Activities created successfully',
        );

        //Return the result array
        return $this->status;
    }

     
    /**
    * Returns an json obj of insert Update Strength Training Week  for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function insertUpdateStrengthTrainingWeek($params)
    {
        $weeks = $params['week_data'];

        foreach ($weeks as $key => $week) {
            $rsobj = $this->dbcon->Execute(
                GET_STRENGTH_USER_TEST_TRAINING_WEEK_EXIST,
                array($params['strength_user_test_id'], $params['userId'], $key)
            );

            $data = array(
                    'r_strength_user_test_id' => $params['strength_user_test_id'],
                    'r_user_id' => $params['userId'],
                    'week' => $key,
                    'week_start_date' => $week['start_date'],
                    'week_end_date' => $week['end_date'],
            );

            if ($rsobj->RecordCount()) {
                $rsUpdates = $this->dbcon->GetUpdateSQL($rsobj, $data);
                $this->dbcon->Execute($rsUpdates);
            } else {
                $rsInserts = $this->dbcon->GetInsertSQL($rsobj, $data);
                $this->dbcon->Execute($rsInserts);
            }
        }

        $this->status = array(
            'status' => 'success',
            'status_code' => 200,
            'status_message' => 'Training weeks created successfully',
        );

        //Return the result array
        return $this->status;
    }
     
    /**
    * Returns an json obj of remove Strength Test Activity for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function removeStrengthTestActivity($params)
    {
        $activities = json_decode($params['activities']);
        foreach ($activities as $activity) {
            $item = (array) $activity;
            $rsobj = $this->dbcon->Execute(
                    GET_STRENGTH_USER_TEST_ACTIVITY_EXIST,
                    array($params['strengthUserTestId'], $item['checkedval'])
            );
            if ($rsobj->RecordCount() > 0) {
                $data['is_deleted'] = 1;
                $rsUpdates = $this->dbcon->GetUpdateSQL($rsobj, $data);
                $this->dbcon->Execute($rsUpdates);
            }
        }

        $this->status = array(
            'status' => 'success',
            'status_code' => 200,
            'status_message' => 'Delete strength user test activities success',
        );

        //Return the result array
        return $this->status;
    }
    
    /**
    * Returns an json obj of get Strength User List a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function getStrengthUserList($params)
    {
        $rsobj = $this->dbcon->Execute(GET_STRENGTH_USER_LIST, array($params['usrid']));
        $strengthuserlist = array();
        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $strengthuserlist[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get User Basic Information For Point System',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'strengthuserlist' => $strengthuserlist,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'User Basic Information Not Found',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'strengthuserlist' => '',
            );
        }

        //Return the result array
        return $this->status;
    }
     
    /**
    * Returns an json obj of getMember Strength Test Id a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getMemberStrengthTestId($params)
    {
        $rsobj = $this->dbcon->Execute(GET_STRENGTH_USER_TEST_LATEST_ID, array($params['userId']));

        if ($rsobj->RecordCount()) {
            $testId = $rsobj->fields['strength_user_test_id'];

            //Set the status message
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Get member strength test id success',
                'strength_user_test_id' => $testId,
            );
        } else {

            //Set the status message
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get member strength test id failed',
                'strengthUserTestId' => 0,
            );
        }

        return $this->status;
    }
     
    /**
    * Returns an json obj of get Strength Test Detail a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function getStrengthTestDetail($params)
    {
        if (!isset($params['strength_user_test_id'])) {
            $strengthUserTest = $this->getMemberStrengthTestId($params);
            $params['strength_user_test_id'] = $strengthUserTest['strength_user_test_id'];
        }

        $rsobj = $this->dbcon->Execute(GET_STRENGTH_USER_TEST_EXIST, array($params['strength_user_test_id']));

        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Get member strength test detail success',
                'rows' => $rsobj->GetRows(),
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get member strength test detail error',
            );
        }

        //Return the result array
        return $this->status;
    }
     
    /**
    * Returns an json obj of strength test list a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function getStrengthTestList($params)
    {
        $query = '';

        if ($params['flag'] == 1) {
            $wre = 'ORDER BY SUT.strength_user_test_id DESC';
            $query .= GET_STRENGTH_TEST_LIST_BY_USER.''.$wre;
        } else {
            $query .= GET_STRENGTH_TEST_LIST_BY_USER;
        }

        $rsobj = $this->dbcon->Execute($query, array($params['userId']));
        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Get member strength test list success',
                'rows' => $rsobj->GetRows(),
            //    "totalcount"        => $this->getLastQueryTotalCount()        
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get member strength test list error',
            );
        }

        //Return the result array
        return $this->status;
    }
     
    /**
    * Returns an json obj of get strength test activities  a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function getStrengthTestActivities($params)
    {
        $rsobj = $this->dbcon->Execute(GET_STRENGTH_USER_TEST_ACTIVITIES, array($params['strength_user_test_id']));

        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Get member strength test activities success',
                'rows' => $rsobj->GetRows(),
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get member strength test activities error',
            );
        }

        //Return the result array
        return $this->status;
    }

    /**
    * Returns an json obj of update strength activity value  a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function updateActivityStrengthValue($params)
    {
        $rsobj = $this->dbcon->Execute(GET_STRENGTH_USER_TEST_ACTIVITY_BY_ID, array($params['activityId']));

        if ($rsobj->RecordCount()) {
            $data['strength'] = $params['strength'];
            $rsUpdates = $this->dbcon->GetUpdateSQL($rsobj, $data);
            $this->dbcon->Execute($rsUpdates);
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Activity strength value update success',
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Activity strength value update error',
            );
        }

        //Return the result array
        return $this->status;
    }
} //Class End.
;
