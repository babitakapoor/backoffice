<?php
/**
 * PHP version 5.
 
 * @category Modules
 
 * @package Message
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description DB access functions to handle message related actions.
 */

 /**
 * Class to handle Message related functions.
 
 * @category Modules
 
 * @package Admin
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/
 */
class app_version_listingModel
{
    public $dbcon;
    public $status;

    /**
     * Class constructor.
     * @param ADOConnection|array $dbcon connection arguments
     */
    public function __construct(ADOConnection $dbcon)
    {
        $this->dbcon = $dbcon;

        $this->status = array(
            'status' => 'error',
            'status_code' => 1,
            'status_message' => 'Opps an error as occurred',
        );
    }
	
	public function addAppVersion($param = array())
	{
		$this->status = array(
					'status' => 'error',
					'status_code' => 0,
					'status_message' => 'Problem with adding app version',
				);
		if(!empty($param))
		{
			$client_app_version =  trim($param['client_app_version']);
			$coach_app_version =  trim($param['coach_app_version']);
			$checkQuery = "select id from t_app_versions";
			$getData = $this->dbcon->Execute($checkQuery);
			if($getData->RecordCount())
			{
				$id = $getData->fields['id'];
				$sql = "update t_app_versions set client_app_version ='{$client_app_version}',coach_app_version='{$coach_app_version}'  where id = {$id}";
			}
			else
			{
				$sql = "insert into t_app_versions (client_app_version,coach_app_version) values ('{$client_app_version}','{$coach_app_version}')";
			}
			
			if($this->dbcon->Execute($sql))
			{
				$this->status = array(
						'status_code' => 200,
						'status_message' => 'App version added successfully',
					);
			}
		}
		return $this->status;
	}
	
	public function getAppEditVersion($param = array())
	{
		$this->status = array(
					'status' => 'error',
					'status_code' => 0,
					'status_message' => 'Problem with adding app version',
				);
		if(!empty($param))
		{
			$checkQuery = "select * from t_app_versions";
			$getData = $this->dbcon->Execute($checkQuery);
			if($getData->RecordCount())
			{
				$result = $getData->fields;
				$this->status = array(
						'status_code' => 200,
						'status_message' => 'App version',
						'data' => $result
					);
			}
			else
			{
				$this->status = array(
						'status_code' => 101,
						'status_message' => 'Not Present',
					);
			}
		}
		
		return $this->status;
	}
	
	public function getAppVersion($param = array())
	{
		$this->status = array(
					'status' => 'error',
					'status_code' => 0,
					'status_message' => 'Problem with adding app version',
				);
		if(!empty($param))
		{
			if($param['userType'] == 1)
			{
				$field = 'coach_app_version';
			}
			else
			{
				$field = 'client_app_version';
			}
			$checkQuery = "select {$field} as app_version from t_app_versions";
			$getData = $this->dbcon->Execute($checkQuery);
			if($getData->RecordCount())
			{
				$result = $getData->fields;
				$this->status = array(
						'status_code' => 200,
						'status_message' => 'App version',
						'data' => $result
					);
			}
			else
			{
				$this->status = array(
						'status_code' => 101,
						'status_message' => 'Not Present',
					);
			}
		}
		
		return $this->status;
	}
	
	
    public function getAppVersions($param = array())
    {
		//echo "<pre>";print_r($param);
		$result = array();
		$where = '';
        $leftJoin = '';
        $andClubCondtion = '';
        
        $type = $param['userType'];
        $limitStart = $param['limitStart'];
        $limitEnd = $param['limitEnd'];
        $limit = ' limit '.$limitStart.','.$limitEnd;
        
        $companyId = isset($param['companyId']) ? $param['companyId'] : -1;

        $where .= ' where users.r_company_id='.$companyId.' and users.is_deleted<>1 ';
		$where .= ' and users.r_usertype_id = '.$type;
		
		if($type == 1)
		{
			$leftJoin .= ' left join t_employee_centre employee on (employee.r_user_id = users.user_id)left join t_clubs as clubs on (employee.r_club_id = clubs.club_id)';
			$andClubCondtion .= ' and employee.r_club_id = '.$param['clubId'];
			if (isset($param['clubId']) && $param['clubId'] != '') 
			{
				
				$where .= (isset($param['clubId']) && $param['clubId'] != '') ? $andClubCondtion : '';
			}
			else
			{
				$where .= (isset($param['authorizedClubId'])&& $param['authorizedClubId'] != '') ? ' and users.r_club_id IN ('.$param['authorizedClubId'].')' : '';
			}
		}
		else
		{
				$leftJoin .= ' left join t_clubs as clubs on (users.r_club_id = clubs.club_id)';
				$andClubCondtion .= ' and users.r_club_id = '.$param['clubId'];
				$where .= (isset($param['clubId']) && $param['clubId'] != '') ? $andClubCondtion : '';
		}
		
		if(isset($param['searchValue']) && $param['searchValue'] != '')
		{
			$where .=  " and (first_name like '".$param['searchValue']."%'  or username like '".$param['searchValue']."%'  or last_name like '".$param['searchValue']."%' or clubs.club_name like '".$param['searchValue']."%')";
		}
		

		if(isset($param['searchType']) && $param['searchType'] != '' && $param['searchValue'] != '' && $param['searchType'] == 'status')
		{
			$where .= ' HAVING '.$param['searchType']." like '".$param['searchValue']."%' ";
		}
		
		if($param['labelField'] == 'first_name' || $param['labelField'] == 'user_id')
		{
			$table = 'users';
		}
		elseif($param['labelField'] == 'club_name')
		{	
			$table = 'clubs';
		}
		else
		{
			$table = 'app_version';
		}
		
		$intialQuery = "select CONCAT(users.first_name,' ',users.last_name) as user_name,users.user_id as u_id,users.r_club_id,app_version.*,clubs.club_id,clubs.club_name  from  t_users as users left join t_user_app_versions as app_version on (users.user_id = app_version.user_id and app_version.status =1) ";
		$sql = $intialQuery.$leftJoin. $where.' group by users.user_id ORDER BY '.$table.'.'.$param['labelField'].' '.$param['sortType'].$limit;
		//echo $sql;die;
		$obj = $this->dbcon->Execute($sql);
		
		if ($obj->RecordCount())
		{
			$total_records = 0;
			$countintialQuery = "select count(distinct users.user_id) as u_id from  t_users as users left join t_user_app_versions as app_version on (users.user_id = app_version.user_id and app_version.status =1) ";
			$countsql  = $countintialQuery.$leftJoin. $where;
			$countobj = $this->dbcon->Execute($countsql);
			
			if ($countobj->RecordCount())
			{
				$total_records = $countobj->fields['u_id'];
			}
			
			while (!$obj->EOF) 
			{
				$result[] = $obj->fields;
				$obj->MoveNext();
			}
			if($total_records == 1)
			{
				$result = $result[0];
			}
			
			
			$this->status = array(
								'status' => 'success',
								'status_code' => 1,
								'status_message' => 'App version Listing',
								'total_records' => $total_records,
								'total_count' => $this->getLastQueryTotalCount(),
								'data' => $result
							);
			
		}
		else
		{
			 $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'No record found.',
                'total_records' => $obj->RecordCount(),
				'total_count' => $this->getLastQueryTotalCount(),
				'data' => $result
            );
		}
		
		return $this->status;
	}
	
	public function getDetailAppVersions($params = array())
    {
		$result = array();
		$userid  = $params['id'];
		$sql = "select * from t_user_app_versions where user_id = {$userid} order by id desc";
		$obj = $this->dbcon->Execute($sql);
		if ($obj->RecordCount())
		{
			while (!$obj->EOF) 
			{
				$result[] = $obj->fields;
				$obj->MoveNext();
			}
			
			$this->status = array(
								'status' => 'success',
								'status_code' => 1,
								'status_message' => 'App version Listing',
								'total_records' => $obj->RecordCount(),
								'total_count' => $this->getLastQueryTotalCount(),
								'data' => $result
							);
		}
		else
		{
			 $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'No record found.',
                'total_records' => $obj->RecordCount(),
				'total_count' => $this->getLastQueryTotalCount(),
				'data' => $result
            );
		}
		return $this->status;
	}
	
	
	
	public function getLastQueryTotalCount()
    {
		$rsobj = $this->dbcon->Execute(GET_QUERY_LAST_TOTAL_COUNT);
		$toatlCount = 0;

        if ($rsobj->RecordCount()) {
            $row = $rsobj->GetRows();
            $toatlCount = $row[0]['total_count'];
        }

        return $toatlCount;
    }
    
} // End Class.
;
