<?php
/**
 * PHP version 5.
 
 * @category Modules
 
 * @package Log
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description DB access functions to handle log related actions.
 */
require_once SQL_PATH.DS.'log.php';
 /**
 * Class for functions to handle log related actions.
 
 * @category Modules
 
 * @package Log
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/
 */
class logModel
{
    public $dbcon;
    public $status;

    /**
     * Class constructor.
     * @param ADOConnection|array $dbcon connection arguments
     */
    public function __construct(ADOConnection $dbcon)
    {
        $this->dbcon = $dbcon;

        $this->status = array(
            'status' => 'success',
            'status_code' => 200,
            'status_message' => 'Success',
        );
    }

    
    /**
    * log Services.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    */
    public function logServices($params)
    {
        if ($params) {
        };
        /*
        if (WEBSERVICE_LOG_TO_DB != 1) {
            $status = array(
                        'status' => 'error',
                        'error_code' => '0',
                        'status_message' => 'Log functionality disabled, to enable user change settings in config file',
            );

            return $status;
        }

        $webServiceUrl = isset($params['webServiceUrl']) ? $params['webServiceUrl'] : '';
        $response = isset($params['response']) ? $params['response'] : '';
        $userId = isset($params['userId']) ? $params['userId'] : '';
        $clubId = isset($params['r_club_id']) ? $params['r_club_id'] : '';
        $companyId = isset($params['r_company_id']) ? $params['r_company_id'] : '';
        $userTestId = isset($params['userTestId']) ? $params['userTestId'] : '';
        $reference = isset($params['reference']) ? $params['reference'] : '';
        $type = isset($params['type']) ? $params['type'] : '';// PS/IOS/CCA
        $dateTime = date('Y-m-d H:i:s');

        //Construct the data to update or insert
        $data = array(
            'webservice_url' => $webServiceUrl,
            'response' => $response,
            'reference' => $reference,
            'r_user_id' => $userId,
            'r_user_test_id' => $userTestId,
            'type' => $type,
             'r_company_id' => $companyId,
             'r_club_id' => $clubId,
            'added_date' => $dateTime,
        );
        $rsobj = $this->dbcon->Execute(GET_LOG);
        $insertSql = $this->dbcon->GetInsertSql($rsobj, $data);
        $this->dbcon->Execute($insertSql);
        */
        $status = array(
                'response' => array(
                    'status' => 'success',
                    'error_code' => 0,
                    'status_message' => 'Log Successfully Added',
                ),
        );

        return $status;
    }

    /**
    * To get log report.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    */
    public function getLogReport($params)
    {
        $logReport = array();
        $errCode = 1;
        $errMsg = 'List not retrieved';

        //$searchValue = isset($params['searchValue']) ? $params['searchValue'] : -1;
        //$searchType = isset($params['searchType']) ? $params['searchType'] : -1;
        $filterQry  =   '';
        if (isset($params['searchType'])) {
            if (trim($params['searchValue']) != '') {
                $filterQry = ' AND '.$params['searchType']." LIKE '".trim($params['searchValue'])."%'";
            }
        }
        $getReports = 'SELECT 
                        SQL_CALC_FOUND_ROWS l.*,u.user_id,l.r_user_test_id,date(l.added_date) AS addedDate 
                      FROM t_log l 
                        LEFT JOIN t_users u ON u.user_id=l.r_user_id 
                        LEFT JOIN t_user_test ut ON ( ut.r_user_id=u.user_id AND ut.user_test_id=l.r_user_test_id ) 
                      WHERE l.log_id IS NOT NULL'.$filterQry;

        $filterQry = $getReports.' ORDER BY '.$params['labelField'].' '.$params['sortType'];

        $rsobj = $this->dbcon->Execute($filterQry);
        if ($rsobj->RecordCount()) {
            $errCode = 0;
            $errMsg = 'List retrieved successfully';
            while (!$rsobj->EOF) {
                $logReport[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
        }

        //return result
        $status = array(
            'status' => 'error',
            'status_code' => $errCode,
            'status_message' => $errMsg,
            'total_records' => $rsobj->RecordCount(),
            // 'sql' => $rsobj->sql,
            'logReport' => $logReport,
            'totalCount' => $this->getLastQueryTotalCount(),
        );

        return $status;
    }

    /**
    * This function used to get total count when using limit.
    *
    * @return integer obj
    */
    public function getLastQueryTotalCount()
    {
        $rsobj = $this->dbcon->Execute(GET_QUERY_LAST_TOTAL_COUNT);
        $toatlCount = 0;
        if ($rsobj->RecordCount()) {
            $row = $rsobj->GetRows();
            $toatlCount = $row[0]['total_count'];
        }

        return $toatlCount;
    }
}
