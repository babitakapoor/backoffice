<?php
/**
 * PHP version 5.
 
 * @category Modules
 
 * @package Login
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description DB access functions to handle login related actions.
 */
require_once SQL_PATH.DS.'login.php';
 /**
 * Class for functions to handle login related actions.
 
 * @category Modules
 
 * @package Login
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/
 */
class loginModel
{
    public $dbcon;
    public $status;

    /**
     * Class constructor.
     * @param ADOConnection|array $dbcon connection arguments
     */
    public function __construct(ADOConnection $dbcon)
    {
        $this->dbcon = $dbcon;

        $this->status = array(
            'status' => 'error',
            'status_code' => 0,
            'status_message' => 'No result found',
        );
    }

    /**
    * Authenticate & Returns the Login User Information(On Success)
    *
    * @param array $params service parameter, Keys:Name,Password & Role, LoginType
    *
    * @return array object
    */
    public function authenticate($params)
    {
        //echo "5";
        $username = isset($params['username']) ? $params['username'] : '';
        $password = isset($params['password']) ? $params['password'] : '';
        /*MK Added 04 Jan 2015 Task No.3946 */
        $usertype_id = isset($params['usertype_id']) ? $params['usertype_id'] : -1;
        $user = null;
        //echo "4";
        if ($usertype_id != -1 && $usertype_id != 3) {
			
		
            $rsobj = $this->dbcon->Execute(GET_USER_LOGIN_BYTYPE, array($username, $username, $password, $usertype_id));
        } 
		 elseif($usertype_id == 3){
		//$password = md5($password);
		/* $rsobjs = $this->dbcon->Execute(GET_USER_LOGIN_MEMBER_PASSWORD, array($username,$usertype_id));
		$password_hash = $rsobjs->fields['password'];
			if (password_verify($password, $password_hash)) {
				
			  $rsobj = $this->dbcon->Execute(GET_USER_LOGIN_MEMBER, array($username, $username, $usertype_id));
			 
			} */
			
				 $rsobj = $this->dbcon->Execute(GET_USER_LOGIN_MEMBER, array($username, $username, $password, $usertype_id));
				 
			} 
		else {
			  /* $rsobjc = $this->dbcon->Execute(GET_USER_LOGIN_COACH_PASSWORD, array($username));
			$password_hash = $rsobjc->fields['password'];
		
			if (password_verify($password, $password_hash)) {
				
            $rsobj = $this->dbcon->Execute(GET_USER_LOGIN, array($username, $username));
			} */
		
			$rsobj = $this->dbcon->Execute(GET_USER_LOGIN, array($username, $username, $password));
        }
        //echo GET_USER_LOGIN_BYTYPE;
        /*MK Edited 04 Jan 2015 Task No.3946 - Ends Login with User Type to Limit the coach / client login*/
        if ($rsobj->RecordCount()) {
			
            while (!$rsobj->EOF) {
                $user = $rsobj->fields;
                $rsobj->MoveNext();
				
            }
        }
		
        //echo "5";
		
        if (!empty($user)) {
		
            //status_code 1 - authenticated user, 2 - invalid user name and pass, 3 - Not authorized
            $userTypeAccept = array(1, 4, 2, 3, 9);
            if ($user['is_deleted'] == 1) {
                $this->status['data'] = $user;
                $this->status['data']['status_code'] = 4;
            } elseif (!empty($user['usertype_id']) &&  !in_array($user['usertype_id'], $userTypeAccept)) {
                $this->status['data']['status_code'] = 3;
                $this->status['data']['usertype_id'] = -1;
            } elseif (in_array($user['usertype_id'], $userTypeAccept) && $user['r_status_id']!=2) {
				$query = "select company.company_id, company.company_name,company.company_name_slug, users.associated_company_id, users.user_id FROM t_users AS users inner join t_associated_companies AS company ON ( users.associated_company_id = company.company_id ) where  users.user_id = {$user['user_id']} limit 1";
                $companyObj = $this->dbcon->Execute($query);
                if ($companyObj->RecordCount()) 
                {
					$user['associate_company_id'] = $companyObj->fields['company_id'];
					$user['associate_company_name'] = $companyObj->fields['company_name'];
					$user['associate_company_name_slug'] = $companyObj->fields['company_name'];
				}
                $this->status['data'] = $user;
                $this->status['data']['status_code'] = 1;
            }
			  elseif ($user['usertype_id']==3 && $user['r_status_id']==2) {
				 
                //$this->status['data'] = $user;
                $this->status['data']['status_code'] = 5;
            } 
			
        } else {
            $this->status['data']['status_code'] = 2;
            $this->status['data']['usertype_id'] = -1;
        }

        return array(
            'movesmart' => $this->status,
        );
    }

    /**
    * Get & Returns permitted pages Based on the User Role
    *
    * @param array $params service parameter, Keys:FileName,UserTypeId(Role)
    *
    * @return array object
    */
    public function getUserPagePermissions($params)
    {
      
        $fileName = isset($params['fileName']) ? $params['fileName'] : '';
        $userTypeId = isset($params['userTypeId']) ? $params['userTypeId'] : '';

        $rsobj = $this->dbcon->Execute(GET_USER_PAGE_PERMISSION, array($userTypeId, $fileName));
        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'User page permissions success',
                'userPermission' => $rsobj->getRows(),
            );
        }

        return $this->status;
    }
    
     /**
    * Direct Login
    *
    * @param array $params service parameter, Keys:FileName,UserTypeId(Role)
    *
    * @return array object
    */
    /*
    public function directLogin($params)
    {
        //$userName = isset($params['userName']) ? $params['userName'] : '';
        $password = isset($params['password']) ? $params['password'] : '';

        $rsobj = $this->dbcon->Execute(GET_USER_LOGIN, array($username, $username, $password));
        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'User page permissions success',
                'userPermission' => $rsobj->getRows(),
            );
        }

        return $this->status;
    }
    */
    /////////////////////////Site core Function ///////////////////////////////////////////
    public function saveSiteCoreData($params)
    {
		$this->status = array(
						'status_code' => 101,
						'status_message' => 'Problem in adding data',
					);
		if(!empty($params))
		{
			$checkRecord = "select * from t_user_sitecore_data where user_id = {$params['user_id']}";
			$getData = $this->dbcon->Execute($checkRecord);
			
			if($getData->RecordCount())
			{
				$mode = 'update';
				$modified = date('Y-m-d H:i');
				$sql = "update t_user_sitecore_data set company_id = {$params['company_id']},modified = '{$modified}' where user_id = {$params['user_id']}";
			}
			else
			{
				$mode = 'save';
				$created = date('Y-m-d H:i');
				$modified = date('Y-m-d H:i');
				$sql = "insert t_user_sitecore_data (user_id,user_sitecore_id,company_id,created,modified)  values ({$params['user_id']},'{$params['user_sitecore_id']}',{$params['company_id']},'{$created}','{$modified}' )";
			}
			
			if($this->dbcon->Execute($sql))
			{
				$getLastRecord = "select sitecore_data.user_id as sitecore_user_id,sitecore_data.user_sitecore_id as sitecore_site_user_id,sitecore_data.company_id as sitecore_company_id,companies.company_id as companies_company_id,companies.company_name as companies_company_name,companies.company_name_slug as companies_company_slug,users.user_id as users_user_id,users.first_name as user_first_name,users.last_name as user_last_name from t_user_sitecore_data as sitecore_data inner join t_associated_companies as companies on (sitecore_data.company_id = companies.company_id) inner join t_users as users on (sitecore_data.user_id = users.user_id) where sitecore_data.user_id = {$params['user_id']} limit 1";
				
				
				$getData = $this->dbcon->Execute($getLastRecord);
				$result = $getData->fields;
				
				$this->status = array(
						'status_code' => 200,
						'mode' => $mode,
						'data' => $result
					);
			}
			else
			{
				$this->status = array(
						'status_code' => 101,
					);
			}
		}
		return $this->status;
		die;
	}
	
	public function updateSiteCoreData($params)
	{
		$this->status = array(
						'status_code' => 101,
						'status_message' => 'Problem in updating data',
					);
		if(!empty($params))
		{
			$getusers  = "select users.user_id,users.associated_company_id,users.first_name,users.last_name,companies.company_id as companies_company_id,companies.company_name as companies_company_name,companies.company_name_slug as companies_company_slug from t_users as users inner join t_associated_companies as companies  where users.associated_company_id != 0";
			$getData = $this->dbcon->Execute($getusers);
			if($getData->RecordCount())
			{
				$i =0;
				 while (!$getData->EOF) 
				 {
					$companyid = $getData->fields['companies_company_id'];
					$userid = $getData->fields['user_id'];
					
					$modified = date('Y-m-d H:i');
					$sql = "update t_user_sitecore_data set company_id = {$companyid},modified = '{$modified}' where user_id = {$userid}";
					$this->dbcon->Execute($sql);
					
					$getLastRecord = "select sitecore_data.user_id as sitecore_user_id,sitecore_data.user_sitecore_id as sitecore_site_user_id,sitecore_data.company_id as sitecore_company_id,companies.company_id as companies_company_id,companies.company_name as companies_company_name,companies.company_name_slug as companies_company_slug,users.user_id as users_user_id,users.first_name as user_first_name,users.last_name as user_last_name from t_user_sitecore_data as sitecore_data inner join t_associated_companies as companies on (sitecore_data.company_id = companies.company_id) inner join t_users as users on (sitecore_data.user_id = users.user_id) where sitecore_data.user_id = {$userid} limit 1";
					
					$getLastData = $this->dbcon->Execute($getLastRecord);
					
					$data[$i]['sitecore_site_user_id'] = $getLastData->fields['sitecore_site_user_id'];
					$data[$i]['users_user_id'] =  $getLastData->fields['users_user_id'];
					$data[$i]['companies_company_id'] =  $getLastData->fields['companies_company_id'];
					$data[$i]['companies_company_name'] =  $getLastData->fields['companies_company_name'];
					$data[$i]['companies_company_slug'] =  $getLastData->fields['companies_company_slug'];
					$data[$i]['user_first_name']  =  $getLastData->fields['user_first_name'];
					$data[$i]['user_last_name'] =  $getLastData->fields['user_last_name'];
					$i++;
					$getData->MoveNext();
				}
				$this->status = array(
						'status_code' => 200,
						'mode' => 'update',
						'data' => $data
					);
			}
			else
			{
				$this->status = array(
						'status_code' => 101,
					);
			}
		}
		return $this->status;
		die;
	}
	
	/////////////////////////Site core Function ends///////////////////////////////////////////
}
