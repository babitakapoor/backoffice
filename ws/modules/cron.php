<?php
/**
 * PHP version 5.
 
 * @category Modules
 
 * @package Cron
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description DB access functions to handle cron related actions.
 */
require_once SQL_PATH.DS.'cron.php';
 /**
 * Class for DB access functions to handle cron related actions.
 
 * @category Modules
 
 * @package Cron
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/
 */
class CronModel
{
    public $dbcon;
    public $status;

    /*
     * Class constructor.
     * @param array $dbcon connection arguments  
     * /
    public function __construct($dbcon)
    {
        $this->dbcon = $dbcon;

        $this->status = array(
            'status' => 'success',
            'status_code' => 200,
            'status_message' => 'Success',
        );
    }

    
    /**
    * get Center List Details.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array obj
    */
    public function getCenterList($params)
    {
        //$categories = array();
        $category = array();
        //$status = array();
        $clubid = isset($params['clubid']);

        $rsobj = $this->dbcon->Execute(GET_CENTER_LIST, $clubid);
        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $category[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
        }
        //Set the status message
        $this->status = array(
            'status' => 'success',
            'status_code' => 200,
            'status_message' => 'Categories successfully retrieved',
        );
        //Return the result array
        return array(
            'memberdetails' => array(
                'member' => $category,
            ),
        );
    }

    
    /**
    * Get Club Details from External application..
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array obj
    */
    public function addClubDetails($params)
    {
        $company_club_id = isset($params['company_club_id']) ? 
            $params['company_club_id'] : -1;
        $company_id = isset($params['company_id']) ? $params['company_id'] : -1;
        $club_name = isset($params['club_name']) ? $params['club_name'] : -1;
        $street = isset($params['street']) ? $params['street'] : -1;
        $number = isset($params['number']) ? $params['number'] : -1;
        $bus = isset($params['bus']) ? $params['bus'] : -1;
        $post_code = isset($params['post_code']) ? $params['post_code'] : -1;
        $location_id = isset($params['location_id']) ? $params['location_id'] : -1;
        $email_id = isset($params['email_id']) ? $params['email_id'] : -1;

        //echo GET_CLUB_DETAILS;
        $rsobj = $this->dbcon->Execute(GET_CLUB_DETAILS, $company_club_id);

        $datetime = date('Y-m-d H:i:s');
        //Construct the data to update or insert
        $data = array(
            'r_company_id' => $company_id,
            'company_club_id' => $company_club_id,
            'club_name' => $club_name,
            'street' => $street,
            'number' => $number,
            'bus' => $bus,
            'post_code' => $post_code,
            'location' => $location_id,
            'email_id' => $email_id,
            'is_deleted' => 0,
            'updation_date' => $datetime,
            'created_date' => $datetime,
            'modified_date' => $datetime,
            'r_companyid' => 1,
            'created_by' => 1,
            'modified_by' => 1,
        );

        if ($rsobj->RecordCount()) {
            $update_sql = $this->dbcon->GetUpdateSql($rsobj, $data);
            $rsobj = $this->dbcon->Execute($update_sql);
            $status = array(
                'response' => array(
                    'status' => 'success',
                    'status_code' => 200,
                    'status_message' => 'Clubs Successfully Updated',
                ),
            );
        } else {
            $insert_sql = $this->dbcon->GetInsertSql($rsobj, $data);
            $rsobj = $this->dbcon->Execute($insert_sql);

            $status = array(
                'response' => array(
                    'status' => 'success',
                    'status_code' => 200,
                    'status_message' => 'Clubs Successfully Added',
                ),
            );
        }

        return $status;
    }

    
    /**
    * Get Club Details from External application..
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array obj
    */
    public function getClubDetails($params)
    {
        $is_deleted = isset($params['is_deleted']) ? $params['is_deleted'] : -1;
        $company_id = isset($params['company_id']) ? $params['company_id'] : -1;

        $rsobj = $this->dbcon->Execute(
            GET_CLUB_DETAIL, array($company_id, $is_deleted)
        );
        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $category[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
        }
        $this->status = array(
            'status' => 'success',
            'status_code' => 200,
            'status_message' => 'Categories successfully retrieved',
        );

        return array(
            'clublist' => array(
                'club' => $category,
            ),
        );
    }

    
    /**
    * Get Coach  from External application.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array obj
    */
    
    public function getCoachAddOrUpdate($params)
    {
        $company_id = isset($params['company_id']) ? $params['company_id'] : '';
        $club_id = isset($params['club_id']) ? $params['club_id'] : '';
        $status_id = isset($params['status_id']) ? $params['status_id'] : '';
        $first_name = isset($params['first_name']) ? $params['first_name'] : '';
        $last_name = isset($params['last_name']) ? $params['last_name'] : '';
        $gender = isset($params['gender']) ? $params['gender'] : '';
        $email = isset($params['email']) ? $params['email'] : '';
        $username = isset($params['username']) ? $params['username'] : '';
        $password = isset($params['password']) ? $params['password'] : '';
        $person_id = isset($params['person_id']) ? $params['person_id'] : '';

        $rsobj = $this->dbcon->Execute(
            GET_COACH_ADDOREDITQUERY, array($email, $company_id)
        );
        $dateTime = date('Y-m-d H:i:s');
        if ($rsobj->RecordCount()) {
            $data = array(
                'r_usertype_id' => '3',
                'r_company_id' => $company_id,
                'r_club_id' => $club_id,
                'r_status_id' => $status_id,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'gender' => $gender,
                'email' => $email,
                'username' => $username,
                'password' => $password,
                'person_id' => $person_id,
                'created_by' => '1',
                'created_date' => $dateTime,
                'modified_by' => '1',
                'modified_date' => $dateTime,
            );
            $rsUpdates = $this->dbcon->GetUpdateSql($rsobj, $data);
            $this->dbcon->Execute($rsUpdates);
        } else {
            $data = array(
                'r_company_id' => $company_id,
                'r_club_id' => $club_id,
                'r_status_id' => $status_id,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'gender' => $gender,
                'email' => $email,
                'username' => $username,
                'password' => $password,
                'person_id' => $person_id,
                'created_by' => '1',
                'created_date' => $dateTime,
                'modified_by' => '1',
                'modified_date' => $dateTime,
            );
            $rsInserts = $this->dbcon->GetInsertSql($rsobj, $data);
            $this->dbcon->Execute($rsInserts);
        }
        //Set the status message
        $status = array(
            'status' => 'success',
            'status_code' => 200,
            'status_message' => 'Users successfully Added.',
        );

        return $status;
    }

   
    
    /**
    * Change status for interrupted test.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array obj
    */
    public function changeStatusForInterruptedTest($params)
    {
        if ($params){
            
        }
        $minutes = (DEFAULT_MAX_TEST_TIME + 600) / 60;
        $rsobj = $this->dbcon->Execute(
            GET_STATUS_FOR_INTERUPT_TEST, array($minutes)
        );
        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $data['status'] = '30';
                $data['is_test_completed'] = 'interrupted';
                $data['reason'] = 'interrupted from cron';
                $rsUpdates = $this->dbcon->GetUpdateSql($rsobj, $data);
                $this->dbcon->Execute($rsUpdates);
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Successfully retrieved',
                // 'sql' => $rsobj->sql,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 200,
                'status_message' => 'No Results found!',
            );
        }
        //Return the result array
        return array(
            'movesmart' => $this->status,
        );
    }

    
    /**
    * This function used to change cycle assigned status.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array obj
    */
    public function changeCycleAssignedStatus($params)
    {
        $rsobj = $this->dbcon->Execute(GET_CYCLE_ASSIGNED_FOR_TEST_CRON);
        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $data['r_equipment_id'] = '0';
                $data['status'] = '0';
                $rsUpdates = $this->dbcon->GetUpdateSql($rsobj, $data);
                $this->dbcon->Execute($rsUpdates);
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Successfully updated',
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 200,
                'status_message' => 'No Results found!',
            );
        }
        //Return the result array
        return array(
            'movesmart' => $this->status,
        );
    }
} // End Class.
;
