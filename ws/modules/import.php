<?php
/**
 * PHP version 5.
 
 * @category Modules
 
 * @package Import
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description DB access functions to handle import related actions.
 */
require_once SQL_PATH.DS.'import.php';
 /**
 * Class to handle import related actions.
 
 * @category Modules
 
 * @package Import
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/
 */
class importModel
{
    public $dbcon;
    public $errorstatus;

    /*
     * Class constructor.
     * @param array $dbcon connection arguments  
     * /
    public function __construct($dbcon)
    {
        $this->dbcon = $dbcon;
        $this->status = array(
                'status' => 'error',
                'status_code' => 1,
                'status_message' => 'Oops an error as occurred. Please try after sometime',
            );
    }

    /**
    * Function to create a user.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array obj
    */
    public function createUser($params)
    {
        //$last_insert_id = 0;
        $params['external_application_user_id'] = isset($params['external_application_user_id']) ? (int) $params['external_application_user_id'] : 0;
        $params['r_company_id'] = isset($params['r_company_id']) ? (int) $params['r_company_id'] : die('Param is missing');
        $rsobj = $this->dbcon->Execute(SELECT_USERS, array($params['external_application_user_id'], $params['r_company_id']));
        if ($rsobj->RecordCount()) {
            $userId = $rsobj->fields['user_id'];
            $update_sql = $this->dbcon->GetUpdateSql($rsobj, $params);
            $rsobj = $this->dbcon->Execute($update_sql);
            $this->status = array(
                'response' => array(
                    'status' => 'success',
                    'status_code' => '200',
                    'status_message' => 'user updated Successfully',
                    'userId' => $userId,
                    'insert_status' => '0',
                ),
            );
        } else {
            //Dynamically construct insert query
            $insert_sql = $this->dbcon->GetInsertSql($rsobj, $params);
            $rsobj = $this->dbcon->Execute($insert_sql);
            $userId = $this->dbcon->Insert_ID();

            //Update user uid begins
            $uid = md5($userId.'_'.$params['r_company_id'].'_'.$params['r_club_id'].'_'.$params['email']);
            $this->dbcon->Execute("UPDATE `t_users` SET `uid`='$uid' WHERE `user_id`='$userId'");
            //Update user uid ends

            $this->status = array(
                'response' => array(
                    'status' => 'success',
                    'status_code' => '200',
                    'status_message' => 'user created successfully',
                    'userId' => $userId,
                    'insert_status' => '1',
                ),
            );
        }

        //Update user personal info begins
        if ($userId > 0) {

            //Check zip code and city name
            $paramC['zipCode'] = $params['zipCode'];
            $paramC['cityName'] = $params['cityName'];
            $cityId = $this->getCityIdByNameandCode($paramC);
            //Check zip code and city name

            $paramP = array('userId' => $userId,
                            'dob' => $params['dob'],
                            'height' => isset($params['height']) ? $params['height'] : '',
                            'weight' => isset($params['weight']) ? $params['weight'] : '',
                            'address' => isset($params['address']) ? $params['address'] : '',
                            'doorno' => isset($params['doorNo']) ? $params['doorNo'] : '',
                            'bus' => isset($params['bus']) ? $params['bus'] : '',
                            'zipcode' => $cityId,
                            'r_city_id' => $cityId,
                            'phone' => isset($params['phone']) ? $params['phone'] : '',
                            'mobile' => isset($params['mobile']) ? $params['mobile'] : '',
                            );
            $this->updateUserPersonalInfo($paramP);
        }

        //Update user personal info ends
          return $this->status;
    }

    
    /**
    * Function to get city id by city name and zip code.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return integer obj
    */
    public function getCityIdByNameandCode($params)
    {
        $cityId = 0;
        if (trim($params['zipCode']) != '' && trim($params['cityName']) != '') {
            $rsobj = $this->dbcon->Execute(GET_CITY_EXIST, array($params['zipCode'], $params['cityName']));
            $data = array('zipcode' => $params['zipCode'], 'city_name' => $params['cityName']);
            if ($rsobj->RecordCount()) {
                $cityId = $rsobj->fields['city_id'];
            } else {
                //Dynamically construct insert query
                $insert_sql = $this->dbcon->GetInsertSql($rsobj, $data);
                $rsobj = $this->dbcon->Execute($insert_sql);
                $cityId = $this->dbcon->Insert_ID();
            }
        }

        return $cityId;
    }

       
    /**
    * Function Add/Update user personal info.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array obj
    */
    public function updateUserPersonalInfo($params)
    {
        $rsobj = $this->dbcon->Execute(GET_USER_PERSONAL_EXIST, array($params['userId']));

        $dob = date('Y-m-d', strtotime($params['dob']));
        $data = array('dob' => $dob,
                      'height' => isset($params['height']) ? $params['height'] : '',
                      'weight' => isset($params['weight']) ? $params['weight'] : '',
                      'r_user_id' => isset($params['userId']) ? $params['userId'] : '',
                      'address' => isset($params['address']) ? $params['address'] : '',
                      'doorno' => isset($params['doorno']) ? $params['doorno'] : '',
                      'bus' => isset($params['bus']) ? $params['bus'] : '',
                      'zipcode' => isset($params['zipcode']) ? $params['zipcode'] : '',
                      'r_city_id' => isset($params['r_city_id']) ? $params['r_city_id'] : '',
                      'phone' => isset($params['phone']) ? $params['phone'] : '',
                      'mobile' => isset($params['mobile']) ? $params['mobile'] : '',
                     );

        if ($rsobj->RecordCount()) {
            $rsUpdates = $this->dbcon->GetUpdateSql($rsobj, $data);
            $this->dbcon->Execute($rsUpdates);
            $status = array(
                        'status' => 'success',
                        'status_code' => 200,
                        'status_message' => 'User personal details update success',
                        );
        } else {
            //Dynamically construct insert query
            $rsInserts = $this->dbcon->GetInsertSql($rsobj, $data);
            $this->dbcon->Execute($rsInserts);
            $status = array(
                        'status' => 'success',
                        'status_code' => 200,
                        'status_message' => 'User personal details insert success',
                        );
        }

        return $status;
    }

    
    /**
    * Function to create a new company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array obj
    */
    public function createCompany($params)
    {
        $last_insert_id = 0;
        $params['company_name'] = isset($params['company_name']) ? $params['company_name'] : '';
        $params['updation_date'] = date('Y-m-d H:i:s');
        $params['created_by'] = 1;
        $params['created_date'] = date('Y-m-d H:i:s');
        $params['modified_by'] = 1;
        $params['modified_date'] = date('Y-m-d H:i:s');

        $rsobj = $this->dbcon->Execute(SELECT_COMPANY);
         //Dynamically construct insert query         
         $insert_sql = $this->dbcon->GetInsertSql($rsobj, $params);
        $rsobj = $this->dbcon->Execute($insert_sql);
        $last_insert_id = $this->dbcon->Insert_ID();
        $this->status = array(
                'response' => array(
                    'status' => 'success',
                    'status_code' => 1,
                    'status_message' => 'company imported successfully',
                    'companyId' => $last_insert_id,
                ),
         );

        return $this->status;
    }

    
    /**
    * Function to list companies.
    *
    * @return array obj
    */
    public function listCompanies()
    {
        $companies = array();
        $rsobj = $this->dbcon->Execute(SELECT_COMPANIES);
        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $companies[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
        }
        //Set the status message
        $this->status = array(
            'status' => 'success',
            'status_code' => 200,
            'status_message' => 'Categories successfully retrieved',
        );
        //Return the result array
        return array(
            'companyDetails' => array(
                'company' => $companies,
            ),
        );
    }
    
     /**
    * Function to list user status.
    *
    * @return array obj
    */
    public function listUserStatus()
    {
        $status = array();
        $rsobj = $this->dbcon->Execute(SELECT_STATUS);
        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $status[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
        }
        //Set the status message
        $this->status = array(
            'status' => 'success',
            'status_code' => 200,
            'status_message' => 'Categories successfully retrieved',
        );
        //Return the result array
        return array(
            'statusDetails' => array(
                'status' => $status,
            ),
        );
    }

    /**
    * Function to get profession list.
    *
    * @return array obj
    */
    public function getProfession()
    {
        //$professionList = array();
        $rsobj = $this->dbcon->Execute(GET_PROFESSION_LIST);

        if ($rsobj->RecordCount()) {
            $this->status = array('status' => 'success',
                                  'status_code' => 200,
                                  'professionList' => $rsobj->getRows(),
                                  );
        } else {
            //Set the status message
            $this->status = array(
                'status' => 'error',
                'status_code' => 1,
                'status_message' => 'No records found',
            );
        }

        //Return the result array
        return $this->status;
    }

    /* To import profession list * /
    public function importProfession($param)
    {
        //$professionList = array();
        //$param = json_decode($_REQUEST['param'], 1);
        $professionName = (isset($param['professionName'])) ? ucfirst(trim(strtolower($param['professionName']))) : '';
        $rsobj = $this->dbcon->Execute(GET_PROFESSION_EXIST, array($param['professionName']));
        $professionId = '';
        if ($rsobj->RecordCount()) {
            $row = $rsobj->getRows();
            if (isset($row[0]['profession_id'])) {
                $professionId = $row[0]['profession_id'];
            }
            //Set the status message
            $this->status = array(
                'status' => 'error',
                'status_code' => 1,
                'status_message' => 'Profession already exist',
                'professionId' => $professionId,
            );
        } else {
            if ($professionName != '') {
                $data = array(
                    'profession_name' => $professionName,
                    'created_by' => 1,
                    'created_date' => date('Y-m-d H:i:s'),
                    );
                $rsInserts = $this->dbcon->GetInsertSql($rsobj, $data);
                $rsInsert = $this->dbcon->Execute($rsInserts);
                if ($rsInsert) {
                    $this->status = array(
                        'status' => 'success',
                        'status_code' => 200,
                        'status_message' => 'Profession added successfully',
                        'professionId' => $this->dbcon->Insert_ID(),
                    );
                }
            }
        }

        //Return the result array
        return $this->status;
    }

 
    /**
    * Function to list User Type.
    *
    * @return array obj
    */
    public function listUserType()
    {
        $userType = array();
        $rsobj = $this->dbcon->Execute(SELECT_USER_TYPE);
        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $userType[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
        }
        //Set the status message
        $this->status = array(
            'status' => 'success',
            'status_code' => 200,
            'status_message' => 'Categories successfully retrieved',
        );
        //Return the result array
        return array(
            'userTypeDetails' => array(
                'userType' => $userType,
            ),
        );
    }

    /**
    * Function to import club.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array obj
    */
    public function importClub($params)
    {
        $last_insert_id = 0;
        $params['guid'] = isset($params['guid']) ? (int) $params['guid'] : die('Param is missing');
        $params['r_company_id'] = isset($params['r_company_id']) ? (int) $params['r_company_id'] : die('Param is missing');
        $rsobj = $this->dbcon->Execute(SELECT_CLUBS, array($params['guid'], $params['r_company_id']));

        if ($rsobj->RecordCount()) {
            $update_sql = $this->dbcon->GetUpdateSql($rsobj, $params);
            $rsobj = $this->dbcon->Execute($update_sql);

            $this->status = array(
                'response' => array(
                    'status' => 'success',
                    'status_code' => 2,
                    'status_message' => 'Club Updated Successfully',
                ),
            );
        } else {
            //Dynamically construct insert query
            $insert_sql = $this->dbcon->GetInsertSql($rsobj, $params);
            $rsobj = $this->dbcon->Execute($insert_sql);
            $last_insert_id = $this->dbcon->Insert_ID();

            $this->status = array(
                'response' => array(
                    'status' => 'success',
                    'status_code' => 1,
                    'status_message' => 'Club Imported successfully',
                    'clubId' => $last_insert_id,
                    'guid' => $params['guid'],
                ),
            );
        }

        return $this->status;
    }

    
    /**
    * Function to import Slot type.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array obj
    */
    public function importSlotType($params)
    {
        $last_insert_id = 0;
        $params['slot_id'] = isset($params['slot_id']) ? (int) $params['slot_id'] : die('Param Slot Id is missing');
        $params['slot_name'] = (isset($params['slot_name']) && $params['slot_name'] != '') ? $params['slot_name'] : die('Required Slot Name');
        $params['slot_category'] = (isset($params['slot_category']) && $params['slot_category'] != '') ? (int) $params['slot_category'] : die('Required Slot Category');

        $rsobj = $this->dbcon->Execute(SELECT_SLOTTYPE, array($params['slot_id']));

        if ($rsobj->RecordCount()) {
            $update_sql = $this->dbcon->GetUpdateSql($rsobj, $params);
            $rsobj = $this->dbcon->Execute($update_sql);

            $this->status = array(
                'response' => array(
                    'status' => 'success',
                    'status_code' => 2,
                    'status_message' => 'Slot Updated Successfully',
                ),
            );
        } else {
            //Dynamically construct insert query
            $insert_sql = $this->dbcon->GetInsertSql($rsobj, $params);
            $rsobj = $this->dbcon->Execute($insert_sql);
            $last_insert_id = $this->dbcon->Insert_ID();

            $this->status = array(
                'response' => array(
                    'status' => 'success',
                    'status_code' => 2,
                    'status_message' => 'Slot Imported successfully',
                    'slotId' => $last_insert_id,
                ),
            );
        }

        return $this->status;
    }

    /**
    * Function to import personal life info.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array obj
    */
    public function importPersonalInfo($params)
    {
        //$last_insert_id = 0;
        $params['userId'] = isset($params['r_user_id']) ? (int) $params['r_user_id'] : die('Param is missing');
        $rsobj = $this->dbcon->Execute(SELECT_PERSONALINFO, array($params['userId']));

        if ($rsobj->RecordCount()) {
            $update_sql = $this->dbcon->GetUpdateSql($rsobj, $params);
            $rsobj = $this->dbcon->Execute($update_sql);

            $this->status = array(
                'response' => array(
                    'status' => 'success',
                    'status_code' => 2,
                    'status_message' => 'Personal Info updated Successfully',
                ),
            );
        } else {
            //Dynamically construct insert query
            $insert_sql = $this->dbcon->GetInsertSql($rsobj, $params);
            $rsobj = $this->dbcon->Execute($insert_sql);

            $this->status = array(
                'response' => array(
                    'status' => 'error',
                    'status_code' => 1,
                    'status_message' => 'Person Info Added Sucessfully',
                ),
            );
        }

        return $this->status;
    }

     /**
    * Function to get external application user id.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array obj
    */
    public function getMemberuserIdBasedExternalApplicationUserId($params)
    {
        $params['external_application_user_id'] = isset($params['external_application_user_id']) ? $params['external_application_user_id'] : '';
        $rsobj = $this->dbcon->Execute(GET_USERID_BASED_PERSONID, array($params['external_application_user_id'], $params['companyId']));
        if ($rsobj->RecordCount()) {
            $this->status = array(
                'response' => array(
                    'status' => 'success',
                    'status_code' => '200',
                    'status_message' => 'Member details successfully retrieved',
            'userId' => $rsobj->fields['user_id'],
            // 'sql' => $rsobj->sql,
                ),
            );
        } else {
            $this->status = array(
                'response' => array(
                    'status' => 'error',
                    'status_code' => '0',
                    'status_message' => 'No Member details found',
                ),
            );
        }

        return $this->status;
    }

    
    /**
    * Function to import booking list.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array obj
    */
    public function importBooking($params)
    {
        //$professionList = array();
        //$userList = explode(',', $params['userList']);
        $params['start_date'] = date('Y-m-d', strtotime($params['start_date']));

        $sql = '';
        $userId = '';
        $flag = 0;

        foreach ($params['usersList'] as $bookingUser) {
            $param['external_application_user_id'] = $bookingUser['externalApplicationUserId'];
            $params['calendar_booking_id'] = $bookingUser['calendarSlotBookingId'];
            $param['companyId'] = $params['r_company_id'];
            $getMemberId = $this->getMemberuserIdBasedExternalApplicationUserId($param);
            $params['r_user_id'] = $getMemberId['response']['userId'];

            $rsobj = $this->dbcon->Execute(GET_BOOKING, array($params['r_company_id'], $params['r_club_id'], $params['r_slot_id'], $params['start_date'], $params['start_time'], $params['end_time'], $params['r_user_id']));
            $userId .= $param['r_user_id'].'_';

            if ($rsobj->RecordCount()) {
                exit;
                $rsUpdates = $this->dbcon->GetUpdateSql($rsobj, $params);
                $this->dbcon->Execute($rsUpdates);
                $flag = 1;
                $sql .= $rsUpdates.'|';
            } else {
                //Dynamically construct insert query        
                $insert_sql = $this->dbcon->GetInsertSql($rsobj, $params);
                $rsobj = $this->dbcon->Execute($insert_sql);
                $flag = 1;
                $sql .= $insert_sql.'|';
            }
        }

        $this->status = array(
                    'response' => array(
                        'status' => 'error',
                        'status_code' => '0',
                        'status_message' => 'booking import failed',
                    ),
             );

        if ($flag == 1) {
            $this->status = array(
                        'response' => array(
                            'status' => 'success',
                            'status_code' => '200',
                            'status_message' => 'booking import success',
                        ),
                 );
        }

        //Return the result array
        return $this->status;
    }

    /**
    * Function to import Medical Against.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array obj
    */
    public function importMedicalAgainst($params)
    {
        $params['external_application_user_id'] = $params['externalApplicationUserId'];
        $param['companyId'] = $params['companyId'];
        $getMemberId = $this->getMemberuserIdBasedExternalApplicationUserId($param);
        $userId = $getMemberId['response']['userId'];

        if ($userId > 0) {
            $rsobj = $this->dbcon->Execute(MEDICAL_AGAINST_USER_EXIST, array($userId));
            $dateTime = date('Y-m-d H:i:s');

            $data = array('high_blood_pressure' => (isset($params['highBloodPressure']) && $params['highBloodPressure'] == 1) ? 1 : 0,
                          'low_blood_pressure' => (isset($params['lowBloodPressure']) && $params['lowBloodPressure'] == 1) ? 1 : 0,
                          'stroma_underfunction' => (isset($params['stromaUnderfunction']) && $params['stromaUnderfunction'] == 1) ? 1 : 0,
                          'stroma_overfunction' => (isset($params['stromaOverfunction']) && $params['stromaOverfunction'] == 1) ? 1 : 0,
                          'asthma' => (isset($params['asthma']) && $params['asthma'] == 1) ? 1 : 0,
                          'diabetes' => (isset($params['diabetes']) && $params['diabetes'] == 1) ? 1 : 0,
                          'osteoporose' => (isset($params['osteoporose']) && $params['osteoporose'] == 1) ? 1 : 0,
                          'others_desc' => isset($params['othersDesc']) ? $params['othersDesc'] : '',
                         );
            if ($rsobj->RecordCount()) {
                $rsUpdates = $this->dbcon->GetUpdateSql($rsobj, $data);
                $this->dbcon->Execute($rsUpdates);

                $this->status = array(
                    'response' => array(
                        'status' => 'success',
                        'status_code' => 200,
                        'status_message' => 'Import Medical Against Updated Successfully',
                    ),
                );
            } else {
                $data['r_user_id'] = $userId;
                $data['created_by'] = 2;
                $data['created_date'] = $dateTime;
                $rsInserts = $this->dbcon->GetInsertSql($rsobj, $data);
                $this->dbcon->Execute($rsInserts);
                $this->status = array(
                    'response' => array(
                        'status' => 'success',
                        'status_code' => 200,
                        'status_message' => 'Import Medical Against Added Successfully',
                    ),
                );
            }
        }

        return $this->status;
    }

    /**
    * Function to import Medical joint problems.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array obj
    */
    public function importMedicalJointProblem($params)
    {
        $params['external_application_user_id'] = $params['externalApplicationUserId'];
        $param['companyId'] = $params['companyId'];
        $getMemberId = $this->getMemberuserIdBasedExternalApplicationUserId($param);
        $userId = $getMemberId['response']['userId'];

        if ($userId > 0) {
            $rsobj = $this->dbcon->Execute(MEDICAL_JOINTPROBLEM_USER_EXIST, array($userId));
            $dateTime = date('Y-m-d H:i:s');

            $data = array('cervial_vertebr' => (isset($params['cervialVertebr']) && $params['cervialVertebr'] == 1) ? 1 : 0,
                          'thoraecal_vertebr' => (isset($params['thoraecalVertebr']) && $params['thoraecalVertebr'] == 1) ? 1 : 0,
                          'lower_back' => (isset($params['lowerBack']) && $params['lowerBack'] == 1) ? 1 : 0,
                          'shoulder' => (isset($params['shoulder']) && $params['shoulder'] == 1) ? 1 : 0,
                          'elbow' => (isset($params['elbow']) && $params['elbow'] == 1) ? 1 : 0,
                          'hand' => (isset($params['hand']) && $params['hand'] == 1) ? 1 : 0,
                          'hip' => (isset($params['hip']) && $params['hip'] == 1) ? 1 : 0,
                          'knee' => (isset($params['knee']) && $params['knee'] == 1) ? 1 : 0,
                          'osteoporose' => (isset($params['osteoporose']) && $params['osteoporose'] == 1) ? 1 : 0,
                          'others_desc' => isset($params['othersDesc']) ? $params['othersDesc'] : '',
                         );
            if ($rsobj->RecordCount()) {
                $rsUpdates = $this->dbcon->GetUpdateSql($rsobj, $data);
                $this->dbcon->Execute($rsUpdates);

                $this->status = array(
                    'response' => array(
                        'status' => 'success',
                        'status_code' => 200,
                        'status_message' 
                            => 'Import Medical Joint Problem Datas Updated Successfully',
                    ),
                );
            } else {
                $data['r_user_id'] = $userId;
                $data['created_by'] = 2;
                $data['created_date'] = $dateTime;
                $rsInserts = $this->dbcon->GetInsertSql($rsobj, $data);
                $this->dbcon->Execute($rsInserts);
                $this->status = array(
                    'response' => array(
                        'status' => 'success',
                        'status_code' => 200,
                        'status_message' 
                            => 'Import Medical Joint Problem Datas Added Successfully',
                    ),
                );
            }
        }

        return $this->status;
    }

    /**
    * Function to import Medical Muscle Injury.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array obj
    */
    public function importMedicalMuscleInjury($params)
    {
        $param['personId'] = $params['personId'];
        $param['companyId'] = $params['companyId'];
        $getMemberId = $this->getMemberuserIdBasedExternalApplicationUserId($param);
        $userId = $getMemberId['response']['userId'];

        if ($userId > 0) {
            $rsobj = $this->dbcon->Execute(
                MEDICAL_MUSCLEINJURY_USER_EXIST, array($userId)
            );
            $dateTime = date('Y-m-d H:i:s');

            $data = array(
                        'back' => (isset($params['back']) && 
                            $params['back'] == 1) ? 1 : 0,
                          'buttock' => (isset($params['buttock']) && 
                            $params['buttock'] == 1) ? 1 : 0,
                          'stroma' => (isset($params['stroma']) && 
                            $params['stroma'] == 1) ? 1 : 0,
                          'shoulder' => (isset($params['shoulder']) && 
                            $params['shoulder'] == 1) ? 1 : 0,
                          'leg' => (isset($params['leg']) && 
                            $params['leg'] == 1) ? 1 : 0,
                          'arm' => (isset($params['arm']) && 
                            $params['arm'] == 1) ? 1 : 0,
                          'osteoporose' => (isset($params['osteoporose']) && 
                            $params['osteoporose'] == 1) ? 1 : 0,
                          'others_desc' => isset($params['othersDesc']) ? 
                            $params['othersDesc'] : '',
                    );
            if ($rsobj->RecordCount()) {
                $rsUpdates = $this->dbcon->GetUpdateSql($rsobj, $data);
                $this->dbcon->Execute($rsUpdates);

                $this->status = array(
                    'response' => array(
                        'status' => 'success',
                        'status_code' => 200,
                        'status_message' 
                            => 'Import Medical Muscle Datas Updated Successfully',
                    ),
                );
            } else {
                $data['r_user_id'] = $userId;
                $data['created_by'] = 2;
                $data['created_date'] = $dateTime;
                $rsInserts = $this->dbcon->GetInsertSql($rsobj, $data);
                $this->dbcon->Execute($rsInserts);
                $this->status = array(
                    'response' => array(
                        'status' => 'success',
                        'status_code' => 200,
                        'status_message' 
                            => 'Import Medical Muscle Datas Added Successfully',
                    ),
                );
            }
        }

        return $this->status;
    }

    /**
    * Function to import medical risk factor.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array obj
    */
    public function importMedicalRiskFactor($params)
    {
        $param['personId'] = $params['personId'];
        $param['companyId'] = $params['companyId'];
        $getMemberId = $this->getMemberuserIdBasedExternalApplicationUserId($param);
        $userId = $getMemberId['response']['userId'];

        if ($userId > 0) {
            $rsobj = $this->dbcon->Execute(
                MEDICAL_RISKFACTOR_USER_EXIST, array($userId)
            );
            $dateTime = date('Y-m-d H:i:s');

            $data = array('high_blood_pressure' => (isset($params['highBloodPressure']) && 
                $params['highBloodPressure'] == 1) ? 1 : 0,
                          'low_blood_pressure' 
                            => (isset($params['lowBloodPressure']) && 
                            $params['lowBloodPressure'] == 1) ? 1 : 0,
                          'coronary_problems' 
                            => (isset($params['coronaryProblems']) && 
                            $params['coronaryProblems'] == 1) ? 1 : 0,
                          'coronary_heartdisease' 
                            => (isset($params['coronaryHeartdisease']) &&
                            $params['coronaryHeartdisease'] == 1) ? 1 : 0,
                          'heart_infarct' => (isset($params['heartInfarct']) && 
                            $params['heartInfarct'] == 1) ? 1 : 0,
                          'asthma' => (isset($params['asthma']) && 
                            $params['asthma'] == 1) ? 1 : 0,
                          'rheuma' => (isset($params['rheuma']) && 
                            $params['rheuma'] == 1) ? 1 : 0,
                          'osteoporose' => (isset($params['osteoporose']) && 
                            $params['osteoporose'] == 1) ? 1 : 0,
                          'pregnancy' => (isset($params['pregnancy']) && 
                            $params['pregnancy'] == 1) ? 1 : 0,
                          'central_nerval_problems' => (isset($params['centralNervalProblems']) && $params['centralNervalProblems'] == 1) ? 1 : 0,
                          'alkohol' => (isset($params['alkohol']) && 
                            $params['alkohol'] == 1) ? 1 : 0,
                          'nicotin' => (isset($params['nicotin']) && 
                            $params['nicotin'] == 1) ? 1 : 0,
                          'stress' => (isset($params['stress']) && 
                            $params['stress'] == 1) ? 1 : 0,
                          'others_desc' => isset($params['othersDesc']) ? 
                            $params['othersDesc'] : '',
                         );
            if ($rsobj->RecordCount()) {
                $rsUpdates = $this->dbcon->GetUpdateSql($rsobj, $data);
                $this->dbcon->Execute($rsUpdates);

                $this->status = array(
                    'response' => array(
                        'status' => 'success',
                        'status_code' => 200,
                        'status_message' 
                            => 'Import Medical Risk Data Updated Successfully',
                    ),
                );
            } else {
                $data['r_user_id'] = $userId;
                $data['created_by'] = 2;
                $data['created_date'] = $dateTime;
                $rsInserts = $this->dbcon->GetInsertSql($rsobj, $data);
                $this->dbcon->Execute($rsInserts);
                $this->status = array(
                    'response' => array(
                        'status' => 'success',
                        'status_code' => 200,
                        'status_message' 
                            => 'Import Medical Risk Datas Added Successfully',
                    ),
                );
            }
        }

        return $this->status;
    }

    /**
    * Function to cancel booking list.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array obj
    */
    public function cancelBooking($params)
    {
        //$professionList = array();
        $userList = explode(',', $params['userList']);
        $sql = '';
        foreach ($userList as $bookingUser) {
            $param['external_application_user_id'] = $bookingUser;
            $param['companyId'] = $params['r_company_id'];
            $getMemberId = $this->getMemberuserIdBasedExternalApplicationUserId($param);
            $params['r_user_id'] = $getMemberId['response']['userId'];
            $rsobj = $this->dbcon->Execute(
                GET_BOOKING, 
                array(
                    $params['r_company_id'], $params['r_club_id'], 
                    $params['slot'], $params['start_date'], 
                    $params['start_time'], $params['end_time'], 
                    $params['r_user_id'])
                );
            $paramsCancel['is_canceled'] = 1;
            if ($rsobj->RecordCount()) {
                $rsUpdates = $this->dbcon->GetUpdateSql($rsobj, $paramsCancel);
                $this->dbcon->Execute($rsUpdates);
                $sql = $rsUpdates;
                $status = 'Success';
                $statusMsg = 'booking cancelled successfully';
                $status_code = 1;
            } else {
                $status = 'Error';
                $statusMsg = 'No Booking Details Found.';
                $sql = '-';
                $status_code = 200;
            }
        }
        $this->status = array(
                        'response' => array(
                            'status' => $status,
                            'status_code' => $status_code,
                            'status_message' => $statusMsg,
                        ),
                 );
        //Return the result array
        return $this->status;
    }

    /**
    * Function to update password.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array obj
    */
    public function updatePassword($params)
    {
        $rsobj = $this->dbcon->Execute(
            GET_UPDATEPASSWORD, 
            array($params['external_application_user_id'])
        );
        $data = array('password' => isset($params['password']) ?
            $params['password'] : '');

        $msg = 'Record Not Found';
        $type = 'Error';
        $code = '0';

        if ($rsobj->RecordCount()) {
            $rsUpdates = $this->dbcon->GetUpdateSql($rsobj, $data);
            $this->dbcon->Execute($rsUpdates);

            $msg = 'Password Changed Successfully';
            $type = 'Success';
            $code = '1';
        }

        return array(
                        'status' => $type,
                        'status_code' => $code,
                        'status_message' => $msg,
                        );
    }
}    // end of the class 
;
