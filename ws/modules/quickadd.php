<?php
/**
 * PHP version 5.
 
 * @category Modules
 
 * @package QuickAddPopup
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description DB access functions to handle quick add popup related actions.
 */
require_once SQL_PATH.DS.'quickadd.php'; 
/**
 * Class to handle Quick Add related functions.
 
 * @category Modules
 
 * @package Admin
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/
*/
class quickaddModel
{
    public $dbcon;
    public $status;
    public $error_general;

    /**
     * Class constructor.
     * @param ADOConnection|array $dbcon connection arguments
     */
    public function __construct(ADOConnection $dbcon)
    {
        $this->dbcon = $dbcon;
        $this->status = array(
            'status' => 'success',
            'status_code' => 200,
            'status_message' => 'Success',
        );

        $this->error_general = array(
            'response' => array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Some error occured. Please try later.',
            ),
        );
    }

    /**
    * Returns an json obj of insert City for a company.
    *
    * @param array $param service parameter, Search arguments 
    *
    * @return array object object
    */
    public function insertCity($param)
    {
        if (isset($param['city_name']) && ($param['city_name'] != '')) {
            $cityName = $param['city_name'];
        } else {
            $cityName = -1;
        }
        $rsobj = $this->dbcon->Execute(CREATE_CITY, $cityName);
        if ($rsobj->RecordCount()) {
            $this->status = array(
                    'response' => array(
                        'status' => 'Error',
                        'status_code' => 1,
                        'status_message' => 'City is Alreay Exists',
                    ),
                );
        } else {
            $insert_sql = $this->dbcon->GetInsertSQL($rsobj, $param);
            $result = $this->dbcon->Execute($insert_sql);
            $cityId = $this->dbcon->Insert_ID();
            if ($result) {
                $this->status = array(
                    'response' => array(
                        'status' => 'success',
                        'status_code' => 1,
                        'status_message' => 'City successfully Inserted',
                        'insert_id' => $cityId,
                    ),
                );
            } else {
                $this->status = $this->error_general;
            }
        }

        return $this->status;
    }

    /**
    * Returns an json obj of insert Company for a company.
    *
    * @param array $param service parameter, Search arguments 
    *
    * @return array object object
    */
    /*public function insertCompany($param)
    {
        if (isset($param['company_name']) && ($param['company_name'] != '')) {
            $companyName = $param['company_name'];
        } else {
            $companyName = -1;
        }

        $rsobj = $this->dbcon->Execute(CREATE_COMPANY, $companyName);

        if ($rsobj->RecordCount()) {
            $this->status = array(
                'response' => array(
                        'status' => 'Error',
                        'status_code' => 1,
                        'status_message' => 'Company already Exists',
                    ),
                );
        } else {
            $insert_sql = $this->dbcon->GetInsertSql($rsobj, $param);
            $result = $this->dbcon->Execute($insert_sql);
            $companyId = $this->dbcon->Insert_ID();
            if ($result) {
                $this->status = array(
                    'response' => array(
                        'status' => 'success',
                        'status_code' => 1,
                        'status_message' => 'Company details successfully Inserted',
                        'insert_id' => $companyId,
                    ),
                );
            } else {
                $this->status = array('status' => $this->error_general); // 'sql' => $rsobj->sql);
            }
        }

        return $this->status;
    }
    */
    /**
    * Returns an json obj of insert Profession for a company.
    *
    * @param array $param service parameter, Search arguments 
    *
    * @return array object object
    */
    /*public function insertProfession($param)
    {
        if (isset($param['profession_name']) && ($param['profession_name'] != '')) {
            $professionName = $param['profession_name'];
        } else {
            $professionName = -1;
        }
        $rsobj = $this->dbcon->Execute(CREATE_PROFESSION, $professionName);
        if ($rsobj->RecordCount()) {
            $this->status = array(
                    'response' => array(
                        'status' => 'Error',
                        'status_code' => 1,
                        'status_message' => 'Already Profession Exists',
                    ),
                );
        } else {
            $insert_sql = $this->dbcon->GetInsertSql($rsobj, $param);
            $result = $this->dbcon->Execute($insert_sql);
            $companyId = $this->dbcon->Insert_ID();
            if ($result) {
                $this->status = array(
                    'response' => array(
                        'status' => 'success',
                        'status_code' => 1,
                        'status_message' => 'Profession details successfully Inserted',
                        'insert_id' => $companyId,
                    ),
                );
            } else {
                $this->status = array('status' => $this->error_general);//, // 'sql' => $rsobj->sql);
            }
        }

        return $this->status;
    }
    */
} //Class End.
;
