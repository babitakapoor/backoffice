<?php
/**
 * PHP version 5.
 
 * @category Modules
 
 * @package Admin
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description DB access functions to handle admin related actions.
 */
require_once SQL_PATH.DS.'admin.php';
/**
 * Class to handle Admin - Club Activities related functions.
 
 * @category Modules
 
 * @package Admin
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/
 */
class adminModel
{
    public $dbcon;
    public $status;

    /**
     * Class constructor.
     * @param ADOConnection $dbcon connection arguments
     */
    public function __construct(ADOConnection $dbcon)
    {
        $this->dbcon = $dbcon;

        $this->status = array(
            'status' => 'success',
            'status_code' => 200,
            'status_message' => 'Success',
        );

        $this->error_general = array(
            'response' => array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Some error occurred. Please try later.',
            ),
        );
    }

    /**
    * Returns an json obj of plan-list for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    */
    public function getPlanListByCompany($params)
    {
        $limit = '';
        if (isset($params['limitStart']) && isset($params['limitEnd'])) {
            $limitStart = $params['limitStart'];
            $limitEnd = $params['limitEnd'];
            $limit = 'LIMIT '.$limitStart.','.$limitEnd;
        }

        $filterQry = '';

        //To get filter data
        if (isset($params['searchType'])) {
            if (trim($params['searchValue']) != '') {
                $filterQry = 'AND '.$params['searchType'].
                    " LIKE '".trim($params['searchValue'])."%'";
            }
        }

        $sort = 'ORDER BY `plan_id` ASC ';

        if (isset($params['labelField']) && isset($params['sortType'])) {
            $sort = 'ORDER BY '.$params['labelField'].' '.$params['sortType'];
        }

        $qry = GET_PLAN_LIST_BY_COMPANY.' '.$filterQry.' '.$sort.' '.$limit;

        $rsobj = $this->dbcon->Execute($qry, array($params['companyId']));

        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Get plan result success',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'rows' => $rsobj->GetRows(),
                'totalCount' => $this->getLastQueryTotalCount(),
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get plan result failed',
                // 'sql' => $rsobj->sql,
            );
        }

        //Return the result array    
        return $this->status;
    }

    /**
    * Returns an json obj of plan-Detail for a chosen plan.
    *
    * @param array $params service parameter, Keys: PlanId
    *
    * @return array object object
    */
    public function getPlanDetailById($params)
    {
        $rsobj = $this->dbcon->Execute(
            GET_PLAN_DETAIL_BY_PLAN_ID, array($params['planId'])
        );

        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Get plan result success',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'rows' => $rsobj->GetRows(),
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get plan result failed',
                // 'sql' => $rsobj->sql,
            );
        }

        //Return the result array    
        return $this->status;
    }

    /**
    * Deletes a plan chosen.
    *
    * @param array $params service parameter, Keys: PlanId
    *
    * @return Object of Result Array
    */
    public function deletePlanById($params)
    {
        $rsobj = $this->dbcon->Execute(GET_CLUB_ID_EXIST, array($params['planId']));
        if ($rsobj->RecordCount()) {

            //Update club id delete status
            $this->dbcon->Execute(
                UPDATE_PLAN_ID_SOFT_DELETE, array($params['planId'])
            );

            //Set the status message
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'plan delete success',
                'rows' => $rsobj->GetRows(),
                // 'sql' => $rsobj->sql,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'plan delete failed',
            );
        }
        //Return the result array    

        return $this->status;
    }

    /**
    * Returns a count of Total Records (without mining the limits). Common for Admin Module .
    *
    * @return Integer
    */
    public function getLastQueryTotalCount()
    {
        $rsobj = $this->dbcon->Execute(GET_QUERY_LAST_TOTAL_COUNT);

        $toatlCount = 0;

        if ($rsobj->RecordCount()) {
            $row = $rsobj->GetRows();
            $toatlCount = $row[0]['total_count'];
        }

        return $toatlCount;
    }

    /**
    * Add / Update Records & Returns Result For a New or Edited Plan Details
    *
    * @param array $params service parameter, Keys: PlanId, Update Params:companyId,loggedUserId,clubId
    *
    * @return Integer
    */
    public function updatePlanClubDetails($params)
    {
        $dateTime = date('Y-m-d H:i:s');
        $data = array(
            'r_company_id' => $params['companyId'],
            'r_plan_id' => $params['planId'],
            'created_by' => $params['loggedUserId'],
            'created_date' => $dateTime,
            'modified_date' => $dateTime,
            'modified_by' => $params['loggedUserId'],
        );
        $clubIds = explode(',', $params['clubId']);
        $flagClub = 0;
        foreach ($clubIds as $clubId) {
            if ($clubId) {
                $data['r_club_id'] = $clubId;
                $rsPlanclub = $this->dbcon->Execute(
                    GET_PLAN_CLUB_DETAIL_BY_PLAN_ID, 
                    array($params['planId'], $clubId)
                );
                if (!$rsPlanclub->RecordCount()) {
                    //Dynamically construct insert query         
                    $insert_sql = $this->dbcon->GetInsertSQL($rsPlanclub, $data);
                    $this->dbcon->Execute($insert_sql);
                    $planId = $this->dbcon->Insert_ID();
                    if ($planId) {
                        $flagClub = 1;
                    }
                }
            }
        }
        //set is deleted club ids.
        $unSetClubdata = array(
            'is_deleted' => 1,
            'r_plan_id' => $params['planId'],
            'created_by' => $params['loggedUserId'],
            'created_date' => $dateTime,
            'modified_date' => $dateTime,
            'modified_by' => $params['loggedUserId'],
        );
        $unClubIds = explode(',', $params['unClubId']);
        foreach ($unClubIds as $unClubId) {
            if ($unClubId) {
                $rsPlanclubUnset = $this->dbcon->Execute(
                    GET_PLAN_CLUB_DETAIL_BY_PLAN_ID, 
                    array($params['planId'], $unClubId)
                );
                if ($rsPlanclubUnset->RecordCount()) {
                    $unSetClubdata['r_club_id'] = $unClubId;
                    //Dynamically construct update query                
                    $rsUpdates = $this->dbcon->GetUpdateSQL(
                        $rsPlanclubUnset, $unSetClubdata
                    );
                    $this->dbcon->Execute($rsUpdates);
                    $flagClub = 1;
                }
            }
        }
        //Club Unset End

        if ($flagClub > 0) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Plan Club details update success',
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Plan Club Details No Records Affected',
            );
        }
        //Return the result array
        return $this->status;
    }

    /**
    * Get & Returns Records of the Planned Club List for a company
    *
    * @param array $params service parameter, Keys: PlanId,Company_Id
    *
    * @return array object
    */
    public function getPlanClubList($params)
    {
        $company_id = isset($params['company_id']) ? $params['company_id'] : -1;
        $planId = isset($params['planId']) ? $params['planId'] : -1;
        $rsobj = $this->dbcon->Execute(
            GET_PLAN_CLUB_DETAIL_DROPDOWN, array($company_id, $planId)
        );
        if ($rsobj->RecordCount()) {
            $clubPlanlist   =   array();
            while (!$rsobj->EOF) {
                $clubPlanlist[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Plan Club Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'clubPlanlist' => $clubPlanlist,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Plan Club Details Not Found.',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                'clubPlanlist' => '',
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }
    
    /**
    * Get & Returns Records Strength Machine
    *
    * @param array $params service parameter, Keys: PlanId,Company_Id
    *
    * @return array object
    */
    public function getStrengthMachineList($params)
    {
        $pid = isset($params['pid']) ? $params['pid'] : 0;
        if ($pid) {
            $rsobj = $this->dbcon->Execute(
                GET_STRENGTH_MACHINE_LIST_ID, 
                array($params['pid'], $params['clubid'])
            );
        } else {
            $rsobj = $this->dbcon->Execute(
                GET_STRENGTH_MACHINE_LIST, array($params['clubid'])
            );
        }

        if ($rsobj->RecordCount()) {
            $strengthMachine    =   array();
            while (!$rsobj->EOF) {
                $strengthMachine[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Strength Machine Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'strengthMachine' => $strengthMachine,
            'affectedrows' => $this->getLastQueryTotalCount(),
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Strength Machine Details Not Found.',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                'strengthMachine' => '',
            );
        }
        //Return the result array    
        return  $this->status;
    }

    /**
    * Get & Returns Records(List) of Machines For a Program
    *
    * @param array $params service parameter, Keys: ProgramId(pid),UserId, ClubId
    *
    * @return array object
    */
    public function getMachineListByProgramId($params)
    {
        $rsobj = $this->dbcon->Execute(
            GET_MACHINE_LIST_PROG_ID, 
            array(
                $params['userId'], 
                $params['clubid'], 
                $params['pid'], 
                $params['is_deleted']
            )
        );
        if ($rsobj->RecordCount()) {
            $strengthMachine    =   array();
            while (!$rsobj->EOF) {
                $strengthMachine[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Strength Machine Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'strengthMachine' => $strengthMachine,
                'affectedrows' => $this->getLastQueryTotalCount(),
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Strength Machine Details Not Found.',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                'strengthMachine' => '',
            );
        }

        return  $this->status;
    }

    /**
    * Get & Returns Records(List) of Machines For a Club Chosen
    *
    * @param array $params service parameter, Keys: ClubId
    *
    * @return array object
    */
    public function getStrengthMachineClubList($params)
    {
        $clubId = isset($params['clubId']) ? $params['clubId'] : -1;
        $rsobj = $this->dbcon->Execute(
            GET_STRENGTH_MACHINE_CLUB_LIST, array($clubId)
        );
        if ($rsobj->RecordCount()) {
            $strengthMachine    =   array();
            while (!$rsobj->EOF) {
                $strengthMachine[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Strength Machine Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'strengthMachine' => $strengthMachine,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Strength Machine Details Not Found.',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                'strengthMachine' => '',
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }

    /**
    * Get & Returns Records(List) of Activities Of a Club Chosen or All
    *
    * @param array $params service parameter, Keys: ClubId (optoinal)
    *
    * @return array object
    */
    public function getActivities($params)
    {
        if (isset($params['clubId'])) {
            $rsobj = $this->dbcon->Execute(
                GET_ACTIVITIES_LIST_CLUB, array($params['clubId'])
            );
        } else {
            $rsobj = $this->dbcon->Execute(GET_ACTIVITIES_LIST);
        }

        if ($rsobj->RecordCount()) {
            $activities =   array();
            while (!$rsobj->EOF) {
                $activities[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Strength Machine Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'activities' => $activities,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Strength Machine Details Not Found.',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                'activities' => '',
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }

    /**
    * Get & Returns Records(List) of Activities Of a Club Chosen
    *
    * @param array $params service parameter, Keys: ClubId
    *
    * @return array object
    */
    public function getActivitiesClub($params)
    {
        $clubId = isset($params['clubId']) ? $params['clubId'] : -1;
        $rsobj = $this->dbcon->Execute(GET_ACTIVITIES_CLUB_LIST, array($clubId));
        if ($rsobj->RecordCount()) {
            $activitiesClub =   array();
            while (!$rsobj->EOF) {
                $activitiesClub[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Strength Machine Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'activitiesClub' => $activitiesClub,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Strength Machine Details Not Found.',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                'activitiesClub' => '',
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }

    /**
    * Update & Returns Result On Update Strngth Machine
    *
    * @param array $params service parameter, Keys: ClubId , UserId(oggedUserId)
    *
    * @return array object
    */
    public function updateStrengthMachineDetails($params)
    {
        $dateTime = date('Y-m-d H:i:s');
        $data = array(
            'r_club_id' => $params['clubId'],
            'created_by' => $params['loggedUserId'],
            'created_date' => $dateTime,
            'modified_date' => $dateTime,
            'modified_by' => $params['loggedUserId'],
        );
        // $strengthMachine = explode(",", $params['strengthMachineId']);
        $strengthMachine = $params['strengthMachineId'];
        $flagClub = 0;
        foreach ($strengthMachine as $strengthMachineList) {
            if ($strengthMachineList) {
                $data['r_strength_machine_id'] = $strengthMachineList;
                $rsPlanclub = $this->dbcon->Execute(
                    GET_STRENGTH_MACHINE_MAP, 
                    array($params['clubId'], $strengthMachineList)
                );
                if (!$rsPlanclub->RecordCount()) {
                    //Dynamically construct insert query         
                    $insert_sql = $this->dbcon->GetInsertSQL($rsPlanclub, $data);
                    $this->dbcon->Execute($insert_sql);
                    $planId = $this->dbcon->Insert_ID();
                    if ($planId) {
                        $flagClub = 1;
                    }
                }
            }
        }
        //Club Unset End        
        if ($flagClub > 0) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Strength Machine Club details update success',
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Strength Machine Details No Records Affected',
            );
        }
        //Return the result array
        return $this->status;
    }

    /**
    * Get & Returns Records(List) Of Members for a club chosen
    *
    * @param array $params service parameter, Keys: ClubId
    *
    * @return array object
    */
    public function getMemberByClub($params)
    {
        $rsobj = $this->dbcon->Execute(GET_MEMBER_BY_CLUB, array($params['clubid']));
        $memberlist = array();
        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $memberlist[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get club member Details',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'memberlist' => $memberlist,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Strength Machine Details Not Found.',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'memberlist' => '',
            );
        }
        //Return the result array    
        return $this->status;
        // foreach($memberlist as $member){
            // $rsobj = $this->dbcon->Execute(
            //  GET_MEMBER_BY_CLUB, array($params['clubid'])
            // );
        // }
    }
    /**
    * Add / Update & Returns Result Of Updated/Added 
        Activities Details for a coach / Logged In User
    *
    * @param array $params, Keys: ClubId, ActivitiesId, Data:modified_by,created_by,club_id,etc.
    *
    * @return array object
    */
    public function updateActivitiesDetails($params)
    {
        $dateTime = date('Y-m-d H:i:s');
        $data = array(
            'r_club_id' => $params['clubId'],
            'created_by' => $params['loggedUserId'],
            'created_date' => $dateTime,
            'modified_date' => $dateTime,
            'modified_by' => $params['loggedUserId'],
        );
        $activitiesId = explode(',', $params['activitiesId']);
        $flagClub = 0;
        foreach ($activitiesId as $activitiesIdList) {
            if ($activitiesIdList) {
                $data['r_activity_id'] = $activitiesIdList;
                $rsPlanclub = $this->dbcon->Execute(
                    GET_ACTIVITIES_MAP, array($params['clubId'], $activitiesIdList)
                );
                if (!$rsPlanclub->RecordCount()) {
                    //Dynamically construct insert query         
                    $insert_sql = $this->dbcon->GetInsertSQL($rsPlanclub, $data);
                    $this->dbcon->Execute($insert_sql);
                    $planId = $this->dbcon->Insert_ID();
                    if ($planId) {
                        $flagClub = 1;
                    }
                }
            }
        }

        //set is deleted club ids.
        $unSetClubdata = array(
            'is_deleted' => 1,
            'r_club_id' => $params['clubId'],
            'created_by' => $params['loggedUserId'],
            'created_date' => $dateTime,
            'modified_date' => $dateTime,
            'modified_by' => $params['loggedUserId'],
        );
        $unActivitiesId = explode(',', $params['unActivitiesId']);
        foreach ($unActivitiesId as $unActivitiesIdList) {
            if ($unActivitiesIdList) {
                $rsPlanclubUnset = $this->dbcon->Execute(
                    GET_ACTIVITIES_MAP, array($params['clubId'], $unActivitiesIdList)
                );
                if ($rsPlanclubUnset->RecordCount()) {
                    $unSetClubdata['r_activity_id'] = $unActivitiesIdList;
                    //Dynamically construct update query                
                    $rsUpdates = $this->dbcon->GetUpdateSQL(
                        $rsPlanclubUnset, $unSetClubdata
                    );
                    $this->dbcon->Execute($rsUpdates);
                    $flagClub = 1;
                }
            }
        }
        //Club Unset End        
        if ($flagClub > 0) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Activities Club details update success',
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Activities Details No Records Affected',
            );
        }
        //Return the result array
        return $this->status;
    }

    /**
    * Get & Returns Details Of  Body Composition Test Information
    *
    * @param array $params service parameters
    *
    * @return array object
    */
    public function getBcompMethod($params)
    {
        $rsobj = $this->dbcon->Execute(
            GET_BCOMP_METHODS, array($params['is_deleted'])
        );

        if ($rsobj->RecordCount()) {
            $methods    =   array();
            while (!$rsobj->EOF) {
                $methods[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Strength Machine Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'bcompmethods' => $methods,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Strength Machine Details Not Found.',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                'activities' => '',
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }

    /**
    * Get & Returns List Of  Active Body Composition 
    *
    * @param array $params service parameters
    *
    * @return array object
    */
    
    public function getActiveBcompMethods($params)
    {
        $clubId = isset($params['clubId']) ? $params['clubId'] : -1;
        $rsobj = $this->dbcon->Execute(GET_ACTIVE_BCOMP_METHODS, array($clubId));
        if ($rsobj->RecordCount()) {
            $activemethods  =   array();
            while (!$rsobj->EOF) {
                $activemethods[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Strength Machine Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'activeBcompMethods' => $activemethods,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Strength Machine Details Not Found.',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                'activeBcompMethods' => '',
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }

    /**
    * Update Body Composition Method for a Club 
    *
    * @param array $params service parameters
    *
    * @return array object
    */
    public function updateClubBcompMethods($params)
    {
        /* ACID Start here - ensures that all operations within
        the work unit are completed successfully; otherwise, 
        the transaction is aborted at the point of failure and 
        previous operations are rolled back to their former state. */
        $revertFlag = 0;
        $this->dbcon->BeginTrans();
        $this->dbcon->Execute(REMOVE_AVAILABLE_BCOMP, array($params['clubId']));
        //$activitiesId = explode(',', $params['activitiesId']);
        $rsInsBcomp = $this->dbcon->Execute(
            GET_AVAILABLE_BCOMP, array($params['clubId'])
        );
        $bCompId    =   0;
        foreach ($params['bcomp_methods'] as $bcomp_methods) {
            $data = array(
                'r_club_id' => $params['clubId'],
                'r_bcomp_id' => $bcomp_methods,
            );
            if (!$rsInsBcomp->RecordCount()) {
                //Dynamically construct insert query         
                $insert_sql = $this->dbcon->GetInsertSQL($rsInsBcomp, $data);
                $this->dbcon->Execute($insert_sql);
                $bCompId = $this->dbcon->Insert_ID();
                if (!$bCompId) {
                    $revertFlag = 1;
                }
            }
        }

        if ($revertFlag == 0) {
            $this->dbcon->CommitTrans(); 
            /* When a successful transaction is completed, the COMMIT 
            command should be issued so that the changes to all 
            involved tables will take effect. */
            $this->status = array(
                'status' => 'success',
                'status_code' => '200',
                'status_message' => 'Member test to reanalyze success',
                'newTestId' => $bCompId,
            );
        } else {
            $this->dbcon->RollbackTrans(); 
            /* If a failure occurs, a ROLLBACK command should be issued to 
            return every table referenced in the transaction to its previous state. */
            $this->status = $this->error_general;
        }
        //Return the result array
        return $this->status;
    }
}

//End Class.
;
