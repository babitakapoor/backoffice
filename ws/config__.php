<?php
/**
 * PHP version 5.
 
 * @category General
 
 * @package Config
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description environment level variables.
 */
/** 
 * Database configurations.
 */
ini_set('display_errors', '0');
define('DS',    '/');
define('DIR_DB_CLASS', 'adodb'.DS);

//Default test taking maximum time in seconds
// 2100 = 35 minutes
define('DEFAULT_MAX_TEST_TIME', 2100);

/* To Enable/Disable database service call log */
define('WEBSERVICE_LOG_TO_DB', 0);

// Settings based on server: 
// This is for Local machines.

if (strstr($_SERVER['HTTP_HOST'], 'localhost')) {
    define('DB_SERVER_NAME',    'localhost');
    define('DB_USER_NAME',    'root');
    define('DB_PASSWORD',        '');
    define('DB_NAME',            'movesmart_new');
    define('DB_TYPE',            'mysqlt');
    define('WEBSERVICE_VERSION', '1.0');
    define('OGONE_PREFIX', 'FL_');
}

if (strstr($_SERVER['HTTP_HOST'], '172.22.2.59')) {
    define('DB_SERVER_NAME',    '172.22.2.59');
    define('DB_USER_NAME',    '172.22.2.59');
    define('DB_PASSWORD',        '');
    define('DB_NAME',            'movesmart');
    define('DB_TYPE',            'mysqlt');
    define('WEBSERVICE_VERSION', '1.0');
    define('OGONE_PREFIX', 'FL_');
}

if (strstr($_SERVER['HTTP_HOST'], 'movesmart.insoft.com')) {

        //Db tmp fix
        define('DB_SERVER_NAME',    '182.72.70.79');
    define('DB_USER_NAME',    'root');
    define('DB_PASSWORD',        'Ins0ft1nd1a');
    define('DB_NAME',            'movesmartapp');
    define('DB_TYPE',            'mysqlt');
    define('WEBSERVICE_VERSION', '1.0');
    define('OGONE_PREFIX', 'FL_');
        //Db tmp fix

        /*define('DB_SERVER_NAME', 	'localhost');
        define('DB_USER_NAME',	'root');
        define('DB_PASSWORD',		'Ins0ft1nd1a');
        define('DB_NAME',			'movesmartApp');
        define('DB_TYPE',			'mysqlt');
        define('WEBSERVICE_VERSION','1.0'); 
        define("OGONE_PREFIX", "FL_");*/
}

if (strstr($_SERVER['HTTP_HOST'], 'movesmart.insoft.in')) {

        //Db tmp fix
        define('DB_SERVER_NAME',    'localhost');
    define('DB_USER_NAME',    'movesmart_user');
    define('DB_PASSWORD',        'Ins0ft1nd1a');
    define('DB_NAME',            'movesmart');
    define('DB_TYPE',            'mysqlt');
    define('WEBSERVICE_VERSION', '1.0');
    define('OGONE_PREFIX', 'FL_');
        //Db tmp fix

        /*define('DB_SERVER_NAME', 	'localhost');
        define('DB_USER_NAME',	'root');
        define('DB_PASSWORD',		'Ins0ft1nd1a');
        define('DB_NAME',			'movesmartApp');
        define('DB_TYPE',			'mysqlt');
        define('WEBSERVICE_VERSION','1.0'); 
        define("OGONE_PREFIX", "FL_");*/
}

if (strstr($_SERVER['HTTP_HOST'], 'movesmart.bluerail.nl')) {

        //Db tmp fix
        define('DB_SERVER_NAME',    'localhost');
    define('DB_USER_NAME',    'root');
    define('DB_PASSWORD',        '');
    define('DB_NAME',            'movesmart');
    define('DB_TYPE',            'mysqlt');
    define('WEBSERVICE_VERSION', '1.0');
    define('OGONE_PREFIX', 'FL_');
        //Db tmp fix

        /*define('DB_SERVER_NAME', 	'localhost');
        define('DB_USER_NAME',	'root');
        define('DB_PASSWORD',		'Ins0ft1nd1a');
        define('DB_NAME',			'movesmartApp');
        define('DB_TYPE',			'mysqlt');
        define('WEBSERVICE_VERSION','1.0'); 
        define("OGONE_PREFIX", "FL_");*/
}

/* 
 * General configurations
 */
define('DEBUG', 0);

define('DEVELOPMENTSERVER',   true);

define('HTTP_SERVER',        'http://'.$_SERVER['SERVER_NAME'].DS);

define('HTTPS_SERVER',       'https://'.$_SERVER['SERVER_NAME'].DS);

define('BASE_PATH',          realpath(__DIR__.DS));

define('SQL_PATH',             realpath(__DIR__.DS.'sql'));

define('MODULE_PATH',         realpath(__DIR__.DS.'modules'));

define('LIBRARY_PATH',         realpath(__DIR__.DS.'library'));

define('LIMIT_DEVICE_LIST',    20);

define('CCA_UPDATE_TIME_INTERVAL',    5);
