<?php
/**
 * PHP version 5.
 
 * @category SQL
 
 * @package Strength
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Sql Queries to handle strength related DB access.
 */
define(
    'GET_STRENGTH_USER_TEST_EXIST', 'SELECT * FROM `t_strength_user_test` 
    WHERE `strength_user_test_id`=?'
);

define(
    'GET_STRENGTH_USER_TEST_ACTIVITY_EXIST', 'SELECT * 
    FROM `t_strength_user_test_activity`
    WHERE `r_strength_user_test_id`=? AND `r_strength_machine_id`=? AND `is_deleted`=0'
);

define(
    'GET_STRENGTH_USER_TEST_ACTIVITY_PRGM_EXIST', 'SELECT *
    FROM `t_strength_user_test_activity`
    WHERE `r_strength_programid`=? AND 
    `r_strength_machine_id`=? AND `is_deleted`=0'
);

define(
    'DELETE_STRENGTH_TEST_ACTIVITY', 'DELETE 
    FROM `t_strength_user_test_activity`
    WHERE `strength_user_test_activity_id`=? '
);

define(
    'GET_STRENGTH_USER_TEST_LATEST_ID', 'SELECT * 
    FROM `t_strength_user_test` 
    WHERE `r_user_id`=? ORDER BY `strength_user_test_id` DESC LIMIT 1'
);

define(
    'GET_STRENGTH_USER_TEST_ACTIVITIES', 'SELECT 
        ST.*, SM.machine_name FROM `t_strength_user_test_activity` ST
        INNER JOIN `t_strength_machine` SM 
            ON SM.strength_machine_id=ST.r_strength_machine_id
        WHERE ST.`r_strength_user_test_id`=? AND ST.`is_deleted`=0 
            ORDER BY SM.machine_name ASC'
);

define(
    'GET_STRENGTH_USER_TEST_ACTIVITY_BY_ID', 'SELECT * 
    FROM `t_strength_user_test_activity`
    WHERE `strength_user_test_activity_id`=?'
);
/*define('GET_STRENGTH_TEST_LIST_BY_USER', "SELECT SUT.*,
    CONCAT(U.first_name,' ',U.last_name) coach_name,
    GROUP_CONCAT(r_strength_machine_id,'###',strength) test_activities 
    FROM `t_strength_user_test` SUT INNER JOIN `t_strength_user_test_activity` 
    TA ON TA.`r_strength_user_test_id`=SUT.`strength_user_test_id` AND 
    TA.`is_deleted`=0 
    INNER JOIN `t_users` U ON U.`user_id`=SUT.`coach_id`
    WHERE SUT.`r_user_id`=? GROUP BY SUT.`strength_user_test_id`"
);*/

define(
    'GET_STRENGTH_TEST_LIST_BY_USER', "SELECT SUT.*,sp.description as testid,
    CONCAT(U.first_name,' ',U.last_name) coach_name,
    GROUP_CONCAT(r_strength_machine_id,'###',strength) test_activities 
    FROM `t_strength_user_test` SUT 
    LEFT OUTER JOIN `t_strength_user_test_activity` TA 
        ON TA.`r_strength_user_test_id`=SUT.`strength_user_test_id` 
        AND TA.`is_deleted`=0 
    LEFT OUTER JOIN `t_users` U ON U.`user_id`=SUT.`coach_id`
    LEFT OUTER JOIN t_strength_program sp
        ON sp.strength_program_id = SUT.r_strength_program_id
    WHERE SUT.`r_user_id`=? GROUP BY SUT.`strength_user_test_id`"
);

define(
    'GET_STRENGTH_USER_TEST_TRAINING_WEEK_EXIST', 'SELECT * 
    FROM `t_strength_user_test_week` 
    WHERE `r_strength_user_test_id`=? AND `r_user_id`=? AND `week`=?'
);
/* SK added*/
/*define(
    'GET_POINTS_ACHIEVE_EXIST', 
    "SELECT * FROM `t_points_achieve` WHERE `r_user_id`=? 
    AND `r_user_test_id`=? AND `week`=?"
);*/
define(
    'GET_TEST_BY_WEEK', 'SELECT * FROM t_strength_user_test_week wk 
    WHERE wk.r_strength_user_test_id = ? AND wk.r_user_id= ? AND wk.week = ?'
);

define(
    'GET_STRENGTH_USER_LIST', 'SELECT strusrtest.coach_id,usr.user_id,
    usr.first_name,personalinfo.userimage 
    FROM t_strength_user_test strusrtest 
    LEFT OUTER JOIN t_users usr ON usr.user_id=strusrtest.r_user_id
    LEFT OUTER JOIN t_personalinfo personalinfo 
        ON personalinfo.r_user_id=usr.user_id
    WHERE usr.is_deleted<>1 AND strusrtest.coach_id=? 
    GROUP BY strusrtest.r_user_id'
);
