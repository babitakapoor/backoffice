<?php
/**
 * PHP version 5.
 
 * @category SQL
 
 * @package Test
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Sql Queries to handle test related DB access.
 */
define(
    'GET_EVOLUTIONLIST_QUERY',
    "SELECT pi.dob, 
    if(u.gender=0,'male','female') as gender, 
    s.status as status_test, date(ut.test_date) as test_date,
    ut.fitness_level,ut.age_on_test,t.test_name,ut.test_prepared_date,
    ut.weight_on_test,ut.start_load_test,cm.r_coach_id
         FROM t_users u 
         LEFT OUTER JOIN t_personalinfo pi ON pi.r_user_id=u.user_id 
         LEFT OUTER JOIN t_user_test_parameter ut ON ut.r_user_id=u.user_id
         LEFT OUTER JOIN t_coach_member cm ON cm.r_user_id=u.user_id 
         LEFT OUTER JOIN t_test t ON t.test_id=ut.r_user_test_id
         LEFT OUTER JOIN t_status s ON s.status_id=u.r_status_id
         WHERE u.r_usertype_id=3 and u.user_id=? and u.is_deleted<>1"
);

define(
    'GET_TEST_SPORT_SPECIFIC_DATA', 'SELECT * FROM `t_test_result`
    WHERE `r_user_id`=? AND `r_user_test_id`=?'
);

define(
    'GET_TEST_RESULT_DATA_EXIST', 'SELECT * FROM `t_test_result`
    WHERE `r_user_id`=? AND `r_user_test_id`=?'
);

define(
    'GET_TEST_USER_DATA_EXIST', 'SELECT * FROM `t_users` 
    WHERE `user_id`=? AND `r_status_id`=4'
);

define(
    'GET_TEST_RESULT_EXIST_BY_TEST_ID', 'SELECT * FROM `t_test_result` 
    WHERE `r_user_test_id`=?'
);

define(
    'GET_TEST_ITEMS_BY_ID', 'SELECT * FROM `t_test_items` 
    WHERE `r_test_id`=?'
);

define(
    'GET_USER_TEST_DATA_EXIST', 'SELECT * FROM `t_user_test`
    WHERE `r_user_id`=? AND `user_test_id`=?'
);

define(
    'GET_USER_REANALYZE_TEST_DATA_EXIST', 'SELECT * FROM `t_user_test` 
    WHERE `r_user_id`=? AND `user_test_id`=? AND `status`=3'
);

//Get the data for the test result form from the  t_member_test_result 
define(
    'GET_TEST_RESULT_INFO', "SELECT *, 
        IF(U.gender=0,'male','female') AS gender,s.status,
        UT.test_level AS user_test_level   
        FROM t_users AS U 
        LEFT JOIN t_user_test UT 
            ON UT.r_user_id = U.user_id AND UT.status IN(?) 
            AND UT.user_test_id = (SELECT MAX(user_test_id) FROM t_user_test 
            WHERE r_user_id=U.user_id AND `status` IN (?)) 
        LEFT JOIN t_user_test_parameter AS t 
            ON t.r_user_test_id = UT.user_test_id
        LEFT JOIN t_status AS s ON s.status_id = U.r_status_id 
        LEFT JOIN t_personalinfo AS p ON p.r_user_id = U.user_id
        LEFT JOIN t_coach_member AS cm ON cm.r_user_id = U.user_id 
        LEFT JOIN t_test AS test ON test.test_id = UT.r_test_id 
        WHERE U.user_id= ?"
);

define(
    'GET_TEST_RESULT_BY_TEST_ID', "SELECT *, 
    IF(U.gender=0,'male','female') AS gender,s.status,
    UT.test_level AS user_test_level FROM t_users AS U 
    INNER JOIN t_user_test UT ON UT.r_user_id = U.user_id 
    LEFT JOIN t_user_test_parameter AS t ON t.r_user_test_id = UT.user_test_id 
    LEFT JOIN t_status AS s ON s.status_id = U.r_status_id 
    LEFT JOIN t_personalinfo AS p ON p.r_user_id = U.user_id
    LEFT JOIN t_coach_member AS cm ON cm.r_user_id = U.user_id
    LEFT JOIN t_test AS test ON test.test_id = UT.r_test_id 
    LEFT JOIN t_test_result TSR ON TSR.r_user_test_id=UT.user_test_id
    WHERE U.user_id= ? AND UT.user_test_id=? "
);

define(
    'GET_EVOLUTIONFITNESSLIST_QUERY',
    "SELECT testres.sportmed_graph,test.test_name,
    testres.adjust_sportmed_graph,utp.fitness_level,utp.r_user_id,
    date(utp.test_date) as test_date,utp.r_user_test_id,utp.max_hr,
    utp.iant_hr,utp.max_p,utp.iant_p,utp.fitness_level,
    CONCAT(u.first_name,' ',u.last_name) as usersname,p.dob,
    IF(u.gender=0,'male','female') AS gender, r_coach_id as coach_id,
    s.status as status
    FROM t_user_test_parameter utp
    INNER JOIN `t_user_test` UT 
        ON UT.user_test_id = utp.r_user_test_id AND UT.`status`='3'						
    LEFT JOIN t_users u ON u.user_id = utp.r_user_id
    LEFT JOIN t_status AS s ON s.status_id = u.r_status_id
    LEFT JOIN t_personalinfo AS p ON p.r_user_id = u.user_id
    LEFT JOIN t_coach_member AS cm ON cm.r_user_id = u.user_id
    LEFT JOIN t_test AS test ON test.test_id = UT.r_test_id
    LEFT JOIN t_test_result AS testres 
        ON testres.r_user_id = utp.r_user_id AND
        testres.r_user_test_id=utp.r_user_test_id
        WHERE u.r_usertype_id=3 AND utp.r_user_id=? and u.is_deleted<>1
        ORDER BY UT.user_test_id DESC"
);

define(
    'GET_EVOLUTIONBIOMETRICS_QUERY',
    'SELECT te.test_evolution_id,date(ut.test_start_date) as test_start_date,
        te.fitness_level,te.weight,te.belly_girth,te.fat_percentage,
        te.fat_kg,te.water,te.bmi,te.method,te.biceps,te.triceps,
        te.abdomen FROM t_test_evolution te 
        LEFT OUTER JOIN t_user_test ut ON ut.user_test_id=te.r_user_test_id 
        LEFT OUTER JOIN t_users u ON u.user_id=ut.r_user_id
        WHERE u.r_usertype_id=3 AND u.user_id=? AND u.is_deleted<>1'
);

define(
    'GET_USERLISTWITHCLUB_DROPDOWN', 'SELECT first_name,last_name,user_id
                                    FROM t_users
                                    WHERE r_usertype_id=3 and is_deleted=0 '
);

define(
    'GET_EMAILLISTWITHUSER_DROPDOWN', 'SELECT email as textValue,
        user_id as resultValue
        FROM t_users
        WHERE user_id=?'
);

define(
    'GET_HEARTRATEUSERVALUE', 'SELECT ut.r_user_id,ut.user_test_id,p.dob,
        u.gender,cm.r_coach_id,s.status,IF(u.gender=0,"male","female") AS
        gender,date(utp.test_date) as test_date,utp.max_hr,utp.iant_hr,
        ut.heart_rate_value,ut.load_value,
        ut.cool_down_start_time,ut.cool_down_stop_time
        FROM t_user_test ut 
        LEFT OUTER JOIN t_users u ON u.user_id=ut.r_user_id
        LEFT OUTER JOIN t_personalinfo AS p ON p.r_user_id = ut.r_user_id
        LEFT OUTER JOIN t_coach_member AS cm ON cm.r_user_id = ut.r_user_id
        LEFT JOIN t_user_test_parameter AS utp 
            ON utp.r_user_test_id = ut.user_test_id 
        LEFT JOIN t_status AS s ON s.status_id = u.r_status_id
        WHERE u.r_usertype_id=3 AND ut.r_user_id=? AND ut.status=3 
            ORDER BY ut.user_test_id DESC LIMIT 1'
);

define(
    'GET_HEARTRATEUSERVALUE_GRAPH', 'SELECT ut.r_user_id,ut.user_test_id,p.dob,
        u.gender,cm.r_coach_id,s.status,IF(u.gender=0,"male","female") AS
        gender,date(utp.test_date) as test_date,utp.max_hr,utp.iant_hr,
        ut.heart_rate_value,ut.load_value,
        ut.cool_down_start_time,ut.cool_down_stop_time,ut.current_test_time
            FROM t_user_test ut 
            LEFT OUTER JOIN t_users u ON u.user_id=ut.r_user_id
            LEFT OUTER JOIN t_personalinfo AS p ON p.r_user_id = ut.r_user_id
            LEFT OUTER JOIN t_coach_member AS cm ON cm.r_user_id = ut.r_user_id
            LEFT JOIN t_user_test_parameter AS utp
                ON utp.r_user_test_id = ut.user_test_id 
            LEFT JOIN t_status AS s ON s.status_id = u.r_status_id
            WHERE u.r_usertype_id=3 AND ut.r_user_id=? AND ut.status=3
                AND ut.user_test_id=? LIMIT 1'
);

define('GET_TEST_QUERY', 'SELECT * FROM t_test');

define(
    'GET_USER_TEST_RESULT', 'SELECT * FROM t_user_test 
    WHERE user_test_id=? AND r_user_id=? AND status!=3'
);

define(
    'GET_USER_TEST_PARAMETER', 'SELECT * FROM t_user_test_parameter
    WHERE r_user_id = ? AND r_user_test_id=?'
);

define(
    'GET_MEMBER_TEST_PARAMETER', 'SELECT * FROM `t_user_test_parameter` 
    WHERE `r_user_test_id`=?'
);

define(
    'GET_TRAINING_WORKLOAD_DETAILS', "SELECT us.first_name,p.dob,us.gender,s.status,
    tp.weight_on_test,tp.fitness_level,tp.target,tp.intensity, ha.*,
    (SELECT min(points_collected_date) FROM t_training_points_transaction 
        WHERE r_user_test_id= ?) AS start_training_date ,
    (SELECT max(points_collected_date) FROM t_training_points_transaction 
        WHERE r_user_test_id= ?) AS last_training_date,
    (SELECT concat(first_name,' ',last_name) from t_coach_member AS tcm 
    LEFT JOIN t_users AS tu ON tu.user_id=tcm.r_coach_id 
        where tcm.r_user_id=ha.r_user_id AND ha.r_user_test_id = ? ) 
        AS coach_name,tt.test_name FROM `t_points_achieve` ha
    LEFT JOIN `t_user_test_parameter` tp ON tp.r_user_test_id=ha.r_user_test_id 
    LEFT JOIN `t_users` us ON us.user_id=ha.r_user_id 
    LEFT JOIN `t_user_test` ut ON ut.user_test_id=ha.r_user_test_id 
    LEFT JOIN `t_test` tt ON tt.test_id=ut.r_test_id 
    LEFT JOIN `t_personalinfo` p ON p.r_user_id=ha.r_user_id
    LEFT JOIN `t_status` s ON s.status_id=us.r_status_id WHERE 
    (CURDATE() BETWEEN ha.week_start_date AND ha.week_end_date) 
    AND ha.r_user_test_id= ? GROUP BY ha.r_user_test_id"
);

define(
    'GET_TRAINING_WORKLOAD', "SELECT *,
    date_format(workout_date,'%d-%m-%Y') as workout_date,
    date_format(modified_date,'%d-%m-%Y') as modified_date,
    (SELECT concat(first_name,' ',last_name) 
        FROM t_users AS u WHERE u.user_id=tw.modified_by) AS coach_name 
    FROM t_training_workload AS tw
    WHERE r_user_id=? AND `r_user_test_id`=? 
        ORDER BY workout_date ASC"
);

define(
    'GET_WORKLOAD', 'SELECT * FROM t_training_workload 
    WHERE training_workload_id=?'
);

define(
    'GET_TEST_SPORTMED_GRAPH_DATA', 'SELECT * FROM `t_test_result`
    WHERE `r_user_id`=? AND `r_user_test_id`=?'
);

define(
    'UPDATEUSERTEST_INTERRUPT', 'SELECT * FROM t_user_test 
    WHERE user_test_id=?'
);

define(
    'INSERTUSERTEST_INTERRUPT', 'INSERT INTO t_user_test (r_user_id,
        r_test_id,r_equipment_id,r_device_id,r_club_id,test_start_date,
        test_end_date,test_level,weight,status,load_value,kcal) 
        SELECT r_user_id,r_test_id,r_equipment_id,r_device_id,
        r_club_id,test_start_date,test_end_date,test_level,weight,?,
        load_value,? FROM t_user_test WHERE user_test_id=?'
);

//To get param values in edit page and avoid passing multi parameters
define(
    'GET_TEST_MEMBER_LIST_ALONE', "SELECT U.*,
        date_format(UT.test_start_date,'%d-%m-%Y') as test_start_date,
        C.club_name, COUNT(user_test_id) as total_test,
        MAX(user_test_id) as last_test_id,
        if(U.gender=0,'male','female') as gender,
        if(U.email='',U.username, U.email) as emailusername 
        FROM t_user_test UT 
       INNER JOIN t_users U ON U.user_id=UT.r_user_id
       INNER JOIN t_clubs C ON C.club_id=U.r_club_id
       WHERE UT.status=3 AND U.user_id=? "
);

define(
    'GET_TEST_MEMBER_LIST', "SELECT SQL_CALC_FOUND_ROWS U.*,
        date_format(UT.test_start_date,'%d-%m-%Y') as test_start_date,
        C.club_name, COUNT(user_test_id) as total_test, 
        MAX(user_test_id) as `last_test_id`,
        if(U.gender=0,'male','female') as gender,
        if(U.email='',U.username, U.email) as emailusername 
        FROM `t_user_test` UT 
        INNER JOIN `t_users` U ON U.`user_id`=UT.`r_user_id`
        INNER JOIN `t_clubs` C ON C.`club_id`=U.`r_club_id`
        WHERE UT.`status`=3 AND U.`r_company_id`=? AND U.`is_deleted`=0 "
);

define(
    'GET_TEST_DETAIL_NAV_LINKS', 'SELECT user_test_id FROM t_user_test UT 
    WHERE UT.status=3 AND UT.r_user_id=?'
);

define(
    'GET_MEMBER_IDS_FOR_TEST', 'SELECT `r_user_id` FROM `t_user_test`'
);

define(
    'GET_ACTIVITY', 'SELECT * FROM t_activity WHERE activity_id=?'
);

define(
    'GET_TRAINING_POINTS_INFO', 'SELECT ha.week,ha.target_points,
    SUM(ptr.achieved_point) as achieved_point FROM `t_points_achieve` ha 
    LEFT JOIN t_training_points_transaction ptr 
        ON ptr.r_user_test_id = ha.r_user_test_id 
        AND ptr.week = ha.week AND ptr.is_deleted=0 
    WHERE ha.r_user_id=? AND ha.r_user_test_id=? GROUP BY ha.week'
);

define(
    'GET_TRAINING_POINTS_ACHIEVE',
    'SELECT min(week_start_date) AS week_start_date,
        max(week_end_date) AS week_end_date FROM t_points_achieve 
    WHERE r_user_test_id=?'
);

define(
    'GET_TRAINING_POINTS_ACHIEVE_WEEK', 'SELECT * FROM t_points_achieve 
    WHERE (? BETWEEN week_start_date AND week_end_date) AND r_user_test_id=?'
);

define(
    'GET_TRAINING_PARAMETERS', 'SELECT * FROM t_user_test_parameter 
    WHERE r_user_test_id=?'
);

define(
    'GET_HEART_RATE_ZONE', 'SELECT * FROM t_heartrate_zone_activity 
        INNER JOIN `t_activity` 
            ON t_activity.activity_id=t_heartrate_zone_activity.activity_id
        WHERE r_user_id=? AND r_user_test_id=? AND `activity_used`=1 '
);

define(
    'GET_POINTS_TRANSACTION', 'SELECT * FROM t_training_points_transaction 
      WHERE `training_points_transaction_id`=? AND r_user_id=? AND r_user_test_id=?'
);

define(
    'GET_HEART_RATE_ZONE_WITH_ACTIVITY', 'SELECT hza.activity_id,
        a.activity_name,hza.hr_zone_a,hza.points_rate,
        a.r_activity_group_id,a.activity_image 
        FROM t_heartrate_zone_activity AS hza 
        LEFT JOIN t_activity AS a ON a.activity_id = hza.activity_id 
        WHERE hza.r_user_id=? AND hza.r_user_test_id=? AND hza.`activity_used`=1 '
);

define(
    'GETUPDATE_ACHIEVEDPOINT', 'SELECT * FROM t_training_points_transaction 
    WHERE `training_points_transaction_id`=? AND r_user_id=? AND r_user_test_id=?'
);

define(
    'GETACHIEVEDPOINTBY_MEMBERWEEKLIST', "SELECT PA.*,
        IFNULL(SUM(TP.achieved_point), 0) as  achieved_point,
        date_format(PA.week_start_date,'%d-%m-%Y') as week_start_date, 
        date_format(PA.week_end_date,'%d-%m-%Y') as week_end_date 
        FROM t_points_achieve PA
        LEFT JOIN `t_training_points_transaction` TP 
            ON TP.r_user_test_id=PA.r_user_test_id AND TP.week=PA.week 
            AND TP.is_deleted=0 
            WHERE PA.r_user_id=? AND PA.r_user_test_id=? 
        GROUP BY PA.`week` ORDER BY PA.week"
);

define(
    'GETACHIEVEDPOINTBY_MEMBERWEEKLIST_GRID', 'SELECT SQL_CALC_FOUND_ROWS PA.*,
      IFNULL(SUM(TP.achieved_point), 0) as achieved_point 
      FROM t_points_achieve PA
        LEFT JOIN `t_training_points_transaction` TP 
          ON TP.r_user_test_id=PA.r_user_test_id AND TP.week=PA.week AND TP.is_deleted=0 
      WHERE PA.r_user_id=? AND PA.r_user_test_id=?'
);

define(
    'GET_ACHIEVED_POINTS_TOTAL', 'SELECT 
    sum(achieved_point) as achieved_point 
    FROM t_points_achieve WHERE r_user_id=? AND r_user_test_id=?'
);

define(
    'GET_POINTS_TRANSATION_ID', 'SELECT tp.*,
        hra.points_rate,ta.r_activity_group_id 
        FROM t_training_points_transaction tp 
        INNER JOIN `t_heartrate_zone_activity` hra 
            ON hra.r_user_test_id=tp.r_user_test_id 
            AND hra.activity_id=tp.activity_id
        INNER JOIN `t_activity` ta ON ta.activity_id=tp.activity_id 									
        WHERE training_points_transaction_id=?'
);

define(
    'GET_MEMBER_TEST_LASTID_SET', "SELECT user_test_id 
    FROM t_user_test 
    WHERE status='3' AND r_user_id=?
    ORDER BY user_test_id ASC"
);

define(
    'CANCEL_MEMBER_TEST_EXIST', 'SELECT * FROM `t_user_test`
    WHERE `r_user_id`=? AND `user_test_id`=? AND status=0'
);

define(
    'GET_COLLECTEDPOINT_DEVICE_WEEKS', 'SELECT TP.week,TP.activity_id,
    TP.activity_name,sum(TP.achieved_point) as achieved_point,
    TD.training_device_name
    FROM t_training_points_transaction TP
    LEFT JOIN t_activity A ON A.activity_id = TP.activity_id
    LEFT JOIN t_training_device TD 
        ON TD.training_device_id = A.r_training_device_id
    WHERE TP.r_user_id=? AND TP.r_user_test_id=? AND week=? 
        AND TP.`is_deleted`=0 GROUP BY TP.activity_id'
);

define(
    'GET_COLLECTEDPOINT_DEVICE_ALL', 'SELECT TP.week,TP.activity_id,
    TP.activity_name,sum(TP.achieved_point) as achieved_point,
    TD.training_device_name
    FROM t_training_points_transaction TP
    INNER JOIN t_activity A ON A.activity_id = TP.activity_id
    LEFT JOIN t_training_device TD 
        ON TD.training_device_id = A.r_training_device_id
    WHERE TP.r_user_id=? AND TP.r_user_test_id=? AND TP.`is_deleted`=0 
    GROUP BY TP.activity_id'
);

define(
    'GET_MANAGE_POINTS_LIST', "SELECT SQL_CALC_FOUND_ROWS U.*,
        date_format(UT.test_start_date,'%d-%m-%Y') as test_start_date,
        C.club_name,P.userimage, COUNT(user_test_id) as total_test,
        MAX(user_test_id) as `last_test_id`,
        if(U.gender=0,'male','female') as gender,
        if(U.email='',U.username, U.email) as emailusername,(
        SELECT week FROM t_points_achieve AS A WHERE 
            (CURDATE() BETWEEN A.week_start_date AND A.week_end_date ) 
            AND A.r_user_id=U.user_id ORDER BY r_user_test_id DESC LIMIT 1) 
            AS week,
        (SELECT target_points FROM t_points_achieve AS A WHERE 
            (CURDATE() BETWEEN A.week_start_date AND A.week_end_date) 
            AND A.r_user_id=U.user_id ORDER BY r_user_test_id DESC LIMIT 1)
            AS target_points,
        (SELECT FORMAT(SUM(PT.achieved_point),1) FROM t_points_achieve AS A 
        INNER JOIN `t_training_points_transaction` PT 
            ON PT.r_user_test_id=A.r_user_test_id 
            AND PT.`is_deleted`=0 AND PT.week=A.week
        INNER JOIN `t_user_test` UTS ON UTS.user_test_id=A.r_user_test_id
            AND UTS.`status`=3
        WHERE (CURDATE() BETWEEN A.week_start_date AND A.week_end_date) 
            AND A.r_user_id=U.user_id) AS achieved_point
        FROM `t_user_test` UT 
        INNER JOIN `t_users` U ON U.`user_id`=UT.`r_user_id`
        LEFT JOIN `t_personalinfo` P ON P.`r_user_id`=U.`user_id`
        INNER JOIN `t_clubs` C ON C.`club_id`=U.`r_club_id`	
        WHERE U.`r_status_id`=4 AND UT.status=3 AND U.`r_company_id`=?"
);


define(
    'GET_ALL_TEST_DATES_MEMBER', "SELECT    
    date_format(min(P.week_start_date),'%d-%m-%Y') AS week_start_date,
    date_format(max(P.week_end_date),'%d-%m-%Y') AS week_end_date,
    P.r_user_test_id FROM `t_points_achieve` P
    INNER JOIN `t_user_test` UT ON UT.user_test_id=P.r_user_test_id 
        AND UT.status=3 
    WHERE P.r_user_id=? GROUP BY P.`r_user_test_id` 
        ORDER BY P.r_user_test_id DESC"
);

/*Query Added By Yusuff To get The particular coach information*/
define(
    'GET_COACHINFO_QUERY',    "SELECT u.user_id as coach_id,u.email, 
        CONCAT(u.first_name,' ',u.last_name) as coach_name
        FROM t_users u 
        where u.r_usertype_id=1 AND u.user_id=? and u.is_deleted<>1"
);
/*Query End here */;
