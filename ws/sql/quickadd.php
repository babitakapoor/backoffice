<?php
/**
 * PHP version 5.
 
 * @category SQL
 
 * @package Quick_Popup
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Sql Queries to handle quick popup related DB access.
 */
// To add or Edit the city.
define('CREATE_CITY', 'SELECT * FROM t_city WHERE city_name = ?');
define('CREATE_COMPANY', 'SELECT * FROM t_company WHERE company_name = ?');
define(
    'CREATE_PROFESSION', 'SELECT * FROM t_profession 
    WHERE profession_name = ?'
);
