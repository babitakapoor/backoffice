<?php
/**
 * PHP version 5.
 
 * @category SQL
 
 * @package Admin
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Sql Queries to handle admin related DB access.
 */
define(
    'GET_PLAN_DETAIL_BY_PLAN_ID', 'SELECT * FROM `t_plan` 
    WHERE `plan_id` = ?'
);
/*
define(
    'GET_PLAN_DETAIL_BY_EXTERNAL_PLAN_ID', 'SELECT * FROM `t_plan` 
            WHERE `external_plan_id` = ?'
);*/

define(
    'GET_PLAN_CLUB_DETAIL_BY_PLAN_ID', 'SELECT * FROM t_plan_clubs 
    WHERE is_deleted=0 AND r_plan_id=? AND r_club_id=?'
);

define(
    'GET_PLAN_LIST_BY_COMPANY', 'SELECT SQL_CALC_FOUND_ROWS * FROM `t_plan` 
        WHERE `r_company_id` = ? AND `is_deleted` = 0'
);

define(
    'UPDATE_PLAN_ID_SOFT_DELETE', 'UPDATE `t_plan` SET `is_deleted`=1 
        WHERE plan_id=?'
);

define(
    'GET_PLAN_CLUB_DETAIL_DROPDOWN', 'SELECT c.club_id,c.club_name 
        FROM t_plan_clubs AS pc
        LEFT JOIN t_clubs AS c ON c.club_id = pc.r_club_id	
        WHERE  pc.is_deleted=0 AND pc.r_company_id= ? AND r_plan_id=?'
);
/* SK- changes For New Strength Program Created */
define(
    'GET_STRENGTH_MACHINE_LIST', 'SELECT SQL_CALC_FOUND_ROWS 
        sm.*,@s:=@s+1 as sno,sm.machine_name as name,
        sm.strength_machine_id as mid,sm.`type` as `typedata` 
        FROM t_strength_machine sm,(SELECT @s:= 0) AS s 
        WHERE sm.clubid =? AND sm.is_deleted = 0 ORDER BY sm.machine_name ASC'
);
define(
    'GET_STRENGTH_MACHINE_LIST_ID', 'SELECT SQL_CALC_FOUND_ROWS 
        @s:=@s+1 as sno,sm.strength_machine_id as mid,
        sm.machine_name as name,sc.r_strength_user_test_id as programid
        FROM t_strength_user_test_activity sc 
        LEFT OUTER JOIN t_strength_machine sm 
        ON sc.r_strength_machine_id = sm.strength_machine_id
        LEFT OUTER JOIN t_strength_user_test sut 
        ON sut.strength_user_test_id = sc.r_strength_user_test_id
        ,(SELECT @s:= 0) AS s 
        WHERE sut.r_strength_program_id = ? AND sut.clubid = ?'
);
define(
    'GET_ACTIVITIES_LIST', 'SELECT * FROM t_activity 
                ORDER BY activity_name ASC'
);

define(
    'GET_ACTIVITIES_LIST_CLUB', 'SELECT * FROM t_activity a  
          INNER JOIN `t_activity_club` ca 
          ON ca.r_club_id=? AND a.activity_id=ca.r_activity_id
          ORDER BY a.activity_name ASC'
);

define(
    'GET_STRENGTH_MACHINE_CLUB_LIST', 'SELECT * FROM t_strength_machine_club mc
        LEFT JOIN t_strength_machine AS m 
        ON m.strength_machine_id = mc.r_strength_machine_id
        WHERE mc.is_deleted=0 AND mc.r_club_id=? 
        ORDER BY m.machine_name ASC'
);

define(
    'GET_ACTIVITIES_CLUB_LIST', 'SELECT * FROM t_activity_club ac
        LEFT JOIN t_activity AS a 
        ON a.activity_id = ac.r_activity_id
        WHERE ac.is_deleted=0 AND ac.r_club_id=? 
        ORDER BY a.activity_name ASC'
);

define(
    'GET_STRENGTH_MACHINE_MAP', 'SELECT * FROM t_strength_machine_club 
        WHERE is_deleted=0 AND r_club_id = ? AND r_strength_machine_id=?'
);
define(
    'GET_ACTIVITIES_MAP', 'SELECT * FROM t_activity_club 
        WHERE is_deleted=0 AND r_club_id = ? AND r_activity_id=?'
);

/* 05052015 Below Queries are Added By JahabarYusuff.M on 05-may-2015 , 
For  body Composition Methods tab  05052015*/
define(
    'GET_BCOMP_METHODS', 'SELECT bcomp_id,method_name FROM t_bcomp_method 
            WHERE is_deleted =? ORDER BY method_name ASC'
);
define(
    'GET_ACTIVE_BCOMP_METHODS', 'SELECT  activeMethod.method_club_bcomp_id, 
        activeMethod.r_bcomp_id,bcompMethod.method_name 
    FROM `t_bcomp_method_club` as activeMethod 
    LEFT JOIN t_bcomp_method as bcompMethod 
    ON bcompMethod.bcomp_id = activeMethod.r_bcomp_id
    WHERE bcompMethod.is_deleted=0 AND  activeMethod.r_club_id=? 
    ORDER BY bcompMethod.method_name ASC'
);
define(
    'REMOVE_AVAILABLE_BCOMP', 'DELETE FROM t_bcomp_method_club 
            WHERE t_bcomp_method_club.r_club_id = ?'
);
define(
    'GET_AVAILABLE_BCOMP', 'SELECT * FROM t_bcomp_method_club 
            WHERE  r_club_id = ? '
);
/* SK-Added to get list of programs by its program id and user id and clubid */
define(
    'GET_MACHINE_LIST_PROG_ID', 'SELECT SQL_CALC_FOUND_ROWS 
        sm.strength_machine_id as id,
        sm.machine_name as name FROM t_strength_program sp 
        LEFT OUTER JOIN t_strength_user_test sut 
            ON sut.r_strength_program_id = sp.strength_program_id
        LEFT OUTER JOIN t_strength_user_test_activity sa 
            ON sa.r_strength_user_test_id = sp.strength_program_id
        LEFT OUTER JOIN t_strength_machine sm 
            ON sm.strength_machine_id = sa.r_strength_machine_id
        WHERE sut.r_user_id =? AND  sm.clubid = ? 
            AND sp.strength_program_id = ?  AND sm.is_deleted = ? 
        GROUP BY sm.strength_machine_id'
);
/* End  05052015 */
define(
    'GET_MEMBER_BY_CLUB', 'SELECT SQL_CALC_FOUND_ROWS * FROM t_users usr 
            WHERE usr.r_club_id=?'
);
