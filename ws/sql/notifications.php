<?php
/**
 * PHP version 5.
 
 * @category SQL
 
 * @package Message
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Sql Queries to handle message related DB access.
 */
define(
    'GET_NOTIFICATIONS', 
	"SELECT noti.id,noti.name,noti.slug from t_notification_pages as noti where slug != 'push'"
	);

define(
    'GET_NOTIFICATIONS_MESSAGES', 
	"SELECT 
		not_mess.id as not_message_id,not_mess.notification_days_id,not_mess.t_notification_type_id,
		not_day.id as not_day_id,not_day.name as not_day_name,
		not_type.id as not_type_id,not_type.name as not_type_name
		from t_notification_messages as not_mess 
		inner join t_notification_days as not_day on (not_mess.notification_days_id = not_day.id)
		inner join t_notification_types as not_type on (not_mess.t_notification_type_id = not_type.id) where not_mess.notification_pages_id = ? and not_mess.t_notification_type_id = ?"
	);
define(
    'GET_NOTIFICATIONS_DAY', 
	"SELECT id,name as day_name,'day_number' from t_notification_days"
	);
define(
    'GET_NOTIFICATIONS_TYPE', 
	"SELECT slug,id,name as type_name  from t_notification_types"
	);
	
define(
    'GET_NOTIFICATIONS_PAGE_ID', 
	"SELECT id from t_notification_pages where slug = ?"
	);

define(
    'CHECK_EXISTING_RECORD', 
	"SELECT id from t_notification_messages where notification_pages_id = ? and t_notification_type_id = ? and t_notification_user_profile_id = ? and notification_days_id = ? and t_notification_user_profile_categorie_id =?"
	);
define(
    'INSERT_NOTIFICATION_MESAGE', 
	'INSERT INTO t_notification_messages
    (notification_pages_id,t_notification_type_id, t_notification_user_profile_id, notification_days_id,t_notification_user_profile_categorie_id,created,modified)
	VALUES
	(?, ?, ?, ?, ?,?,?)'
);

define(
    'INSERT_MESAGE', 
	'INSERT INTO t_notification_message_data
    ( notification_messages_id,t_notification_score_categorie_id,created,modified)
	VALUES
	(?, ?, ?, ?)'
);
define(
    'INSERT_MESSAGE_TIP', 
	'INSERT INTO t_notifications_tips
    (t_notification_message_data_id,tip_number,message,created,modified)
	VALUES
	(?, ?, ?, ?,?)'
);

define(
    'GET_NOTIFICATIONS_DATA', 
	'SELECT *  from t_notification_messages where id=?'
);

define(
    'UPDATE_NOTIFICATION_DATA', 
	"UPDATE t_notification_messages SET 
	t_notification_user_profile_id=?,
	t_notification_user_profile_categorie_id=?,
	modified = ?
    WHERE id=?"
);

define(
    'DELETE_NOTIFICATION_DATA', 
	"delete from t_notification_messages WHERE id=?"
);

define(
    'GET_NOTIFCATION_TIP', 
	'SELECT *  from t_notification_tips_type'
);

define(
    'GET_NOTIFCATION_TOP', 
	'SELECT *  from  t_notification_tops_type where special_top = ?'
);

define(
    'ADD_NOTIFICATIONS_MESSAGE_TIP', 
	'INSERT INTO t_notification_message_tips
    (notification_messages_id,notification_tips_type_id,created,modified)
	VALUES
	(?, ?, ?, ?)'
);

define(
    'ADD_NOTIFICATIONS_MESSAGE_TOP', 
	'INSERT INTO t_notification_message_tops
    (notification_messages_id,notification_tops_type_id,created,modified)
	VALUES
	(?, ?, ?, ?)'
);

define(
    'ADD_NOTIFICATIONS_PROFILE', 
	'INSERT INTO t_notification_profiles
    (notification_messages_id,	profile_id,created,modified)
	VALUES
	(?, ?, ?, ?)'
);

define(
    'UPDATE_NOTIFICATIONS_MESSAGE_TIP', 
	"UPDATE t_notification_message_tips SET 
	notification_tips_type_id=?,
	modified = ?
    WHERE id = ?"
);

define(
    'UPDATE_NOTIFICATIONS_MESSAGE_TOP', 
	"UPDATE t_notification_message_tops SET 
	notification_tops_type_id=?,
	modified = ?
    WHERE id = ?"
);

define(
    'UPDATE_NOTIFICATIONS_PROFILE', 
	"UPDATE t_notification_profiles SET 
	profile_id = ?,
	modified = ?
    WHERE id = ?"
);

define(
    'GET_MESSAGE_PROFILE', 
	'select * from t_notification_profiles where notification_messages_id = ?'
);
define(
    'GET_MESSAGE_TIP', 
	'select * from t_notification_message_tips where notification_messages_id = ?'
);
define(
    'GET_MESSAGE_TOP', 
	'select * from t_notification_message_tops where notification_messages_id = ?'
);

define(
    'GET_MESSAGE', 
	'select * from t_notification_message_data where notification_messages_id = ?'
);
define(
    'GET_NOTIFICATION_MESSAGE_TIP', 
	'select * from  t_notifications_tips where t_notification_message_data_id = ?'
);
define(
    'DELETE_EXISTING_MESSAGE', 
	"delete from t_notification_message_data WHERE notification_messages_id=?"
);

define(
    'DELETE_EXISTING_TOP', 
	'delete from t_notification_message_tops where notification_messages_id = ?'
);
define(
    'DELETE_EXISTING_TIP', 
	'delete from t_notification_message_tips where notification_messages_id = ?'
);
define(
    'DELETE_EXISTING_PROFILE', 
	'delete from t_notification_profiles where notification_messages_id = ?'
);
define(
    'GET_QUESTION_TOPICS', 
	'Select * from  t_form_topics where group_id = ?'
);

define(
    'GET_SCORE_CTEGORIES', 
	"SELECT * from t_notification_score_categories order by name desc"
);

define(
    'GET_MESSAGE_ID', 
	"SELECT id from t_notification_message_data where notification_messages_id = ? "
);
define(
    'DELETE_EXISTING_MESSAGE_TIP', 
	"delete from t_notifications_tips where t_notification_message_data_id in(?) "
);


define(
    'GET_USER', 
	"Select * from t_users where user_id = ? and r_usertype_id =3"
);

define(
    'GET_DAY_ID', 
	"Select id from t_notification_days where day_number = ? "
);
define(
    'GET_PROFILES', 
	"Select id,name from t_notification_user_profiles"
);
define(
    'CHECK_TOPIC_CONTENT_RECORD', 
	"Select id from t_notification_contents where  t_form_topic_id =? and  t_notification_page_id =? and t_notification_type_id =?"
);
define(
    'INSERT_TOPIC_CONTENT', 
	'INSERT INTO t_notification_contents
    (t_form_topic_id, t_notification_page_id,t_notification_type_id,special_top,t_notification_days_id,created,modified)
	VALUES
	(?, ?, ?, ?,?,?,?)'
);
define(
    'INSERT_TOPIC_CONTENT_DATA', 
	'INSERT INTO t_notification_content_data
    (t_notification_content_id,t_notification_user_profile_id,t_notification_score_categorie_id,message_number, content,created,modified)
	VALUES
	(?, ?, ?, ?,?,?,?)'
);

define(
    'GET_CONTENT', 
	"Select content.id as content_id,content.t_form_topic_id as form_id,content.t_notification_page_id as page_id,content.t_notification_type_id as type_id,form_topic.*,not_type.* from t_notification_contents as content 
	inner join t_form_topics as form_topic on (content.t_form_topic_id = form_topic.topic_id) 
	inner join t_notification_types as not_type on (content.t_notification_type_id = not_type.id)
	where  t_notification_page_id = ?"
);

define(
    'GET_CONTENT_RECORD', 
	"Select content.id as content_id,content.t_form_topic_id as form_id,content.t_notification_page_id as page_id,content.t_notification_type_id as type_id,content.special_top as special_top from t_notification_contents as content 
	where  content.id = ?"
);

define(
    'GET_CONTENT_DATA', 
	"Select * from t_notification_content_data where t_notification_content_id = ?"
);

define(
    'GET_PROFILE_CTEGORIES', 
	"Select * from t_notification_user_profile_categories"
);
define(
    'CHECK_TOPIC_SPECIAL_CONTENT_RECORD', 
	"Select id from t_notification_contents where t_notification_page_id =? and t_notification_type_id =? and special_top=?"
);

define(
    'GET_SPECIAL_CONTENT', 
	"Select content.id as content_id,content.special_top as special_top_id,content.t_notification_page_id as page_id,content.t_notification_type_id as type_id,
	top.id as top_id,top.name as top_day_name,not_type.* from t_notification_contents as content 
	inner join  t_notification_tops_type as top on (content.special_top = top.id) 
	inner join t_notification_types as not_type on (content.t_notification_type_id = not_type.id)
	where  t_notification_page_id = ?"
);

define(
    'GET_BEHAVIOUR_DATA', 
	"Select * from t_notification_messages where id = ?"
);

define(
    'GET_BEHAVIOUR_CONTENT', 
	"Select content.id as content_id,content.t_form_topic_id as form_id,content.t_notification_page_id as page_id,content.t_notification_type_id as type_id from t_notification_contents as content 
	where  content.t_notification_page_id = ? and content.t_notification_type_id = ? and content.t_notification_days_id = ?"
);

define(
    'GET_BEHAVIOUR_CONTENT_DATA', 
	"Select * from  t_notification_content_data	where  t_notification_content_id = ?"
);

/////////////////////////////front end queries////////////////////////

define(
    'GET_QUTION_OPTIONS', 'SELECT * FROM t_formelement_options  WHERE r_form_elementid =? and f_isdelete<>1'
);

define(
    'GET_CREDITS_DATA', 
	"Select max_value from  t_formelement_options where  r_form_elementid = ? order by id desc limit 1"
);
define(
    'GET_FORM_ELEMENT_ID', 
	"Select formelement_id from  t_question_language where formelement_id = ? order by id desc limit 1"
);
define(
    'GET_FORM_ELEMENT_DATA', 
	"Select form_id,type,score from  t_form_element where id = ? order by id desc limit 1"
);
define(
    'GET_FORM_ELEMENT_DATA_TEST', 
	"Select id,score from  t_form_element where form_id = ? order by id desc limit 2"
);
define(
    'GET_FORM_DATA', 
	"Select 
		frm.r_topic_id as r_topic_id, 
		frmTopic.topic_name,
		frmTopic.topic_id 
		from  
		t_form frm ,
		t_form_topics frmTopic
		where (frmTopic.topic_id = frm.r_topic_id) and  form_id = ? order by form_id desc limit 1"
	);

//////////////chnage
define(
    'GET_SUITABLE_NOTIFICATION', 
	"Select id,t_notification_user_profile_categorie_id,t_notification_user_profile_id from  t_notification_messages where 
	notification_pages_id = ? and 
	notification_days_id =? and 
	t_notification_type_id = ? and 
	(t_notification_user_profile_id in(?,?) and t_notification_user_profile_categorie_id in(?,?))
	order by id desc limit 1"
);

define(
    'GET_SUITABLE_QUICK_WIN', 
	"Select mess_tip.id as mess_tip_id, mess_tip.notification_tips_type_id,not_tip.id,not_tip.tip_number from t_notification_message_tips  as mess_tip inner join t_notification_tips_type as not_tip on (mess_tip.notification_tips_type_id = not_tip.id) where notification_messages_id = ? "
);

define(
    'GET_SUITABLE_TOP', 
	"Select mess_top.id as mess_tip_id, mess_top.notification_tops_type_id,not_top.id,not_top.top_number from  t_notification_message_tops  as mess_top inner join t_notification_tops_type as not_top on (mess_top.notification_tops_type_id = not_top.id) where notification_messages_id = ? "
);

define(
    'GET_SUITABLE_CONTENT', 
	"Select id from  t_notification_contents where t_form_topic_id = ? and t_notification_page_id =? and t_notification_type_id = ? order by id desc limit 1"
);

define(
    'GET_FORM_DAT', 
	"Select form_id from t_form where r_quesgroup_id =? and r_quesphase_id = ? and r_topic_id =? order by form_id desc limit 1"
);

define(
    'GET_FORM_ELEMENT', 
	"Select id from t_form_element where form_id =?  order by id desc limit 2"
);
define(
    'GET_FORM_ELEMENT_All', 
	"Select id from t_form_element where form_id =? "
);


define(
    'CHECK_NOTIFICATION', 
	'select * from t_notification_message_users where user_id = ? and notification_pages_id = ? and notification_days_id = ? and 
	notification_type = ?  and profile_id = ? order by id desc'
);


define(
    'INSERT_USER_NOTIFICATION_DATA', 
	'INSERT INTO  t_notification_message_users
    (user_id, 
	notification_pages_id,
	notification_days_id,
	notification_type,
	profile_id,
	content_data_id,
	special_top,
	message_number,
	created,
	modified)
	VALUES
	(?, ?, ?, ?,?,?,?,?,?,?)'
);

define(
    'GET_SUITABLE_TOP_NOTIFICATION', 
	"Select 
		id
		from  t_notification_contents 
		where 
		t_notification_page_id = ? and 
		special_top =? and 
		t_notification_type_id = ? 
		order by id desc limit 1"
);

define(
    'GET_SUITABLE_TOP_CONTENT', 
	"Select id from  t_notification_contents where 
	t_form_topic_id = ? and 
	t_notification_page_id =? and 
	t_notification_type_id = ? and 
	special_top	 != 0
	order by id desc limit 1"
);

define(
    'CHECK_SPECIAL_TOP_NOTIFICATION', 
	'select * from t_notification_message_users where 
	user_id = ? and 
	notification_pages_id = ? and 
	notification_days_id = ? and 
	notification_type = ?  and 
	profile_id = ? and 
	special_top = 1
	order by id desc'
);


define(
    'GET_SUITABLE_BEHAVIOUR_NOTIFICATION', 
	"Select 
	id,
	t_notification_user_profile_categorie_id,
	t_notification_user_profile_id 
	from  
	t_notification_messages where 
	notification_pages_id = ? and 
	notification_days_id =? and 
	t_notification_type_id = ? 
	order by id desc limit 1"
);

define(
    'GET_SUITABLE_BEHAVIOUR', 
	"Select mess_top.id as mess_tip_id, mess_top.notification_tops_type_id,not_top.id,not_top.top_number from  t_notification_message_tops  as mess_top inner join t_notification_tops_type as not_top on (mess_top.notification_tops_type_id = not_top.id) where notification_messages_id = ? "
);
define(
    'GET_SUITABLE_BEHAVIOUR_CONTENT', 
	"Select id from  t_notification_contents where t_form_topic_id = 0 and t_notification_page_id = ? and t_notification_type_id = ? and t_notification_days_id = ?  order by id desc limit 1"
);

/////////////////////push /////////////////////////////

define(
    'GET_PUSH_TYPE', 
	"Select id,name from  t_notification_push_type"
);
define(
    'CHECK_PUSH_EXISTING_RECORD', 
	"SELECT id from t_notification_push_messages where notification_type_id = ? and notification_push_activity_id = ? and notification_days_id = ? "
	);
define(
    'INSERT_PUSH_NOTIFICATION_MESAGE', 
	'INSERT INTO t_notification_push_messages
    (notification_type_id, notification_push_activity_id,notification_days_id,profile_id,profile_cat_id,created,modified)
	VALUES
	(?, ?, ?, ?, ?,?,?)'
);
define(
    'INSERT_PUSH_MESAGE', 
	'INSERT INTO t_notification_push_message_data
    ( notification_push_messages_id,sending_time,message,created,modified)
	VALUES
	(?, ?, ?, ?,?)'
);

define(
    'GET_PUSH_NOTIFICATIONS_TYPE', 
	"SELECT id,name  from t_notification_push_activity where activity_type = ?"
	);
define(
    'GET_PUSH_NOTIFICATIONS', 
	"SELECT noti.id,noti.name,noti.slug from t_notification_pages as noti where slug = 'push'"
	);



define(
    'GET_PUSH_NOTIFICATIONS_LIST', 
	'SELECT not_push.id as not_id,not_push.notification_push_activity_id,not_push.notification_type_id,not_days.id as not_day_id,not_push.notification_days_id,not_days.name from t_notification_push_messages as not_push left join  t_notification_days as not_days on (not_push.notification_days_id = not_days.id)'
);

define(
    'GET_PUSH_NOTIFICATIONS_MESSAGES', 
	'SELECT not_data.* from t_notification_push_message_data as not_data  where notification_push_messages_id =?'
);

define(
    'GET_PUSH_NOTIFICATIONS_DATA', 
	"SELECT * from t_notification_push_messages where id = ? "
);
define(
    'UPDATE_PUSH_NOTIFICATION_MESAGE', 
	"UPDATE t_notification_push_messages SET 
	notification_push_activity_id = ?,
	profile_id = ?,
	profile_cat_id = ?,
	modified = ?
    WHERE id = ?"
);
