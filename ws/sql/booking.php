<?php
/**
 * PHP version 5.
 
 * @category SQL
 
 * @package Booking
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Sql Queries to handle booking related DB access.
 */
define(
    'GET_SLOT_TYPES', ' '
    .'SELECT slot_id,slot_name '
    .'FROM `t_slot_type`'
);

define(
    'GET_UESR_BOOKING', ' '
    .'SELECT booking.booking_id, booking.r_slot_id, slot.slot_name,
    booking.r_club_id,club.club_name, booking.r_company_id, '
    .'company.company_name, booking.start_date, booking.start_time, 
		booking.end_time, booking.calendar_booking_id, booking.booking_status '
    .'FROM t_booking booking '
    .'INNER JOIN t_slot_type slot ON booking.r_slot_id =  slot.slot_id '
    .'INNER JOIN t_clubs club ON club.club_id = booking.r_club_id '
    .'INNER JOIN t_company company ON booking.r_company_id = company.company_id '
    .'WHERE booking.r_user_id=?'
);
define(
    'GET_BOOKING_DETAILS', ' '
    .'SELECT * '
    .'FROM t_booking WHERE booking_id=?'
);
