<?php
/**
 * PHP version 5.
 
 * @category SQL
 
 * @package Import
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Sql Queries to handle import related DB access.
 */
define(
    'SELECT_USERS', 'SELECT * FROM t_users 
		WHERE external_application_user_id =? and r_company_id=?'
);
define(
    'SELECT_COMPANIES', 'SELECT company_id as companyId,
		company_name as companyName FROM t_company where is_deleted=0'
);
define(
    'SELECT_STATUS', 'SELECT * FROM t_status'
);
define(
    'SELECT_USER_TYPE', 'SELECT usertype_id as userTypeId,
		usertype FROM t_usertype where status =1'
);
define(
    'SELECT_CLUBS', 'SELECT * FROM t_clubs where guid=? 
	and r_company_id=?'
);
define(
    'SELECT_PERSONALINFO', 'SELECT * FROM t_personalinfo 
	where r_user_id=?'
);
define(
    'SELECT_COMPANY', 'SELECT * FROM t_company'
);
define(
    'GET_PROFESSION_LIST', 'SELECT * FROM `t_profession`'
);
define(
    'GET_PROFESSION_EXIST', 'SELECT * FROM `t_profession` 
	WHERE `profession_name`=?'
);
define(
    'GET_BOOKING', 'SELECT * FROM t_booking 
	WHERE r_company_id =? and r_club_id=? and r_slot_id=? 
	and start_date=? and start_time=? and end_time=? and r_user_id=?'
);
define(
    'GET_USERID_BASED_PERSONID', 'SELECT user_id FROM t_users 
	WHERE external_application_user_id=? AND `r_company_id`=?'
);
define(
    'MEDICAL_AGAINST_USER_EXIST', 'SELECT * FROM t_medical_against 
	WHERE r_user_id=?'
);
define(
    'MEDICAL_JOINTPROBLEM_USER_EXIST', 'SELECT * FROM t_medical_jointproblem 
	WHERE r_user_id=?'
);
define(
    'MEDICAL_MUSCLEINJURY_USER_EXIST', 'SELECT * FROM t_medical_muscleinjury 
	WHERE r_user_id=?'
);
define(
    'MEDICAL_RISKFACTOR_USER_EXIST', 'SELECT * FROM t_medical_riskfactor 
	WHERE r_user_id=?'
);
define(
    'GET_USER_PERSONAL_EXIST', 'SELECT * FROM `t_personalinfo` 
	WHERE r_user_id=?'
);
define(
    'SELECT_SLOTTYPE', 'SELECT * FROM t_slot_type 
	WHERE slot_id=?'
);
define(
    'GET_UPDATEPASSWORD', 'SELECT * FROM t_users 
	WHERE external_application_user_id=?'
);
define(
    'GET_CITY_EXIST', 'SELECT * FROM `t_city` 
	WHERE `zipcode`=? AND `city_name`=?'
);
