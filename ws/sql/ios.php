<?php
/**
 * PHP version 5.
 
 * @category SQL
 
 * @package IOS
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Sql Queries to handle ios related DB access.
 */
/*define(
    'GET_MEMBERLIST_FOR_TEST', "SELECT users.user_id AS userId,
            users.r_club_id AS clubId,users.r_company_id AS companyId,
            users.first_name AS firstName,users.last_name AS lastName,
            users.email,personal.userimage AS imageURL,usertest.weight, 
            device.device_code AS HeartRateMonitorId,
            usertest.r_equipment_id,equ.equipment_name,usertest.user_test_id,
            testParameter.test_level,testParameter.start_load_test,
                if(users.gender=0,'Male','Female') as gender,
                testParameter.age_on_test,usertest.rpm,
                testParameter.fitness_level,
                    (SELECT CONCAT(achieved_point,'/',target_points) 
                    FROM t_points_achieve AS pa WHERE (CURDATE() BETWEEN 
                    week_start_date AND week_end_date) AND 
                    pa.r_user_test_id = usertest.user_test_id LIMIT 1) AS points
                    FROM t_user_test usertest
                    LEFT JOIN t_users users 
                        ON users.user_id = usertest.r_user_id
                    LEFT JOIN t_user_test_parameter testParameter 
                        ON testParameter.r_user_test_id = usertest.user_test_id
                    LEFT JOIN t_personalinfo personal 
                        ON personal.r_user_id = users.user_id
                    LEFT JOIN t_device_member deviceMember 
                        ON deviceMember.r_user_id = users.user_id 
                            AND deviceMember.is_deleted=0
                    LEFT JOIN t_device device 
                        ON device.device_id = deviceMember.r_device_id
                    LEFT JOIN t_equipment equ 
                        ON equ.equipment_id = usertest.r_equipment_id
                    WHERE users.r_company_id= ? AND users.r_club_id= ? 
                        AND users.r_usertype_id = '3' "
);
*/
/*define(
    'GET_MEMBERLIST_FOR_TRAINING', " SELECT usertest.user_test_id,
        usertest.test_start_date,
        usertest.status,users.user_id AS userId,users.person_id personId,
        users.r_club_id AS clubId,users.r_company_id AS companyId,
        users.first_name AS firstName,users.last_name AS lastName,
        users.email,personal.dob, personal.userimage AS imageURL,
        usertest.weight, device.device_code AS HeartRateMonitorId,
        usertest.r_equipment_id,equ.equipment_name,usertest.test_level,
        IF(users.gender=0,'Male','Female') AS gender,testParameter.age_on_test,
        usertest.rpm,testParameter.fitness_level,
        users.external_application_user_id
        FROM t_user_test usertest  
            INNER JOIN `t_booking` B ON B.r_slot_id != 37 
                AND B.r_user_id=usertest.r_user_id AND B.`start_date`=CURDATE() 
                AND TIME(CONCAT(B.`start_date`,' ',B.`start_time`)+Interval ? 
                minute)<=TIME(NOW()) AND 
                TIME(CONCAT(B.`start_date`,' ',B.`end_time`)+Interval ?
                minute)>=TIME(NOW())
            LEFT JOIN t_users users ON users.user_id = usertest.r_user_id
            LEFT JOIN t_user_test_parameter testParameter 
                ON testParameter.r_user_test_id = usertest.user_test_id
            LEFT JOIN t_personalinfo personal 
                ON personal.r_user_id = users.user_id 
            LEFT JOIN t_device_member deviceMember 
                ON deviceMember.r_user_id = users.user_id 
                AND deviceMember.is_deleted=0 
            LEFT JOIN t_device device 
                ON device.device_id = deviceMember.r_device_id 
            LEFT JOIN t_equipment equ 
                ON equ.equipment_id = usertest.r_equipment_id 
            WHERE users.r_company_id= ? AND users.r_club_id= ?
                AND users.r_usertype_id = '3' 
                AND users.r_status_id = '4'
                AND usertest.user_test_id=(SELECT max(user_test_id) FROM 
                    t_user_test ut WHERE ut.r_user_id = usertest.r_user_id 
                    AND ut.status=3)"
);
*/
define(
    'GET_MEMBERLIST_INSERTFORTEST_IOSQUERY',
    'SELECT *
                         FROM t_user_test
                         WHERE user_test_id=?'
);

define(
    'GET_TESTINFOFORCOACH_IOSQUERY',
    'SELECT c.club_id,c.club_name,u.user_id,u.first_name,
        u.last_name,p.userimage as imageURL,ut.status,ut.heart_rate_value,
        ut.rpm,ut.rpm_group,ut.load_value,ut.current_test_time,p.height,
        ut.weight,
        eq.equipment_id as machineId,eq.equipment_name as machineName,
        testParameter.test_level as startLevel,testParameter.start_load_test,
        ut.user_test_id as userTestId,ut.kcal,testParameter.age_on_test,
        ut.cool_down_start_time,ut.cool_down_stop_time,
        ut.heart_rate_with_cool_down
             FROM t_users u 
             LEFT OUTER JOIN t_clubs c ON c.club_id=u.r_club_id
             LEFT OUTER JOIN t_user_test ut 
                ON ut.user_test_id = (SELECT ut1.user_test_id FROM 
                t_user_test ut1 WHERE u.user_id = ut1.r_user_id  
                ORDER BY ut1.user_test_id DESC LIMIT 1)
             LEFT  JOIN t_user_test_parameter  testParameter 
                ON testParameter.r_user_test_id = ut.user_test_id
             LEFT OUTER JOIN t_personalinfo p ON p.r_user_id=u.user_id
             LEFT OUTER JOIN t_equipment eq ON eq.equipment_id = ut.r_equipment_id
             WHERE u.r_usertype_id=3 and u.r_status_id=5 and
                ut.status NOT IN (0,3,30,40,50) and u.r_company_id=? and 
                u.r_club_id=? and u.is_deleted<>1'
);

/*define(
    'GET_USERTESTID_INSERTHRM_IOSQUERY',
    'SELECT *
                         FROM t_user_test
                         WHERE user_test_id=?'
);*/

/*define(
    'GET_INSERTHERTRATEFORTEST_IOSQUERY',
    'SELECT uh.r_test_id,uh.r_user_id,uh.hrm
                         FROM t_user_heartrate uh
                         WHERE r_user_id=? and r_test_id=?'
);*/

/*define(
    'GET_GETUSERLASTTEST_QUERY',
    'SELECT uh.r_test_id
                         FROM t_user_heartrate uh
                         WHERE r_user_id=?
                         ORDER BY user_heartrate_id DESC
                         LIMIT 1'
);*/

//Get Member Test Process Details
/*define(
    'GET_MEMBER_TEST_PROCESS_DETAILS',
    'SELECT * FROM t_test_process
            WHERE r_user_id=? AND r_machine_id=? AND r_equipment_id=?'
);*/

//Get Person Id for Member
/*define(
    'GET_MEMBER_PERSONID',
    "SELECT person_id FROM t_users WHERE user_id='1'"
);*/

define(
    'GET_TESTLEVEL_IOSQUERY',
    "SELECT ut.r_user_id as userId,ut.test_level,ut.weight,
        if(ut.status=0,'Stop',
        if(ut.status=1,'Running','Cool Down')) as testStatus,
            ut.load_value
             FROM t_user_test ut
             WHERE ut.r_user_id=?
             ORDER BY ut.user_test_id desc
             LIMIT 1"
);

//Get Test Details
define(
    'GET_TEST_DETAILS', 'SELECT * FROM t_test 
            WHERE test_name=? AND status=0'
);

/*define(
    'GET_DEVICE_MEMBER_TEST', "SELECT user_test_id FROM t_user_test 
            WHERE status!='3' AND r_user_id=?"
);*/

/*define(
    'GET_BOOKING_STATUS', 'SELECT * FROM `t_booking` 
            WHERE r_user_id=? AND calendar_booking_id=?'
);*/

/*define(
    'GET_DEVICE_HEART_RATE', 'SELECT * FROM t_device 
            WHERE device_code = ? AND r_club_id=? AND r_company_id=? AND status=0'
);*/

/*define(
    'GET_DEVICE_MEMBER_HEART_RATE', 'SELECT * FROM t_device_member
                WHERE r_device_id = ? AND is_deleted = 0'
);*/

/*define(
    'GET_DEVICE_MEMBER_HEART_RATE_EXIST', 'SELECT * FROM t_device_member 
            WHERE r_device_id = ? AND `r_user_id`=?'
);*/

//Note Heartrate monitor id removed and member
define(
    'GET_DEVICE_ASSOCIATED_MEMBER', "SELECT *,
        IF(tu.gender=0,'Male','Female') AS gender,
        (SELECT `weight_on_test` FROM `t_user_test` ut 
        INNER JOIN `t_user_test_parameter` utp 
            ON utp.r_user_test_id=ut.user_test_id
            WHERE ut.`r_user_id`=tu.user_id AND ut.`status`=3 
            ORDER BY ut.user_test_id DESC LIMIT 1)  weight_on_test
       FROM  t_users tu 
       LEFT JOIN t_personalinfo pf on pf.r_user_id=tu.user_id 
       LEFT JOIN `t_booking` B ON B.r_slot_id != 37 AND B.r_user_id=tu.user_id
        AND  B.`start_date`=CURDATE() AND 
        TIME(CONCAT(B.`start_date`,' ',B.`start_time`)+Interval ?  
        minute)<=TIME(NOW()) AND TIME(CONCAT(B.`start_date`,' ',B.`end_time`)
        +Interval ?  minute)>=TIME(NOW())
       WHERE tu.r_club_id=? AND tu.r_company_id=? AND tu.user_id=? 
       AND tu.r_usertype_id=3 AND tu.is_deleted =0"
);

define(
    'GET_MEMBER_ASSOCIATED_WITH_HRM', 'SELECT * FROM t_device AS td 
        LEFT OUTER JOIN t_device_member tdm ON td.device_id=tdm.r_device_id
        LEFT JOIN t_users tu ON tu.user_id=tdm.r_user_id  
        LEFT JOIN t_personalinfo pf on pf.r_user_id=tu.user_id 
        WHERE tu.is_deleted=0 and tdm.is_deleted=0'
);

define(
    'GET_CYCLE_AVAILABILITY_CLUB', 'SELECT t_equipment.* 
        FROM t_equipment t_equipment 
           INNER JOIN t_clubs c ON c.club_id=t_equipment.r_club_id
           WHERE t_equipment.r_club_id=? AND t_equipment.r_company_id=?
           AND t_equipment.equipment_name=?'
);
define(
    'GET_CYCLE_ASSOCIATED_MEMBERS', 
    "SELECT u.user_id,u.email, u.person_id as member_id, u.first_name,         
    u.last_name,ut.r_equipment_id,c.club_name,c.club_id,
    e.equipment_name cycle,ut.status as currentStatusOfTest,
    if(ut.status=10 || ut.status=20,'Not started',
        if(ut.status=1,'Started',if(ut.status=2,'Cooldown',
            if(ut.status=3 ||ut.status=30 ,'Stopped','')))) as test_status
                FROM t_users u 
                INNER JOIN t_user_test ut ON ut.r_user_id=u.user_id
                    AND ut.user_test_id= (SELECT MAX(user_test_id) FROM 
                    t_user_test WHERE r_user_id=u.user_id)
                LEFT JOIN t_equipment e ON e.equipment_id=ut.r_equipment_id
                INNER JOIN t_clubs c ON c.club_id=u.r_club_id 
                WHERE u.r_usertype_id=3  AND ((ut.status='3' AND 
                TIME_TO_SEC(TIMEDIFF(NOW(),ut.test_end_date))<=10) 
                    OR (u.r_status_id=5 AND ut.status
                    NOT IN('0','3','30','40','50')) 
                    OR (u.r_status_id=5 AND ut.status='3' AND 
                    ut.is_test_completed='stopped' 
                    AND TIME_TO_SEC(TIMEDIFF(NOW(),
                    ut.test_end_date))<=10)) AND u.r_club_id=? 
                    AND u.r_company_id=? 
                    AND u.is_deleted<>1  
                    ORDER BY ut.user_test_id DESC"
);

define(
    'GET_MEMBER_ASSOCIATED_CYCLE', "SELECT UT.*, U.user_id,U.email,
        U.person_id as member_id, U.first_name,         
        U.last_name,if(U.gender=0,'Male','Female') as gender,
        P.age_on_test,UP.userimage,D.device_code FROM `t_equipment` E 
            INNER JOIN `t_user_test` UT ON UT.`r_equipment_id`=E.`equipment_id` 
                AND UT.`r_club_id`=E.`r_club_id` AND UT.`status`=20 
            LEFT JOIN `t_user_test_parameter` P 
                ON P.`r_user_test_id`=UT.`user_test_id` 
            INNER JOIN `t_users` U ON U.`user_id`=UT.`r_user_id` 
            INNER JOIN `t_personalinfo` UP ON UP.`r_user_id`=U.`user_id` 
            LEFT JOIN `t_device` D ON D.`device_id`=UT.`r_device_id` 
            WHERE E.`r_company_id`=? AND E.`r_club_id`=?
                AND E.`equipment_name`=? LIMIT 1"
);

define(
    'GET_CYCLE_LIST', 'SELECT * FROM t_equipment WHERE  equipment_id =?'
);

define(
    'GET_USER_CURRENT_TEST_ID', 'SELECT MAX(user_test_id) as current_test_id,
                r_user_id FROM t_user_test 
                    WHERE r_user_id=(SELECT user_id 
                        FROM t_users WHERE user_id=? AND r_company_id=? 
                        AND r_club_id=?)'
);

define(
    'GET_USER_TEST', 'SELECT * FROM t_user_test 
        WHERE r_club_id=? AND r_user_id=? AND user_test_id=?'
);

define(
    'GET_CYCLEIDSTATUS_LIST',
    'SELECT equipment_id,equipment_name,status
                         FROM t_equipment
                         WHERE r_club_id=? AND r_company_id=?'
);

define(
    'GET_CYCLEASSOCIATEDWITHTABLET_LIST',
    'SELECT Is_associated_with_tablet,modified_by,modified_date
                         FROM t_equipment
                         WHERE equipment_id=?'
);

define(
    'GET_ASSIGNCYCLEWITHMEMBER_LIST',
    'SELECT r_equipment_id,modified_by,modified_date
                         FROM t_user_test
                         WHERE r_user_id=? ORDER BY user_test_id DESC LIMIT 1'
);

define(
    'GET_TESTLEVELS_TABLET', 'SELECT * FROM t_user_test WHERE user_test_id=?'
);

define(
    'GET_HEARTRATE_TABLET', "SELECT ut.user_test_id, utp.training_type, 
            utp.fitness_level, ut.heart_rate_value,utp.age_on_test,
            utp.weight_on_test,ut.test_level,if(u.gender=0,'M','F') as gender,
            utp.fitness_level,ut.heart_rate_without_cool_down,
            ut.heart_rate_with_cool_down,ut.cool_down_start_time,
            ut.cool_down_stop_time FROM t_user_test AS ut 
            LEFT JOIN t_user_test_parameter AS utp 
                ON ut.user_test_id = utp.r_user_test_id
            LEFT JOIN t_users AS u ON u.user_id = ut.r_user_id
                WHERE ut.user_test_id=?"
);

define(
    'GET_TEST_RESULT', 'SELECT * FROM t_test_result WHERE r_user_test_id=?'
);

define(
    'GET_USER_TEST_PARAMETER_EXIST', 'SELECT * FROM `t_user_test_parameter`
                WHERE `r_user_test_id`=?'
);

define(
    'GET_CYCLE_AVAILABILITY_FOR_CLUB', 'SELECT equipment_id AS cycleId,
            equipment_name AS cycleName,Is_associated_with_tablet AS isAssociated,
            tablet_id AS tabletId,status AS isAvailability
            FROM t_equipment WHERE r_club_id= ? AND r_company_id=?'
);

define(
    'GET_DEVICE_STATUS', 'SELECT device_member_id FROM t_device AS d
        LEFT JOIN t_device_member AS dm ON dm.r_device_id = d.device_id
        WHERE dm.r_user_id=? AND d.device_code != ?'
);

define(
    'GET_DEVICE_MEMBER_ID', 'SELECT * FROM t_device_member 
            WHERE device_member_id = ? '
);

define(
    'UPDATE_DEVICE_OTHER_USER_MAPPING', 
    'UPDATE `t_device_member` SET `is_deleted`=1 
        WHERE `r_device_id`=?'
);
define(
    'UPDATE_DEVICE_USER_MAPPING', 
    'UPDATE `t_device_member` SET `is_deleted`=1 
    WHERE `r_user_id`=?'
);

define(
    'GET_DEVICE_STATUS_CURRENT', 'SELECT device_member_id FROM t_device AS d
            LEFT JOIN t_device_member AS dm ON dm.r_device_id = d.device_id
            WHERE dm.r_user_id=? AND d.device_code = ?'
);

define(
    'GET_DEVICE_CURRENT', 'SELECT device_member_id FROM t_device AS d
             LEFT JOIN t_device_member AS dm ON dm.r_device_id = d.device_id
             WHERE dm.is_deleted=0 AND dm.r_user_id=? AND d.device_code = ?'
);

define(
    'GET_EQUIPMENTID', 'SELECT equipment_id
            FROM t_equipment
            WHERE equipment_name=? AND r_company_id=? AND r_club_id=?'
);

define(
    'GET_CYCLETEST_LEVELS',
    'SELECT load_value,status
            FROM t_user_test
            WHERE r_equipment_id=? AND r_club_id=? AND status in(0,1,2,10,20)  
            ORDER BY user_test_id desc LIMIT 1'
);

define(
    'GET_USERTESTRPM',
    'SELECT user_test_id,cool_down_start_time,cool_down_start_time,rpm_group
         FROM t_user_test
         WHERE r_equipment_id=? AND r_club_id=? AND status<>3 
         AND status<>30 ORDER BY user_test_id DESC LIMIT 1'
);

define(
    'GET_UPDATERPMUSERTEST',
    'SELECT *
                         FROM t_user_test
                         WHERE user_test_id=?'
);

define(
    'GET_POINTS_ACHIEVE',
    'SELECT *
         FROM t_points_achieve
         WHERE (CURDATE() BETWEEN week_start_date AND week_end_date) AND r_user_id=? 
         AND r_user_test_id=?'
);

define(
    'GET_TRANSACTION', 'SELECT * FROM t_training_points_transaction'
);

define(
    'GET_TRAINING_DEVICE', 'SELECT 
        training_device_id,training_device_name FROM t_training_device 
        WHERE status=1'
);

define(
    'COOL_DOWN_TEST_EXIST', 'SELECT * FROM `t_cool_down_history` 
    WHERE `r_user_test_id`=?'
);
define(
    'COOL_DOWN_HISTORY_ID_EXIST', 'SELECT 
        MAX(`cool_down_history_id`) `history_id` FROM `t_cool_down_history` 
        WHERE `r_user_test_id`=?'
);
define(
    'COOL_DOWN_BY_HISTORY_ID', 'SELECT * FROM `t_cool_down_history` 
        WHERE `cool_down_history_id`=?'
);

define(
    'GET_USER_TEST_ID_EXIST', 'SELECT * FROM `t_user_test` 
            WHERE `user_test_id`=? AND status IN(40) '
);
define(
    'GET_USER_TEST_PARAMETER_ID_EXIST', 'SELECT * FROM `t_user_test_parameter` 
        WHERE `r_user_test_id`=?'
);
define(
    'GET_USER_TEST_MEMBER_PERSONAL_EXIST', 'SELECT * FROM `t_personalinfo` 
        WHERE `r_user_id`=?'
);

define(
    'GET_DEVICE_EXIST_IN_RUNNING_TEST', 'SELECT * FROM `t_user_test` 
    WHERE `r_device_id`=? AND `status` IN(1,2) '
);

define(
    'GET_MEMBER_LIST_BY_SEARCH', 'SELECT user_id userId, first_name firstName,
        last_name lastName, email emailId, userimage imageURL, gsm, 
        device_code hrmId FROM `t_users` usr 
        INNER JOIN `t_personalinfo` usrp ON usrp.r_user_id=usr.user_id
        LEFT JOIN `t_device_member` dm ON dm.r_user_id=usr.user_id 
        AND dm.`is_deleted`=0
        LEFT JOIN `t_device` d ON d.device_id=dm.r_device_id
        WHERE usr.`r_usertype_id`=? AND usr.`r_company_id`=? AND usr.`r_club_id`=? '
);

define(
    'GET_MEMBER_LIST_SEARCH_USING_HRM_DEVICE', "SELECT usr.r_status_id,
    usr.user_id AS userId,usr.person_id personId,usr.r_club_id AS clubId, 
    usr.r_company_id AS companyId, usr.first_name AS firstName,
    usr.last_name AS lastName, usr.email, personal.dob, 
    personal.userimage AS imageURL, device.device_code AS HeartRateMonitorId,
    IF(usr.gender=0,'Male','Female') AS gender,
    IF(usr.r_status_id=4,'yes','no') AS trainingStatus FROM  `t_device` device
    LEFT JOIN `t_device_member` dmember ON dmember.r_device_id=device.device_id 
        AND dmember.`is_deleted`=0
    LEFT JOIN `t_users` usr ON usr.user_id=dmember.r_user_id 
    LEFT JOIN t_personalinfo personal ON personal.r_user_id = usr.user_id 
    WHERE device.`r_company_id`=? AND device.`r_club_id`=? "
);

define(
    'GET_MEMBERLIST_FOR_STRENGTH_TRAINING', "SELECT strength_user_test_id,
        U.user_id AS userId, U.person_id personId, U.r_club_id AS clubId,
        U.r_company_id AS companyId, U.first_name AS firstName,
        U.last_name AS lastName, U.email, P.dob, P.userimage AS imageURL,
        UT.weight, IF(U.gender=0,'Male','Female') AS gender 
    FROM t_strength_user_test UT  
    INNER JOIN `t_booking` B ON B.r_slot_id=37 AND B.r_user_id=UT.r_user_id
        AND B.`start_date`=CURDATE() AND 
        TIME(CONCAT(B.`start_date`,' ',B.`start_time`)+Interval ?
        minute)<=TIME(NOW()) AND TIME(CONCAT(B.`start_date`,' ',B.`end_time`)
        +Interval ? minute)>=TIME(NOW())
    INNER JOIN t_users U ON U.user_id = UT.r_user_id 
        AND U.r_company_id=? AND U.r_club_id=?
    INNER JOIN t_personalinfo P ON P.r_user_id = U.user_id 
    WHERE UT.strength_user_test_id=(SELECT strength_user_test_id 
        FROM t_strength_user_test T WHERE T.r_user_id = UT.r_user_id 
        ORDER BY strength_user_test_id DESC LIMIT 1)"
);

define(
    'GET_ACTIVITY_DETAIL_FOR_STRENGTH_TRAINING', 'SELECT TW.week,STA.strength,
        sp.reputation,SP.strength_percentage,SP.time,
        SP.rest FROM `t_strength_user_test_week` TW
        INNER JOIN `t_strength_user_test_activity` STA 
            ON  STA.`r_strength_user_test_id`=TW.`r_strength_user_test_id`
        INNER JOIN `t_strength_user_test` ST 
            ON ST.`strength_user_test_id`=STA.`r_strength_user_test_id`
        INNER JOIN `t_strength_program_training` SP 
            ON SP.`r_strength_program_id`=ST.`r_strength_program_id` 
            AND SP.`training_week`=TW.`week`
        WHERE TW.`r_user_id`=? AND TW.`r_strength_user_test_id`=? 
            AND (CURDATE() BETWEEN TW.week_start_date AND TW.week_end_date) 
            AND STA.`r_strength_machine_id`=?'
);

define(
    'GET_MEMBER_LAST_TEST_DETAIL_NEW', "SELECT TP.*, U.*,
        date_format(UT.test_start_date,'%d-%m-%Y') as test_start_date,
        C.club_name, COUNT(user_test_id) as total_test, 
        MAX(user_test_id) as last_test_id,
        if(U.gender=0,'male','female') as gender,
        if(U.email='',U.username, U.email) as emailusername FROM t_user_test UT 
           INNER JOIN t_users U ON U.user_id=UT.r_user_id
           INNER JOIN t_clubs C ON C.club_id=U.r_club_id
           INNER JOIN t_user_test_parameter TP 
            ON TP.r_user_test_id=UT.user_test_id
           WHERE UT.status=3 AND U.user_id=? 
            ORDER BY UT.`user_test_id` DESC LIMIT 1"
);
