<?php
/**
 * PHP version 5.
 
 * @category SQL
 
 * @package Cron
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Sql Queries to handle cron related DB access.
 */
define(
    'GET_CENTER_LIST', 'SELECT fitclass_center_id as clubid,
			fitclass_center_name as clubname FROM t_fitclass_center 
			where status=?'
);

define(
    'GET_CLUB_DETAILS', 'SELECT * FROM t_clubs WHERE company_club_id = ?'
);

define(
    'GET_STATUS_FOR_INTERUPT_TEST', 'SELECT * FROM t_user_test 
			WHERE status IN (1,2,10,20,40) AND test_start_date <= DATE_SUB(NOW(), 
				INTERVAL ? MINUTE) '
);

define(
    'GET_CYCLE_ASSIGNED_FOR_TEST_CRON', 'SELECT * FROM t_user_test 
		WHERE status IN (0,10,20) AND `r_equipment_id` > 0 
			AND DATE(test_start_date) < DATE(NOW()) '
);
