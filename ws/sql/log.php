<?php
/**
 * PHP version 5.
 
 * @category SQL
 
 * @package Log
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Sql Queries to handle log related DB access.
 */
define('GET_LOG', 'SELECT * FROM t_log');

define('GET_QUERY_LAST_TOTAL_COUNT', 'SELECT FOUND_ROWS() as total_count');
