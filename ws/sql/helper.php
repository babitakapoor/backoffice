<?php

class Helper
{
    /**
     * @author Manikandan
     * @param string $raw_sql
     * @param array $parameters
     * @return string
     */
    static public function debugPDO($raw_sql, $parameters) {

        $keys = array();
        $values = $parameters;

        foreach ($parameters as $key => $value) {

            // check if named parameters (':param') or anonymous parameters ('?') are used
            if (is_string($key)) {
                $keys[] = '/' . $key . '/';
            } else {
                $keys[] = '/[?]/';
            }

            // bring parameter into human-readable format
            if (is_string($value)) {
                $values[$key] = "'" . $value . "'";
            } elseif (is_array($value)) {
                $values[$key] = implode(',', $value);
            } elseif (is_null($value)) {
                $values[$key] = 'NULL';
            }
        }

        /*
        echo "<br> [DEBUG] Keys:<pre>";
        print_r($keys);

        echo "\n[DEBUG] Values: ";
        print_r($values);
        echo "</pre>";
        */

        $raw_sql = preg_replace($keys, $values, $raw_sql, 1, $count);

        return $raw_sql;
    }

    static public function GetUniqueKey($count=10){
        $unikey  =   substr(uniqid(), 0, $count);
        return $unikey;
    }

    static public function EncryptKeyLicence($key,$datastr)
    {
        /* PURELY THOUGHT BY MK * But Commented * /
        $encrevkey  =   "$" . strrev($key) . strlen($key) . "$" ;
        $datastr    =   $encrevkey.$datastr;
        $revdatastr =   base64_encode(strrev($datastr));
        return $revdatastr;
        */
        return cryptoJsAesEncryptBase64Covered($key,$datastr);
    }

    static public function DecryptKeyLicence($key,$encstr)
    {
        /* PURELY THOUGHT BY MK * But Commented * /
        $rawithkey  =   strrev(base_decode($encstr));
        $encrevkey  =   "$".strrev($key) . strlen($key) . "$" ;
        $decstrarr  =   explode($encrevkey,$rawithkey);
        return $decstrarr[0] ? $decstrarr[0]:"";
        */
        return cryptoJsAesDecryptBase64Covered($key,$encstr);

    }

    static public function EncryptPassword($datastr,$key='')
    {
        if($key==''){
            return encryptdefault($datastr);
        }
        return encrypt($datastr, $key);
    }

    static public function DecryptPassword($encstr,$key='')
    {
        if($key==''){
            return decryptdefault($encstr);
        }
        return decrypt($encstr, $key);
    }

    static public function DecryptRequestPost($data,$key='')
    {

        return cryptoJsAesDecrypt( $key, $data);
        //return cryptoJsAesDecryptBase64Covered($datastr, $key);
    }

    static public function DecryptRequestGet($encstr,$key='')
    {
        if($key==''){
            return decryptdefault($encstr);
        }
        return decrypt($encstr, $key);
    }

    static public function WriteFile($content,$path,$filename,$ext="txt")
    {
        $fl =   fopen($path."/".$filename . "." . $ext,"w");
        fwrite($fl,$content);
        fclose($fl);
    }
    static public function MakeZipDir($path)
    {
        $zip = new ZipArchive();
        $zip->open("test.zip", ZipArchive::CREATE);
        foreach(glob($path . '/*') as $file){
            $zip->addFile($file);
        }
        $zip->close();/**/
        //shell_exec("zip -r $path $path");

    }
    static public function ConvertImageToBase64($path_to_image,$isremote=false)
    {
        $ext    =   pathinfo($path_to_image, PATHINFO_EXTENSION);
        if($isremote){
            $temppath   =   '../tempfiles/temp'.time().".".$ext;
            copy($path_to_image,$temppath);
            $path=  $temppath;
        } 
		else 
		{
            $path = $path_to_image;
        }
		
        if(file_exists($path)) {
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($path);
			//echo $data."<br/>";
            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
            //unlink($path);
            return $base64;
        }
        return false;
    }

}