<?php
/**
 * PHP version 5.
 
 * @category Include
 
 * @package Sidebar
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description content which need to put it on the sidebar.
 */
?>
<!--  Side block-->
<div class="left-menu-sec">
    <ul class="left-menu">
        <?php
        $folder = ((isset($_SESSION['displayRespUser'])) && ($_SESSION['displayRespUser'] == 1)) ? 'member' : 'coach';
        /* Dynamic menu display begins */
        $sidebarMenu = '';
        $userType = isset($_SESSION['user']['usertype_id']) ? $_SESSION['user']['usertype_id'] : 0;

        global $menuListDynamic;
        if (is_array($menuListDynamic)) {
            foreach ($menuListDynamic as $mnRow) {
                if ($mnRow['parent_id'] == 0 && $mnRow['position'] == 'left') {
                    //To set menu href link
                    $mHref = '#';
                    if ($mnRow['file_name'] != '#') {
                        $mHref = '../'.$folder.'/index.php?p='.$mnRow['file_name'];
                    }
                    $selClass = ($_REQUEST['p'] == $mnRow['file_name']) ? 'active' : '';
                    //To set in visible menu pages parent menu active
                    $subFiles = explode(',', isset($mnRow['sub_file_names']) ? $mnRow['sub_file_names'] : '');
                    if (is_array($subFiles)) {
                        foreach ($subFiles as $fileNames) {
                            if ($_REQUEST['p'] == $fileNames) {
                                $selClass = 'active';
                                break;
                            }
                        }
                    }
                    //To set in visible menu pages parent menu active

                    $sidebarMenu .= <<<SIDEBAR_MENU
                                        <li><a href="{$mHref}" class="{$selClass}">{$mnRow['menu_name']}</a>
SIDEBAR_MENU;

                //Generate sub menus
                    $sMenuFlag = 0;
                    $subMenuHtml = '';
                    $sMenuFlagDisp = 'none';
                    foreach ($menuListDynamic as $subRow) {
                        if ($mnRow['menu_page_id'] == $subRow['parent_id'] && $subRow['position'] == 'left') {
                            //To set menu href link
                            $sMenuFlag = 1;
                            $subDisp = 'none';
                            $mHref = '#';
                            if ($subRow['file_name'] != '#') {
                                $append = '';
                                if ($_SESSION['user']['usertype_id'] == '3') {
                                    $append .= '&id='.$_SESSION['user']['user_id'];
                                }
                           $mHref = '../'.$folder.'/index.php?p='.$subRow['file_name'].$append;
                       }
                       $selClass = ($_REQUEST['p'] == $subRow['file_name']) ? 'active' : '';

                       if ($selClass == 'active') {
                           $sMenuFlagDisp = 'block';
                       }

                       $subMenuHtml .= <<<SIDEBAR_MENU
                                            <li><a href="{$mHref}" class="{$selClass}">{$subRow['menu_name']}</a></li>
SIDEBAR_MENU;
                        }
                    }
                    if ($sMenuFlag == 1) {
                        $marginTopMenu = ($sMenuFlagDisp == 'block') ? 'margin-top: -15px;' : '';
                        $sidebarMenu .= <<<SIDEBAR_MENU
                                    <ul style="display:{$sMenuFlagDisp};{$marginTopMenu}">{$subMenuHtml}</ul>
SIDEBAR_MENU;
                    }
                    //Generate sub menus

                    $sidebarMenu .= <<<SIDEBAR_MENU
                                        </li>
SIDEBAR_MENU;
                }
            }
        }
        echo $sidebarMenu;
        /* Dynamic menu display ends */
        ?>
    </ul>
</div>