<?php
/**
 * PHP version 5.
 
 * @category Include
 
 * @package Header_Content
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description content which need to stay on the header.
 */
?>
<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo TITLE; ?></title>
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
        <!--<link href="<?php echo FOLDER_PATH.CSS_PATH.'common/master.css?version='.MY_VERSION; ?>"
            rel="stylesheet" type="text/css"/>
        <link href="<?php echo FOLDER_PATH.CSS_PATH.'common/jquery.dataTables.css?version='.MY_VERSION; ?>"
            rel="stylesheet" type="text/css"/> -->
        <link href="<?php echo FOLDER_PATH.CSS_PATH.'common/layoutcss.css?version='.MY_VERSION; ?>"
              rel="stylesheet"
              type="text/css"/>
              <link href="<?php echo FOLDER_PATH.CSS_PATH.'jquery.dataTables.min.css';?>"
              rel="stylesheet"
              type="text/css"/>
			  <!--*************for auto search option***********-->
			  <link href="<?php echo FOLDER_PATH.CSS_PATH.'jquery.flexdatalist.min.css';?>"
              rel="stylesheet"
              type="text/css"/><!--**********end************/-->
    <?php
    if (isset($_SESSION['ipadInternalView'])) {
        $inIpadView = 1;
        ?>
    <!--   <link href="<?php echo FOLDER_PATH.CSS_PATH.'common/hideContentTab.css?version='.MY_VERSION;
        ?>" rel="stylesheet" type="text/css"/> -->
    <?php 
    } ?>
        <!-- <link href="<?php echo FOLDER_PATH.CSS_PATH.'common/jqueryUi.css?version='.MY_VERSION; ?>"
            rel="stylesheet" type="text/css"/>
        <link href="<?php echo FOLDER_PATH.CSS_PATH.'common/custom.css?version='.MY_VERSION; ?>"
            rel="stylesheet" type="text/css"/> -->
        <?php
        if (isset($_REQUEST['p']) && !empty($_REQUEST['p'])) {
            $p = $_REQUEST['p'];
            $p = basename($p);
            /*if(!file_exists(FOLDER_PATH . CSS_PATH . 'common' . '/' . $p . '.css')) {

            } else {
                echo '<link href="' . FOLDER_PATH . CSS_PATH . 'common' . '/' . $p . '.css?version=' . MY_VERSION . '"
                    rel="stylesheet" type="text/css"/>';
            }*/
        }

        /* To calculate page load time */
        $logStartTime = microtime(true);
        /* To calculate page load time */
        ?>

        <!-- include  Language File -->
        <?php
        if (isset($_SESSION['language'])) {
            if ($_SESSION['language'] == 'EN') {
                include_once '../lang/en.php';
            }
            if ($_SESSION['language'] == 'FR') {
                include_once '../lang/fr.php';
            }
            if ($_SESSION['language'] == 'NL') {
                include_once '../lang/nl.php';
            }
        } else {
            include_once '../lang/en.php';
        }

        ?>
        <script type="text/javascript" src="../lang/en.js?"></script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <link rel="shortcut icon" href="<?php echo IMG_PATH.DS; ?>fav.ico" />
    <script type="text/javascript" src="<?php
        echo FOLDER_PATH, JS_PATH.'library/jquery-1.9.1.js?version='.MY_VERSION; ?>"></script>
    <script type="text/javascript" src="<?php
        echo FOLDER_PATH, JS_PATH.'library/jqueryUi.js?version='.MY_VERSION; ?>"></script>
    <script type="text/javascript" src="<?php
        echo FOLDER_PATH, JS_PATH.'library/jquery.dataTables.min.js?version='.MY_VERSION; ?>"></script>
    <script type="text/javascript" src="<?php
        echo FOLDER_PATH, JS_PATH.'library/ckeditor/ckeditor.js?version='.MY_VERSION; ?>"></script>
    <script type="text/javascript" src="<?php
        echo FOLDER_PATH, JS_PATH.'common.js?version='.MY_VERSION; ?>"></script>
 <script type="text/javascript" src="<?php echo FOLDER_PATH, JS_PATH.'jquery.dataTables.min.js';?>"></script>
 
        <!--Auto Generate-->
        <?php
        if ($_REQUEST['p'] == 'coachManagementEdit'
            || $_REQUEST['p'] == 'manageMembersEdit'
            || $_REQUEST['p'] == 'lmoManageMember'
        ) {
            ?>
            <script type="text/javascript" src="<?php echo FOLDER_PATH, JS_PATH.'autoComplete.js?version='.MY_VERSION;
            ?>"></script>
        <?php 
        } ?>

        <!--Chart-->
        <?php
        $graphPagesArr = array(
            'testHistoryGraph', 'heartRateGraph', 'fitGraph', 'sportMedGraph_old',
            'evolutionBioMetrics', 'evolutionFitnessLevel', 'bioMetric', 'bodyCompositionTest',
            'dashboard', 'testResultsListDetail', 'managePointsEdit', 'testfollow',
            'memberTestStatus_old', 'heartRateFitGraphZoom'
        );
        if (in_array($_REQUEST['p'], $graphPagesArr)) {
            ?>
            <script src="<?php echo FOLDER_PATH, JS_PATH.'highcharts.js?version='.MY_VERSION;
            ?>"></script>
            <script src="<?php echo FOLDER_PATH, JS_PATH.'highcharts-3d.js?version='.MY_VERSION;
            ?>"></script>
            <script src="<?php echo FOLDER_PATH, JS_PATH.'highcharts-more.js?version='.MY_VERSION;
            ?>"></script>
            <script src="<?php echo FOLDER_PATH, JS_PATH.'highcharts-draggable.js?version='.MY_VERSION;
            ?>"></script>
            <script src="<?php echo FOLDER_PATH, JS_PATH.'regression.js?version='.MY_VERSION;
            ?>"></script>
        <?php 
        } ?>

        <script src="<?php echo FOLDER_PATH, JS_PATH.'mask.js?version='.MY_VERSION; ?>"></script>	
        <?php
        if (isset($_REQUEST['p']) && !empty($_REQUEST['p'])) {
            $p = $_REQUEST['p'];
            $p = basename($p);
            if (file_exists(BASE_DIR.JS_PATH.'/'.$p.'.js')) {
                echo '<script type="text/javascript" src="'.FOLDER_PATH, JS_PATH.'/'.$p.'.js?version='.MY_VERSION.'">
                </script>';
            }
        }
        ?>

    </head>
    <body>
        <div id="customPopoverlay" style="min-height: 100%; width: 100%;"></div>
        <div id="customPopbox">
            <div class="customPopcontent">
                <div id="customPopmessage"></div>
                <div style="clear:both"></div>
            </div>
        </div>

        <!-- Page ajax loader -->
        <div id="pageLoader"></div>
        <div id="pageLoaderLightBox"></div>
        <!---->

        <div class="pagewrapper">
            <!-- validation alert message box begins -->
                <div class="com-alert-msg">
                    <h2>Required Result <a class="icon icon-popupcls" href="javascript:void('0')">close</a></h2>
                    <div class="alert-content">
                    </div>
                </div>
                <div class="popup-mask popup-mask-alert"></div>
            <!-- validation alert message box ends -->
            <div id="flashMsg"></div>
            <div class="header">
                <div class="head-left">
                    <div class="logo">
                        <img src="<?php echo IMG_PATH.DS; ?>logo.png" width="135" height="40" alt="logo" />
                    </div>
                    <button class="nav-toggle" type="button">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <!-- Show user club name -->

                <?php
                if (isset($_SESSION['currentClubName']) && !(isset($_SESSION['showCommonAccess']))) {
                    echo '<div class="header-club-name-disp">';
                    echo $_SESSION['currentClubName'].' ('.ucfirst($_SESSION['currentClubRole']).')';
                    echo '</div>';
                }

                /* get menus */
                $paramMenu['isVisible'] = 1;
                if (USER_RIGHTS_CHECK_FOR_APPLICATION == 1) {
                    $paramMenu['isVisibleUser'] = 1;
                    $paramMenu['userTypeId'] = isset(
                        $_SESSION['user']['usertype_id']
                    ) ? $_SESSION['user']['usertype_id'] : 0;
                }
                $menuListDynamic = $this->settings->getMenuPages($paramMenu);
                /* get menus */

                if (isset($_SESSION['associatedClubIds'])) {
                    $clubMultipleExist = explode(',', $_SESSION['associatedClubIds']);
                    $showChangeClubPop = (count($clubMultipleExist) > 1) ? 1 : 0;
                    $_SESSION['showChangeClub'] = $showChangeClubPop;
                }
            ?>
                <!-- Show user club name -->
                <?php require_once 'menu.php'; ?>
            </div>
            <?php require_once 'sidebar.php'; ?>

            <!-- include quick popup page-->
            <?php require_once 'quickAddPopup.php'; ?>
            <!-- include quick popup page-->

            
            <?php
            /* show change centre popup if more then 1 club */
            if ((isset($_SESSION['showChangeClub'])) && ($_SESSION['showChangeClub'] == 1)) {
                include_once 'changeClubPopup.php';
            }
            /* show change centre popup if more then 1 club */

            //To construct previous page, 
            if (isset($_SERVER['HTTP_REFERER'])) {
                $historyPg = explode('p=', $_SERVER['HTTP_REFERER']);
                $historyPg = explode('&', isset($historyPg[1]) ? $historyPg[1] : '');
                $_SESSION['HISTROY_P'] = $historyPg[0];

                if ($_SESSION['HISTROY_P'] != $_REQUEST['p']) {
                    //checking for, if navigate same page will traverse
                    $_SESSION['HISTROY_PREVIOUS'] = $_SERVER['HTTP_REFERER'];
                }
            }
            ?>