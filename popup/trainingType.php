<?php	
/**
 * Author        : Punitha Subramani
 * Since         : 9-Dec
 * Modified By   : Punitha Subramani
 * Modified Date : 9-Dec
 * Description   : Training type for SportMed Graph Page.
 **/
require '../lang/en.php';
?>
<script>

var currentYear = new Date().getFullYear() + 1;
$("#trainingDate").datepicker({
    dateFormat: 'dd-mm-yy',
    changeMonth: true,
    changeYear: true,
    yearRange: '1960:' + currentYear
});

var training_type_parent=$(".training_type:checked").val();
$('.training_type_pop').each(function(){
    if($(this).val()==training_type_parent){
        $(this).attr("checked","checked");
    }
});
function nextTrainingProgram(hideContent,showContent){
    if(hideContent == 'thirdBox') {
        var activityCheckedCnt = $('.activity_lists:checked').length;
        var $ajaxmsg=$(".ajaxMsg");
        $ajaxmsg.hide();
        if(activityCheckedCnt < 1) {
            $ajaxmsg.html('Choose some activity and proceed').show();
            return false;
        }
    }
    $("#"+hideContent).hide();
    $("#"+showContent).show();
}
</script>
<div class="col10" id="firstBox" style="display:none">
    <div class="row-sec pad5">
        <div class="ajaxMsg" style="display:none;"></div>
    </div>
    <div class="row-sec">
        <div>
            <div class="in-cell cus-check get-check">
                <input type="radio" class="oxy training_type_pop" value="1" name="trainingType" id="endurance" />
                <label for="endurance">Focus On Endurance</label>
            </div>
            <div class="in-cell cus-check get-check">
                <input type="radio" class="oxy training_type_pop" value="0" name="trainingType" id="slimfit" />
                <label for="slimfit">Focus On "Slim and Fit"</label>
            </div>
       </div>
    </div>
    <div class="row-sec btn-sec">
        <input type="button" onclick="nextTrainingProgram('firstBox','secondBox');"
               class="btn black-btn fr" value="Next>>">
    </div>
</div>

<div class="col10" id="secondBox" style="display:none">
    <div class="row-sec pad5">
    <div class="ajaxMsg"></div>
</div>
<div class="row-sec">
    <div>
        <div class="in-cell cus-check get-check">
            <input type="radio" class="oxy" value="1" name="weeks_points_to_improve" id="improve" checked />
            <label for="improve">Week Point Target is to improve FL</label>
        </div>
        <div class="in-cell cus-check get-check">
            <input type="radio" class="oxy" value="0" name="weeks_points_to_improve" id="actual" />
            <label for="actual">Week Point Target is to keep actual FL</label>
        </div>
    </div>
</div>
<div class="row-sec btn-sec">
    <input type="button" onclick="nextTrainingProgram('secondBox','thirdBox');" class="btn black-btn fr" value="Next>>">
</div>
</div>

<div class="col10" id="thirdBox" style="display:none">
    <div class="row-sec pad5">
        <div class="ajaxMsg"></div>
    </div>
    <div style="height:300px; width:340px; overflow-y:scroll;border:1px solid #ccc;margin:0 0 5px 0;">
        <div class="row-sec">
            <div>
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                <?php
                    // Get the Master Strength Machine Lists
                    $params['is_deleted'] = 0;
                    $activitiesList = $this->admin->getActivities($params);

                    $param['userId'] = $_REQUEST['editId'];
                    $memberBasicDetail = $this->members->getMemberBasicDetail($param);

                    // Get the Master Strength Machine Clubs
                    $paramMachine['clubId'] = $memberBasicDetail['r_club_id'];
                    $activitiesClubLists = $this->admin->getActivitiesClub($paramMachine);
    $activitiesClubs    =   array();
    foreach ($activitiesClubLists as $activitiesClubList) {
        $activitiesClubs[] = $activitiesClubList['r_activity_id'];
    }
    foreach ($activitiesList as $activitiesLists) {
        $checked = '';
        if (in_array($activitiesLists['activity_id'], $activitiesClubs)) {
            $checked = ' checked="checked"';
        }?>
                    <tr>
                        <td>
                            <div class="in-cell cus-check fl activity_row_popup">
                                <input type="checkbox"
                                       class="oxy activity_lists"
                                       value="<?php echo $activitiesLists['activity_id']?>"
                                       name="activity_lists[]"
                                       id="activity_<?php echo $activitiesLists['activity_name']?>"
                                        <?php echo $checked?> title="<?php echo $activitiesLists['activity_name']?>">
                                <label for="activity_'<?php echo $activitiesLists['activity_name'];?>">
                                    <?php echo $activitiesLists['activity_name'];?>
                                </label>
                            </div>
                        </td>
                    </tr>';
    <?php } ?>
                </table>
            </div>
        </div>
    </div>
    <div class="row-sec btn-sec">
        <input type="button" onclick="nextTrainingProgram('thirdBox','fourthBox');"
               class="btn black-btn fr" value="Next>>">
    </div>
</div>

<div class="col10" id="fourthBox" ><!--style="display:none"-->
    <div class="row-sec pad5">
        <div class="ajaxMsg"></div>
    </div>
    <div class="row-sec">
        <div>
            <div class="in-cell cus-check get-check" onclick="nextTrainingProgram('trainingDate','startTraining');">
                <input type="radio" class="oxy" value="0" name="start_training_on_date" id="duringDate" checked />
                <label for="duringDate">Start Training During Checking</label>
                <!--<div>
                    <br/>
                    <input type="text" name="" id="startTraining" />
                </div>-->
            </div>
            <div class="in-cell cus-check get-check"
                 onclick="nextTrainingProgram('startTraining','trainingDate');">
                <input type="radio" class="oxy" value="1" name="start_training_on_date" id="onDate">
                <label for="onDate">Start Training on date</label>
                <div>
                    <br/>
                    <input type="text" name="training_date" id="trainingDate" title="Training Date"
                           class="icon-datepiker"
                           value="<?php echo date('d-m-Y'); ?>"
                           style="display:none" />
                </div>
            </div>
       </div>
    </div>
    <div class="row-sec btn-sec">
        <input type="button" class="btn black-btn fr" value="Complete" onclick="sportMedSave()" />
    </div>
</div>