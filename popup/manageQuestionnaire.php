<?php
    /**
     * Author        : Nesarajan M
     * Since         : 15-Oct-2014
     * Modified By   :
     * Modified Date :
     * Description   : Add/Edit menu popup page.
     **/
    require '../lang/en.php';
    global $LANG;
    // if(isset($editId)) {
        // $param['questionnaireId'] = $editId;
        // $getQuestionnaireDetail = $this->settings->getQuestionnaireDetail($param);
        // $row = $getQuestionnaireDetail['getQuestionnaireDetail'];
    // }

    //To get menus list
    $questionnaires = $this->settings->getquestionnaire();
    $groups = $this->settings->getQuesGroup();
    $phases = $this->settings->getQuesPhase();
    $quesactivity = $this->settings->getQuesActivity();
    $grouplist = isset($groups['getGroupDetail']) ? $groups['getGroupDetail'] : array();
    $phaseslist = isset($phases['getPhaseDetail']) ? $phases['getPhaseDetail'] : array();
    $quesactivitylist = isset($quesactivity['getQuesActivity']) ? $quesactivity['getQuesActivity'] : array();
?>
<div class="acc-row add-menu">
    <div class="ajaxMsg" style="display:none;"></div>
    <div class="acc-content">
        <div class="row-sec mb15">
            <div class="col5">
                <label class="fl" for="phase_name">
                    Phase Name<span class="required">*</span>
                </label>
                <select name='phase_name' id='phase_name' class='phase_name'>
                    <option value="0">Select Phase</option>
        <?php
        foreach ($phaseslist as $phase) {
            ?>
                        <option value="<?=$phase['phase_id']?>"><?=$phase['phase_name']?></option>
                <?php
        } ?>
                </select>
            </div>
        </div>
        <div class="row-sec mb15">
            <div class="col5">
                <label class="fl" for="group_name">
                    Group Name<span class="required">*</span>
                </label>
                <select name='group_name' id='group_name' class="group_name">
                    <option value="0">Select Group</option>
        <?php
        foreach ($grouplist as $group) { ?>
                    <option value="<?php echo $group['group_id']?>">
                        <?php echo $group['group_name']?>
                    </option>
        <?php  } ?>
                </select>
            </div>
        </div>
        <div class="row-sec mb15">
            <div class="row-sec mb15">
                <label class="fl" for="questopic">Topic<span class="required">*</span></label>
                <input type="text" id="questopic" value="">
            </div>
        </div>
            <div class="row-sec mb15 activityques" style="display:none">
                <div class="col5">
                    <label class="fl" for="activity_name">
                        Activity Question<span class="required">*</span>
                    </label>
                    <select name='activity_name' id='activity_name' class='activity_name'>
                        <option value="0">Select</option>
        <?php
        foreach ($quesactivitylist as $activiy) {
            ?>
                <option value="<?php echo $activiy['activity_id']?>">
                    <?php echo $activiy['activity_name']?>
                </option>
        <?php
        } ?>
                    </select>
                </div>
            </div>
            <div class="questiontext" style="display:none">
                <div class="row-sec mb15 " >
                    <div class="row-sec mb15">
                        <label class="fl">Question Text<span class="required">*</span></label>
                        <input type="text" id="questtxt" value="" placeholder="Question in English">
                    </div>
                    <div class="row-sec mb15">
                        <!-- <label class="fl"></label> -->
                        <input type="text" id="questtxt" value="" placeholder="Question in English">
                    </div>
                    <div class="row-sec mb15">
                        <!-- <label class="fl"></label> -->
                        <input type="text" id="questtxt" value="" placeholder="Question in English">
                    </div>
                    <span class="addquescnt" onclick="addQuesContent()">+</span>
                </div>
                <div class="row-sec mb15">
                <label class="fl" for="questype">Question Type<span class="required">*</span></label>
                <select name='questype' id='questype' class='questype'>
                    <option value="0">Select</option>
                    <option value="1">Number</option>
                    <option value="2">Radio button</option>
                    <option value="3">Slider</option>
                </select>
            </div>
            </div>
            <div class="row-sec mb15">
                <label class="fl" for="questype">Question Type<span class="required">*</span></label>
                <select name='questype' id='questype' class='questype'>
                    <option value="0">Select</option>
                    <option value="1">Number</option>
                    <option value="2">Radio button</option>
                    <option value="3">Slider</option>
                </select>
            </div>
            <div class="row-sec mb15">
                <div class="row-sec mb15">
                    <label class="fl" for="quesval">Vaule<span class="required">*</span></label>
                    <input type="text" id="quesval" value="" />
                </div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
    <div class="row-sec btn-sec">
        <input type="hidden" name="questionnaireId" id="questionnaireId" value="">
        <input type="hidden" name="quesId" id="quesId" value="">
        <input type="button" class="pop_cancel_btn btn black-btn fr" value="<?php echo $LANG['btnCancel'];?>">
        <input type="button" onclick="saveQuesttion()"class="btn black-btn fr" value="<?php echo $LANG['btnSave'];?>">
    </div>
