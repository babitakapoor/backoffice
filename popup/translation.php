
<?php
    /**
     * Author        : Nesarajan M
     * Since         : 15-Oct-2014
     * Modified By   : 
     * Modified Date : 
     * Description   : Add/Edit menu popup page.
     **/
    require '../lang/en.php';
    $param['menuPageId'] = '';
    global $LANG;
if (isset($editId)) {
    $param['menuPageId'] = $editId;
    $getMenuPageDetail = $this->settings->getMenuPageDetail($param);
    $row = $getMenuPageDetail['getMenuPageDetail'];
}
//To get language list
/*PK Add 2016/02/04*/
$languagetype = $this->settings->getLanguageActiveType();
$languagelistrows = isset($languagetype['languageTypeDetails']) ? $languagetype['languageTypeDetails'] : array();
if ($languagetype['total_records'] == 1) {
    $languagelistrows = array($languagelistrows);
}
//To get translationkey list
/*PK Add 2016/02/04*/
$translationkey = $this->settings->getTranslationkeyType();
$translationkeyrows = (
    isset($translationkey['translationkeyTypeDetails'])
) ? $translationkey['translationkeyTypeDetails'] : array();
if ($translationkey['total_records'] == 1) {
    $translationkeyrows = array($translationkeyrows);
}
?>
<div class="acc-row add-menu">
    <div class="ajaxMsg" style="display:none;"></div>
    <form method="post" action = "<?php $_PHP_SELF ?>" >
        <div class="acc-content">
                <div class="row-sec mb15">
                    <div class="col5">
                        <label class="fl" for="manageMenuLanguageTypeId">
                            Language :<span class="required">*</span>
                        </label>
                        <select id="manageMenuLanguageTypeId" name="menuLanguageTypeId" >
                            <option value="">--<?php echo $LANG['select']; ?>--</option>

        <?php
        foreach ($languagelistrows as $row) {
            echo "<option value='".$row['language_id']."' >".ucfirst(strtolower($row['title'])).'</option>';
        }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="row-sec mb15">
                    <div class="col5">
                        <label class="fl" for="manageMenutranslationkeyTypeId">
                            Key :<span class="required">*</span>
                        </label>
                        <select id="manageMenutranslationkeyTypeId" name="menuTranslationkeyTypeId" >
                            <option value="">--<?php echo $LANG['select']; ?>--</option>
        <?php
        foreach ($translationkeyrows as $row) { ?>
            <option value="<?php echo $row['translationkey_id']?>" >
                <?php echo ucfirst(strtolower($row['transaction_key_desc']))?>
            </option>
        <?php } ?>
                        </select>
						
                    </div>
					<!--sdshdhsfhsd-->
	<!--input type="text" name="key" id="key" onkeyup="autocomplete(this)"/-->

					<!--dhghd-->
					
                </div>
                <div class="row-sec mb15">
                        <div class="col5">
                            <label class="fl">Context :<span class="required">*</span></label>
                            <input type="text"
                                   id="menuDisplayName" name="menuDisplayName"
                                   class="form-control" placeholder="context"
                                   value="" />
                        </div>
                </div>
                <div class="clear"></div>
                <div class="row-sec btn-sec">
                    <input type="hidden" name="languageTypeId" id="menuPageId"
                           value="<?php echo $param['menuPageId']; ?>">
                    <input type="button"
                           class="pop_cancel_btn btn black-btn fr" value="<?php echo $LANG['btnCancel'];?>">
                    <input type="button" onclick="saveLanguageTrnslation('<?php echo $param['menuPageId']; ?>')"
                           class="btn black-btn fr" value="<?php echo $LANG['btnSave'];?>">
                </div>
        </div>
    </form>
    </div>
<!-- Add menu popup -->

<script>
/* function autocomplete(string){
	alert("hello");
  die;
   // $.ajax({url: "../ajax/index.php?p=translation",
	type: "POST",
	data: 'keyid=' + string,	
	success: function(result){
       // $("#div1").html(result);
    }});
} */
</script>
  