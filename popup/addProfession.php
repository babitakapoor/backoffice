<?php
/**
 * Author        : Punitha Subramani
 * Since         : 13-Oct-2014
 * Modified By   : Punitha Subramani
 * Modified Date : 14-Oct-2014
 * Description   : Quick add profession page.
 **/
global $LANG;
?>
<div class="col10">
    <div class="row-sec pad5">
        <div class="ajaxMsg"></div>
    </div>
    <div class="row-sec pad10">
        <div class="col5">
            <label class="fl" for="professionName">
                <?php echo $LANG['professionName'];?> :<span class="required">*</span>
            </label>
            <input type="text" class="form-control" name="profession" id="professionName" />
        </div>
    </div>
    <div class="row-sec btn-sec">
        <input type="button" class="pop_cancel_btn btn black-btn fr"
               value="<?php echo $LANG['cancel']; ?>" />
        <input type="button" onclick="quickAddProfession();"
               class="btn black-btn fr" value="<?php echo $LANG['save']; ?>" />
    </div>
</div>