<?php
    /**
     * Author        : Nesarajan M
     * Since         : 15-Oct-2014
     * Modified By   : 
     * Modified Date : 
     * Description   : Add/Edit menu popup page.
     **/
    require '../lang/en.php';
    global $LANG;
?>
<div class="acc-row add-menu">
    <div class="acc-content">
        <div class="row-sec mb15">
            <div class="col5">
                Decide how you want to determine the workload for the program
            </div>
        </div>
        <div class="row-sec btn-sec">
            <input type="button" onclick=""
                   id="enter_value_manually"
                   class="btn black-btn fr" value="<?php echo $LANG['enterValuesManually'];?>">
            <!--Later Onclick:decideWorkloadProgram(2);-->
            <input type="button" onclick=""
                   id="calculate_max_strength"
                   class="btn black-btn fr"
                   value="<?php echo $LANG['calculateMaxStrength'];?>">
            <!--Later Onclick:decideWorkloadProgram(1);-->
        </div>
    </div>
</div>
<!-- Add/Edit menu popup -->