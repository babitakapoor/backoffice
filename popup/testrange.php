<?php
include("../lang/en.php");
global $LANG;
    $languages = $this->settings->getLanguageActiveType();
    $languageactivelist = isset($languages['languageTypeDetails']) ? $languages['languageTypeDetails']:array();
    if($languages['total_records']==1){
        $languageactivelist	=	array($languageactivelist);
    }
    //$dataresult = $this->settings->getGroupTopics($params);
?>
<script>
    $(".clshtmlcontentbox").each(function(idx, obj){
        var attrid  =   $(obj).attr("id");
        var ins =   CKEDITOR.instances[attrid];
        if(ins){
            CKEDITOR.remove(ins);
        }
        CKEDITOR.replace(attrid);
    });
</script>
<div class="row-sec mb15">
    <div class="row-sec mb15">
        <div class="row-sec mb15">
            <label class="fl">Range Based Text</label>
        </div>
    </div>
    <div class="row-sec mb15" >
        <input type="text" id="minrangepts" class="three_fields"
               value="" placeholder="Min Range" disabled  title="Minimum Range"/>
        <input type="text" id="maxrangepts" class="three_fields"
               value="" placeholder="Max Range" disabled title="Maximum Range"/>
        <select id="selectopic" name="selectopic" title="Select Topic">

        </select>
    </div>
    <!--<div class="row-sec mb15">
        <div class="row-sec mb15">
            <label class="fl">Sucess Text<span class="required">*</span></label>
            <textarea class="sucesstxt text_type_1" placeholder="Sucess Text  in English" languageid="9"></textarea>
        </div>
        <div class="row-sec mb15">
            <label class="fl">&nbsp;</label>
            <textarea class="sucesstxt text_type_1" placeholder="Sucess Text  in russian" languageid="10"></textarea>
        </div>
        <div class="row-sec mb15">
            <label class="fl">&nbsp;</label>
            <textarea class="sucesstxt text_type_1" placeholder="Sucess Text in dutch" languageid="11"></textarea>
        </div>
    </div>-->
    <div class="row-sec mb15">
        <?php $i=0;
        foreach($languageactivelist as $lang){
            $sucesstxtlabel = ($i==0) ?'Sucess Text':'&nbsp;';
        ?>
            <label class="fl">Sucess Text<span class="required">*</span></label>
            <div class="clear" ></div>
            <div class="row-sec mb15">
                <!--<label class="fl"><?=$sucesstxtlabel;?></label>-->
                <textarea name="editor1" id="text_type1_<?php echo $lang['language_id']?>"
                          class="clshtmlcontentbox sucesstxt text_type_1"
                          placeholder="Sucess Text in <?php echo $lang['title']?>"
                          languageid="<?php echo $lang['language_id']?>"></textarea>
            </div>
            <?  $i=$i+1;
        } ?>
    </div>
    <div class="row-sec mb15">
        <?php $i=0;
        foreach($languageactivelist as $lang){
            $quickwintxtlabel = ($i==0) ?'Quick Win Text':'&nbsp;';
        ?>
            <label class="fl">Quick Win Text<span class="required">*</span></label>
            <div class="clear" ></div>
            <div class="row-sec mb15">
                <!--<label class="fl"><?=$quickwintxtlabel;?></label>-->
                <textarea name="editor1" id="text_type2_<?php echo $lang['language_id']?>"
                          class="clshtmlcontentbox quickwintxt text_type_2"
                          placeholder="QuickWin Text in <?php echo $lang['title']?>"
                          languageid="<?php echo $lang['language_id']?>"></textarea>
            </div>
            <?  $i=$i+1;
        } ?>
    </div>
    <div class="row-sec mb15">
        <?php $i=0;
        foreach($languageactivelist as $lang){
            $goaltxtlabel = ($i==0) ?'Goal Text':'&nbsp;';
        ?>
            <label class="fl">Goal Text<span class="required">*</span></label>
            <div class="clear" ></div>
            <div class="row-sec mb15">
                <!--<label class="fl"><?=$goaltxtlabel;?></label>-->
                <textarea name="editor1" id="text_type3_<?=$lang['language_id']?>"
                          class="clshtmlcontentbox goaltxt text_type_3"
                          placeholder="Goal Text in <?=$lang['title']?>"
                          languageid="<?=$lang['language_id']?>"></textarea>
            </div>
            <?  $i=$i+1;
        } ?>
    </div>
    <div class="row-sec mb15">
        <?php $i=0;
        foreach($languageactivelist as $lang){
            $advicetxtlabel = ($i==0) ?'Advice Text':'&nbsp;';
        ?>
            <label class="fl">Advice Text<span class="required">*</span></label>
            <div class="clear" ></div>
            <div class="row-sec mb15">
                <!--<label class="fl"><?=$advicetxtlabel;?></label>-->
                <textarea  name="editor1" id="text_type4_<?=$lang['language_id']?>"
                           class="clshtmlcontentbox reducetxt text_type_4"
                           placeholder="Advice Text in <?=$lang['title']?>"
                           languageid="<?=$lang['language_id']?>">
                </textarea>
            </div>
            <?  $i=$i+1;
        } ?>
    </div>
</div>
<div class="clear"></div>
<div class="row-sec btn-sec">
    <input type="hidden" id="groupid" value="">
    <input type="hidden" id="phaseid" value="">
    <input type="hidden" id="rangeid" value="">
    <input type="button" class="pop_cancel_btn btn black-btn fr" value="<?php echo $LANG['btnCancel'];?>">
    <input type="button" onclick="manageTextRange()"class="btn black-btn fr" value="<?php echo $LANG['btnSave'];?>">
</div>