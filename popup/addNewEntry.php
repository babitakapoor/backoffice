<?php
    /**
     * Author        : Nesarajan M
     * Since         : 15-Oct-2014
     * Modified By   : 
     * Modified Date : 
     * Description   : Add/Edit menu popup page.
     **/
require '../lang/en.php';

?>
<style>
	.add_new_entry label{width:200px !important}
	.add_new_entry{
		float: left;
		margin-left: 200px;
		padding: 100px;
		padding-left: 50px;
		padding-right: 153px;
		padding-top: 0;
		width: 600px;
	}
</style>
<div class="acc-row add-menu add_new_entry">
    <div class="ajaxMsg" style="display:none;"></div>
    <form method="post" action = "<?php $_PHP_SELF ?>" >
        <div class="acc-content">
			<div class="row-sec mb15">
				<div class="col5">
					<label class="fl" for="manageMenuLanguageTypeId">
						Select Translation Key :<span class="required">*</span>
					</label>
					<select name="translation_key" id="translation_key">
						<option value="">Select Key</option>
						<option value="BTN">Button</option>
						<option value="LBT">LBT</option>
						<option value="LBL">Label</option>
						<option value="PAGE">Page</option>
						<option value="TEXT">Text</option>
					</select>
				</div>
			</div>
			<div class="row-sec mb15">
				<div class="col5">
					<label class="fl" for="manageMenuLanguageTypeId">
						Translation description :<span class="required">*</span>
					</label>
					<input type="text" name="translation_key_description" id="translation_key_description" required/>
				</div>
			</div>
			<div class="clear"></div>
			<div class="row-sec btn-sec">
				<input type="button" class="pop_cancel_btn btn black-btn fr" value="<?php echo $LANG['btnCancel'];?>">
				<input type="button" onclick="saveLanguageTrnslationNewEntry()" class="btn black-btn fr" value="<?php echo $LANG['btnSave'];?>">
			</div>
        </div>
    </form>
    </div>
<!-- Add menu popup -->
