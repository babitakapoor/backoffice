<?php
    /**
     * Author        : Nesarajan M
     * Since         : 15-Oct-2014
     * Modified By   :
     * Modified Date :
     * Description   : Add/Edit menu popup page.
     **/
    require '../lang/en.php';
global $LANG;
if(!isset($param)){
    $param  =array();
}
$param['companyId'] = '';
if (isset($editId)) {
    $param['companyId'] = $editId;
    $getCompanyDetail = $this->settings->getCompanyDetail($param);
	//print_r($getCompanyDetail);
    $row = $getCompanyDetail['getCompanyDetail'];
}
//To get menus list
$brands = $this->settings->getBrand();
?>
<div class="acc-row add-menu">
    <div class="ajaxMsg" style="display:none;"></div>
    <div class="acc-content">
        <div class="row-sec mb15">
            <label class="fl" for="companyName">
                Company Name :<span class="required">*</span>
            </label>
            <input type="text" id="companyName" name="companyName" class="form-control"
                   placeholder="Company Name"
                   value="<?php echo (isset($row['company_name'])) ? $row['company_name'] : ''; ?>"  />
        </div>
    </div>
    <div class="acc-content">
        <div class="col5">
            <label class="fl">
                Company address :<span class="required">*</span>
            </label>
            <input type="text" id="companyaddress"
                   name="companyaddress" class="form-control" placeholder="Company Address"
                   value="<?php echo (isset($row['company_address'])) ? $row['company_address'] : ''; ?>"  />
        </div>
    </div>
	<input type="hidden" id="createdDate" name="createdDate" value="<?php echo date('Y-m-d h:i:s');?>"  />
	<input type="hidden" id="modifiedDate" name="modifiedDate" value="<?php echo date('Y-m-d h:i:s');?>"  />
    <div class="clear"></div>
    <div class="row-sec btn-sec">
        <input type="hidden" name="companyId"
               id="companyId" value="<?php echo $param['companyId']; ?>">
        <input type="button" class="pop_cancel_btn btn black-btn fr"
               value="<?php echo $LANG['btnCancel'];?>">
        <input type="button" onclick="saveManageCompany('<?php echo $param['companyId']; ?>');"
               class="btn black-btn fr" value="<?php echo $LANG['btnSave'];?>">
    </div>
</div>
<script>
/************21july2017*******************/
function saveManageCompany(id){
    var companyName=$("#companyName").val();
    var companyAddress=$("#companyaddress").val();
	var createdDate=$("#createdDate").val();
	var modifiedDate=$("#modifiedDate").val();
   // var loggedUserId=$(".loggedUserId").val();
	if(companyName==''){
		alert("Please fill Company name");
	}
	else if(companyAddress==''){
		alert("please fill company address");
		
	}
	else{
    $.ajax({
         url: '../ajax/index.php?p=settings',
         type: "POST",
         data:{
             'action':'saveCompanyType',
             companyName:companyName,
             companyAddress:companyAddress,
             createdDate:createdDate,modifiedDate:modifiedDate,'id':id
         },
         success: function(data) {
			//response
         var response = JSON.parse(data); 
			 var code  = response.status_code;
			if(code && code==1){  
			ajLoaderOff();
            flashMsgDisplay('Insert successfully', 'success-msg');			
			 //alert('Insert successfully');
			}
			else if(code && code==2)
				{
				ajLoaderOff();
			// alert('Already Exist');
			flashMsgDisplay('Already Exist', 'success-msg');	
			}
			$(".popup-holder").hide();
         $(".popup-mask").hide();
            setTimeout(function() {
            location.reload();
        }, 5000);
        }
     });
	}
}
</script>