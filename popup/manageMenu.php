<?php
    /**
     * Author        : Nesarajan M
     * Since         : 15-Oct-2014
     * Modified By   : 
     * Modified Date : 
     * Description   : Add/Edit menu popup page.
     **/
require '../lang/en.php';
global $LANG;
$param['menuPageId'] = '';
$selected = $selected1 = '';
if (isset($editId)) {
    $param['menuPageId'] = $editId;
    $getMenuPageDetail = $this->settings->getMenuPageDetail($param);
    $row = $getMenuPageDetail['getMenuPageDetail'];
    if ($row > 0) {
        $selected = $selected1 = '';
        if ($row['menu_type'] == 2) {
            $selected = 'selected';
        } else {
            $selected1 = 'selected';
        }
    }
}
//To get menus list
$menus = $this->settings->getParentMenus();
?>
<div class="acc-row add-menu">
    <div class="ajaxMsg" style="display:none;"></div>
    <div class="acc-content">
            <div class="row-sec mb15">
                    <div class="col5">
                        <label class="fl">
                            <?php echo $LANG['menuDisplayName']; ?> :<span class="required">*</span>
                        </label>
                        <input type="text" id="menuDisplayName" name="menuDisplayName"
                               class="form-control" placeholder="menu Display Name"
                        value="<?php echo (isset($row['name'])) ? $row['name'] : ''; ?>"  />
                    </div>
                </div>
            <div class="row-sec mb15">
                    <div class="col5">
                        <label class="fl">
                            <?php echo $LANG['menuName']; ?> :<span class="required">*</span>
                        </label>
                        <input type="text" id="menuName"
                               name="menuName" class="form-control" placeholder="Menu Name"
                               value="<?php echo (isset($row['menu_name'])) ? $row['menu_name'] : ''; ?>"  />
                    </div>
                </div>

        <?php
                    $parentDisp = 'none';
    if (isset($row['parent_id']) && $row['parent_id'] > 0) {
        $parentDisp = 'block';
    }
                ?>
        <div class="row-sec mb15">
                   <div class="">
                <label class="fl"><?php echo $LANG['isParent'];?>  :</label>
                <div class="input-group">
                  <div class="in-cell cus-check">
                    <input type="radio" class="oxy" value="1" name="isParent" id="is_parent_yes" 
        <?php echo (isset($row['parent_id']) && $row['parent_id'] == 0) ? 'checked="checked"' : ''; ?> >
                    <label for="is_parent_yes"><?php echo $LANG['yes'];?></label>
                  </div>
                  <div class="in-cell cus-check">
                    <input type="radio" class="oxy" value="0" name="isParent" id="is_parent_no" 
        <?php echo (isset($row['parent_id']) && $row['parent_id'] > 0) ? 'checked="checked"' : ''; ?> >
                    <label for="is_parent_no"><?php echo $LANG['no'];?> </label>
                  </div>
                </div>
              </div>
            </div>


                <div class="row-sec" style="display:<?php echo $parentDisp; ?>;" id="parentMenuContainer" >
                    <div class="col5">
                        <label class="fl" for="parentId">
                            <?php echo $LANG['parentMenu'];?>:
                        </label>
                        <div class="select-custom">
                            <select name="parentId" id="parentId">
                                <option value="0">--SELECT--</option>
                                <?php
        if (is_array($menus)) {
            foreach ($menus as $menu) {
                $sel = '';
                if(isset($row['parent_id']) && $row['parent_id'] == $menu['menu_page_id']) {
                    $sel = 'selected="selected"';
                } ?>
                <option value="<?php echo $menu['menu_page_id']?>" <?php echo $sel;?>>
                    <?php echo $menu['name']?>
                </option>
            <?php }
        } ?>
                            </select>
                        </div>
                    </div>
                </div>

            <div class="row-sec mb15">
            <label class="fl" for="menu_type">Menu Type<span class="required">*</span></label>
            <select name="menu_type" id="menu_type">
                <option value="">--<?php echo $LANG['select']; ?>--</option>
                <option value="1" <?php echo $selected1;?>>admin</option>
                <option value="2" <?php echo $selected;?>>app</option>
            </select>
            </div>

            <div class="row-sec mb15">
                   <div class="">
                <label class="fl"><?php echo $LANG['position'];?>:</label>
                <div class="input-group">
                  <div class="in-cell cus-check">
                    <input type="radio" class="oxy" value="top" name="position" id="position_top" 
        <?php echo (isset($row['position']) && $row['position'] == 'top') ? 'checked="checked"' : ''; ?> >
                    <label for="position_top"><?php echo $LANG['top'];?></label>
                  </div>
                  <div class="in-cell cus-check">
                    <input type="radio" class="oxy" value="left" name="position" id="position_left"
        <?php echo (isset($row['position']) && $row['position'] == 'left') ? 'checked="checked"' : ''; ?> >
                    <label for="position_left"><?php echo $LANG['left'];?></label>
                  </div>
                </div>
              </div>
            </div>
            <div class="row-sec mb15">
                   <div class="">
                <label class="fl"><?php echo $LANG['isVisible'];?>:</label>
                <div class="input-group">
                  <div class="in-cell cus-check">
                    <input type="radio" class="oxy" value="1" name="isVisible" id="is_visible_yes"
                           checked="checked" >
                    <label for="is_visible_yes"><?php echo $LANG['yes'];?></label>
                  </div>
                  <div class="in-cell cus-check">
                    <input type="radio" class="oxy" value="0" name="isVisible" id="is_visible_no"
        <?php echo (isset($row['is_visible']) && $row['is_visible'] == 0) ? 'checked="checked"' : ''; ?> >
                    <label for="is_visible_no"><?php echo $LANG['no'];?></label>
                  </div>
                </div>
              </div>
            </div>
                <div class="row-se">
                    <label class="fl"><?php echo $LANG['menuOrder'];?> :<span class="required">*</span></label>
                    <input type="text" class="form-control" placeholder="Menu Order" id="menuOrder" name="menuOrder"
                    value="<?php echo (isset($row['menu_order'])) ? $row['menu_order'] : ''; ?>" >
            </div>
            <div class="row-sec pad10">
                <div class="col5">
                    <label class="fl">
                        <?php echo $LANG['fileName'];?> :<span class="required">*</span></label>
                    <input type="text" class="form-control"
                           placeholder="File Name" id="fileName" name="fileName"
                           value="<?php echo (isset($row['file_name'])) ? $row['file_name'] : ''; ?>" >
                </div>
            </div>
            <div class="clear"></div>
            <div class="row-sec mb15">
    <div class="">
                <div class="user-photo-holder dotline-sepl">
                    <div class="photo-cls" style="display:block">
                        <a id="delete-member-image"
                           class="delete-image"
                           data-role-deleteimg="user-up-photo.jpg" data-role-defpath="" style="">
                    </div>
                    <div class="user-up-photo">
                        <img id="imagePreview"
                             src="<?php echo isset($row['icon']) ? SERVICE_MENU_ICON.$row['icon'] : '';?>"
                             width="146" height="128" alt="User photo" />
                    </div>
                    <div class="row-sec mb15">
                        <div class="fileUpload btn black-btn">
                            <span><?php echo $LANG['browse']; ?></span>
                                <input type="file" class="upload"
                                       onchange="displayImage(this,'n','195','menuIcons')"
                                       accept="image/*" id="uploadBtn" name="menuIcons" id="menuIcons" />
                        </div>
                        </div>
                    <span style="display:none;"
                          class="ajax_image_loader ajax-loader"><?php echo $LANG['loading']; ?></span>
                </div>
            </div>
        </div>
        <input type="hidden" value="" id="menuIconDetails" />
            <div class="row-sec btn-sec">
                <input type="hidden" name="menuPageId" id="menuPageId"
                       value="<?php echo $param['menuPageId']; ?>">
                <input type="button" class="pop_cancel_btn btn black-btn fr"
                       value="<?php echo $LANG['btnCancel'];?>">
                <input type="button"
                       onclick="manageMenuPopup('<?php echo $param['menuPageId']; ?>');"
                       class="btn black-btn fr" value="<?php echo $LANG['btnSave'];?>">
            </div>

    </div>
    </div>
<!-- Add/Edit menu popup -->
