<?php	
    /**
     * Author        : Nesarajan M
     * Since         : 10-Nov-2014
     * Modified By   : 
     * Modified Date : 
     * Description   : Show email existing users lists.
     **/
    require '../lang/en.php';

    $params['emailId'] = $editId;
    $result = $this->members->getExistingEmailList($params);
    $list = $result['rows'];
if (!isset($list[0]['user_id'])) {
    $list = array($list);
}
?>
<div>Email already exists in LMO. Do you want to continue with same email?</div>
<div>&nbsp;</div>

<div class="grid-block" style="max-height:300px; overflow-y:auto;">
    <input type="hidden" class="paramNone" value="?p=coachManagementEdit">
    <!--If navigate back to the page. Move the all query string to another page -->
    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="coachListGridTab">
        <thead>
            <tr class="grid-title">
                <td>#</td>
                <td>Name</td>
                <td>Email ID</td>
                <td>Role</td>
            </tr>
        </thead>
        <tbody>
    <?php
            $det = '';
    foreach ($list as $res) {
        $det .= <<<DET
                    <tr>
                    <td>{$res['user_id']}</td>
                    <td>{$res['first_name']} {$res['last_name']}</td>
                    <td>{$res['email']}</td>
                    <td>{$res['usertype']}</td>
                    </tr>
DET;
    }
            echo $det;
            ?>
    </tbody></table>
</div>
<div>&nbsp;</div>
<div class="col10">
    <div class="row-sec btn-sec">
        <input type="button" class="pop_cancel_btn btn black-btn fr" value="No">
        <input type="button" onclick="" class="btn black-btn fr" value="Yes">
        <!--Later / Unwanted Trimmed Onclick:submitLmoUserForm();-->
    </div>
</div>
<input type="hidden" class="paramNone" value="?p=coachManagementEdit">
<!--If navigate back to the page. Move the all query string to another page -->
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="coachListGridTab">
    <thead>
    <tr class="grid-title">
        <td>#</td>
        <td>Name</td>
        <td>Email ID</td>
        <td>Role</td>
    </tr>
    </thead>
    <tbody>
    <?php
    $det = '';
    foreach ($list as $res) {
        $det .= <<<DET
                    <tr>
                    <td>{$res['user_id']}</td>
                    <td>{$res['first_name']} {$res['last_name']}</td>
                    <td>{$res['email']}</td>
                    <td>{$res['usertype']}</td>
                    </tr>
DET;
    }
    echo $det;
    ?>
    </tbody>
</table>
<div>&nbsp;</div>
<div class="col10">
    <div class="row-sec btn-sec">
        <input type="button" class="pop_cancel_btn btn black-btn fr" value="No">
        <input type="button" onclick="" class="btn black-btn fr" value="Yes">
        <!--Later Unwanted:submitLmoUserForm();-->
    </div>
</div>