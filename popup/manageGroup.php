<?php
    /**
     * Author        : Nesarajan M
     * Since         : 15-Oct-2014
     * Modified By   :
     * Modified Date :
     * Description   : Add/Edit menu popup page.
     **/
require '../lang/en.php';
global $LANG;
if(!isset($param)){
    $param  =array();
}
$param['groupId']   ='';
if (isset($editId)) {
    $param['groupId'] = $editId;
    $getGroupDetail = $this->settings->getGroupDetail($param);
    $row = $getGroupDetail['getGroupDetail'];
}
//To get menus list
$groups = $this->settings->getGroup();
?>
<div class="acc-row add-menu">
    <div class="ajaxMsg" style="display:none;"></div>
        <div class="acc-content">
            <div class="row-sec mb15">
                <label class="fl">
                    <?php echo $LANG['groupName']; ?> :<span class="required">*</span>
                </label>
                <input type="text" id="groupName" name="groupName" class="form-control"
                       placeholder="Group Name"
                       value="<?php echo (isset($row['group_name'])) ? $row['group_name'] : ''; ?>" required />
            </div>
        </div>
        <div class="clear"></div>
            <div class="row-sec btn-sec">
                <input type="hidden" name="groupId" id="groupId"
                       value="<?php echo $param['groupId']; ?>">
                <input type="button" class="pop_cancel_btn btn black-btn fr"
                       value="<?php echo $LANG['btnCancel'];?>">
                <input type="button" onclick="saveManageGroup('<?php echo $param['groupId']; ?>');"
                       class="btn black-btn fr" value="<?php echo $LANG['btnSave'];?>">
             </div>
    </div>