/*
@Description   : To handle manage permission js actions
@Author        : Nesarajan M
@Created Date  : 28-Oct-2014
@Modified By   : 
@Modified Date : 
*/
$(document).ready(
    function() {
        /* To enable/disable all checkbox permission in row*/
        $(".checkAllPerms").click(
            function(evt) {
                var $this        =    $(this);
                var $chkinput    =    $this.find("input");
                var thisId = $chkinput.attr('id');
                var $assocelem   =   $("."+thisId);
                if($this.hasClass('checked')) {
                    $chkinput.removeAttr('checked');
                    $this.removeClass("checked");
                    $assocelem.prop('checked', false);
                    $assocelem.parent().removeClass("checked");
                } else {
                    $this.find("input").attr('checked', 'checked');
                    $this.addClass("checked");
                    $assocelem.parent().addClass("checked");
                    $assocelem.prop('checked',true);
                }
                evt.stopPropagation();
                evt.preventDefault();
                /*
                var thisChecked = $(this).hasClass('checked'); //$(this).prop('checked');
                console.log(thisChecked);
                var thisId = $(this).find('input').attr('id');
                $("."+thisId).prop('checked', false);
                $("."+thisId).parent().removeClass("checked");
                console.log("."+thisId);
                if(thisChecked == true) {
                $("."+thisId).parent().addClass("checked");
                $("."+thisId).prop('checked',true);
                }
                */
            }
        );
        /* To enable/disable all checkbox permission in row */
    }
);

    $(document).delegate(
        '.check_box',"click",function(){
            console.log('.check_box click');
            var $this        =    $(this);
            var $chkinput    =    $this.find("input");
            if($this.hasClass('checked')) {
                console.log('.check_box click remove');
                $chkinput.removeAttr('checked');
                $this.removeClass("checked");
            } else {
                console.log('.check_box click check');
                $this.find("input").attr('checked', 'checked');
                $this.addClass("checked");
            }
        }
    );    

