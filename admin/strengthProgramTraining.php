<?php
/**
 * PHP version 5.
 
 * @category Admin
 
 * @package StrengthProgramTraining
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description To display training program list.
 */
global $LANG;
?>
<!-- Assign member to club ends -->

<div class="grid-block">
    <table width="100%"
        border="0" cellspacing="0" 
        cellpadding="0" id="manageClubEditTab">
    <thead>
    <tr class="grid-title">
        <td><?php echo $LANG['trainingWeek']; ?></td>
        <td><?php echo $LANG['series']; ?></td>
        <td><?php echo $LANG['strengthRep']; ?></td>
        <td><?php echo $LANG['time']; ?></td>
        <td><?php echo $LANG['strengthPercentage']; ?></td>
        <td><?php echo $LANG['rest']; ?></td>
        <!--<td><?php //echo $LANG['action']; ?></td>-->
    </tr>
    </thead>
        <tbody class="strngthtrain">
        <?php
        if (!empty($trainingList[0]) && count($trainingList) > 0) {
            $strengthProgramId  =   isset($strengthProgramId) ? $strengthProgramId:0;
            foreach ($trainingList as $key => $value) {
                if ($_SESSION['page_edit'] == 1) {
                    $editLink = <<<EDIT_LINK
                        <a title="{$LANG['titleEdit']}" 
                        class="btn-link btn-inline dotline-sep icon-edit-menu" 
                        href="index.php?p=strengthProgramEdit&id=
                        {$strengthProgramId}&strengthProgramTrainingId=
                        {$value['strength_program_training_id']}">
                        <span class="icon icon-edit"></span>
                        </a>
EDIT_LINK;
                }
                if ($_SESSION['page_delete'] == 1) {
                    $deleteLink = <<<EDIT_LINK
                                <a title="{$LANG['titleDelete']}" 
                                class="btn-link btn-inline icon-delete-training" 
                                strengthProgramTrainingId
                                ="{$value['strength_program_training_id']}">
                                <span class="icon icon-cls-sm"></span>
                                </a>
EDIT_LINK;
                }?>
                <tr>
                    <td>
                        <?php
                            echo $value['training_week'];
                        ?>
                    </td>
                    <td>
                        <?php
                            echo $value['series'];
                        ?>
                    </td>
                    <td>
                        <?php
                            echo $value['reputation'];
                        ?>
                    </td>
                    <td>
                        <?php
                            echo $value['time'];
                        ?>
                    </td>
                    <td>
                        <?php
                            echo $value['strength_percentage'];
                        ?>
                    </td>
                    <td>
                        <?php
                            echo $value['rest'];
                        ?>
                    </td>
                    <td>
                        <?php
                            echo $editLink.$deleteLink;
                        ?>
                    </td>
                </tr>
                <?php
            }
        } else {
            ?>
            <tr>
                <td colspan="20">
                    <?php
                        echo $LANG['noRecordsFound'];
                    ?>
                </td>
            </tr>
            <?php
        } ?>
        </tbody>
    </table>
</div>