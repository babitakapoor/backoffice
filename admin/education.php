<?php
/**
 * PHP version 5.
 
 * @category Admin
 
 * @package CoachManagementEdit
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Add/Edit Education page
 */
    require"../lang/en.php";

    global $LANG;
    $param['companyId'] = COMPANY_ID;
    $arrayListMember = $this->club->getClubListByCompany($param);
    $clublist=( 
        (!$arrayListMember['rows'][0])
    ) ? array($arrayListMember['rows']):$arrayListMember['rows'];
    $groups = $this->settings->getQuesGroup();
    $grouplist=isset($groups['getGroupDetail']) ?
        $groups['getGroupDetail']:array();
    $languages=$this->settings->getLanguageActiveType(); 
    $languageactivelist = (
        isset($languages['languageTypeDetails']) 
    ) ? $languages['languageTypeDetails']:array();
    if ($languages['total_records']==1) {
        $languageactivelist    =    array($languageactivelist);
    }
    $phaseid = PHASE_Check;
    $phasesrange = $this->settings->getQuesPhaseRange(DEFAULT_LANG);
    $phaseslist
        =isset($phases['getPhaseDetail'])?$phases['getPhaseDetail']:array();
    $phasesrangelist
        = isset($phasesrange['phaserangeinfo'])?
        $phasesrange['phaserangeinfo']:array();
    if ($phasesrange['total_records']==1) {
        $phasesrangelist    =    array($phasesrangelist);
    }
    $indexArrayGroup = array();
    if (is_array($phasesrangelist) && count($phasesrangelist) > 0 ) {
        foreach ($phasesrangelist as $res) {
            if ($res['r_phase_id'] == PHASE_Check) {
                $indexArrayGroup[$res['r_group_id']][] = $res;
            }
        }
    }?>
    <script>
    //$(document).ready(function() {
        /*setTimeout(function() {
            $(".clshtmlcontentbox").each(function(idx, obj) {
                var attrid=$(obj).attr("id")
                var ins=CKEDITOR.instances[attrid]
                if (ins) {
                    CKEDITOR.remove(ins);
                }
                CKEDITOR.replace( attrid );
            });
        },3000);*/
    //});
    </script>
<div class="content-wrapper" id="manage-members">
    <div class="question_outr" style="margin-top:50px;" >
        <div class="question_box" > 
            <h2 class="pg_htr_h2" >EDUCATION</h2>
            <div class="question_box_inr" >
            <?php 
            $grouplist = (
                isset($groups['getGroupDetail']) 
            ) ? $groups['getGroupDetail']:array();
            // if ($phase['phase_id']==1) {
                    // $grouplist[]= array(
                        // "group_id"=>GROUP_DUMMY_General,
                        // "group_name"=>"General"
                    // );
                // }
            foreach ($grouplist as $group) {
                $returnobj = array();   
                $groupid =$group['group_id'];
                $educationgrouplist
                    =$this->settings->getEducationByGroupId($groupid);
                $edudata= (
                    isset($educationgrouplist['eduinfo']) 
                ) ? $educationgrouplist['eduinfo']:array();
                if (!empty($edudata)) {
                    foreach ($edudata as $eduinfodet) {
                        if (isset($eduinfodet['langid'])) {
                            $daycnt    =   $eduinfodet['days'];
                            $returnobj[$daycnt][$eduinfodet['langid']]['text']
                                =$eduinfodet['info'];
                            $returnobj[$daycnt][$eduinfodet['langid']]['eduid']
                                =$eduinfodet['eduid'];
                            $returnobj[$daycnt][$eduinfodet['langid']]['id']
                                =$eduinfodet['langdescid'];
                            $returnobj[$daycnt]['selidx']
                                =$eduinfodet['indextype'];
                        }
                    }
                }
                // print_r($returnobj);
                //print_r($educationgrouplist);?>
                <div class="qtn_inr" 
                    id="idgroupquestionbox<?php echo $group['group_id'];?>">
                    <h2 groupid="<?php echo $group['group_id'];?>">
                        <?php echo $group['group_name'];?> education</h2>
                    <div class="qtn_inr_bx divnone">
            <!--<select id="cpyfase_<?php echo $group['group_id']?>">
                <option value="0">Select fase</option><?php
                $pseid  =   isset($phase['phase_id']) ? $phase['phase_id']:0;
                foreach ($phases['getPhaseDetail'] as $selectphase) {
                    if ($selectphase['phase_id']!=$phase['phase_id']) { ?>
                        <option 
                            value="<?php echo $selectphase['phase_id'];?>">
                            <?php echo ucfirst($selectphase['phase_name']); ?>
                        </option>
                    <?php 
                    }
                }?>
                </select>
                <select id="cpygroup_<?php echo $group['group_id']?>">
                    <option value="0">Select Group</option>
                    <?php 
                    foreach ($grouplist as $selectgroup) {
                        //if ($selectgroup['group_id']!=$group['group_id']) {?>
                        <option 
                            value="<?php echo $selectgroup['group_id'];?>">
                            <?php 
                            echo ucfirst($selectgroup['group_name']); ?>
                        </option>
                    <?php //}
                    }
                    echo "</select>"; ?>
                    <input 
                        type="button" 
                        class="btn cpyques"  
                        value="<?php echo "Copy";?>">
                        <button 
                        class="add_qtn_btn" 
                        onclick="loadAddEducationPopup('Add Education',
                            'addeducation',
                            '<?php echo $group['group_id'];?>')"  
                        groupid="<?php echo $group['group_id'];?>">
                        Add Education Form</button>
                    -->
                    <form class="questiontext" id="addques">
                    <?php for ($days=1;$days<=14;$days++) { ?>
                        <div class="row-sec mb15">
                            <div class="dayslbl">
                                <span class="clsdaytag">
                                    Day -<?php echo $days?>
                                </span>
                                <div class="clear"></div>
                                <?php if ($days != 14) { 
                                    $eduindexdata = (
                                        isset($returnobj[$days])
                                    )?$returnobj[$days]['selidx']:'';
                                    // echo $eduindexdata;
                                    ?>
                                <div class="row-sec mb15">
                                    <label class="fl" for="indextype" >Info Type</label>
                                    <select 
                                        name="indextype" 
                                        id="indextype" 
                                        class="indextype_<?php echo $days?>
                                         iseditdisable" 
                                        oldvalue="">
                                    <option value="0">Select</option>
                                    <?php 
                                    if (is_array($phasesrangelist) 
                                        && count($phasesrangelist) > 0 
                                    ) {
                                        $selected ='';
                                        // print_r($indexArrayGroup);s
                                        foreach ($indexArrayGroup as $key=>$res) {
                                            if ($key != $group['group_id']) {
                                                foreach ($res as $ind=>$value) {
                                                    $idxvalue
                                                        =$value['r_group_id'].
                                                    ":".($ind+1);
                                                    $selected ='';
                                                    if (($eduindexdata==$idxvalue)
                                                    ) {
                                                        $selected = 'selected';
                                                    }
                                                ?>
                                                <option 
                                                    value="<?php 
                                                    echo $idxvalue?>" 
                                                    <?php echo $selected?>>
                                                    <?php 
                                                    echo $value['group_name']?>
                                                    <?php echo $ind+1?>
                                                </option>
                                                <?php 
                                                } ?>
                                            <?php 
                                            }
                                        }
                                    } ?>
                                    </select>
                                </div>
<?php 
}
        $i=0;
foreach ($languageactivelist as $lang) { 
    $quesinfotxtlabel = ($i==0) ?'Info':'&nbsp;';
    $educationdata = (
        isset($returnobj[$days]) 
        && isset($returnobj[$days][$lang['language_id']])
    ) ? $returnobj[$days][$lang['language_id']]['text']:'';
    $educationdataid = (
        isset($returnobj[$days]) 
        && isset($returnobj[$days][$lang['language_id']])
    ) ? $returnobj[$days][$lang['language_id']]['id']:'';
    $educationdataeduid = (
        isset($returnobj[$days]) 
        && isset($returnobj[$days][$lang['language_id']])
    ) ? $returnobj[$days][$lang['language_id']]['eduid']:''; ?>
            <label class=""> Info(<?php echo $lang['title'];?>)</label>
            <div class="clear" ></div>
            <div class="row-sec mb15">
                <input 
                    type="hidden" 
                    class="eduid<?php 
                        echo $group['group_id'];?><?php 
                        echo $days?>" 
                    value="<?php echo $educationdataeduid; ?>"/>
                <input 
                    type="hidden" 
                    class="edutitletxtlangid_<?php 
                        echo $group['group_id'];?><?php 
                        echo $days?><?php 
                        echo $lang['language_id']?>" 
                    value="<?php echo $educationdataid; ?>"/>
             <textarea 
                name="editor" 
                id="eduinfotxt<?php 
                    echo $lang['language_id']?><?php 
                    echo $group['group_id'];?><?php 
                    echo $days?>" 
                class="eduinfo ckeditor eduinfo<?php 
                    echo $lang['language_id']?> eduinfotxt_<?php 
                    echo $group['group_id'];?><?php 
                    echo $days?> edutxtarea" 
                placeholder="Info in <?php echo $lang['title']?>" 
                languageid="<?php echo $lang['language_id']?>">
                <?php echo $educationdata;?>
             </textarea>
            </div>
<?php  
    $i=$i+1;
} 
?>
           </div>
        </div>
<?php 
} 
?>
        <div class="clear"></div>
        <div class="row-sec btn-sec">
            <input 
                type="hidden" 
                name="questionnaireId" 
                id="questionnaireId" 
                value=""> 
            <input 
                type="hidden" 
                name="groupid" 
                id="groupid" 
                value="<?php echo $group['group_id'];?>"> 
            <input 
                type="button" 
                class="pop_cancel_btn btn black-btn fr" 
                value="<?php echo $LANG['btnCancel'];?>">
            <input type="button" 
                onclick="saveEducation(<?php echo $group['group_id'];?>)" 
                class="btn black-btn fr" value="<?php echo $LANG['btnSave'];?>">
        </div>  
       </form>
        <?php 
        /*$param = array();
        $param['groupid'] = $group['group_id'];
        $educationlist = $this->settings->getEducation($param);
        $education 
        = isset($educationlist['getEducation']) ?
            $educationlist['getEducation']:array();
        if ($educationlist['total_records']==1) {
        $education=array($education);
        }*/
        ?>
       <!--<table>
        <tr>
                                    <th>Form Name</th>
                                    <th>Title</th>
                                    <th>Info </th>
                                    <th class="wdh_100" >Action</th>
                                </tr>
                                <tbody class="gridcnt">

                                    <tr><td colspan="4">No results found</td></tr>
                                </tbody>
       </table>-->
      </div>
     </div>
        <?php
            }
        ?>
    </div>
            <!--
            <div class="question_box_inr" >
                <div class="qtn_inr">
                    <h2 >General education</h2>
                </div>
            </div>
            -->
        </div>

    </div>
</div>
<input 
    type="hidden" 
    class="form-control loggedUserId" 
    value="<?php echo $_SESSION['user']['user_id']; ?>" 
    name="loggedUserId"  />