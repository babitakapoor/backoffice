<?php
/**
 * PHP version 5.
 
 * @category Admin
 
 * @package Adminactivities
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description:Get all the activities.
 */
global $LANG;
?>
<div class="search-list-form">
<form name="associatedClubs" id="associatedClubs" action="" method="post">
   <div class="row-sec">
    <div class="strength-machine-left strength-machine-left-bottom">
    <?php
        $clubsActivities = array();
    if(isset($activitiesClubList) and count($activitiesClubList)>0) {
        foreach ($activitiesClubList as $key => $value) {
            $clubsActivities[] = $value['r_activity_id'];
        }
    } ?>
        <label for="centerListLtact" style="display: none;" ></label>
        <select id="centerListLtact" name="centerListLtact[]"
        multiple="multiple" style="height:150px;min-width:220px;" >
    <?php
    if(isset($activitiesList) and count($activitiesList)>0) {
        foreach ($activitiesList as $row) {
            if (!in_array($row['activity_id'], $clubsActivities)) {
                echo "<option value='" . $row['activity_id'] . "'>" .
                    $row['activity_name'] . '</option>';
            }
        }
    }
    ?>
        </select>
    </div>

    <div class="strength-machine-center">
            <span class="icon-graph icon-rightshift left_CheckAlls"></span>
            <a class="move_rights">
                <img id="preview_user_image" src="<?php echo IMG_PATH.DS.'right.png';?>" />
            </a>
            <br>
            <a class="move_lefts">
                <img id="preview_user_image" src="<?php echo IMG_PATH.DS.'left.png';?>" />
            </a>
            <span class="icon-graph icon-leftshift right_CheckAlls"> </span>
    </div>

    <div class="strength-machine-right">
        <label for="centerListRtact" style="display: none;" ></label>
        <select id="centerListRtact" name="centerListRtact[]"
        multiple="multiple" style="height:150px;min-width:220px;" >
    <?php
    if (!empty($activitiesClubList[0]) && count($activitiesClubList) > 0) {
        foreach ($activitiesClubList as $rowPlan) {
            echo "<option value='".$rowPlan['r_activity_id']."'>".
            $rowPlan['activity_name'].'</option>';
        }
    }
            ?>
            </select>
    </div>
    </div>
    <div class="strength-machine-top">
    <input type="submit" class="btn black-btn" value="<?php echo $LANG['btnSave']; ?>"/>
    <input type="hidden" name="clubId" value="<?php echo $_REQUEST['id'];?>">
    </div>
    </form>
</div>
<div class="clear">&nbsp;</div>

<script>

/** Plan centers Mapping Tab Activities**/
  
</script>

