<?php
/**
 * PHP version 5.
 
 * @category Admin
 
 * @package Settings
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Get all the Strength Machine Clubs.
 */
global $LANG;
?>
<div class="search-list-form">
<form name="associatedClubs" id="associatedClubs" action="" method="post">
   <div class="row-sec">
       <div class="strength-machine-left strength-machine-left-bottom">
        <!--div class="strength-machine-left-bottom">
            <label>
                <span class="left_CheckAll">Select All</span> 
                | <span class="left_unCheckAll">Deselect All</span>
            </label> 
        </div>-->
    <?php
             $clubsMachine = array();
    if(isset($machineClubList) and count($machineClubList)>0) {
        foreach ($machineClubList as $key => $value) {
            $clubsMachine[] = $value['r_strength_machine_id'];
        }
    }
            ?>
           <label for="centerListLf" style="display: none;" ></label>
        <select id="centerListLf"
            name="centerListLf[]" 
            multiple="multiple" 
            style="height:150px;min-width:220px;" >
        <?php
        if(isset($strengthMachineList) and count($strengthMachineList)>0) {
            foreach ($strengthMachineList as $row) {
                if (!in_array($row['strength_machine_id'], $clubsMachine)) {
                    echo '<option value="' . $row['mid'] . '">' . $row['name'] . '</option>';
                }
            }
        } else {
            echo '<option value="0">No Machines found</option>';
        }
                        ?>
         </select>
    </div>
       <div class="strength-machine-center">
        <span class="icon-graph icon-rightshift left_CheckAll"></span>
        <a class="move_right">
                <img id="preview_user_image" src="<?php echo IMG_PATH.DS.'right.png';?>" />
            </a>
        <br>
        <a class="move_left">
                <img id="preview_user_image" src="<?php echo IMG_PATH.DS.'left.png';?>" />
            </a>
        <span class="icon-graph icon-leftshift right_CheckAll"> </span>
    </div>
       <div class="strength-machine-right">
        <!--div>
                <label>
                    <span class="right_CheckAll">Select All</span> 
                    | <span class="right_unCheckAll">Deselect All</span></label> 
                </div>-->
           <label for="centerListRt" style="display: none;" ></label>
        <select
                id="centerListRt" name="centerListRt[]" 
                multiple="multiple" style="height:150px;min-width:220px;" >
    <?php
    if (!empty($machineClubList[0]) && count($machineClubList) > 0) {
        foreach ($machineClubList as $rowPlan) {
            echo "<option value='".$rowPlan['r_strength_machine_id']."'>".
                $rowPlan['machine_name'].'</option>';
        }
    }
            ?>
            </select>
    </div>
 </div>
    <div class="strength-machine-top">
        <input type="submit" class="btn black-btn"
            value="<?php echo $LANG['btnSave']; ?>" />
        <input type="hidden" name="clubId"
            value="<?php echo $_REQUEST['id'];?>">
        </div>
 </form>
</div>
<div class="clear">&nbsp;</div>