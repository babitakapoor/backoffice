<?php 
/**
 * PHP version 5.
 
 * @category Admin
 
 * @package Translation
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description To display stregth list.
 */
 /* To List the strength Program in Dropdown */
global $LANG;
$appData = 	$this->appVersionListing->getAppEditVersion();
$clientversion = '';
$coachversion = '';
if(isset($appData['data']) && !empty($appData['data']))
{
	$clientversion = $appData['data']['client_app_version'];
	$coachversion = $appData['data']['coach_app_version'];
}


?>
<style>
.grid-block label{width:200px}
.grid-block textarea{resize:none}
</style>
<!--To List the translation language -->
<div class="content-wrapper" id="manage-members">
    <div class="con-sec pt100">
        
		<!-- Flash message begins -->
		<div>
			<?php
			if (isset($_SESSION['flMsg']) || isset($_REQUEST['mess'])) 
			{
                if (isset($_SESSION['flMsg']['flashMessageError'])) {
                    echo '<div class="pageFlashMsg error">'.
                        $_SESSION['flMsg']['flashMessageError'].'</div>';
                } elseif (isset($_SESSION['flMsg']['flashMessageSuccess'])) {
                    echo '<div class="pageFlashMsg success">'.
                        $_SESSION['flMsg']['flashMessageSuccess'].'</div>';
                }
				elseif (isset($_REQUEST['mess'])) {
                    echo '<div class="pageFlashMsg error" style="height:40px">'.
                        $_REQUEST['mess'].'</div>';
                }
                unset($_SESSION['flMsg']);
            } 
			?>
			<div>&nbsp;</div>
		</div>
		<div class="tabOuterDiv">
			<ul class="tabs">
				<li class="current">
					<a href="#tab-1">Add App Version</a>
				</li>
			</ul>
            <div class="clear"></div>
			<div class="tabs-container">
				<div id="tab-1" class="tabscontent testResultListGrid">
					<!--grid-->
					<div class="grid-block" style="border:none !important;margin: 0 auto;width:50%;">
						<form id="content_form">
							
							<div style="padding:10px;float:left;width:100%">
								<label><?php echo $LANG['currentClientVersion']; ?></label>
								<?php echo $clientversion ?>
							</div>
							<div style="padding:10px;float:left;width:100%">
								<label><?php echo $LANG['currentCoachVersion']; ?></label>
								<?php echo $coachversion ?>
							</div>
							
							<div style="padding:10px;float:left;width:100%">
								<label><?php echo $LANG['clientaddVersion']; ?><span style="color:red">*</span></label>
								<input type ="text" placeholder="Client App Version" name = "client_app_version" value ="<?php echo $clientversion ?>" required="required"/>
							</div>
							<div style="padding:10px;float:left;width:100%">
								<label><?php echo $LANG['coachaddVersion']; ?><span style="color:red">*</span></label>
								<input type ="text" placeholder="Coach App Version" name = "coach_app_version" value ="<?php echo $coachversion ?>" required="required"/>
							</div>
							
							<div style="padding:10px;float:left;width:100%">
								<input class="btn black-btn fr" value="Save" type="submit" name="version_submit">
							</div>
						</form>
					</div>
				</div>
			</div>
        </div>
	</div>
</div>

