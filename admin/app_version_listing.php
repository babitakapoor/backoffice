<?php
/**
 * PHP version 5.
 
 * @category Admin
 
 * @package Coachlist
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Get Cycle Associated with members
 */

global $LANG;
/* To List the Clubs in Dropdown */

$params['company_id'] = COMPANY_ID;
$param['companyId'] = COMPANY_ID;
$params['is_deleted'] = 0;
$params['authorizedClubId'] = '';
$param['authorizedClubId'] = '';

if (isset($_SESSION['club']['authorizedClubId']))
{
    $params['authorizedClubId'] = $_SESSION['club']['authorizedClubId'];
    $param['authorizedClubId'] = $_SESSION['club']['authorizedClubId'];
}
$clubList = $this->club->getClubList($params);

if($_REQUEST['type'] == 'coaches')
{
	$param['userType'] = 1;
}
else
{
	$param['userType'] = 3;
}

/* $userType : Param Types should be any one of these  all/employee/coach/admin/backOffice/member*/
$param['loggedUserType'] = $_SESSION['user']['usertype_id'];

//To search param 	
$param['clubId'] = (isset($_REQUEST['clubId']) ? $_REQUEST['clubId'] : '');
$param['searchValue'] = $_REQUEST['searchValue'];

/*To sort param , If the param label field is empty / default value  should define at else part.*/
$reqp = $_REQUEST['p'];
$param['labelField'] = 'user_id';

if (isset($_SESSION['pageName'][$reqp])) 
{
    $param['labelField'] = $_SESSION['pageName'][$reqp];
}
$param['sortType']  ='asc';
if (isset($_SESSION['pageName'][$reqp]) && $_SESSION['sortType'] == 2) 
{
    $param['sortType']='desc' ;
}
 //Company id


//Pagination code starts
$limitStart = 0;
$limitEnd = PAGINATION_SHOW_PER_PAGE;

if (isset($_REQUEST['offset']) && $_REQUEST['offset'] > 0) 
{
    $limitEnd = $_REQUEST['offset'];
}
if (isset($_REQUEST['page']) && $_REQUEST['page'] > 0)
{
    $limitStart = ($_REQUEST['page'] - 1) * $limitEnd;
}

$param['limitStart'] = $limitStart;
$param['limitEnd'] = $limitEnd;

// Result set	
$arrayListCoach = $this->appVersionListing->getAppVersions($param); 

//Total count to create pagination
$totalCount = $arrayListCoach['total_records'];
if($totalCount == 1)
{
	$arrayList[0] = $arrayListCoach['data'];
}
else
{
	$arrayList = $arrayListCoach['data'];
}

/* Search Labels */
$customSearchArray = array('user_name' => 'Name','status' => 'Status');

$appData = 	$this->appVersionListing->getAppVersion($param['userType']);
$version = 0;
if(isset($appData['data']) && !empty($appData['data']))
{
	$version = $appData['data']['app_version'];
	
}

?>
 <style>
 .btn {
    -moz-user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;
    cursor: pointer;
    display: inline-block;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857;
    margin-bottom: 0;
    
    text-align: center;
    touch-action: manipulation;
    vertical-align: middle;
    white-space: nowrap;
}
.black-btn {
    background: #0095ac none repeat scroll 0 0;
    margin: 0 2px;
}


ul.tabs-main::after {
    clear: both;
    content: " ";
    display: block;
}
ul.tabs-main {
    border-bottom: 4px solid #0095ac !important;
}
ul.tabs-main {
    border-bottom: 4px solid #e40043;
}
ul.tabs-main li.current, ul.tabs-main li.current:hover {
    background: #0095ac none repeat scroll 0 0;
    border: 1px solid #0095ac;
}

ul.tabs-main li {
    background-color: #dfdfdf;
    border: 1px solid #dfdfdf;
    color: #4d515f;
    cursor: pointer;
    float: left;
    font-size: 14px;
    margin: 0 2px 0 0;
    min-width: 84px;
    text-align: center;
}
ul, li {
    list-style: outside none none;
    margin: 0;
}
ul.tabs-main li.current a {
    color: #ffffff !important;
}
ul.tabs-main li a {
    color: #363636;
    display: block;
    padding: 6px 10px;
}
 </style>
<div class="content-wrapper" id="coach-list">
    <div class="con-title-sec pos-fixed mt40">
        <h1>
            <span class="icon icon-coachmng"></span>
            <?php echo $LANG['app_details']; ?>
        </h1>
        <div class="user-features">
          <ul>
            <li>
                <a href="<?php echo SERVICE_BASEURL."admin/index.php?p=settings" ?>" title="<?php echo $LANG['backToSettings']; ?>"><span class="icon icon-back" ></span></a>
            </li>
          </ul>
        </div>
    </div>
    <div class="con-sec pt100">
		<div class="row-sec coach-search-sec search-list-form">
			<form name="coachsearch" id="searchFilterForm" action="" method="get">
				<div class="col6">
					<!--<label><?php echo $LANG['select']; ?> :</label>-->
					 <div>
						<input type="hidden" name="p" value="app_version_listing">
						<input type="hidden" name="type" value="<?php echo $_REQUEST['type'] ?>">
						<input type="hidden" name="labelField" id="labelField" value="<?php echo $param['labelField']; ?>">
						<input type="hidden" name="sortType" id="sortType" value="<?php echo $param['sortType']; ?>">
					 </div>
				   <!-- Club list begins -->
					<input type ="hidden" value =1 id ="searchType" />
					<?php 
					/*if (isset($_SESSION['showCommonAccess'])) 
					{
					?>
						<div class="select-custom">
							<label for="clubId" style="display: none;"></label>
							<select  id="clubId" name="clubId">
								<option value="">-<?php echo $LANG['all'];?>-</option>
								<?php
									foreach ($clubList as $row) 
									{
										$selected = '';
										if (isset($param['clubId']) && ($param['clubId'] == $row['club_id'])) 
										{
											$selected =  "selected='selected'";
										}
										echo "<option value='".$row['club_id']."' $selected>".$row['club_name'].'</option>';
									}
								?>
							</select>
						</div>
					<?php  
					} 
					else 
					{
					?>
						<input type="hidden" id="clubId" value="<?php echo $_SESSION['club']['authorizedClubId'];?>" >
					<?php 
					} */
					?>
					<input type="hidden" id="clubId" value="<?php echo $_SESSION['club']['authorizedClubId'];?>" >
					<!-- Custom search variable select option ends here -->
				</div>
				<div class="col6">
					<a href="<?php echo SERVICE_BASEURL."admin/index.php?p=app_version_listing&type=coaches"; ?>">
						<input type="button" value="<?php echo $LANG['clear']; ?>"class="btn black-btn fr" id="clear_search" class="fr"  />
					</a>
					<input type="submit" value="<?php echo $LANG['search']; ?>" id="searchFilterSubmit" class="btn black-btn fr" />
					<label for="searchValue" style="display: none;" ></label>
					<input type="text" name="searchValue" id="searchValue" class="fr" value="<?php echo isset($_REQUEST['searchValue']) ?$_REQUEST['searchValue'] : ''; ?>" />
				</div>
		   </form>
		</div>
		<div class="tabOuterDiv">
			<ul class="tabs-main">
				<li class="">
					<?php
					if($_REQUEST['type'] == 'coaches')
					{
					?>
					   <a href="<?php echo SERVICE_BASEURL."admin/index.php?p=coachList"; ?>"><?php echo $LANG['coachList']; ?></a>
					<?php
					}
					else
					{
					?>
						<a href="<?php echo SERVICE_BASEURL."admin/index.php?p=resetusers"; ?>"><?php echo $LANG['resetUserslist']; ?></a>
					<?php
					}
					?>
				</li>
				<li class="current">
					<a href="<?php echo SERVICE_BASEURL."admin/index.php?p=app_version_listing&type=".$_REQUEST['type']; ?>"><?php echo $LANG['appDetails'] ?></a>
				</li>
			</ul>
			<div class="clear"></div>
			<div class="tabs-container">
				<div id="tab-1" class="tabscontent">
					<div class="clear"></div>
					<p class="mb15"><?php echo $LANG['totalUserCount']; ?> :<span class="count-block"><?php echo $totalCount; ?></span></p>
					<div class="grid-block" id="coachListGriddiv">
						<!--If navigate back to the page. Move the all query string to another page -->
						<table width="100%" border="0" cellspacing="0"cellpadding="0" id="coachListGridTab">
							<thead>
								<tr class="grid-title toggle-label" linkData="<?php echo $reqp; ?>">
									<td>
										<?php
											$clsdesc=$clsasc='';
											if ($param['labelField']=='user_id') 
											{
												if ($param['sortType']=='desc') 
												{
													$clsdesc='active';
												} 
												else 
												{
													$clsasc='active';
												}
											}
											echo $LANG['user_id']; 
										?>
										<span class="spinner">
											<a href="javascript:;" onClick="sortingField('user_id', '1','<?php echo $reqp; ?>')" class="spi-arr-up <?php echo $clsasc;?>"> </a>
											<a href="javascript:;" onClick="sortingField('user_id', '2','<?php echo $reqp; ?>')" class="spi-arr-down <?php echo $clsdesc;?>"></a>
										</span>
									</td>
									<td>
										<?php
											$clsdesc=$clsasc='';
											if ($param['labelField']=='first_name') 
											{
												if ($param['sortType']=='desc') 
												{
													$clsdesc='active';
												} 
												else 
												{
													$clsasc='active';
												}
											}
											echo $LANG['firstName']; 
										?>
										<span class="spinner">
											<a href="javascript:;" onClick="sortingField('first_name', '1','<?php echo $reqp; ?>')" class="spi-arr-up <?php echo $clsasc;?>"> </a>
											<a href="javascript:;" onClick="sortingField('first_name', '2','<?php echo $reqp; ?>')" class="spi-arr-down <?php echo $clsdesc;?>"></a>
										</span>
									</td>
									<td>
										<?php
											$clsdesc=$clsasc='';
											if ($param['labelField']=='club_name') 
											{
												if ($param['sortType']=='desc') 
												{
													$clsdesc='active';
												} 
												else 
												{
													$clsasc='active';
												}
											}
											echo $LANG['clubName']; 
										?>
										<span class="spinner">
											<a href="javascript:;" onClick="sortingField('club_name', '1','<?php echo $reqp; ?>')" class="spi-arr-up <?php echo $clsasc; ?>"></a>
											<a href="javascript:;" onClick="sortingField('club_name', '2','<?php echo $reqp; ?>')" class="spi-arr-down <?php echo $clsdesc;?>"></a>
										</span>
									</td>
									<td>
										<?php
											$clsdesc=$clsasc='';
											if ($param['labelField']=='app_platform') 
											{
												if ($param['sortType']=='desc') 
												{
													$clsdesc='active';
												} 
												else 
												{
													$clsasc='active';
												}
											}
											echo $LANG['appPlatform']; 
										?>
										<span class="spinner">
											<a href="javascript:;" onClick="sortingField('app_platform', '1','<?php echo $reqp; ?>')" class="spi-arr-up <?php echo $clsasc; ?>"></a>
											<a href="javascript:;" onClick="sortingField('app_platform', '2','<?php echo $reqp; ?>')" class="spi-arr-down <?php echo $clsdesc;?>"></a>
										</span>
									</td>
									<td>
										<?php
											$clsdesc=$clsasc='';
											if ($param['labelField']=='app_version') 
											{
												if ($param['sortType']=='desc') 
												{
													$clsdesc='active';
												} 
												else 
												{
													$clsasc='active';
												}
											}
											echo $LANG['appVersion']; 
										?>
										<span class="spinner">
											<a href="javascript:;" onClick="sortingField('app_version', '1','<?php echo $reqp; ?>')" class="spi-arr-up <?php echo $clsasc; ?>"></a>
											<a href="javascript:;" onClick="sortingField('app_version', '2','<?php echo $reqp; ?>')" class="spi-arr-down <?php echo $clsdesc;?>"></a>
										</span>
									</td>
									<td>
										<?php
											$clsdesc=$clsasc='';
											if ($param['labelField']=='device_version') 
											{
												if ($param['sortType']=='desc') 
												{
													$clsdesc='active';
												} 
												else 
												{
													$clsasc='active';
												}
											}
											echo $LANG['deviceVersion']; 
										?>
										<span class="spinner">
											<a href="javascript:;" onClick="sortingField('device_version', '1','<?php echo $reqp; ?>')" class="spi-arr-up <?php echo $clsasc; ?>"></a>
											<a href="javascript:;" onClick="sortingField('device_version', '2','<?php echo $reqp; ?>')" class="spi-arr-down <?php echo $clsdesc;?>"></a>
										</span>
									</td>
									<td>
										<?php
											$clsdesc=$clsasc='';
											if ($param['labelField']=='device_model') 
											{
												if ($param['sortType']=='desc') 
												{
													$clsdesc='active';
												} 
												else 
												{
													$clsasc='active';
												}
											}
											echo $LANG['deviceModel']; 
										?>
										<span class="spinner">
											<a href="javascript:;" onClick="sortingField('device_model', '1','<?php echo $reqp; ?>')" class="spi-arr-up <?php echo $clsasc; ?>"></a>
											<a href="javascript:;" onClick="sortingField('device_model', '2','<?php echo $reqp; ?>')" class="spi-arr-down <?php echo $clsdesc;?>"></a>
										</span>
									</td>
									<td>
										<?php
											$clsdesc=$clsasc='';
											if ($param['labelField']=='start_date') 
											{
												if ($param['sortType']=='desc') 
												{
													$clsdesc='active';
												} 
												else 
												{
													$clsasc='active';
												}
											}
											echo $LANG['startDate']; 
										?>
										<span class="spinner">
											<a href="javascript:;" onClick="sortingField('start_date', '1','<?php echo $reqp; ?>')" class="spi-arr-up <?php echo $clsasc; ?>"></a>
											<a href="javascript:;" onClick="sortingField('start_date', '2','<?php echo $reqp; ?>')" class="spi-arr-down <?php echo $clsdesc;?>"></a>
										</span>
									</td>
									<td>
										<?php
											$clsdesc=$clsasc='';
											if ($param['labelField']=='status') 
											{
												if ($param['sortType']=='desc') 
												{
													$clsdesc='active';
												} 
												else 
												{
													$clsasc='active';
												}
											}
											echo $LANG['status']; 
										?>
										<span class="spinner">
											<a href="javascript:;" onClick="sortingField('status', '1','<?php echo $reqp; ?>')" class="spi-arr-up <?php echo $clsasc; ?>"></a>
											<a href="javascript:;" onClick="sortingField('status', '2','<?php echo $reqp; ?>')" class="spi-arr-down <?php echo $clsdesc;?>"></a>
										</span>
									</td>
									<td>
										Action
									</td>
								</tr>
							</thead>
							<?php
								// Pagination Class in body section
								if (isset($arrayList) && !empty($arrayList) && isset($arrayList[0]['u_id']))
								{
									
									$listurl = SERVICE_BASEURL."admin/index.php?p=app_version_detail_listing&id=";
									$listappversion = '<a 
													title="App Versions" 
													href="'.$listurl.'$u_id$&type='.$param['userType'].'" 
													style ="padding:4px 12px;float:left;margin-left: 19px;"
													class="btn black-btn" >
													 <span>App Versions</span>
													</a>';
									
									//Get current link that is not consider ajax page
									$this->paginator->getUrlLink($param = 1);
									echo $this->paginator->displayItemsPagination(
										$arrayList, array(
											'u_id',
											'user_name',
											'club_name',
											'app_platform',
											'app_version',
											'device_version',
											'device_model',
											'start_date',
											'status',
											'specValues2' => $listappversion
										),
										array('startTag' => '<tr>',
										   'midTagOpen' => '<td>',
										   'midTagClose' => '</td>',
										   'endTag' => '</tr>'
										),
										$version
									);
								} 
								else 
								{
									echo '<tr><td colspan="20">'.$LANG['noResult'].'</td></tr>';
								}
							?>
						</table>
					</div>
					<div class="pagination-block">
					<?php
						if (isset($arrayList) && !empty($arrayList) && isset($arrayList[0]['u_id'])) 
						{
							echo $this->paginator->displayPagination($totalCount);
						} 
					?>
					</div>
                </div>
            </div>
        </div>
   </div>
</div>
