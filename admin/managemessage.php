<?php
/**
 * PHP version 5.
 
 * @category Admin
 
 * @package Managemessage
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Add/Edit menu popup page.
 */
    require '../lang/en.php';
    // if(isset($editId)) {
        // $param['questionnaireId'] = $editId;
        // $getQuestionnaireDetail = $this->settings->getQuestionnaireDetail($param);
        // $row = $getQuestionnaireDetail['getQuestionnaireDetail'];
    // }

    //To get menus list                            
    //$questionist = $this->settings->getQuestionaries();
    $param['companyId'] = COMPANY_ID;
$arrayListMember = $this->club->getClubListByCompany($param);

$clublist = (!$arrayListMember['rows'][0]) ? 
    array($arrayListMember['rows']) : $arrayListMember['rows'];
$groups = $this->settings->getQuesGroup();
$grouplist = isset($groups['getGroupDetail']) ? $groups['getGroupDetail'] : array();
?>
<div class="content-wrapper" id="manage-members">
    <div class="question_outr" style="margin-top:50px;" >
        <div class="question_box"><h2 class="pg_htr_h2" >SMS COMPAINS</h2></div>
        <div class="question_box"><h2 class="pg_htr_h2" >MAILING COMPAINS</h2></div>
        <div class="question_box"><h2 class="pg_htr_h2" >SOCIAL MEDIA COMPAINS</h2></div>
        <div class="question_box" >
            <h2 class="pg_htr_h2" >PUSH MESSAGES</h2>
    <?php foreach ($grouplist as $group) {
    ?>
            <div class="question_box_inr" >
                <div class="qtn_inr">
                    <h2 ><?php echo $group['group_name'];
    ?> messages</h2>
                    <div class="qtn_inr_bx divnone">
                        <label for="slctculb" style="display: none;" ></label>
                        <select id="slctculb" class="slctmsgclub">
                            <option value="0">Select</option>
        <?php
        foreach ($clublist as $club) {
            $sel = '';
            echo '<option value="'.$club['club_id'].'" '.$sel.'>'.
                $club['club_name'].'</option>';
        }
    ?>
                        </select>
                        <div class="focus_pushcharcounts">
                            <label for="messagetext" style="display: none;" ></label>
                            <textarea id="messagetext" class="messagetext"></textarea>
                            <label class="leftchars"></label>
                            <button class="add_qtn_btn" onclick="pushMessageClub()">Add Message</button>
                        </div>
                        <div class="clear"></div>

                    </div>
                </div>
            </div>
    <?php 
} ?>
            <div class="question_box_inr" >
                <div class="qtn_inr">
                    <h2 >General messages</h2>
                    <div class="qtn_inr_bx divnone">
                        <select id="slctculb" class="slctmsgclub">
                            <option value="0">Select</option>
        <?php 
        foreach ($clublist as $club) {
            $sel = '';
            echo '<option value="'.$club['club_id'].'" '.$sel.'>'.
                $club['club_name'].'</option>';
        }
                            ?>
                        </select>
                        <div class="focus_pushcharcounts">
                            <label for="messagetext" style="display: none;"></label>
                            <textarea id="messagetext" class="messagetext"></textarea><br/>
                            <label class="leftchars"></label>
                        </div>
                        <button class="add_qtn_btn" onclick="pushMessageClub()">Add Message</button>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" class="form-control loggedUserId"
    value="<?php echo $_SESSION['user']['user_id']; ?>" name="loggedUserId"  />
    </div>