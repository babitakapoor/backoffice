<?php
/**
 * PHP version 5.
 
 * @category Admin
 
 * @package Managequestionnaire
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description To manage user type page access permissions.
 */
//To get menus list
global $LANG;
$questionnaireList = $this->settings->getQuestionaries();
?>
<div class="content-wrapper" id="manage-members">
    <div class="con-title-sec pos-fixed mt40">
        <h1>
            <span class="icon icon-set"></span>
            <?php echo $LANG['manageQuestionnaire']; ?>
        </h1>
        <div class="user-features">
            <ul>
                <li>
                    <a href="../admin/index.php?p=settings"
                       title="<?php echo $LANG['backToSettings']; ?>">
                        <span class="icon icon-back"></span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="con-sec pt100">
    <!-- <div class="select-custom clear-left-menu"></div> -->
    <!--EDISON 20160120 -->
    <div class="row-sec member-search-sec">
        <form name="managequestionnaire"
            id="manageQuestionnaireForm"
            action=""
            method="post">
            <div class="col6">
                <label>&nbsp;</label>
            </div>
            <div class="col6">
<?php 
if ($_SESSION['page_add'] == 1) { ?>
        <a href="../admin/index.php?p=question" 
            class="btn black-btn fr"><?php echo $LANG['addQuestionnaire'];?>
        </a>
<?php 
} 
?>
            </div>
            <input type="hidden"
                class="form-control" 
                value="<?php echo $_SESSION['user']['user_id']; ?>" 
                name="loggedUserId"  />
        </form>
   </div>
        <!-- Flash message begins -->
        <div>
    <?php
    if (isset($_SESSION['flMsg'])) {
        if (isset($_SESSION['flMsg']['flashMessageError'])) {
            echo '<div class="pageFlashMsg error">'.
                $_SESSION['flMsg']['flashMessageError'].'</div>';
        } elseif (isset($_SESSION['flMsg']['flashMessageSuccess'])) {
            echo '<div class="pageFlashMsg success">'.
                $_SESSION['flMsg']['flashMessageSuccess'].'</div>';
        }
        unset($_SESSION['flMsg']);
    }

    ?>
            <div>&nbsp;</div>
        </div>
        <!-- Flash message ends -->

        <div class="tabOuterDiv">
            <ul class="tabs">
                <li class="current">
                    <a href="#tab-1">
                        <?php echo $LANG['questionnaires']; ?>
                    </a>
                </li>
            </ul>
            <div class="clear"></div>
            <div class="tabs-container">
                <div id="tab-1" class="tabscontent">
                    <form action="" method="post">
                        <div class="clear"></div>
                        <!--grid-->
                        <div class="grid-block">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <thead>
                                <tr class="grid-title">
                                    <td><?php echo $LANG['questionnaireId']; ?></td>
                                    <td><?php echo $LANG['questionnaireTitle']; ?></td>
                                    <td><?php echo $LANG['questionnaireText']; ?></td>
                                    <td class="txt-center" ><?php echo $LANG['action']; ?></td>
                                </tr>
                                </thead>
                                <?php
        if (is_array($questionnaireList) && count($questionnaireList) > 0) {
            $result = '';
            foreach ($questionnaireList as $res) {
                $editLink = '';
                $deleteLink = '';
                if ($_SESSION['page_edit'] == 1) {
                                       $editLink = <<<EDIT_LINK
                                <a title="{$LANG['titleEdit']}" 
                                    class="btn-link btn-inline dotline-sep
                                    icon-edit-menu" 
                                    onclick
                                    ="editQuestionary('Add/Edit Questionnaire',
                                        'manageQuestionnaire',{$res['form_id']});">
                                        <span class="icon icon-edit"></span>
                                </a>
EDIT_LINK;
                }
                if ($_SESSION['page_delete'] == 1) {
                    $deleteLink = <<<EDIT_LINK
                                <a title="{$LANG['titleDelete']}" 
                                    class="btn-link btn-inline icon-delete-menu" 
                                    menuIcoId="{$res['form_id']}" 
                                    onclick="showQuickdeletePop
                                    ('Add/Edit Questionnaire',
                                        'deletequestionnaire',{$res['form_id']});">
                                    <span class="icon icon-cls-sm"></span>
                                </a>
EDIT_LINK;
                }
                $result .= <<<QUESTIONNAIRE
                                <tr>
                                    <td>{$res['form_id']}</td>
                                    <td>{$res['form_name']}</td>
                                    <td>{$res['form_text']}</td>
                                    <td>
                                    {$editLink}
                                    {$deleteLink}
                                    </td>
                                </tr>
QUESTIONNAIRE;
            }
            echo $result;
        } else {
            ?>
            <tr>
                <td colspan="5">No result found</td>
            </tr>
            <?php
        } ?>
                            </table>
                        </div>
                        <!-- Save button -->
                    </form>
                    <!-- Save button -->
                </div>
            </div>
        </div>
    </div>
</div>