<?php 
/**
 * PHP version 5.
 
 * @category Admin
 
 * @package Translation
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description To display stregth list.
 */
 /* To List the strength Program in Dropdown */
$notificationSettings = 	$this->notifications->getPushNotificationsSettings();
//echo "<pre>";print_r($notificationSettings);
$nottype = '';
$notday = '';
$mess = '';
$formType = 'save_push_message';
$selectBox = array();
$disableCheck = false;
$profileValues= array();
$savedValues= array();
$profile = '';
$profileCat = '';
if(isset($_REQUEST['id']))
{
	$disableCheck = true;
	$notificationData = $this->notifications->getPushNotificationsData($_REQUEST['id']);
	if(isset($notificationData['data']['message_data']['id']))
	{
		//echo "<pre>";print_r($notificationData['data']);
		$formType = 'edit_push_message';
		$nottype = $notificationData['data']['message_data']['notification_type_id'];
		$notday = $notificationData['data']['message_data']['notification_days_id'];
		$notActivityDay = $notificationData['data']['message_data']['notification_push_activity_id'];
		$profile = $notificationData['data']['message_data']['profile_id'];
		$profileCat = $notificationData['data']['message_data']['profile_cat_id'];
		$messData = $notificationData['data']['mess_data'];
		$selectBox = $this->notifications->getPuhSelectBox($nottype);
	}
	
}
$profiledata = $this->notifications->getProfileData();
$profiles = $profiledata['data']['profile'];
$prfCat = $profiledata['data']['prfCat'];
$types = $this->notifications->getPushType();

?>
<style>
.grid-block label{width:200px}
.grid-block textarea{resize:none}
</style>
<script>
	$(document).ready(function(){
		$(".clshtmlcontentbox").each(function(idx, obj){
			var attrid=$(obj).attr("id");
			var ins=CKEDITOR.instances[attrid];
			if(ins){
				CKEDITOR.remove(ins);
			}
			CKEDITOR.replace( attrid );
		});
		
		$("#content_form input[type='submit']").click( function(e) {
			
			var submitFormVal = true;
			$(".clshtmlcontentbox").each(function(i, j){
				var attrid=$(j).attr("id");
				if($(j).is('[disabled=disabled]') == false)
				{
					console.log(attrid);
					var message = CKEDITOR.instances[attrid].getData().replace(/<[^>]*>/gi, '');
					var messageLength = $.trim(message).length;
					if( !messageLength ) 
					{
						alert( 'Please enter message' );
						submitFormVal =  false;
						return false;
					}
				}
			});
			if(submitFormVal === true)
			{
				return true;
			}
			else
			{
				return false;
			}
		});
		
	});
</script>
<!--To List the translation language -->
<div class="content-wrapper" id="manage-members">
    <div class="con-sec pt100">
        
		<!-- Flash message begins -->
		<div>
			<?php
			if (isset($_SESSION['flMsg']) || isset($_REQUEST['mess'])) 
			{
                if (isset($_SESSION['flMsg']['flashMessageError'])) {
                    echo '<div class="pageFlashMsg error">'.
                        $_SESSION['flMsg']['flashMessageError'].'</div>';
                } elseif (isset($_SESSION['flMsg']['flashMessageSuccess'])) {
                    echo '<div class="pageFlashMsg success">'.
                        $_SESSION['flMsg']['flashMessageSuccess'].'</div>';
                }
				elseif (isset($_REQUEST['mess'])) {
                    echo '<div class="pageFlashMsg error" style="height:40px">'.
                        $_REQUEST['mess'].'</div>';
                }
                unset($_SESSION['flMsg']);
            } 
			?>
			<div>&nbsp;</div>
		</div>
		<div class="tabOuterDiv">
			<ul class="tabs">
				<li class="current">
					<a href="#tab-1">Add Notification Message</a>
				</li>
			</ul>
            <div class="clear"></div>
			<div class="tabs-container">
				<div id="tab-1" class="tabscontent testResultListGrid">
					<div class="row-sec member-search-sec">
						<div class="">
							<a href="../admin/index.php?p=push_notifications" class="btn black-btn fr">
									Back To Push Notification Listing
							</a>
						</div>
					</div>
					<p class="mb15"><span class="count-block"></span></p>
					<!--grid-->
					<div class="grid-block" style="border:none !important;margin: 0 auto;width:50%;">
						<form id="content_form">
							<div style="padding:10px;float:left;width:100%">
								<input type="hidden" name="action_type" value="<?php echo $formType ?>">
								<input type="hidden" name="p" value="<?php echo $_REQUEST['p'] ?>">
								<input type="hidden" name="id" value="<?php echo (isset($_REQUEST['id']))?$_REQUEST['id']:'' ?>">
								<label>Notification Type<span style="color:red">*</span></label>
								<select name="notification_type_id" required='required' id="type_select" <?php echo ($disableCheck == 1)?'disabled="disabled"':'' ?>>
									<option value="">Select Option</option>
									<?php
										foreach($types['data'] as $type)
										{
									?>
											<option value="<?php echo $type['id'] ?>" <?php echo ($nottype==$type['id'])?'selected="selected"':'' ?>><?php echo $type['name'] ?></option>
									<?php
										}
									?>
								</select> 
							</div>
							<?php
							if(!empty($selectBox))
							{
							?>
								<div style="padding:10px;float:left;width:100%;" id="select_box_ajax">
									<label>Activity Type<span style="color:red">*</span></label>
									<select name="notification_push_activity_id" required='required'>
									<option value="">Select Option</option>
									<?php
									if(!isset($selectBox['data'][0]))
									{
										$selectBox['data'] = array('0'=>$selectBox['data']);
									}
									foreach($selectBox['data'] as $selKey => $sel)
									{
									?>
										<option value="<?php echo $sel['id'] ?>" <?php echo ($sel['id'] == $notActivityDay)?'selected="selected"':'' ?>><?php echo $sel['name'] ?></option>
									<?php
									}
									?>
									</select>
								</div>
							<?php
							}
							else
							{
							?>
								<div style="padding:10px;float:left;width:100%;" id="select_box_ajax">
									<label>Activity Type<span style="color:red">*</span></label>
									<select name="notification_push_activity_id" required='required' id="activity_type_select">
										<option value="">Select Option</option>
									</select>
								</div>
							<?php
							}
							?>
							<div style="padding:10px;float:left;width:100%">
								<label>Notification Day<span style="color:red">*</span></label>
								<select name="notification_days_id" required='required' <?php echo ($notday != "")?"disabled='disabled'":'' ?>>
									<option>Select Day</option>
									<?php
										foreach($notificationSettings['data']['day'] as $day)
										{
									?>
											<option value="<?php echo $day['id'] ?>" <?php echo ($notday==$day['id'])?'selected="selected"':'' ?>><?php echo $day['name'] ?></option>
									<?php
										}
									?>
								</select> 
							</div>
							<div style="padding:10px;float:left;width:100%" id="profile_div">
								<label>Profile<span style="color:red">*</span></label>
								<select name="profile_id" required='required'>
									<option value="">Select Profile</option>
									<?php
										foreach($profiles as $prf)
										{
									?>
											<option value="<?php echo $prf['id'] ?>" <?php echo ($prf['id'] == $profile)?'selected="selected"':'' ?>><?php echo $prf['name'] ?></option>
									<?php
										}
									?>
								</select> 
							</div>
							<div style="padding:10px;float:left;width:100%" id="profile_cat_div">
								<label>Profile Type<span style="color:red">*</span></label>
								<select name="profile_cat_id" required='required'>
									<option value="">Select Profile Type</option>
									<?php
										foreach($prfCat as $prfCa)
										{
									?>
											<option value="<?php echo $prfCa['id'] ?>" <?php echo ($prfCa['id'] == $profileCat)?'selected="selected"':'' ?>><?php echo $prfCa['name'] ?></option>
									<?php
										}
									?>
								</select> 
							</div>
							<?php
							if(!empty($messData))
							{
								$notime = false;
								if(!isset($messData[0]))
								{
									$notime = true;
									$messData = array('0'=>$messData);
								}
								foreach($messData as $key => $mess)
								{
									if($key == 0)
									{
										$text = 'First';
									}
									else
									{
										$text = 'Second';
									}
									if((isset($mess['time']) && $mess['time'] != '') && $notime == false)
									{
							?>
										<div style="padding:10px;float:left;width:100%">
											<label><?php echo $text ?> Time<span style="color:red">*</span></label>
											<input type ="text" name="message[<?php echo $key ?>][time]" class="time_fields" value="<?php echo $mess['time'] ?>"/>
										</div> 
							<?php
									}
									if(isset($mess['message']) && $mess['message'] != '')
									{
										$textLabel = ($notime == false)?$text.' Time Message':'Message';
							?>
										<div style="padding:10px;float:left;width:100%">
											<label><?php echo $textLabel ?><span style="color:red">*</span></label>
											<textarea name="message[<?php echo $key ?>][mess]" id="message_<?php echo $key ?>_mess" rows="10" cols="50" class="clshtmlcontentbox reminder_textarea"><?php echo $mess['message'] ?></textarea>
										</div>
							<?php
									}
								}
							?>
								
							<?php
							}
							else
							{
							?>
								<div class="genral_div">
									<div style="padding:10px;float:left;width:100%">
										<label>Message<span style="color:red">*</span></label>
										<textarea name="message[0][mess]" id="general_message_0_mess" rows="10" cols="50"  class="clshtmlcontentbox genral_div" dis><?php echo $mess ?></textarea>
									</div>
								</div>
								<div class="reminder_div" style="display:none">
									
									<div style="padding:10px;float:left;width:100%">
									
										<div style="width:100%;float:left;font-weight:bold;font-size:10px"><span>Please add time in 24 hour format like this 00:00</span></div>
										<div style="width:100%;float:left">
										<label>First Time<span style="color:red">*</span></label>
										<input type ="text" name="message[0][time]" disabled="disabled" class="time_fields"/>
										</div>
									</div> 
									<div style="padding:10px;float:left;width:100%">
										<label>First Time Message<span style="color:red">*</span></label>
										<textarea name="message[0][mess]" id="reminder_message_0_mess" rows="10" cols="50" disabled="disabled" class="clshtmlcontentbox reminder_textarea"><?php echo $mess ?></textarea>
									</div>
									
									<div style="padding:10px;float:left;width:100%">
										<div style="width:100%;float:left;font-weight:bold;font-size:10px"><span>Please add time in 24 hour format like this 00:00</span></div>
										<div style="width:100%;float:left">
										<label>Second Time<span style="color:red">*</span></label>
										<input type ="text" name="message[1][time]" disabled="disabled" class="time_fields"/>
										</div>
									</div> 
									<div style="padding:10px;float:left;width:100%">
										<label>Second Time Message<span style="color:red">*</span></label>
										<textarea name="message[1][mess]" id="reminder_message_1_mess" rows="10" cols="50"  disabled="disabled" class="clshtmlcontentbox reminder_textarea"><?php echo $mess ?></textarea>
									</div>
								</div>
							<?php
							}
							?>
							<div style="padding:10px;float:left;width:100%">
								<input class="btn black-btn fr" value="Save" type="submit">
							</div>
						</form>
					</div>
				</div>
			</div>
        </div>
	</div>
</div>
<script>
	$(function(){
		$('#type_select').change(function(){
			var notificationType = $(this).val();
			if(notificationType != '' && notificationType != undefined)
			{
				$.ajax({
					url: '../ajax/index.php?p=notification',
					type: "POST",
					dataType: 'json',
					data:{'action':'getPushSelectBox','type':notificationType},
					success: function(response){
						if(response.status_code == 200)
						{
							$('#activity_type_select').empty();
							$('#activity_type_select').append($("<option></option>").attr("value","").text('Select Option'));
							$.each(response.data,function(i,j){
								
								$('#activity_type_select').append($("<option></option>").attr("value",j.id ).text(j.name));
							});
							if(notificationType == 3)
							{
								$('#general_message_0_mess').attr('disabled',true);
								$('.genral_div').hide();
								$('#profile_div select').removeAttr('disabled');
								$('#profile_cat_div select').removeAttr('disabled');
								$('.reminder_div input').removeAttr('disabled');
								$('.reminder_textarea').removeAttr('disabled');
								$('.reminder_div').show();
								$('#profile_div').show();
								$('#profile_cat_div').show();
							}
							else
							{
								$('#profile_div select').attr('disabled',true);
								$('#profile_cat_div select').attr('disabled',true);
								
								$('.reminder_div input').attr('disabled',true)
								$('.reminder_textarea').attr('disabled',true);
								$('#profile_div').hide();
								$('#profile_cat_div').hide();
								$('.reminder_div').hide();
								
								$('#general_message_0_mess').removeAttr('disabled');
								$('.genral_div').show();
								
							}
						}
						else
						{
							alert('Something wrong happened! Please try again');
						}
					}
				});
			}
			else
			{
				$('#activity_type_select').empty();
				$('#activity_type_select').append($("<option></option>").attr("value","").text('Select Option'));
			}
		});
	});
</script>
<script type='text/javascript' src='/movesmart/backoffice/js/jquery.mask.js'></script>
<script type="text/javascript">
jQuery(document).ready(function() {	
	//$('.time_fields').mask('00:00');
});
</script>