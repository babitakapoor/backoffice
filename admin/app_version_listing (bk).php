<?php
/**
 * PHP version 5.
 
 * @category Admin
 
 * @package Resetusers
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Get Cycle Associated with members.
 */
 
/**
* Returns reset type string.
*
* @param int $userid user id
*
* @return string type
*/
global $LANG;

if(isset($_REQUEST['type']) && (!is_numeric($_REQUEST['type']) && $_REQUEST['type'] != '') )
{
	$type  = $_REQUEST['type'];
	$appinfo = $this->appVersionListing->getAppVersions($type);
}
else
{
	$url = 'index.php?p=resetusers';
	$this->appVersionListing->redirectUser($url);
	die;
}


$appinforows = isset($appinfo['data']) && !empty($appinfo['data'])?$appinfo['data'] : array();
if ($appinfo['total_records'] == 1) 
{
    $appinforows = array($appinforows);
}
?>
<div class="content-wrapper" id="coach-list">
    <div class="con-title-sec pos-fixed mt40" style="padding-left:10px">
       
        <div class="user-features">
          <ul>
            <li>
                <a href="../admin/index.php?p=settings" title="<?php echo $LANG['backToSettings']; ?>"> <span class="icon icon-back" ></span> </a>
            </li>
          </ul>
        </div>
    </div>
    <div class="con-sec pt100">
        <div class="row-sec" align="center">
			<div class="col9 successSetMessgae success-msg" align="center" style="display:none;">
                <div class="col9 fadeMsg"></div>
            </div>
        </div>
        <div class="tabOuterDiv" style="margin-top:40px">
            <ul class="tabs">
                <li class="current"><a href="#tab-1">
                    <?php echo $LANG['app_details']; ?></a>
                </li>
            </ul>
            <div class="clear"></div>
            <div class="tabs-container">
                <div id="tab-1" class="tabscontent">
                    <div class="clear"></div>
                    <div class="grid-block" id="coachListGriddiv">
						<input type="hidden" class="paramNone" value="?p=coachManagementEdit">
						<table width="100%" border="0" cellspacing="0" cellpadding="0" id="coachListGridTab">
							<thead>
								<tr>
									<td>S.No</td>
									<td>Coach Name</td>
									<td>Club Name</td>
									<td>App Platform</td>
									<td>Device OS</td>
									<td>Device Model</td>
									<td>App Version</td>
									<td>Status</td>
									<td>Activation Period</td>
								</tr>
							</thead>
							<tbody class="pgcnt">
							<?php
								$sno = 1;
								if (count($appinforows) > 0) 
								{
									foreach ($appinforows as $key => $row) 
									{
										$styleRow = '';
										$styleTd = '';
										if($row['status'] != 1)
										{
											$styleRow="background:#b74c4c;";
											$styleTd = 'color:#fff';
										}
								?>
									<tr style="<?php echo $styleRow ?>">
										<td style="<?php echo $styleTd ?>"><?php echo $sno?></td>
										<td style="<?php echo $styleTd ?>"><?php echo $row['user_name']?></td>
										<td style="<?php echo $styleTd ?>"><?php echo $row['club_name'];?></td>
										<td style="<?php echo $styleTd ?>"><?php echo $row['app_platform'];?></td>
										<td style="<?php echo $styleTd ?>"><?php echo $row['device_version']?></td>
										<td style="<?php echo $styleTd ?>"><?php echo $row['device_model']; ?></td>
										<td style="<?php echo $styleTd ?>"><?php echo $row['app_version']; ?></td>
										<td style="<?php echo $styleTd ?>"><?php if($row['status'] == 1) { echo 'Active'; } else { echo 'Inactive'; } ?></td>
										<td style="<?php echo $styleTd ?>">
											<?php echo date('d M Y h:i:s a',strtotime($row['start_date'])); ?>
										</td>
									</tr>
								<?php 
									++$sno;
									}
								} 
								else 
								{
								?>
									<tr>
										<td colspan="7" style="text-align:center">No Results Found</td>
									</tr>
								<?php
								}    
								?>
							</tbody>
						</table>
					</div>
                    <div class="pagination-block">
						<?php
						if (isset($arrayList) && !empty($arrayList) && isset($arrayList[0]['user_id']) && isset($totalCount)) 
						{
							echo $this->paginator->displayPagination($totalCount);
						}
						?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

