<?php
/**
 * PHP version 5.
 
 * @category Admin
 
 * @package ManageBrand
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description To manage user type page access permissions.
 */
global $LANG;
$companylist = $this->settings->getAllCompany();
//print_r($companylist['getCompany']);
$companylistrows = isset($companylist['getCompany']) ? $companylist['getCompany'] : array();
//print_r($companylistrows[0]['company_name']);
if ($companylist['total_records'] == 1) {
    $companylistrows = array($companylistrows);
}
?>
<div class="content-wrapper" id="manage-members">
    <div class="con-title-sec pos-fixed mt40">
      <h1><span class="icon icon-lmo"></span>Manage Company</h1>
      <div class="user-features">
          <ul>
            <li>
                <a href="../admin/index.php?p=settings"
                    title="<?php echo $LANG['backToSettings']; ?>">
                    <span class="icon icon-back"></span>
                </a>
            </li>
          </ul>
      </div>
    </div>
    <div class="con-sec pt100">
    <!-- <div class="select-custom clear-left-menu"></div> --><!--EDISON 20160120 -->
    <div class="row-sec member-search-sec">
        <form name="managebrand" id="managebrandForm" action="" method="post">
            <div class="col6">
                <label>&nbsp;</label>
            </div>
            <div class="col6">
    <?php if ($_SESSION['page_add'] == 1) {
    ?>
            <a href="javascript:void(0);"
                onclick="showQuickAddPop('Add Company','manageCompany');"
                class="btn black-btn fr">
                ADD Company</a>
    <?php
    } ?>
            </div>
            <input type="hidden" class="form-control loggedUserId"
                value="<?php echo $_SESSION['user']['user_id']; ?>"
                name="loggedUserId"  />
        </form>
    </div>
        <!-- Flash message begins -->
        <div>
    <?php

    if (isset($_SESSION['flMsg'])) {
        if (isset($_SESSION['flMsg']['flashMessageError'])) {
            echo '<div class="pageFlashMsg error">'.
                $_SESSION['flMsg']['flashMessageError'].'</div>';
        } elseif (isset($_SESSION['flMsg']['flashMessageSuccess'])) {
            echo '<div class="pageFlashMsg success">'.
                $_SESSION['flMsg']['flashMessageSuccess'].'</div>';
        }
        unset($_SESSION['flMsg']);
    }

    ?>
        <div>&nbsp;</div>
    </div>
    <!-- Flash message ends -->

      <div class="tabOuterDiv">
            <ul class="tabs">
                <li class="current"><a href="#tab-1">Companies</a></li>
            </ul>
            <div class="clear"></div>
            <div class="tabs-container">
            <div id="tab-1" class="tabscontent">
                <div class="clear"></div>
             <!--grid-->
                <div class="grid-block">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <thead>
                            <tr class="grid-title">
                                <td>sno</td>
                                <td>Company Name</td>
                                <td>Address</td>
								<td>Status</td>
								
                                <!-- ED 20160501 -->
                                <td class="actionwidth txt-center"><?php echo $LANG['action']; ?></td>
                            </tr>
                        </thead>
                        <tbody>
        <?php 
                        $sno = 1;
        if (is_array($companylistrows) && count($companylistrows) > 0) {
            $result = '';
            foreach ($companylistrows as $res) {
				$editLink = '';
                $deleteLink = '';
				$enableLink = '';
                if ($_SESSION['page_edit'] == 1) {
                    $editLink = <<<EDIT_LINK
                        <a title="{$LANG['titleEdit']}" 
                            class="btn-link btn-inline dotline-sep icon-edit-menu" 
                            onclick="showQuickAddPop('Edit Company',
                                'manageCompany',{$res['company_id']});">
                            <span class="icon icon-edit"></span>
                        </a>
EDIT_LINK;
                }
				if($res['company_status'] == 1)
				{
					$status = 0;
                if ($_SESSION['page_delete'] == 1) {
					
                    $deleteLink = <<<EDIT_LINK
                        <a title="Disabled" 
                        class="btn-link btn-inline icon-delete-menu" 
                            menuIcoId="{$res['company_id']}" 
                            onclick="deleteCompany('Add/Edit Company',
                                'deleteCompany',
                                {$res['company_id']},{$status});">
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
EDIT_LINK;
                }
				}
				else{
					$status = 1;
				if ($_SESSION['page_delete'] == 1) {
                    $deleteLink = <<<EDIT_LINK
                        <a title="Enabled" 
                        class="btn-link btn-inline icon-delete-menu" 
                            menuIcoId="{$res['company_id']}" 
                            onclick="deleteCompany('Add/Edit Company',
                                'deleteCompany',
                                {$res['company_id']},{$status});">
                            <span class="glyphicon glyphicon-ok"></span>
                        </a>
EDIT_LINK;
                }
				}
                ?>
                                <tr>
                                <td><?php echo $sno; ?></td>
                                <td><?php echo $res['company_name']; ?></td>
                                <td><?php echo $res['company_address']; ?></td>
								<td><?php echo $res['company_status'] == 1 ? 'Enabled' : 'Disabled'; ?></td>
								<td class="txt-center" >
                                    <?php echo $editLink.$deleteLink?>
                                </td>
                                </tr>
                                <?php ++$sno;
            }
        } else {
            ?>
                            <tr><td colspan="5">No Results Found</td></tr>
        <?php
        } ?>
                        </tbody>
                    </table>
                </div>
                </div>
        </div>
</div>
<script>
/**********21july2017********************/
function deleteCompany(popTitle, popPageName, companyId, status){
	
    confirms(LANG['confirmDeleteCompany'], function(confirmResult) {
        if (confirmResult == "true") {
            ajLoaderOn();
            var dataString = "companyId=" + companyId + "&action=deleteCompany" + "&status=" + status;
             $.ajax({
                type: "POST",
                url: "../ajax/index.php?p=settings",
                data: dataString,
                cache: false,
                success: function() {//result
                    ajLoaderOff();
                    //alerts(LANG['deleteSuccess']);
                    flashMsgDisplay(LANG['updatestatusSuccess'], 'success-msg');
                    setTimeout(function() {
                        location.reload();
                    }, 1500);
                }
            });
            closeMsg();
        } else {
            closeMsg();
        }
    });
}
function deleteMachine(popTitle, popPageName, machineId){
    confirms(LANG['confirmDeleteBrand'], function(confirmResult) {
        if (confirmResult == "true") {
            ajLoaderOn();
            var dataString = "machineId=" + machineId + "&action=deleteMachine";
             $.ajax({
                type: "POST",
                url: "../ajax/index.php?p=settings",
                data: dataString,
                cache: false,
                success: function() {//result
                    ajLoaderOff();
                    //alerts(LANG['deleteSuccess']);
                    flashMsgDisplay(LANG['deletebrandSuccess'], 'success-msg');
                    setTimeout(function() {
                        location.reload();
                    }, 1500);
                }
            });
            closeMsg();
        } else {
            closeMsg();
        }
    });
}
</script>