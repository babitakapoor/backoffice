<?php
/**
 * PHP version 5.
 
 * @category Admin
 
 * @package Testrange
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Handle Test Range data.
 */
?>
<script>
	CKEDITOR.replace( 'editor1' );
</script>
<div class="content-wrapper" id="manage-members">
	<div class="con-title-sec pos-fixed mt40">
	  <h1><span class="icon icon-set"></span>
        <?php echo $LANG['manageQuestionnaire']; ?></h1>
	  <div class="user-features">
		  <ul>		  
			<li>
				<a href="../admin/index.php?p=settings" 
                    title="<?php echo $LANG['backToSettings']; ?>">
					<span class="icon icon-back"></span>
				</a>
			</li>
		  </ul>
	  </div>
	</div>
<div class="content-wrapper" id="manage-members">
	<div class="con-title-sec pos-fixed mt40">
	  <div class="user-features">
		  <ul>		  
			<li>
				<a href="../admin/index.php?p=settings" 
                    title="<?php echo $LANG['backToSettings']; ?>">
					<span class="icon icon-back"></span>
				</a>
			</li>
		  </ul>
	  </div>
	</div>
	<div class="con-sec pt100">
	<!-- <div class="select-custom clear-left-menu">
    </div> --><!--EDISON 20160120 -->
	<div class="row-sec member-search-sec">
		
   </div>
<div class="acc-row add-menu">
	<div class="ajaxMsg" display="none;"></div>
	<div class="acc-content">
		<div class="row-sec mb15">
			<div class="row-sec mb15">
				<label class="fl">Point(index) Weight<span 
                    class="required">*</span></label>
				<input type="text" id="pointweight" value="">
			</div>	
		</div>
		<div class="row-sec mb15">
			<div class="row-sec mb15">
				<label class="fl">Max Weight<span 
                    class="required">*</span></label>
				<input type="text" id="maxweight" value="">
			</div>	
		</div>
		<div class="row-sec mb15">
			<div class="row-sec mb15">
				<label class="fl">Range Based Text<span 
                    class="required">*</span></label>
			</div>	
		</div>
		<div class="row-sec mb15">				
			<div class="row-sec mb15">
				<label class="fl">Sucess Text<span 
                    class="required">*</span></label>
				<textarea class="quesinfo" 
                placeholder="Question Info in English" 
                languageid="9"></textarea>
			</div>	
			<div class="row-sec mb15">
				<label class="fl">&nbsp;</label>
				<textarea class="quesinfo" 
                placeholder="Question Info in russian" 
                languageid="10"></textarea>
			</div>	
			<div class="row-sec mb15">
				<label class="fl">&nbsp;</label>
				<textarea class="quesinfo" 
                placeholder="Question Info in dutch" 
                languageid="11"></textarea>
			</div>	
		</div>
		<div class="row-sec mb15">				
			<div class="row-sec mb15">
				<label class="fl">Quick Win Text<span 
                class="required">*</span></label>
				<textarea class="quesinfo" 
                placeholder="Question Info in English" 
                languageid="9"></textarea>
			</div>	
			<div class="row-sec mb15">
				<label class="fl">&nbsp;</label>
				<textarea class="quesinfo" 
                    placeholder="Question Info in russian" 
                    languageid="10"></textarea>
			</div>	
			<div class="row-sec mb15">
				<label class="fl">&nbsp;</label>
				<textarea class="quesinfo" 
                    placeholder="Question Info in dutch" 
                    languageid="11"></textarea>
			</div>	
		</div>
		<div class="row-sec mb15">				
			<div class="row-sec mb15">
				<label class="fl">Goal Text<span 
                class="required">*</span></label>
				<textarea class="quesinfo" 
                    placeholder="Question Info in English" 
                    languageid="9"></textarea>
			</div>	
			<div class="row-sec mb15">
				<label class="fl">&nbsp;</label>
				<textarea class="quesinfo" 
                    placeholder="Question Info in russian" 
                    languageid="10"></textarea>
			</div>	
			<div class="row-sec mb15">
				<label class="fl">&nbsp;</label>
				<textarea class="quesinfo" 
                    placeholder="Question Info in dutch" 
                    languageid="11"></textarea>
			</div>	
		</div>
		<div class="row-sec mb15">				
			<div class="row-sec mb15">
				<label class="fl">Reduce Text<span 
                class="required">*</span></label>
				<textarea class="quesinfo" 
                    placeholder="Question Info in English" 
                    languageid="9"></textarea>
			</div>	
			<div class="row-sec mb15">
				<label class="fl">&nbsp;</label>
				<textarea class="quesinfo" 
                    placeholder="Question Info in russian" 
                    languageid="10"></textarea>
			</div>	
			<div class="row-sec mb15">
				<label class="fl">&nbsp;</label>
				<textarea class="quesinfo" 
                    placeholder="Question Info in dutch" 
                    languageid="11"></textarea>
			</div>	
		</div>
		<div class="clear"></div>
			<div class="row-sec btn-sec">
				<input type="hidden" id="groupid" value="">
				<input type="hidden" id="phaseid" value="">
				<input type="button" 
                    class="pop_cancel_btn btn black-btn fr" 
                    value="<?php echo $LANG['btnCancel'];?>">
				<input type="button" 
                    onclick="manageQuestionnairePopup()"
                        class="btn black-btn fr" 
                        value="<?php echo $LANG['btnSave'];?>">
			</div>  
	</div>	
</div>
<div class="clear"></div>