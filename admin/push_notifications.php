<?php 
/**
 * PHP version 5.
 
 * @category Admin
 
 * @package Translation
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description To display stregth list.
 */
 /* To List the strength Program in Dropdown */
    global $LANG;
    $param['searchType'] = (isset($_REQUEST['searchType']) ? 
        $_REQUEST['searchType'] : '');
    $param['searchValue'] = (isset($_REQUEST['searchValue']) ? 
        ($_REQUEST['searchType'] == 'gender' ? 
            (strstr($_REQUEST['searchValue'], 'f') ? 
                '1' : '0') : $_REQUEST['searchValue']) : '');

$records = 	$this->notifications->getPushNotifications();
//echo "<pre>";print_r($records);die;
?>
<!--To List the translation language -->
<div class="content-wrapper" id="manage-members">
    <div class="con-sec pt100">
        
		<!-- Flash message begins -->
		<div>
			<?php
            if (isset($_SESSION['flMsg'])) 
			{
                if (isset($_SESSION['flMsg']['flashMessageError'])) {
                    echo '<div class="pageFlashMsg error">'.
                        $_SESSION['flMsg']['flashMessageError'].'</div>';
                } elseif (isset($_SESSION['flMsg']['flashMessageSuccess'])) {
                    echo '<div class="pageFlashMsg success">'.
                        $_SESSION['flMsg']['flashMessageSuccess'].'</div>';
                }
                unset($_SESSION['flMsg']);
            } 
			?>
			<div>&nbsp;</div>
		</div>
		<div class="tabOuterDiv">
			<ul class="tabs">
				<li class="current">
					<a href="#tab-1">Push</a>
				</li>
			</ul>
            <div class="clear"></div>
			<div class="tabs-container">
				<div id="tab-1" class="tabscontent testResultListGrid">
					<div class="clear"></div>
					<p class="mb15"><span class="count-block"></span></p>
					<!--grid-->
					<div class="row-sec member-search-sec">
						<div class="">
							<?php 
								if ($_SESSION['page_add'] == 1) { 
							?>
								<a href="?p=add_pushnotification&type" class="btn black-btn fr">
									Add Push Notification Message
								</a>
							<?php 
								} 
							?>
						</div>
					</div>
					<div class="grid-block">
						<input type="hidden" class="paramNone" value="?p=strengthProgramEdit">
						<input type="hidden" id="strength_program_id" value="">
						<div class="pagination-block">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" id="memberListGridTab">
								<thead>
									<tr class="grid-title toggle-label" linkdata="<?php echo $_REQUEST['p'];?>">
										<td>Sr No</td>
										<td>Notification type</td>
										<td>Notification day</td>
										<td align="center">Action</td>
									</tr>
								</thead>
								<tbody class="trnslang">
								<?php
								if(isset($records['data']) && !empty($records['data']))
								{
									$sno= 1;
									$canEdit = false;
									$canDelete = false;
									if ($_SESSION['page_edit'] == 1) 
									{
										$canEdit = true;
									}
									if ($_SESSION['page_delete'] == 1) {
										$canDelete = true;
									}
									//$count = count($tabsDiv[$tabsDiv['slug']]);
									if(!isset($records['data'][0]))
									{
										$records['data'] = array(0=>$records['data']);
									}
									foreach($records['data'] as $notdata)
									{
										//echo "<pre>";print_r($notdata);die;
								?>
										<tr>
											<td><?php echo $sno?></td>
											<td>
												<?php 
													$types = array('1'=>'General','2'=>'Fase 1 Started','3'=>'Fase 1 Reminders');
													echo $types[$notdata['notification_type']];
												?>
											</td>
											<td><?php echo $notdata['day'] ?></td>
											<td class="txt-center" >
												<?php 
												if($canEdit == true)
												{
												?>
													<a title="Edit" href ="?p=add_pushnotification&id=<?php echo $notdata['id'] ?>" class="btn-link btn-inline dotline-sep align-center">
														<span class="icon icon-edit"></span>
													</a>
												<?php
												}
												if($canDelete == true)
												{
												?>
													<a title="Delete" href ="?p=delete_pushnotification&id=<?php echo $notdata['id'] ?>" class="btn-link btn-inline dotline-sep align-center" onclick="if (confirm('Are you sure? You really want to delete this message.')) { return true; } return false;">
														<span class="icon icon-cls-sm"></span>
													</a>
												<?php
												}
												?>
											</td>
										</tr>
								<?php
										++$sno;
									}
								}
								else
								{
								?>
									<tr><td colspan="5">No Results Found</td></tr>
								<?php
								}
								?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
        </div>
	</div>
</div>