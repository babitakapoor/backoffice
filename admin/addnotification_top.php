<?php 
/**
 * PHP version 5.
 
 * @category Admin
 
 * @package Translation
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description To display stregth list.
 */
 /* To List the strength Program in Dropdown */
$notificationSettings = 	$this->notifications->getNotificationsSettings($_REQUEST['type']);
switch($_REQUEST['type'])
{
	case(1):
		$tab = 'Move';
		$urltab = 'movesmart';
		break;
	case(2):
		$tab = 'Mind';
		$urltab = 'mind';
		break;
	case(3):	
		$tab = 'Eat';
		$urltab = 'eat';
		break;
}
$title = 'Add';
$formType = 'save_message';

$profileValues= array();
$savedValues= array();
$notday ='';
$profile = '';
$profileCat = '';
if(isset($_REQUEST['id']))
{
	$disableCheck = true;
	$title = 'Edit';
	$formType = 'edit_message';
	$notificationData = $this->notifications->getNotificationsData($_REQUEST['id']);
	if(isset($notificationData['data']['top_data']))
	{
		if(!isset($notificationData['data']['top_data'][0]))
		{
			$notificationData['data']['top_data'] = array(0=>$notificationData['data']['top_data']);
		}
		foreach($notificationData['data']['top_data'] as $nt)
		{
			$savedValues[] = $nt['top_id'];
		}
	}
	/*if(isset($notificationData['data']['profile_data']))
	{	
		foreach($notificationData['data']['profile_data'] as $nt)
		{
			$profileValues[] = $nt['profile_id'];
		}
	}*/
	$notday = $notificationData['data']['message_data']['notification_days_id'];
	$profile = $notificationData['data']['message_data']['t_notification_user_profile_id'];
	$profileCat = $notificationData['data']['message_data']['t_notification_user_profile_categorie_id'];
}


$params['type'] = 'top';
$params['top_type'] = 0;
$selectBox = $this->notifications->getSelectBox($params);
$selectBoxData = $selectBox['data'];

$profiledata = $this->notifications->getProfileData();
$profiles = $profiledata['data']['profile'];
$prfCat = $profiledata['data']['prfCat'];
?>
<style>
.grid-block label{width:200px}
.grid-block textarea{resize:none}
</style>
<!--To List the translation language -->
<div class="content-wrapper" id="manage-members">
    <div class="con-sec pt100">
        
		<!-- Flash message begins -->
		<div>
			<?php
			if (isset($_SESSION['flMsg']) || isset($_REQUEST['mess'])) 
			{
                if (isset($_SESSION['flMsg']['flashMessageError'])) {
                    echo '<div class="pageFlashMsg error">'.
                        $_SESSION['flMsg']['flashMessageError'].'</div>';
                } elseif (isset($_SESSION['flMsg']['flashMessageSuccess'])) {
                    echo '<div class="pageFlashMsg success">'.
                        $_SESSION['flMsg']['flashMessageSuccess'].'</div>';
                }
				elseif (isset($_REQUEST['mess'])) {
                    echo '<div class="pageFlashMsg error" style="height:40px">'.
                        $_REQUEST['mess'].'</div>';
                }
                unset($_SESSION['flMsg']);
            } 
			?>
			<div>&nbsp;</div>
		</div>
		<div class="tabOuterDiv">
			<ul class="tabs">
				<li class="current">
					<a href="#tab-1"><?php echo $title." ".$tab." ".ucwords(str_replace("_"," ",$_REQUEST['notification_tab'])) ?> Day</a>
				</li>
			</ul>
            <div class="clear"></div>
			<div class="tabs-container">
				<div id="tab-1" class="tabscontent testResultListGrid">
					<div class="row-sec member-search-sec">
						<div class="">
							<a href="../admin/index.php?p=notifications&notification_tab=<?php echo $urltab ?>" class="btn black-btn fr">
									Back To Notification Listing
							</a>
						</div>
					</div>
					<p class="mb15"><span class="count-block"></span></p>
					<!--grid-->
					<div class="grid-block" style="border:none !important;margin: 0 auto;width:50%;">
						<form>
							<div style="padding:10px;float:left;width:100%">
								<input type="hidden" name="action_type" value="<?php echo $formType ?>">
								<input type="hidden" name="p" value="<?php echo $_REQUEST['p'] ?>">
								<input type="hidden" name="notification_pages_id" value="<?php echo $_REQUEST['type'] ?>">
								<input type="hidden" name="notification_types_id" value="<?php echo $_REQUEST['type_id'] ?>">
								<input type="hidden" name="notification_tab" value="<?php echo $_REQUEST['notification_tab'] ?>">
								<input type="hidden" name="id" value="<?php echo (isset($_REQUEST['id']))?$_REQUEST['id']:'' ?>">
							</div>
							<div style="padding:10px;float:left;width:100%">
								<label>Notification Day<span style="color:red">*</span></label>
								<select name="notification_days_id" required='required' <?php echo ($notday !='')?'disabled="disabled"':'' ?>>
									<option value="">Select Day</option>
									<?php
										foreach($notificationSettings['data']['day'] as $day)
										{
									?>
											<option value="<?php echo $day['id'] ?>" <?php echo ($notday==$day['id'])?'selected="selected"':'' ?>><?php echo $day['name'] ?></option>
									<?php
										}
									?>
								</select> 
							</div>
							<div style="padding:10px;float:left;width:100%;" id="select_box_ajax">
								<label>Notifications Top Types<span style="color:red">*</span></label>
								<select name="notification_tops_type_id[]" required='required' multiple='multiple'>
								<?php
								foreach($selectBoxData as $selKey => $sel)
								{
									
								?>
									<option value="<?php echo $sel['id'] ?>" <?php echo (in_array($sel['id'],$savedValues))?'selected="selected"':'' ?>><?php echo $sel['name'] ?></option>
								<?php
								}
								?>
								</select>
							</div>
							<div style="padding:10px;float:left;width:100%">
								<label>Profile<span style="color:red">*</span></label>
								<select name="profile_id" required='required'>
									<option value="">Select Profile</option>
									<?php
										foreach($profiles as $prf)
										{
									?>
											<option value="<?php echo $prf['id'] ?>" <?php echo ($prf['id'] == $profile)?'selected="selected"':'' ?>><?php echo $prf['name'] ?></option>
									<?php
										}
									?>
								</select> 
							</div>
							<div style="padding:10px;float:left;width:100%">
								<label>Profile Type<span style="color:red">*</span></label>
								<select name="profile_cat_id" required='required'>
									<option value="">Select Profile Type</option>
									<?php
										foreach($prfCat as $prfCa)
										{
									?>
											<option value="<?php echo $prfCa['id'] ?>" <?php echo ($prfCa['id'] == $profileCat)?'selected="selected"':'' ?>><?php echo $prfCa['name'] ?></option>
									<?php
										}
									?>
								</select> 
							</div>
							<div style="padding:10px;float:left;width:100%">
								<input class="btn black-btn fr" value="Save" type="submit">
							</div>
						</form>
					</div>
				</div>
			</div>
        </div>
	</div>
</div>
