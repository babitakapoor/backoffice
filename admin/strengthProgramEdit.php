<?php
/**
 * PHP version 5.
 
 * @category Admin
 
 * @package StrengthProgramEdit
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Page to Add/Edit Strength Programs.
 */
global $LANG;
$strengthProgramId = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;
$programDetail = array();
if ($strengthProgramId > 0) {
    $param['strengthProgramId'] = $strengthProgramId;
    $programDetail = $this->settings->getStrengthProgramDetailById($param);
    $programDetail = $programDetail['rows'];
    // To display strength training list grid and training list edit form data
    $strengthProgramTrainingId = isset($_REQUEST['strengthProgramTrainingId']) ? 
        $_REQUEST['strengthProgramTrainingId'] : 0;
    $paramsTr['strengthProgramId'] = $strengthProgramId;
    $trainingList = $this->settings->getStrengthTrainingList($paramsTr);
    $trainingList = isset($trainingList['rows']) ? $trainingList['rows'] : array();
    $trainingEditArr = array();
    if (count($trainingList) > 0) {
        foreach ($trainingList as $row) {
            $strentrainid = $row['strength_program_training_id'];
            if ($strentrainid == $strengthProgramTrainingId) {
                $trainingEditArr = $row;
                break;
            }
        }
    }

    $btnText = isset($strengthProgramTrainingId) ? $LANG['update'] : $LANG['add'];
    // To display strength training list grid and training list edit form data
}
?>
<form method="post" id="strengthProgramForm">
<div class="content-wrapper">  
    <div class="con-title-sec pos-fixed mt40">
        <h1><span class="icon icon-perinfo"></span>
            <?php echo $LANG['strengthProgram']; ?></h1>	   
        <div class="user-features">
            <ul>
                <li><a title="<?php echo $LANG['titleSave']; ?>" class="btn-link">
                        <span class="icon icon-save" id="saveStrengthProgram" 
                            onclick="saveStrengthProgrm()"></span></a>               
                </li>
                <li>
                    <a href="index.php?p=strengthProgram"
                        title="<?php echo $LANG['titleBack']; ?>">
                        <span class="icon icon-back"></span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="con-sec pt100">
        <input type="hidden" id="companyId" name="companyId" 
            value="<?php echo COMPANY_ID; ?>"/>
        <input type="hidden" id="strengthProgramId" name="strengthProgramId" 
            value="<?php echo isset($programDetail['strength_program_id']) ? 
                $programDetail['strength_program_id'] : 0; ?>"/>
        <input type="hidden" class="form-control loggedUserId" 
            value="<?php echo $_SESSION['user']['user_id']; ?>" 
            name="loggedUserId"  />
        
        <div class="accordion-holder mb20">
        <div class="acc-row">
        <div class="acc-row form-control-groub">
            <h2><?php echo $LANG['strengthProgramDetails']; ?><span 
                class="icon icon-down"></span></h2>
                <div class="acc-content"> 
                    <div class="row-sec">                            
                        <div class="row-sec">
                            <div class="col4">
                                <label class="fl" for="description">
                                    <?php echo $LANG['description']; ?> :<span 
                                    class="required">*</span></label>
                                <input type="text" id="description" 
                                    name="description" class="form-control" 
                                    value=
                                    "<?php echo 
                                    isset($programDetail['description']) ? 
                                    $programDetail['description'] : ''; ?>"/>
                            </div>
                            <div class="col4">
                                <label class="fl" for="strengthpgmtype"><?php echo
                                    $LANG['strengthType']; ?> :<span 
                                    class="required">*</span></label>
                                <div  class="select-custom">
                                    <?php
                                        $strengthProgramType 
                                            = json_decode(
                                                STRENGTH_PROGRAM_TYPE, 
                                                true
                                            );
                                    ?>
                                    <select id="strengthpgmtype" name="type">
                                        <option value=''>- <?php 
                                            echo $LANG['select']; ?> -</option>
                            <?php 
                            foreach ($strengthProgramType as $key => $type) {
                                $sel 
                                    = ($programDetail['type'] == $key) ?
                                    'selected=selected' : '';
                                echo '<option value="'.$key.'" '.
                                $sel.'>'.
                                $type.'</option>';
                            }
                            ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        </div>
            <input type="hidden" class="form-control"
                   value="<?php
                   echo $_SESSION['user']['user_id'];
                   ?>"
                   name="loggedUserId"  />
</form>
<?php
    $pgmtraining = 'style="display:none"';
    if ($strengthProgramId > 0) {
        $pgmtraining = 'style="display:block"';
    }?>
<div class="strengthpgmseries" <?php echo $pgmtraining;?>>
    <form method="post" id="strengthProgramTrainingForm">
        <input type="hidden" id="strengthProgramIdRefer"
               name="strengthProgramIdRefer" value=""/>
        <!--<input type="hidden" id="strengthProgramTrainingId"
        name="strengthProgramTrainingId" value=
        "<?php echo $strengthProgramTrainingId; ?>"/>-->
        <div class="form-control-groub">
            <h2><?php echo $LANG['strengthProgramTraining']; ?>
                <span class="icon icon-down"></span></h2>
            <div class="row-sec">
                <input type="button"
                       value="<?php echo $LANG['save']; ?>"
                       id="saveStrengthProgramTraining" class="btn
                       black-btn fr"
                       onclick="saveStrengthPgmTraining()">
            </div>
            <div class="acc-content">
                <div class="row-sec">
                    <div class="row-sec">
                        <div class="col4">
                            <label class="fl" for="training_week">
                                <?php echo $LANG['trainingWeek']; ?> :
                                <span class="required">*</span></label>
                            <input type="text" id="training_week"
                                   name="training_week" class="form-control
                                   training_cls" value="<?php echo
                            isset($trainingEditArr['training_week']) ?
                                $trainingEditArr['training_week'] : '';
                            ?>"/>
                        </div>
                        <div class="col4">
                            <label class="fl" for="series">
                                <?php echo $LANG['series']; ?> :
                            </label>
                            <input type="text" id="series"
                                   name="series" class="form-control
                                   training_cls"
                                   value="<?php echo
                                   isset($trainingEditArr['series']) ?
                                       $trainingEditArr['series'] : '';
                                   ?>"/>
                        </div>
                        <div class="col4">
                            <label class="fl" for="reputation"><?php echo
                                            $LANG['strengthRep']; ?> :</label>
                            <input type="text"
                                            id="reputation" name="reputation" 
                                            class="form-control training_cls" 
                                            value="<?php echo 
                                            isset($trainingEditArr['reputation']) ? 
                                            $trainingEditArr['reputation'] : ''; 
                                            ?>"/>
                        </div>
                    </div>
                </div>
                <div class="row-sec">
                    <div class="row-sec">
                        <div class="col4">
                            <label class="fl" for="time">
                                <?php
                                    echo $LANG['time'];
                                ?> :
                            </label>
                            <input type="text" id="time"
                                   name="time" class="form-control
                                   training_cls" value="<?php echo
                            isset($trainingEditArr['time']) ?
                                $trainingEditArr['time']: '';
                            ?>"/>
                        </div>
                        <div class="col4">
                            <label class="fl" for="strength_percentage">
                                <?php
                                    echo $LANG['strengthPercentage'];
                                ?> :
                            </label>
                            <input type="text" id="strength_percentage"
                                   name="strength_percentage"
                                   class="form-control training_cls"
                                   value="<?php
                                    echo (
                                        isset($trainingEditArr['strength_percentage'])
                                    ) ? $trainingEditArr['strength_percentage']:'';
                                    ?>"/>
                        </div>
                        <div class="col4">
                            <label class="fl" for="rest">
                                <?php
                                    echo $LANG['rest'];
                                ?>:
                            </label>
                            <input type="text" id="rest" name="rest"
                                   class="form-control training_cls"
                                   value="<?php echo
                                   isset($trainingEditArr['rest']) ?
                                       $trainingEditArr['rest'] : ''; ?>"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" class="form-control"
               value="<?php echo $_SESSION['user']['user_id']; ?>"
               name="loggedUserId"  />
    </form>
</div>
<?php //} ?>
<!-- Page tabs atart -->
<?php if ($strengthProgramId > 0) {

} ?>
<div class="acc-row form-control-groub
                coach-role-left strengthtraining" 
                style="display:none">
    <div class="con-sec">
        <div class="tabOuterDiv">
        <?php require_once 'strengthProgramTraining.php'; ?>
            <div class="clear"></div>
        </div>
    </div>
</div>
<?php //} ?>
<!-- Page tabs end -->
