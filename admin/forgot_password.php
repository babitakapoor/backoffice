<?php


/**
 * PHP version 5.
 
 * @category Admin
 
 * @package Login
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description All login Process
 */
//Check user already logged. 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Movesmart Register</title> 
		<link rel="shortcut icon" href="<?php echo IMG_PATH.DS; ?>fav.ico" />
		<link  href="<?php echo FOLDER_PATH.CSS_PATH.'common/custom.css?version='.MY_VERSION; ?>" rel="stylesheet" type="text/css"/>
		<link href="<?php echo FOLDER_PATH.CSS_PATH.'common/login.css?version='.MY_VERSION; ?>" rel="stylesheet" type="text/css"/>
		<!--[if lt IE 9]>
		  <!--<link rel="stylesheet" type="text/css" href="../../css/movesmart/ie8.css" />-- >
		<![endif]-->
		<style>
			
		.log-content {
			margin: 0 auto;
			padding: 10px;
			width: 500px;
		}
		
		.form_field {
			background: transparent none repeat scroll 0 0;
			border: 1px solid #fff;
			color: #fff;
			display: block;
			font-size: 15px;
			margin-bottom: 9px;
			padding: 8px 15px;
			width: 89%;
		}
		
		.loginpage {
			top: 35%;
		}
		
		.form-field input[type="button"] {
			background: #363636 none repeat scroll 0 0;
			border: 0 none;
			color: #fff;
			cursor: pointer;
			display: block;
			font-size: 15px;
			line-height: 22px;
			margin: 0 auto;
			padding: 8px 20px;
		}
		
		select option {
			background: #0095ac;
		}

		</style>
	</head>
	<body>
		<div class="loginpage">
			<div class="log-content">
				<div class="loginlogo">
					<a href="#" ><img src="../images/logo.png" alt="logo" /></a>
				</div>
				<div>
					<?php
					$requestData = array();
					if (isset($_SESSION['flMsg']) || isset($_REQUEST['mess'])) 
					{
					
						if (isset($_SESSION['flMsg']['flashMessageError'])) 
						{
							echo '<div class="pageFlashMsg error" style="height:40px !important">'.$_SESSION['flMsg']['flashMessageError'].'</div>';
							$requestData = $_SESSION['flMsg']['data'];
						} 
						elseif (isset($_SESSION['flMsg']['flashMessageSuccess'])) 
						{
							echo '<div class="pageFlashMsg success" style="height:40px !important">'.$_SESSION['flMsg']['flashMessageSuccess'].'</div>';
						}
						elseif (isset($_REQUEST['mess'])) 
						{
							echo '<div class="pageFlashMsg error" style="height:40px !important">'.$_REQUEST['mess'].'</div>';
						}
						unset($_SESSION['flMsg']);
					} 
					//pr($requestData);
					$token =  $_REQUEST['register_params'];
					?>
					<div>&nbsp;</div>
				</div>
				<div class="form-field">
					<h2>Forgot Password</h2>
					<form name="authentication" id="authentication" method="post" enctype="multipart/form-data">
						<input type="hidden"  name="forgot_password_process" value = "1" />
						<input type="hidden"  name="register_params" value = "<?php echo $token ?>" />
						
						<input type="email" class ="form_field" name="email" placeholder="Email" title="email" required />
						<br>
						<input type="submit" value="Submit" id="forgot_password">
					</form>
				</div>
			</div>
		</div>
		
		</script>
		
	</body>
</html>
