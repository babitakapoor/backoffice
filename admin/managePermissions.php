<?php
/**
 * PHP version 5.
 
 * @category Admin
 
 * @package Managepermissions
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description To manage user type page permissions.
 */
global $LANG;
//To get user type list
$userType = $this->settings->getUserType();

//To get menus list
$menuList = $this->settings->getMenuPermissions();

$params['userTypeId'] = '';

$gridDisplay = 'none';

if (isset($_POST['menuUserTypeId'])) {
    $params['userTypeId'] = $_POST['menuUserTypeId'];
    if ($params['userTypeId'] == 4) {
        $gridDisplay = 'block';
    }
}

//Save user type permissions
$success = '';
if (isset($_POST['submit'])) {
    $params['pageMenuSettings'] = $_POST['pageMenu'];
    $params['userTypeId'] = $_POST['userTypeId'];

    if ($params['userTypeId'] > 0) {
        //Insert user type page access permissions
        $insertPageSettings 
            = $this->settings->insertUserTypePermissions($params);
        $_SESSION['fl'] = array(1, 'Changes saved successfully');
    }
    $gridDisplay = 'block';
}
//Save user type permissions

//To get user type page permission list
if ($params['userTypeId'] > 0) {
    $permissions = $this->settings->getUserTypePagePermission($params);

    if (is_array($permissions)) {
        if (count($permissions) > 0) {
            foreach ($permissions as $row) {
                $permissionArr[$row['r_menu_page_id']] = $row;
            }
        }
    }
}
?>
<div class="content-wrapper" id="manage-members">
    <div class="con-title-sec pos-fixed mt40">
        <h1><span class="icon reanalyze-test"></span>
        <?php echo $LANG['managePermissions']; ?></h1>
        <div class="user-features">
            <ul>
                <li>
                <a href="../admin/index.php?p=settings"
                title="<?php echo $LANG['backToSettings']; ?>">
                    <span class="icon icon-back" ></span>
                </a>
            </li>
          </ul>
      </div>
    </div>
    <div class="con-sec pt100">

      <div class="row-sec member-search-sec">
        <form name="managemenu" id="managemenuForm"
            action="" method="post">
          <div class="col6">
            <label><?php echo $LANG['userType']; ?> :</label>
            <div class="select-custom">
              <label for="manageMenuUserTypeId" style="display: none;"></label>
              <select id="manageMenuUserTypeId" name="menuUserTypeId"
                onchange="this.form.submit();">
                <option value="">--<?php echo $LANG['select']; ?>--</option>
                <?php
    foreach ($userType as $row) {
        if ($_SESSION['user']['usertype_id'] == ROLE_ADMIN) {
            $sel = (isset($params['userTypeId'])
                && $row['usertype_id'] == $params['userTypeId']) ?
                "selected='selected'" : '';
            echo "<option value='".$row['usertype_id']."' $sel>".
                ucfirst(strtolower($row['usertype'])).'</option>';
        } elseif ($_SESSION['user']['usertype_id'] == ROLE_BACKOFFICE) {
            if ($row['usertype_id'] == ROLE_COACH) {
                $sel = (isset($params['userTypeId'])
                    && $row['usertype_id'] == $params['userTypeId']) ?
                    "selected='selected'" : '';
                echo "<option value='".$row['usertype_id']."' $sel>".
                ucfirst(strtolower($row['usertype'])).'</option>';
            }
        }
    }
                ?>
              </select>
            </div>
          </div>
        </form>
      </div>

      <div class="tabOuterDiv">

        <div>
        <div>&nbsp;</div>
        </div>

        <ul class="tabs">
            <li class="current"><a href="#tab-1">
            <?php echo $LANG['menus']; ?></a></li>
        </ul>
        <div class="clear"></div>
        <div class="tabs-container">
        <div id="tab-1" class="tabscontent">
         <form action="" method="post">
             <div class="clear"></div>
             <!--grid-->
            <div class="grid-block">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <thead>
                <tr class="grid-title">
                  <td><?php echo $LANG['displayName']; ?></td>
                  <td><?php echo $LANG['menuName']; ?></td>
                  <td><?php echo $LANG['parentName']; ?></td>
                  <td><?php echo $LANG['fileName']; ?></td>
                  <td class="txt-center"  width="10%">
                    <?php echo $LANG['view']; ?></td>
                  <td class="txt-center" width="10%">
                    <?php echo $LANG['add']; ?></td>
                  <td class="txt-center"  width="10%">
                    <?php echo $LANG['edit']; ?></td>
                  <td class="txt-center"  width="10%">
                    <?php echo $LANG['delete']; ?></td>
                  <td class="txt-center" width="10%">
                    <?php echo $LANG['all']; ?></td>
                </tr>
                </thead>
                <?php
    if (is_array($menuList) && count($menuList) > 0) {

        //Re arrange menus and sub menus
        $mainMenus  =   array();
        foreach ($menuList as $menus) {
            if ($menus['parent_id'] == 0) {
                $mainMenus[] = $menus;
            }
        }

        if (count($mainMenus)>0) {
            $menuListFinal  =   array();
            foreach ($mainMenus as $menu) {
                $menuListFinal[] = $menu;
                $menuPageId = $menu['menu_page_id'];
                foreach ($menuList as $res) {
                    if ($res['parent_id'] == $menuPageId) {
                        $menuListFinal[] = $res;
                    }
                }
            }
            $menuList = $menuListFinal;
        }
        //Re arrange menus and sub menus

        $result = '';
        foreach ($menuList as $res) {
            $menuPageId = $res['menu_page_id'];
            $selAdd = '';
            $selEdit = '';
            $selView = '';
            $selDelete = '';
            $selAll = '';
            $selAdd_class = '';
            $selEdit_class = '';
            $selView_class = '';
            $selDelete_class = '';
            $selAll_class = '';
            if ((isset($permissionArr) && is_array($permissionArr))
                && (isset($permissionArr[$menuPageId]))
            ) {
                $selAdd = ($permissionArr[$menuPageId]['add_f'] == 1) ?
                    'checked="checked"' : '';
                $selEdit = ($permissionArr[$menuPageId]['edit_f'] == 1) ?
                    'checked="checked"' : '';
                $selView = ($permissionArr[$menuPageId]['view_f'] == 1) ?
                    'checked="checked"' : '';
                $selDelete = ($permissionArr[$menuPageId]['delete_f'] == 1) ?
                    'checked="checked"' : '';
                if ($selAdd != '' && $selEdit != '' 
                    && $selView != '' && ($selDelete != '')
                ) {
                    $selAll = 'checked="checked"';
                }
                $selAdd_class = str_replace('="checked"', '', $selAdd);
                $selEdit_class = str_replace('="checked"', '', $selEdit);
                $selView_class = str_replace('="checked"', '', $selView);
                $selDelete_class = str_replace('="checked"', '', $selDelete);
                $selAll_class = str_replace('="checked"', '', $selAll);
            }
            $class = '';
            $menuName = $res['menu_name'];
            $fileName = $res['file_name'];
            if ($res['parent_id'] == 0) {
                $class = 'grid-title';
                $menuName = $res['menu_name'];
                $fileName = '&nbsp;';
                $result .= <<<MENU
MENU;
            }
            $result .= <<<MENU
                        <tr class="{$res['menu_name']}">
                          <td>{$menuName}</td>
                          <td>{$res['name']}</td>
                          <td>{$res['parent_name']}</td>
                          <td>{$fileName}</td>
                          <!-- ED 20160430 -->
                          <td class="checkcenter" > <span 
                            class="check_box {$selView_class}" ><input 
                            type="checkbox" class="permChkBx_{$menuPageId}" 
                            name="pageMenu[{$menuPageId}][view]" 
                            value="1" {$selView} /></span></td>
                          <td class="checkcenter" ><span 
                            class="check_box {$selAdd_class}" ><input 
                            type="checkbox" class="permChkBx_{$menuPageId}" 
                            name="pageMenu[{$menuPageId}][add]" 
                            value="1" {$selAdd} /></span></td>
                          <td class="checkcenter" ><span 
                            class="check_box {$selEdit_class}" ><input 
                            type="checkbox" class="permChkBx_{$menuPageId}" 
                            name="pageMenu[{$menuPageId}][edit]" 
                            value="1" {$selEdit} /></span></td>
                          <td class="checkcenter"><span 
                            class="check_box {$selDelete_class}" ><input 
                            type="checkbox" class="permChkBx_{$menuPageId}" 
                            name="pageMenu[{$menuPageId}][delete]" 
                            value="1" {$selDelete} /></span></td>
                          <td class="checkcenter" ><span 
                            class="check_box checkAllPerms {$selAll_class}" ><input
                            type="checkbox" 
                            class="permChkBx_{$menuPageId}" {$selAll} 
                            id="permChkBx_{$menuPageId}" /></span></td>
                        </tr>
MENU;
        }
        echo $result;
    } else { ?>
                <tr>
                  <td colspan="5">No result found</td>
                </tr>
                <?php
    } ?>
              </table>
            </div>
            <!-- Save button -->
            <div class="row-sec member-search-sec">
                <div class="col6">

                <input type="hidden" name="userTypeId"
                    value="<?php echo $params['userTypeId'] ?>" />
                <?php if ($_SESSION['page_edit'] == 1) {
    ?>

                <!--<input type="button" value="<?php echo $LANG['btnCancel'];
    ?>" class="btn black-btn fl" onClick="myFunction()"> -->

                <a href="../index.php?p=settings" class="btn black-btn fr"
                value="<?php echo $LANG['btnCancel'];
    ?> ">Cancel</a>
                <input type="submit" value="<?php echo $LANG['btnSave'];
    ?>" name="submit" class="btn black-btn">
                <?php
    } ?>
              </div>
            </div>
            </form>
            <!-- Save button -->

        </div>
      </div>
        </div>

    </div>
</div>