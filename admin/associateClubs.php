<?php
/**
 * PHP version 5.
 
 * @category Admin
 
 * @package AssociatedClubs
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Get all the Associated Clubs.
 */
global $LANG;
$imgPath = '';//"../images".DS.;
$params['company_id'] = COMPANY_ID;
$params['is_deleted'] = 0;
$clubList = $this->club->getClubList($params);
$machinelist = $this->settings->machineList($params);
$listmachine =  array();
if (isset($machinelist['movesmart']['data'])) {
    $listmachine =  $machinelist['movesmart']['data'];
}
if ($machinelist['movesmart']['rows'] == 1) {
    $listmachine = array($listmachine);
}?>
<div class="content-wrapper" id="manage-members">
    <!--<div class="con-title-sec pos-fixed mt40">-->
    <div class="con-title-sec  mt40">
      <h1><span class="icon icon-set"></span>
        <?php echo 'manag machine and club'; ?></h1>
      <div class="user-features">
          <ul>
            <li>
                <a href="../admin/index.php?p=settings"
                    title="<?php echo $LANG['backToSettings']; ?>">
                    <span class="icon icon-back"></span>
                </a>
            </li>
          </ul>
      </div>
    </div>
    <div class="search-list-form">
    <form name="associatedClubs" id="associatedClubs" action="" method="POST">
        <label for="scltclub" style="display: none;" ></label>
         <select id="scltclub" name="scltclub">
            <option value="0">Select</option>
    <?php
    foreach ($clubList as $row) {
        // if(!in_array($row['club_id'],$clubsPlan)){
           echo "<option value='".$row['club_id']."'>".
            $row['club_name'].'</option>';
        // }
    }?>
    </select>
       <div class="row-sec multislct">
        <div class="strength-machine-left">
            <!--div class="strength-machine-left-bottom"><label>
            <span class="left_CheckAll">Select All</span> |
            <span class="left_unCheckAll">Deselect All</span>
            </label>
            </div-->
             <?php
             // var_dump($planClubList);
            // $clubsPlan=array();
                // foreach($planClubList as $key=>$value){
                    // $clubsPlan[] = $value['club_id'];
                // }

                ?>
            <label for="centerListLf" style="display: none;" ></label>
              <select id="centerListLf" name="centerListLf[]"
              multiple="multiple"
              style="height:150px;min-width:220px;" >
        <?php
        foreach ($listmachine as $row) {
            // if(!in_array($row['club_id'],$clubsPlan)){
              echo "<option value='".$row['machineid']."'>".
              $row['nameofmachine'].'</option>';
            // }
        }
                            ?>
              </select>
        </div>

        <!--div class="strength-machine-center">
                <a class="move_right">
                    <img id="preview_user_image"
                    src="<?php echo IMG_PATH.DS.'right.png';?>" />
                </a>
                <br>
                <a class="move_left">
                    <img id="preview_user_image"
                    src="<?php echo IMG_PATH.DS.'left.png';?>" />
                </a>
        </div-->
        <div class="strength-machine-center arrow_align">
            <span class="icon-graph icon-rightshift left_CheckAll arrow_icon">
            </span>
            <a class="move_right">
                <img id="preview_user_image"
                src="<?php echo IMG_PATH.DS.'right.png';?>" />
            </a>
            <br>
            <a class="move_left" >
                <img
                    id="preview_user_image"
                    src="<?php echo IMG_PATH.DS.'left.png';?>" />
            </a>
            <span class="icon-graph icon-leftshift right_CheckAll arrow_icon">
           </span>
        </div>
        <div class="strength-machine-right">
                <!--div><label><span class="right_CheckAll">Select All</span> |
                <span class="right_unCheckAll">Deselect All</span></label>
                </div-->
                <label for="centerListRt" style="display: none;"></label>
                 <select id="centerListRt"
                 name="centerListRt[]" multiple="multiple"
                 style="height:150px;min-width:220px;"
                 class="centerListRt">
        <?php
        if (!empty($planClubList[0]) && count($planClubList) > 0) {
            foreach ($planClubList as $rowPlan) {
                echo "<option value='".$rowPlan['club_id']."'>".
                $rowPlan['club_name'].'</option>';
            }
        }?>
        </select>
        </div>
     </div>
     <div class="strength-machine-top">
            <input type="button" class="btn black-btn"
                value="<?php echo $LANG['btnSave']; ?>"
                onclick="saveMachineMapping()"/>
            <input type="hidden"
                class="form-control loggedUserId"
                value="<?php echo $_SESSION['user']['user_id']; ?>"
                name="loggedUserId"  />
            <input type="hidden" name="planId"
                value="<?php echo isset($_REQUEST['id']) ? $_REQUEST['id'] : '';?>">
        </div>
     </form>
    </div>
    <div class="clear">&nbsp;</div>
</div>