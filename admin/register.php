<?php


/**
 * PHP version 5.
 
 * @category Admin
 
 * @package Login
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description All login Process
 */
//Check user already logged. 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Movesmart Register</title> 
		<link rel="shortcut icon" href="<?php echo IMG_PATH.DS; ?>fav.ico" />
		<link  href="<?php echo FOLDER_PATH.CSS_PATH.'common/custom.css?version='.MY_VERSION; ?>" rel="stylesheet" type="text/css"/>
		<link href="<?php echo FOLDER_PATH.CSS_PATH.'common/login.css?version='.MY_VERSION; ?>" rel="stylesheet" type="text/css"/>
		<!--[if lt IE 9]>
		  <!--<link rel="stylesheet" type="text/css" href="../../css/movesmart/ie8.css" />-- >
		<![endif]-->
		<style>
			
		.log-content {
			margin: 0 auto;
			padding: 10px;
			width: 500px;
		}
		
		.form_field {
			background: transparent none repeat scroll 0 0;
			border: 1px solid #fff;
			color: #fff;
			display: block;
			font-size: 15px;
			margin-bottom: 9px;
			padding: 8px 15px;
			width: 89%;
		}
		
		.loginpage {
			top: 35%;
		}
		
		.form-field input[type="button"] {
			background: #363636 none repeat scroll 0 0;
			border: 0 none;
			color: #fff;
			cursor: pointer;
			display: block;
			font-size: 15px;
			line-height: 22px;
			margin: 0 auto;
			padding: 8px 20px;
		}
		
		select option {
			background: #0095ac;
		}

		</style>
	</head>
	<body>
		<?php
			$token =  $_REQUEST['register_params'];
		?>
		<div class="loginpage">
			<div class="log-content">
				<div class="loginlogo">
					<a href="#" ><img src="../images/logo.png" alt="logo" /></a>
				</div>
				<div>
					<?php
					$requestData = array();
					if (isset($_SESSION['flMsg']) || isset($_REQUEST['mess'])) 
					{
					
						if (isset($_SESSION['flMsg']['flashMessageError'])) 
						{
							echo '<div class="pageFlashMsg error" style="height:40px !important">'.$_SESSION['flMsg']['flashMessageError'].'</div>';
							$requestData = $_SESSION['flMsg']['data'];
						} 
						elseif (isset($_SESSION['flMsg']['flashMessageSuccess'])) 
						{
							echo '<div class="pageFlashMsg success" style="height:40px !important">'.$_SESSION['flMsg']['flashMessageSuccess'].'</div>';
						}
						elseif (isset($_REQUEST['mess'])) 
						{
							echo '<div class="pageFlashMsg error" style="height:40px !important">'.$_REQUEST['mess'].'</div>';
						}
						unset($_SESSION['flMsg']);
					} 
					//pr($requestData);
					?>
					<div>&nbsp;</div>
				</div>
				<div class="form-field">
					<h2>Development Register</h2>
					<form name="authentication" id="authentication" method="post" enctype="multipart/form-data">
						
						<input type="hidden"  name="register_process" value ='1' />
						<input type="hidden"  name="register_params" value = "<?php echo $token ?>" />
						
						<input type="text"  class ="form_field"name="first_name" placeholder="Voornaam" title="voornaam" required value ="<?php echo isset($requestData['first_name'])?$requestData['first_name']:''; ?>" />
						<input type="text" class ="form_field"  name="last_name" placeholder="Achternamm" title="achternamm" required value ="<?php echo isset($requestData['last_name'])?$requestData['last_name']:'' ?>" />
						<input type="email" class ="form_field" name="email" placeholder="Email" title="email" required value ="<?php echo isset($requestData['email'])?$requestData['email']:'' ?>" />
						<input type="password" class ="form_field"  name="password"  id="password" placeholder="Wachtwoord" title="wachtwoord"  required  value = "<?php echo isset($requestData['password'])?$requestData['password']:'' ?>" />
						<input type="password" class ="form_field"  id="confirm_password" name="confirm_password"  placeholder="Herhall wachtwoord" title="herhall wachtwoord" required value ="<?php echo 
						isset($requestData['register_process'])?$requestData['register_process']:'' ?>" />
						<input type="text" class ="form_field" name="dob" id="dob"id="dob" placeholder="Geboortedatum" title="geboortedatum" readonly value = "<?php echo isset($requestData['dob'])?$requestData['dob']:'' ?>" />
						<input type="text" class ="form_field" name="phone" placeholder="Telefoon" title="telefoon" value = "<?php echo isset($requestData['phone'])?$requestData['phone']:'1' ?>" />
						<select name="gender" class ="form_field" title="selecteer geslacht" required="required">
							<option value="">Selecteer geslacht</option>
							<option value="male">Male</option>
							<option value="female">Female</option>
						</select>
						<input type="file" class ="form_field" name="user_image" placeholder="Foto" title="foto" />
						
						<br>
						<input type="button" value="Register" id="register">
					</form>
				</div>
			</div>
		</div>
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<link rel="stylesheet" href="/resources/demos/style.css">
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		
		<script>
			$(document).ready(function(){
				$("#dob").datepicker({ 
					yearRange: '-100y:c+nn',
					maxDate: '-10y',
					changeMonth: true,
					changeYear: true
				});
				
				$('#register').click(function(){
					var pass = $('#password').val();
					var cfmpass = $('#confirm_password').val();
					if(pass == cfmpass)
					{
						$('#authentication').submit();
					}
					else
					{
						alert('Password didn\'t matched');
						return false;
					}
				});
			});
		</script>
		
	</body>
</html>
