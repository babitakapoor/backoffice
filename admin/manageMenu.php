<?php
/**
 * PHP version 5.
 
 * @category Admin
 
 * @package ManageMenu
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description To manage user type page access permissions.
 */

global $LANG;
//To get menus list
$menuList = $this->settings->getMenuPages();
?>
<div class="content-wrapper" id="manage-members">
    <div class="con-title-sec pos-fixed mt40">
      <h1><span class="icon icon-set"></span><?php echo $LANG['manageMenu']; ?></h1>
      <div class="user-features">
          <ul>
            <li>
                <a href="../admin/index.php?p=settings"
                    title="<?php echo $LANG['backToSettings']; ?>">
                    <span class="icon icon-back"></span>
                </a>
            </li>
          </ul>
      </div>
    </div>
    <div class="con-sec pt100">
    <!-- <div class="select-custom clear-left-menu">
        <select></select></div> --><!--EDISON 20160120 -->
    <div class="row-sec member-search-sec">
        <form name="managemenu" id="managemenuForm" action="" method="post">
            <div class="col6">
                <label>&nbsp;</label>
            </div>
            <div class="col6">
    <?php if ($_SESSION['page_add'] == 1) {
    ?>
            <a href="javascript:void(0);"
                onclick="showQuickAddPop('Add Menu','manageMenu');"
                class="btn black-btn fr">
                <?php echo $LANG['addMenu'];
    ?></a>
    <?php
    } ?>
            </div>
    </form>
    </div>
        <!-- Flash message begins -->
        <div>
    <?php
    if (isset($_SESSION['flMsg'])) {
        if (isset($_SESSION['flMsg']['flashMessageError'])) {
            echo '<div class="pageFlashMsg error">'.
                $_SESSION['flMsg']['flashMessageError'].'</div>';
        } elseif (isset($_SESSION['flMsg']['flashMessageSuccess'])) {
            echo '<div class="pageFlashMsg success">'.
                $_SESSION['flMsg']['flashMessageSuccess'].'</div>';
        }
        unset($_SESSION['flMsg']);
    }
    ?>
            <div>&nbsp;</div>
        </div>
        <!-- Flash message ends -->

      <div class="tabOuterDiv">
        <ul class="tabs">
            <li class="current"><a href="#tab-1"><?php echo $LANG['menus']; ?></a></li>
        </ul>
        <div class="clear"></div>
        <div class="tabs-container">
        <div id="tab-1" class="tabscontent">
         <form action="" method="post">
             <div class="clear"></div>
             <!--grid-->
            <div class="grid-block">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <thead>
                <tr class="grid-title">
                  <td><?php echo $LANG['displayName']; ?></td>
                  <td><?php echo $LANG['menuName']; ?></td>
                  <td><?php echo $LANG['parentName']; ?></td>
                  <td><?php echo $LANG['isParent']; ?></td>
                  <td><?php echo $LANG['menuType']; ?></td>
                  <td><?php echo $LANG['icon']; ?></td>
                  <td><?php echo $LANG['position']; ?></td>
                  <td><?php echo $LANG['order']; ?></td>
                  <td><?php echo $LANG['isVisible']; ?></td>
                  <td><?php echo $LANG['fileName']; ?></td>
                  <!--ED 20160501 -->
                  <td class="actionwidth txt-center"><?php echo $LANG['action']; ?></td>
                </tr>
                </thead>
                <?php

    $mainMenus  =   array();
    if (is_array($menuList) && count($menuList) > 0) {
        //Re arrange menus and sub menus
        //$menuListFinal = [];
        foreach ($menuList as $menus) {
            if ($menus['parent_id'] == 0) {
                $mainMenus[] = $menus;
            }
        }

        if (count($mainMenus)>0) {
            $menuListFinal  =   array();
            foreach ($mainMenus as $menu) {
                $menuListFinal[] = $menu;
                $menuPageId = $menu['menu_page_id'];
                foreach ($menuList as $res) {
                    if ($res['parent_id'] == $menuPageId) {
                        $menuListFinal[] = $res;
                    }
                }
            }
            $menuList = $menuListFinal;
        }
        //Re arrange menus and sub menus

        $result = '';
        foreach ($menuList as $res) {
            $editLink = '';
            $deleteLink = '';
            if ($_SESSION['page_edit'] == 1) {
                $editLink = <<<EDIT_LINK
                        <a title="{$LANG['titleEdit']}" 
                            class="btn-link btn-inline dotline-sep icon-edit-menu"
                            onclick="showQuickAddPop('Edit Menu',
                                'manageMenu',{$res['menu_page_id']});">
                            <span class="icon icon-edit"></span></a>
EDIT_LINK;
            }
            if ($_SESSION['page_delete'] == 1) {
                $deleteLink = <<<EDIT_LINK
                                <a title="{$LANG['titleDelete']}" 
                                    class="btn-link btn-inline icon-delete-menu" 
                                    onclick="deleteMenu('Add/Edit Menu',
                                        'deletemenu',{$res['menu_page_id']});">
                                    <span class="icon icon-cls-sm"></span></a>
EDIT_LINK;
            }

            $isParent = ($res['parent_id'] == 0) ? 1 : 0;

            $class = '';
            if ($res['parent_id'] == 0) {
                $class = 'grid-title';
                $result .= <<<MENU

MENU;
            }
            $iconimage = '<img src="'.SERVICE_MENU_ICON.''.$res['icon'].'" 
                                height=35px; width=35px;/>';
            $result .=    <<<MENU
                        <tr class="{$class}">
                          <td>{$res['name']}</td>
                          <td>{$res['menu_name']}</td>
                          <td>{$res['parent_name']}</td>
                          <td>{$isParent}</td>
                           <td>{$res['menu_type']}</td>
                           <td>{$iconimage}</td>
                          <td>{$res['position']}</td>
                          <td>{$res['menu_order']}</td>
                          <td>{$res['is_visible']}</td>
                          <td>{$res['file_name']}</td>
                          // ED 20160501
                          <td class="txt-center" >
                            {$editLink}
                            {$deleteLink}
                          </td>
                        </tr>
MENU;
        }
        echo $result;
    } else {
        ?>
                <tr>
                  <td colspan="5">No result found</td>
                </tr>
                <?php
    } ?>
              </table>
            </div>
            <!-- Save button -->
            </form>
            <!-- Save button -->

        </div>
      </div>
        </div>

    </div>
</div>