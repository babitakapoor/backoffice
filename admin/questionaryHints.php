<?php 
/**
 * PHP version 5.
 
 * @category Admin
 
 * @package QuestionaryHints
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Handle QuestionaryHits.
 */


global $LANG;
    $questionhintlist = $this->settings->getQuestionHints();
    $questionhintlisttrows = isset($questionhintlist['getQuestionHints']) ? 
       $questionhintlist['getQuestionHints'] : array();
if ($questionhintlist['total_records'] == 1) {
    $questionhintlisttrows = array($questionhintlisttrows);
}
    ?>
<!--To List the translation language -->
<div class="content-wrapper" id="manage-members">
    <div class="con-title-sec pos-fixed mt40">
      <h1><span class="icon icon-set"></span>
        <?php echo $LANG['questionaryHints']; ?></h1>
      <div class="user-features">
          <ul>
            <li>
                <a href="../admin/index.php?p=settings"
                    title="<?php echo $LANG['backToSettings']; ?>">
                    <span class="icon icon-back"></span>
                </a>
            </li>
          </ul>
      </div>
    </div>
    <div class="con-sec pt100">
    <div class="row-sec member-search-sec">
        <form name="managemenu" id="managemenuForm" action=""
            method="post">
            <div class="col6">
                <label>&nbsp;</label>
            </div>
            <div class="col6">
    <?php if ($_SESSION['page_add'] == 1) {
    ?>
            <a href="javascript:void(0);" onclick="showQuickAddPop
                ('Add questionaryHints','questionaryHints');"
                class="btn black-btn fr">
                <?php echo $LANG['addquestionaryHints'];
    ?></a>
    <?php
    } ?>
            </div>
        </form>
  </div>
        <!-- Flash message begins -->
        <div>
            <?php
    if (isset($_SESSION['flMsg'])) {
        if (isset($_SESSION['flMsg']['flashMessageError'])) {
            echo '<div class="pageFlashMsg error">'.
                $_SESSION['flMsg']['flashMessageError'].'</div>';
        } elseif (isset($_SESSION['flMsg']['flashMessageSuccess'])) {
            echo '<div class="pageFlashMsg success">'.
                $_SESSION['flMsg']['flashMessageSuccess'].'</div>';
        }
        unset($_SESSION['flMsg']);
    }?>
            <div>&nbsp;</div>
    </div>
        <div class="tabOuterDiv">
            <ul class="tabs">
                <li class="current">
                    <a href="#tab-1">
                        <?php echo $LANG['questionaryHints']; ?></a></li>
            </ul>
            <div class="clear"></div>
            <div class="tabs-container">
                <div id="tab-1" class="tabscontent testResultListGrid">
                    <div class="clear"></div>
                    <p class="mb15"><span class="count-block"></span> </p>
                    <!--grid-->
                    <div class="grid-block">
                        <input type="hidden" class="paramNone"
                            value="?p=strengthProgramEdit">
                        <input type="hidden" id="strength_program_id"
                            value="">
                        <!--If navigate back to the page. Move the all
                         query string to another page -->
                        <div class="pagination-block">
                        <table width="100%" border="0" cellspacing="0"
                          cellpadding="0" id="memberListGridTab">
                                <thead>
                                <tr class="grid-title toggle-label"
                                linkdata="<?php echo $_REQUEST['p']; ?>">
                                    <td>
                                        <?php echo $LANG['sno'];?>
                                    </td>
                                    <td>
                                        <?php echo $LANG['phase'];?>
                                    </td>
                                    <td>
                                        <?php echo $LANG['grouplist'];?>
                                    </td>
                                    <td>
                                        <?php echo $LANG['title'];?>
                                    </td>
                                    <td>
                                        <?php echo 'Days';?>
                                    </td>
                                    <td>
                                        <?php echo $LANG['created_on'];?>
                                    </td>
                                    <td class="actionwidth txt-center ">
                                        <?php echo $LANG['action'];?>
                                    </td>
                                </tr>
                                </thead>
                                <tbody class="queshintcnt">
                                <?php
                                $sno = 1;
        if (count($questionhintlisttrows) > 0) {
            foreach ($questionhintlisttrows as $queshint) {
                $editLink = '';
                $deleteLink = '';
                if ($_SESSION['page_edit'] == 1) {
                    $editLink = <<<EDIT_LINK
                        <a title="{$LANG['titleEdit']}" 
                            class="btn-link btn-inline dotline-sep icon-edit-menu" 
                            onclick="editQuesHint('Edit Questionaries Hint',
                            'questionaryHints',{$queshint['t_ques_hint_id']});">
                            <span class="icon icon-edit"></span></a>
EDIT_LINK;
                }

                if ($_SESSION['page_delete'] == 1) {
                    $deleteLink = <<<EDIT_LINK
                        <a title="{$LANG['titleDelete']}" 
                       class="btn-link btn-inline icon-delete-menu" 
                       onclick="deleteQuestionHint({$queshint['t_ques_hint_id']});">
                       <span class="icon icon-cls-sm"></span></a>
EDIT_LINK;
                }

                ?>
                                    <tr><td><?php echo $sno?></td><td>
                                    <?php echo $queshint['phase_name']?></td><td>
                                    <?php echo $queshint['group_name']?></td><td>
                                    <?php echo $queshint['t_lang_hint']?></td><td>
                                    <?php echo $queshint['no_days']?></td><td>
                                    <?php echo $queshint['created_on']?></td><td>
                                    <?php echo $editLink.$deleteLink?></td></tr>
            <?php 
                ++$sno;
            }
        } else {
            ?>
            <tr><td colspan="7">No Results Found</td></tr>
        <?php 
        }    ?>
                                </tbody>                            
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>