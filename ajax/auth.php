<?php
error_reporting(0);
/**
 * PHP version 5.
 
 * @category Ajax
 
 * @package Login
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description To handle all login related ajax request.
 */
try {
	global $isCronVarApi,$LANG;
    $isCronVarApi = 1;
    $method = $_REQUEST['action'];
    
    //validate method name
    if (empty($method)) {
        throw new Exception($LANG['errMethodNotSpecified']);
    }

    switch ($method) {
        //To authenticate user
		case 'authenticate':
			$param['user_data'] = $_POST['data'];
			$data = $this->Auth->authenticate($param);
			echo json_encode($data);
			break;
		case 'genrateToken':
			$param['token'] = $_POST['auth_token'];
			$data = $this->Auth->genrateToken($param);
			echo json_encode($data);
			break;
		case 'getUserData':
			$param['token'] = $_POST['auth_token'];
			$data = $this->Auth->getUserData($param);
			echo json_encode($data);
			break; 
		case 'validateToken':
			$param['token'] = $_POST['auth_token'];
			$data = $this->Auth->validateToken($param);
			echo json_encode($data);
			break;   
	}	
} catch (Exception $e) {
    echo 'Exception: ', $e->getMessage(), "\n";
}
