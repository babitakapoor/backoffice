<?php
/**
 * PHP version 5.
 
 * @category Ajax
 
 * @package AddLight
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description To handle all admin Light related ajax request.
 */
global $LANG;
try {
    global $isCronVarApi;
    $isCronVarApi = 1;
    $method = $_REQUEST['action'];
    //validate method name
    if (empty($method)) {
        throw new Exception($LANG['errMethodNotSpecified']);
    }
    switch ($method) {
    case 'deleteEmployeeCentre':
        $param = $_REQUEST;
        $result = $this->club->deleteEmployeeCentre($param);
        echo json_encode($result);
        break;
    case 'deleteClubById':
        $param = $_REQUEST;
        $result = $this->club->deleteClubById($param);
        break;
    case 'clubAddUpdate':
        $param = $_REQUEST;
        $_SESSION['fl'] = array(1, $LANG['msgDetailsupdateSuccess']);
        $result = $this->club->UpdateClubDetails($param);
        if ($result['status'] == 'insert_success') {
            $_SESSION['fl'] = array(1, $LANG['msgDetailsAddSuccess']);
        }
        break;
            /*
    case 'planAddUpdate':
        $param = $_REQUEST;
        $_SESSION['fl'] = array(1, $LANG['msgDetailsupdateSuccess']);
        $result = $this->admin->updatePlanDetails($param);
        if ($result['status'] == 'insert_success') {
            $_SESSION['fl'] = array(1, $LANG['msgDetailsAddSuccess']);
        }
        break;
            */
    case 'deletePlanById':
        $param = $_REQUEST;
        $result = $this->admin->deletePlanById($param);
        break;
    case 'strengthProgramAddUpdate':
        $param = $_REQUEST;

        $strengthProgramId = $param['strengthProgramId'];
        $_SESSION['fl'] = array(1, $LANG['msgDetailsupdateSuccess']);
        $result = $this->settings->insertUpdateStrengthProgram($param);
        if ($result['status'] == 'insert_success') {
            $_SESSION['fl'] = array(1, $LANG['msgDetailsAddSuccess']);
        }
        break;
    case 'strengthProgramTrainingAddUpdate':
        $param = $_REQUEST;
        $strengthProgramId = $param['strengthProgramIdRefer'];
        $_SESSION['fl'] = array(0, $LANG['oopsError']);
        if ($strengthProgramId > 0) {
            $_SESSION['fl'] = array(1, $LANG['msgDetailsupdateSuccess']);
            $param['strengthProgramId'] = $strengthProgramId;
            $training = $this->settings->insertUpdateStrengthProgramTraining($param);
        }
        break;
    case 'deleteStrengthProgramTrainingById':
        $param = $_REQUEST;
        $result = $this->settings->deleteStrengthProgramTrainingById($param);
        $_SESSION['fl'] = array(1, $LANG['msgDataDeleteSuccess']);
        break;
    case 'deleteStrengthProgramById':
        $param = $_REQUEST;
        $result = $this->settings->deleteStrengthProgramById($param);
        $_SESSION['fl'] = array(1, $LANG['msgDataDeleteSuccess']);
        break;
    }
    if(!isset($result)){
        $result =   array();
    }
    echo json_encode($result);
} catch (Exception $e) {
    echo 'Exception: ', $e->getMessage(), "\n";
}
