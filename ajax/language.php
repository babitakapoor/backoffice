<?php
/**
 * PHP version 5.
 
 * @category Ajax
 
 * @package Language
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description To Get LanguageTranslation Profile Page.
 */
global $LANG;
try {
    global $isCronVarApi;
    $isCronVarApi = 1;
    $method = $_REQUEST['action'];

    //validate method name
    if (empty($method)) {
        throw new Exception($LANG['errMethodNotSpecified']);
    }
    switch ($method) {
    case 'saveLanguage':
        $params = $_REQUEST;
        $page = $this->languageType->insertLanguage($params);
        break;
    case 'editLanguageType';
        $params = $_REQUEST;
        $page = $this->languageType->editLangType($params);
        echo json_encode($page);
        break;
    case 'getLanguageType':
        $params = $_REQUEST;
        $page = $this->languageType->getLanguageType($params);
        $tablehtml = '';
                     $tablehtml .= '<table cellpadding="0" cellspacing="0">
                                        <tbody>';
                             $i = 1;
        foreach ($page['languageTypeDetails'] as $row) {
            $tablehtml .= 
                '<tr>
                    <td>'.$i.'</td>
                    <td>'.$row['title'].'</td>
                    <td>'.$row['created_on'].'</td>
                    <td>'.$row['modified_on'].'</td>
                </tr>';
            $i = $i + 1;
        }
        $tablehtml .= '</tbody></table>';
        echo $tablehtml;
        break;
    case 'default':
        break;
    }
} catch (Exception $e) {
    echo 'Exception: ', $e->getMessage(), "\n";
}
