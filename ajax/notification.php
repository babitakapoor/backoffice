<?php

/**
 * PHP version 5.
 
 * @category Ajax
 
 * @package Settings
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description To handle all settings related ajax request.
 */
if (isset($_POST['action'])) {
    $action = $_POST['action'];

    switch ($action) {

    case 'getSelectBox':
        $param = $_REQUEST;
		$page = $this->notifications->getSelectBox($param);
		echo json_encode($page);
		die;
        break;
	case 'getPushSelectBox':
        $param = $_REQUEST;
		$page = $this->notifications->getPushSelectBox($param);
		if(!isset($page['data'][0]))
		{
			$data = $page['data'];
			$page['data'] = array();
			$page['data'][0] = $data;
		}
		
		echo json_encode($page);
		die;
        break;
	}
}
if (isset($_REQUEST['action'])) {
    $action = $_REQUEST['action'];

    switch ($action) {
		case 'getInappNotification':
			$param = $_REQUEST;
			
			$page = $this->notifications->getInappNotification($param);
			//echo "<pre>";print_r($page);die;
			if(isset($page['data']))
			{
				if(!isset($page['data'][0]))
				{
					$page['data'] = array(0=>$page['data']);
				}
			}
			echo json_encode($page,JSON_UNESCAPED_SLASHES);
			die;
			break;
    }
}
