<?php
 
header('Access-Control-Allow-Origin: *');
require_once 'config.php';
include 'utility/cryptojs-aes.php';
require_once 'utility/securityUtil.php';
require_once 'utility/class.phpmailer.php';

if ($_REQUEST['action'] && $_REQUEST['action'] != '') 
{
	$urlArray['action'] = $_REQUEST['action'];
	$urlArray['p'] = $_REQUEST['p'];
	$urlstring = http_build_query($urlArray);
	
	if (isset($_REQUEST['action'])) {
		$action = $_REQUEST['action'];
		preg_match_all('/(\w+)=([^&]+)/',$_SERVER["QUERY_STRING"], $pairs);
		$value = array_combine($pairs[1], $pairs[2]);
		if(isset($value['clientid']) && $value['clientid'] !='')
		{
			$_REQUEST['clientid'] = $value['clientid'];
		}
		//echo $_REQUEST['clientid'];die;
		switch ($action) {
			case 'RegisterNewMachine':
				$params['clubmachineid'] = $_REQUEST['clubmachineid'];
				$params['machineid'] = $_REQUEST['machineid'];
				break;
			case 'GetMachinesList':
				
				$params['clubmachineid'] = $_REQUEST['clubmachineid'];
				$params['machineid'] = $_REQUEST['machineid'];
				break;
			case 'SetMachinePairing':
				
				$params['clubmachineid'] = $_REQUEST['clubmachineid'];
				$params['machineid'] = $_REQUEST['machineid'];
				$params['clientid'] = $_REQUEST['clientid'];
				$params['hrdevicecode'] = $_REQUEST['hrdevicecode'];
				break;
			case 'GetClientReadyForMachine':
				
				$params['clubmachineid'] = $_REQUEST['clubmachineid'];
				$params['machineid'] = $_REQUEST['machineid'];
				$params['machinemode'] = $_REQUEST['machinemode'];
				$params['devicefound'] = $_REQUEST['devicefound'];
				break;
			case 'SendTrainingData':
				
				$params['clubmachineid'] = $_REQUEST['clubmachineid'];
				$params['machineid'] = $_REQUEST['machineid'];
				$params['clientid'] = $_REQUEST['clientid'];
				$params['trdata'] = $_REQUEST['trdata'];
				break;
			case 'RegisterClientHRDevice':
				
				$params['clubmachineid'] = $_REQUEST['clubmachineid'];
				$params['deviceid'] = $_REQUEST['deviceid'];
				$params['clientid'] = $_REQUEST['clientid'];
				$params['machineid'] = $_REQUEST['machineid'];
				break;
			case 'GetCurrentTrainingStatus':
				
				$params['clubmachineid'] = $_REQUEST['clubmachineid'];
				$params['machineid'] = $_REQUEST['machineid'];
				$params['clientid'] = $_REQUEST['clientid'];
				break;	
			case 'ValidateCoach':
				
				$params['clubmachineid'] = $_REQUEST['clubmachineid'];
				$params['machineid'] = $_REQUEST['machineid'];
				$params['coachpin'] = $_REQUEST['coachpin'];
				break;
			case 'SearchClientByFirstName':
				
				$params['clubmachineid'] = $_REQUEST['clubmachineid'];
				$params['machineid'] = $_REQUEST['machineid'];
				$params['token'] = $_REQUEST['token'];
				$params['searchtxt'] = $_REQUEST['searchtxt'];
				break;
			case 'StartTraining':
				$params['clubmachineid'] = $_REQUEST['clubmachineid'];
				$params['machineid'] = $_REQUEST['machineid'];
				$params['hrdeviceid'] = $_REQUEST['hrdeviceid'];
				$params['clientid'] = $_REQUEST['clientid'];
				break;
			case 'UpdateTrainingCredits':
				
				$params['clubmachineid'] = $_REQUEST['clubmachineid'];
				$params['machineid'] = $_REQUEST['machineid'];
				$params['credits'] = $_REQUEST['credits'];
				$params['trainingdetid'] = $_REQUEST['trainingdetid'];
				$params['trdata'] = $_REQUEST['trdata'];
				$params['clientid'] = $_REQUEST['clientid'];
				break;
			case 'UpdateTrainingStatus':
				//echo "<pre>";print_r($_REQUEST);die;
				$params['clubmachineid'] = $_REQUEST['clubmachineid'];
				$params['machineid'] = $_REQUEST['machineid'];
				$params['status'] = $_REQUEST['status'];
				$params['clientid'] = $_REQUEST['clientid'];
				break;
		}
	}
	
	$postData = $params;
	$result = null;
	$httpResponse = null;
	define('UPLOAD_TYPE_USER_PROFILE', 1);
	define('UPLOAD_TYPE_MACHINE_IMAGE', 2);
	//echo SERVICE_AJAXURL.'?'.$urlstring.'&'.date('Ymdhis');die;
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, trim(SERVICE_AJAXURL.'?'.$urlstring.'&'.date('Ymdhis')));
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_COOKIEFILE, 'cookies.txt');
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
	curl_setopt($curl, CURLOPT_VERBOSE, TRUE);
	$result = curl_exec($curl);
	
    $httpResponse = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    if ($httpResponse == '404') {
        throw new exception('This page doesn\'t exists.');
    } 
	echo $result;
	curl_close($curl);      
}

