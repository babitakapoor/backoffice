<?php


function coverencrypt($encryptstr)
{
    return 'a'.$encryptstr;
}

function uncoverencrypt($encryptstr)
{
    return substr($encryptstr, 1);
}

function encrypt($pure_string, $encryption_key)
{
    $encrypted_string = trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $encryption_key, $pure_string, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));

    return coverencrypt($encrypted_string);
    /*
    $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
    $encrypted_string = mcrypt_encrypt(MCRYPT_BLOWFISH, $encryption_key, utf8_encode($pure_string), MCRYPT_MODE_ECB, $iv);
    return coverencrypt($encrypted_string);
    */
}

/**
 * Returns decrypted original string.
 */
function decrypt($encrypted_string, $encryption_key)
{
    $encrypted_string = uncoverencrypt($encrypted_string);
    $decrypted_string = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $encryption_key, base64_decode($encrypted_string), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));

    return $decrypted_string;

    /*
    $encrypted_string = uncoverencrypt($encrypted_string);
    $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
    $decrypted_string = mcrypt_decrypt(MCRYPT_BLOWFISH, $encryption_key, $encrypted_string, MCRYPT_MODE_ECB, $iv);
    return $decrypted_string;
    */
}

function encryptdefault($pure_string)
{
    $keyvalu = ConnectionProperty::getJSkey();

    return encrypt($pure_string, $keyvalu);
}

function decryptdefault($pure_string)
{
    $keyvalu = ConnectionProperty::getJSkey();

    return decrypt($pure_string, $keyvalu);
}
function getToken($enckey)
{
    return base64_encode(encrypt(time(), $enckey));
}
function validateToken($token, $mthd, $enckey)
{
    $time = time();
    $mtToken = base64_decode($token);
    $rawToken = str_replace($mthd, '', $mtToken);
    $reqtime = decrypt(base64_decode($rawToken), $enckey);

    return  ($time - $reqtime) < TOKEN_DURATION;
    //echo base64_decode($rawToken,$enckey)). "\n\n" . base64_decode($mthd). "\n\n";
}
function extractURLVariables($token)
{
    return base64_decode($token);
}