<?php

header('Access-Control-Allow-Origin: *');
require_once 'config.php';
require_once 'utility/recaptchalib.php';
//$privatekey = "6LfyAxUTAAAAAH8XSprPi3d68hvIr6I_jvfE0HsP-";
//$privatekey = "	6LegGhUTAAAAALTBvTOEhXraK-VHAW6l9WYjd-o2";
$privatekey = '6LfU6RATAAAAALt-TrInHvdvI6_HsLv_StCmArLJ';
$resp = recaptcha_check_answer(
    $privatekey,
    $_SERVER['REMOTE_ADDR'],
    $_POST['recaptcha_challenge_field'],
    $_POST['recaptcha_response_field']
);
if (!$resp->is_valid) {
    echo json_encode(array('success' => false, 'message' => 'Empty/Invalid captcha, Please re-try with new captcha.'));
} else {
    echo json_encode(array('success' => true, 'message' => 'Captcha is valid'));
}
exit;
