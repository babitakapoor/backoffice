<?php

header('Access-Control-Allow-Origin: *');
require_once 'config.php';
require_once 'utility/securityUtil.php';
include 'utility/cryptojs-aes.php';

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
	header('Access-Control-Allow-Headers: hashedkey');
	exit;
}

$postData = $_POST;
$action = $_REQUEST['action'];
$urlstring = "p=auth&action={$action}";
if($action != 'authenticate')
{
	$postData['auth_token'] = $_SERVER['HTTP_HASHEDKEY'];
}
$result = null;
$httpResponse = null;

if ($urlstring != '') 
{
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, trim(SERVICE_AJAXURL.'?'.$urlstring.'&'.date('Ymdhis')));
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_COOKIEFILE, 'cookies.txt');
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
	curl_setopt($curl, CURLOPT_VERBOSE, TRUE);
	$result = curl_exec($curl);
	$httpResponse = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	$returndata['data'] = base64_encode($result);
	echo json_encode( $returndata );   
	curl_close($curl);      
	die;
}

