<?php
/**
 * PHP version 5.
 
 * @category Ajax
 
 * @package RedirectLink
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description To handle all redirect link.
 */

/***
This function is mainly target to onkeypress values passing one page to another page.
***/
$param = $_REQUEST;
$currentLink = $param['currentLink'];
$appendLink = $param['appendLink'];
$paramLink = '';
if (isset($_REQUEST['searchValue'])) {
    $txtSearchVal = $_REQUEST['searchValue'];
    if ($txtSearchVal) {
        $paramLink .= '&searchValue='.$txtSearchVal;
    }
}
if (isset($_REQUEST['clubId'])) {
    $clubId = $_REQUEST['clubId'];
    if ($clubId) {
        $paramLink .= '&clubId='.$clubId;
    }
}
if (isset($_REQUEST['searchType'])) {
    $searchType = $_REQUEST['searchType'];
    if ($searchType) {
        $paramLink .= '&searchType='.$searchType;
    }
}
if (isset($_REQUEST['uid'])) {
    $paramLink .= '&uid='.$_REQUEST['uid'];
}
$currentLink = str_replace('&amp;', '&', $currentLink);
$currentLink .= $paramLink.'&id='.$appendLink;

echo $currentLink;

