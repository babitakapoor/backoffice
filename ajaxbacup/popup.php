<?php
/**
 * PHP version 5.
 
 * @category Ajax
 
 * @package Popup
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description To handle all quic add popup related ajax request.
 */
if (isset($_POST['action'])) {
    $action = $_POST['action'];
    $page = '../popup/'.$action.'.php';
    if (isset($_POST['editId'])) {
        $editId = $_POST['editId'];
    }

    if (file_exists($page)) {
        include_once $page;
    } else {
        echo '';
    }
}
