<?php
/**
 * PHP version 5.
 
 * @category Ajax
 
 * @package Strength
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description To handle all strength related ajax request.
 */
try {
    global $isCronVarApi,$LANG;
    $isCronVarApi = 1;
    $method = $_REQUEST['action'];
    //validate method name
    if (empty($method)) {
        throw new Exception($LANG['errMethodNotSpecified']);
    }
    switch ($method) {
    case 'getStrengthProgramTraining':
        $strengthProgramId = $_POST['strengthProgramId'];
        $paramsTr['strengthProgramId'] = $strengthProgramId;
        $strengthProgramTrainingHhtml 
            = $this->strength->getStrengthProgramTrainingHtml($paramsTr);
        echo $strengthProgramTrainingHhtml;
        exit;
        break;
    case 'updateActivityStrengthValue':
        $params['activityId'] = $_POST['activityId'];
        $params['strength'] = $_POST['strength'];
        $updateActivityStrengthValue 
            = $this->strength->updateActivityStrengthValue($params);
        echo json_encode($updateActivityStrengthValue);
        exit;
        break;
    case 'addNewActivities';

        $testActivity['strength_user_test_id'] = $_REQUEST['pid'];
        $testActivity['strength_machine_data'] = $_REQUEST['strength_machine'];
        if (isset($_REQUEST['clubID'])) {
            $testActivity['clubID'] = $_REQUEST['clubID'];
        }
        $result = $this->strength->insertUpdateUserTestActivity($testActivity);
        echo json_encode($result);
        break;
    case 'removeActivities';
        $params['strengthUserTestId'] = $_REQUEST['pid'];
        $params['activities'] = $_REQUEST['strength_machine'];
        $this->strength->removeStrengthTestActivity($params);
        exit;
        break;
    case 'getStrengthUserList';
        $params['usrid'] = $_REQUEST['usrid'];
        $dataresult = $this->strength->getStrengthUserList($params);
        $strengthusrlist = ($dataresult['strengthuserlist']) ? 
            $dataresult['strengthuserlist'] : array();
        echo json_encode($strengthusrlist);
        break;
    case 'getMachienMappedetailsGrid':
        $paramUser['userId'] = $_REQUEST['userID'];
        $paramUser['flag'] = 0;
        //ge the list  of machines data by userid only.
        $getStrengthTestList = $this->strength->getStrengthTestList($paramUser); 
        if ($getStrengthTestList['status_code'] != 0) {
            $getStrengthTestList = $getStrengthTestList['rows'];
            $params['is_deleted'] = 0;
            $params = $_REQUEST;
            $strengthMachineList = $this->admin->getStrengthMachineList($params);
            $htmlcnt = '';
            $data = '';
            $htmlcnt .= '<div class="grid-block">
                            <table  class="strengthmachiensGrid" cellspacing="0" cellpadding="0">
                                <tbody>
                                    <tr><td class="paddingnone">
                                    <!-- Fields label begins -->
                                        <table class="fl strengthmachiensGrid">
                                            <tr><td class="trheader">machine</td></tr>';
            foreach ($strengthMachineList['strengthMachine']  as $machine) {
                $htmlcnt .= '<tr><td>'.$machine['machine_name'].'</td></tr>';
            }
            $htmlcnt .= '</table>';
            $i = 0;
            foreach ($getStrengthTestList as $test) {
                ++$i;
                $htmlcnt .= <<<TABLE_DATA
                                                        <table class="fl strengthmachiensGrid">
                                                        <tr><td class="trheader">Training {$i}</td></tr>
TABLE_DATA;
                $activities = explode(',', $test['test_activities']);
                $activityArr = array();
                foreach ($activities as $activity) {
                    $values = explode('###', $activity);
                    $activityArr[$values[0]] = isset($values[1]) ? $values[1] : 0;
                }
                foreach ($strengthMachineList['strengthMachine'] as $machine) {
                    $strength = array_key_exists(
                        $machine['strength_machine_id'], $activityArr
                    ) ? 
                        $activityArr[$machine['strength_machine_id']] : '';
                    $htmlcnt .= <<<TABEL_DATA
                            <tr><td>{$strength}</td></tr>
TABEL_DATA;
                }
            }
            $htmlcnt .= '</table>';
            $htmlcnt .= '</td></tr></tbody></table></div>';
            $result = $htmlcnt;
        } else {
            $result = 'No Test are Happend';
        }
        echo $result;
        break;
        /*SN Added-Get Machine List By Id-20160316*/
    case 'getMachineByClubId':
        $params['is_deleted'] = 0;
        $params = $_REQUEST;
        $strengthMachineList = $this->admin->getStrengthMachineList($params);
        $machinelist = isset($strengthMachineList['strengthMachine']) ? 
            $strengthMachineList['strengthMachine'] : 0;
        $machinehtml = '';
        foreach ($machinelist as $machine) {
            $machinehtml .= '<tr>';
            $machinehtml .= '<td><input class="strength_test_activity_option" 
            name="strength_machine[name][strength_machine_id]" type="checkbox"  
            value="'.$machine['strength_machine_id'].'">
            <input type="hidden" class="strengthmachineid" 
            value="'.$machine['strength_machine_id'].'"></td>';
            $machinehtml .= '<td>'.$machine['name'].'</td>';
            $machinehtml .= '</tr>';
        }
        echo $machinehtml;
        break;
    case 'saveProgramWtMachine':
        $params['strength_machine_data'] = $_REQUEST['strength_machine_data'];
        $params['programid'] = $_REQUEST['programid'];
        $params['flag'] = 1;
        $strengthActivity = $this->strength->insertUpdateUserTestActivity($params);
        $status = ($strengthActivity['status_code'] == 200) ? 1 : 0;
        echo $status;
        break;

    }
} catch (Exception $e) {
    echo 'Exception: ', $e->getMessage(), "\n";
}
