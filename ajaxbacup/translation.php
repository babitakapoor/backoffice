<?php
/**
 * PHP version 5.
 
 * @category Ajax
 
 * @package Translations
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description To handle all translation related ajax request.
 */
try {
    global $isCronVarApi,$LANG;
    $isCronVarApi = 1;
    $method = $_REQUEST['action'];

    //validate method name
    if (empty($method)) {
        throw new Exception($LANG['errMethodNotSpecified']);
    }
    switch ($method) {
    case 'saveLanguageTranslation':
        $params = $_REQUEST;
        echo '<pre>';
        print_r($params);
        echo '</pre>';
        $page = $this->languageType->insertLanguageTypePermissions($params);
        break;
    case 'editTranslationType';
        $params = $_REQUEST;
        $page = $this->translatiolanguageType->editTransType($params);
        echo json_encode($page);
        break;
    case 'getTranslationLanguageType':
        $params = $_REQUEST;
        $page = $this->translatiolanguageType->getTranslationLanguageType($params);
        $tablehtml = '';
                $tablehtml .= '<table cellpadding="0" cellspacing="0">
                            <tbody>';
                        $i = 1;
        foreach ($page['translationkeyTypeDetails'] as $row) {
            // foreach($row  as $values){
              $tablehtml .= '<tr>
                                 <td>'.$i.'</td>
                                 <td>'.$row['transaction_key_desc'].'</td>
                                 <td>'.$row['translation_text'].'</td>	
                                 <td>'.$row['title'].'</td>	
                                 <td>'.$row['createdon'].'</td>	
                                 <td>'.$row['modifiedon'].'</td>	
                            </tr>';
            $i = $i + 1;
        }

        $tablehtml .= '</tbody></table>';
        //}
        echo $tablehtml;
        break;
    case 'default':
        break;
    }
} catch (Exception $e) {
    echo 'Exception: ', $e->getMessage(), "\n";
}
