<?php
/**
 * PHP version 5.
 
 * @category Ajax
 
 * @package Club
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description To handle all club related ajax request..
 */
global $LANG;
try {
    global $isCronVarApi;
    $isCronVarApi = 1;
    $method = $_REQUEST['action'];
    //validate method name
    if (empty($method)) {
        throw new Exception($LANG['errMethodNotSpecified']);
    }
    switch ($method) {
        /*
    case 'getCycleCountMember':
        $param = $_REQUEST;
        $result = $this->club->getCycleCountMember($param);
        break;
        /*
    case 'updateCycleWithMember':
        $param = $_REQUEST;
        $result = $this->club->getCycleWithMemberAddOrUpdate($param);
        break;*/
    }
    if(!isset($result)){
        $result =array();
    }
    echo json_encode($result);
} catch (Exception $e) {
    echo 'Exception: ', $e->getMessage(), "\n";
}
