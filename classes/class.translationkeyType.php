<?php
/**
 * PHP version 5.
 
 * @category Classes
 
 * @package TranslationKeyType
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Class to handle translation key type related functions.
 */
class translationkeyType extends common
{
    /*This function used to get all the translationkey List.*/
    public function getTranslationkeyType($dataString)
    {
        $result = parent::webServiceXMLToString(LOCAL_WEBSERVICE_PATH.'getTranslationkeyType.php'.QN.$dataString);

        return json_decode($result, true);
    }
	
	
}
