<?php
/**
 * PHP version 5.
 
 * @category Classes
 
 * @package Log
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Class to handle log related functions.
 */
class log
{
    /*This function used log the point system values.*/
    public function logServices($params)
    {
        try {
            $params['mod'] = 'log';
            $params['method'] = 'logServices';
            $result = $this->webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /*This function used get the log report.*/
    public function getLogReport($params)
    {
        try {
            $params['mod'] = 'log';
            $params['method'] = 'getLogReport';
            $result = $this->webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        if (!isset($result['logReport'][0])) {
            $result['logReport'][0] = $result['logReport'];
        }

        return $result;
    }
}
