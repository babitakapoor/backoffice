<?php
/**
 * PHP version 5.
 
 * @category Classes
 
 * @package coach
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Class to handle coach related functions.
 */
  /**
 * Class to handle userType related functions.
 
 * @category Classes
 
 * @package UserType
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/

 */
class coach extends common
{
    /**
    * Returns an json obj of user information from the t_users and t_personalinfo tables.
    * @param string $params service parameter
    *
    * @return array object object
    */   
    public function getUserPersonalInfoDetails($params)
    {
        try {
            $params['mod'] = 'coach';
            $params['method'] = 'getUserPersonalInfoDetails';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.'?'.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /**
     * Fucntion return the list of all the users based on the usertype pased in the datastring
     * datastring format {"mod":"coach","method":"getCoachList","param":{"usertype":"Coach"}}.
     *
     public function getCoachList($params)
     {
     try {
     $params['mod'] = 'coach';
     $params['method'] = 'getCoachList';
     $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
     } catch (Exception $e) {
     $result = 'Caught Exception:'.$e->getMessage();
     }
     
     return $result;
     }
 
    /**
    * Returns an json obj of Update coach personal info details.
    * @param string $params service parameter
    *
    * @return array object object
    */   
    public function getCoachAddOrUpdate($params)
    {
        try {
            /* Check Email Exist Status Message */
                 $param['email'] = $params['email'];
            $param['mod'] = 'members';
            $param['method'] = 'checkMemberEmailExist';
            if ($params['userId']) {
                $param['userId'] = $params['userId'];
                $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($param));
            } else {
                $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($param));
            }
            if ($result['movesmart']['status'] == 'Error') {
                return $result;
            }
            /* End Email Check */
            $params['mod'] = 'coach';
            $params['method'] = 'getCoachAddOrUpdate';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            $userId = $result['user_id'];
            if (!empty($userId)) {
                //$par = parse_str($_REQUEST['dataPost'], $param);
                $param['workStart'] = parent::siteSaveDateFormat($params['workStart']);
                //$param['workEnd'] = '';
                //if(isset($_REQUEST['workEnd']) && $_REQUEST['workEnd'] != '')
                $param['workEnd'] = parent::siteSaveDateFormat($params['workEnd']);
                $param['userId'] = $userId;
                $param['mod'] = 'coach';
                $param['method'] = 'addCoachPersonalDetails';
                $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($param));
            }
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    /**
    * Returns an json obj of Get Training Program Details of the Member.
    * @param string $params service parameter
    *
    * @return array object object
    */   
     /*public function getTrainingProgramDetails($params)
     {
         try {
             $params['mod'] = 'coach';
             $params['method'] = 'getTrainingProgramDetails';
             $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
         } catch (Exception $e) {
             $result = 'Caught Exception:'.$e->getMessage();
         }

         return $result;
     }
     */
    /**
     * Returns an json obj of  update coach profile image.
     * @param string $params service parameter
     *
     * @return array object object
     */
    public function updateCoachProfileImage($params)
    {
        $params['mod'] = 'coach';
        $params['method'] = 'getSaveProfileImage';
        //Remove the old photo if new one avaliable
        if (isset($params['client_image_old']) && isset($params['client_image'])) {
            if (($params['client_image_old'] != $params['client_image'])) {
                @unlink(IMG_PATH.DS.THEME_NAME.DS.$params['client_image_old']);
            }
        }
        $jsonData = parent::convertArrayToJson($params);
        $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.$jsonData);
        return json_decode($result, true);
    }
    /**
     * Returns an json obj of Coach List.
     * @param string $dataString service parameter
     *
     * @return array object object
     */
    /*public function getCoach($dataString)
    {
        global $LANG;
        $jsonData = parent::convertArrayToJson($dataString);
        $result = parent::webServiceXMLToString(EXTERNAL_WEBSERVICE_PATH.'getCoachList.php'.QN.$jsonData);
        $getArrayList = parent::convertArrayToJson($result);
        $getLists = $getArrayList[THEME_NAME]['coachlist']['coach'];
        if (THEME_NAME == 'movesmart') {
            $company_id = 2;
        } else {
            $company_id = 1;
        }
        if (is_array($getLists) && count($getLists) > 0) {
            foreach ($getLists as $getList) {
                $club_id = $getList['club_id'];
                $status_id = $getList['status_id'];
                $user_type_id = $getList['user_type_id'];
                $first_name = $getList['first_name'];
                $last_name = $getList['last_name'];
                $gender = $getList['gender'];
                $email = $getList['email'];
                $username = $getList['username'];
                $password = $getList['password'];
                $person_id = $getList['person_id'];
                $queryStrings = array(
                    'mod' => 'coach',
                    'method' => 'getCoachAddOrUpdate',
                    'param' => array(
                        'company_id' => $company_id,
                        'club_id' => $club_id,
                        'status_id' => $status_id,
                        'user_type_id' => $user_type_id,
                        'first_name' => $first_name,
                        'last_name' => $last_name,
                        'gender' => $gender,
                        'email' => $email,
                        'username' => $username,
                        'password' => $password,
                        'person_id' => $person_id)
                );
                $dataStringIn = parent::convertArrayToJson($queryStrings);
                $dataStringIns = urlencode($dataStringIn);
                $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.$dataStringIns);
                //$getSResult = json_decode($this->resultSet, true);
            }
        } else {
            $result =   $LANG['noResult'];
        }
        return $result;
    }*/
    /**
     * Returns an json obj of Delete Coach.
     * @param string $params service parameter
     *
     * @return array object object
     */
    public function getCoachDelete($params)
    {
        try {
            $params['mod'] = 'coach';
            $params['method'] = 'getCoachDelete';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        return $result;
    }
    /**
     * Returns an json obj of Update User Profile.
     * @param string $params service parameter
     *
     * @return array object object
     */
    public function updateUserProfile($params)
    {
        try {
            $params['mod'] = 'coach';
            $params['method'] = 'updateUserProfile';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            //echo "test".WEBSERVICE_PATH.QN.http_build_query($params)."--";
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        return $result;
    }
}