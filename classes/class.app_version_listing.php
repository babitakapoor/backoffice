<?php
ob_start();
/**
 * PHP version 5.
 
 * @category Classes
 
 * @package Message
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Class to handle message related functions.
 */
 /**
 * Class to handle userType related functions.
 
 * @category Classes
 
 * @package UserType
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/

 */
class appVersionListing extends common
{
    /**
    * Returns an json obj of get all user messages
    * @param string $params service parameter
    *
    * @return array object object
    */
    
    /**
    * Returns an json obj of insert new message or reply message
    * @param string $params service parameter
    *
    * @return array object object
    */
	
	public function __construct()
	{
		if(isset($_REQUEST['version_submit']) && $_REQUEST['version_submit'] == 'Save')
		{
			$this->addAppVersion($_REQUEST);
		}
	}
	
	public function addAppVersion($params=array())
	{
		try 
		{
			$params['mod'] = 'app_version_listing';
			$params['method'] = 'addAppVersion';
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
			if($result['status_code'] == '200')
			{
				$_SESSION['flMsg']['flashMessageSuccess'] = $result['status_message'];
			}
			else
			{	
				$_SESSION['flMsg']['flashMessageError'] = $result['status_message'];
			}
			$url = 'index.php?p=manageapp';
			header('Location:'.$url);
		} catch (Exception $e) {
			$result = 'Caught Exception:'.$e->getMessage();
		}
	}
	
	public function getAppVersion($type='')
	{
		try 
		{
			$params['mod'] = 'app_version_listing';
			$params['method'] = 'getAppVersion';
			$params['userType'] = $type;
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
		} catch (Exception $e) {
			$result = 'Caught Exception:'.$e->getMessage();
		}
		return $result;
	}
	
	public function getAppEditVersion()
	{
		try 
		{
			$params['mod'] = 'app_version_listing';
			$params['method'] = 'getAppEditVersion';
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
		} catch (Exception $e) {
			$result = 'Caught Exception:'.$e->getMessage();
		}
		return $result;
	}
	
	public function getAppVersions($params=array())
	{
		 try 
		 {
			//echo "cxzczx";die;
			$params['mod'] = 'app_version_listing';
			$params['method'] = 'getAppVersions';
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
			
		} catch (Exception $e) {
			$result = 'Caught Exception:'.$e->getMessage();
		}
		
		return $result;
	}
	
	public function getDetailAppVersions($id='')
	{
		 try 
		 {
			$params['mod'] = 'app_version_listing';
			$params['method'] = 'getDetailAppVersions';
			$params['id'] = $id;
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
			
		} catch (Exception $e) {
			$result = 'Caught Exception:'.$e->getMessage();
		}
		
		return $result;
	}
	
	public function redirectUser($url)
	{
		header('Location:'.$url);
		exit();
	}
}; // End Class.
