<?php
/**
 * PHP version 5.
 
 * @category Classes
 
 * @package Login
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Class to handle login related functions.
 */
    /**
 * Class to handle userType related functions.
 
 * @category Classes
 
 * @package UserType
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/

 */
class login extends common
{
        /**
    * Returns an json obj of authenticate the user using password(login functionality).
    * @param string $params service parameter
    *
    * @return array object object
    */
    
    protected $authkey = 'm0v35m@r75173C0rQ4*k';
    protected $siteCorekey = 'm0v9835m@rr31^1||34';
    
    public function authenticate($params)
    {
         try {
             $params['mod'] = 'login';
             $params['method'] = 'authenticate';
             $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
         } catch (Exception $e) {
             $result = 'Caught Exception:'.$e->getMessage();
         }

         return $result['movesmart']['data'];
     }
        /**
        * Returns an json obj of get user page permissions.
        * @param string $params service parameter
        *
        * @return array object object
        */
        public function getUserPagePermissions($params)
        {
            try {
                $params['mod'] = 'login';
                $params['method'] = 'getUserPagePermissions';
                $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
                if (isset($result['userPermission'])) {
                    $result = $result['userPermission'];
                }
            } catch (Exception $e) {
                $result = 'Caught Exception:'.$e->getMessage();
            }

            return $result;
        }
        
     /////////////////////////Site core Function////////////////////////////////////////////   
        public function createSitecoreUser($user = array())
        {
			if(!empty($user))
			{
				try {
					$sitecorekey = $this->authkey;
					$timestamp = $this->todayTimeStamp();
					$userid = $sitecorekey.'_'.$timestamp."_".$user['user_id'];
					
					$params['mod'] = 'login';
					$params['method'] = 'saveSiteCoreData';
					$params['user_id'] =  $user['user_id'];
					$params['user_sitecore_id'] =  $userid;
					$params['company_id'] =  $user['associate_company_id'];
					
					$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
					
					if (isset($result['status_code']) == '200' && !empty($result['data'])) 
					{
						$this->addSiteCoreUser($result['data'],$result['mode']);
					}
				} catch (Exception $e) {
					$result = 'Caught Exception:'.$e->getMessage();
				}
                
                return true;
			}
		}
		
		public function addSiteCoreUser($data= array(),$mode ='')
		{
			return true;
			$sitecorekey = $this->siteCorekey;
			$userid = $data['sitecore_site_user_id'];
			
			$hasheduserid  = $this->getHash($userid,'hash_user_id');
			$sitecoreKey = $this->getHash($sitecorekey,'hash_sitecore_key');
			
			$tokendata['user_id'] = $data['users_user_id'];
			$tokendata['company_id'] = $data['companies_company_id'];
			
			$sendingData['auth_key'] =  $sitecoreKey;
			$sendingData['user_id'] =  $hasheduserid;
			$sendingData['token_for_today'] =  $this->tokenFortoday($tokendata);
			$sendingData['company_id'] =   $data['companies_company_id'];
			$sendingData['company_name'] =  $data['companies_company_name'];
			$sendingData['company_name_slug'] =  $data['companies_company_slug'];
			$sendingData['user_name'] =  $data['user_first_name']." ".$data['user_last_name'];
			$sendingData['mode'] =  $mode;
			
			pr($sendingData);
			return true;
			//$this->curlSitecoreCall($sendingData,'addToken');
		}
		
		protected function todayTimeStamp()
		{
			return strtotime(date('d-m-Y'));
		}
		
		protected function tokenFortoday($data = array())
		{
			$timestamp = $this->todayTimeStamp();
			$authkey = $this->authkey;
			$companyID = $data['company_id'];
			$userID = $data['user_id'];
			$string = $authkey.'_'.$companyID.'_'.$timestamp.'_'.$userID;
			$hashedTokenForToday = $this->getHash($string,'hash_today_token');
			return $hashedTokenForToday;
		}
		
		protected function getHash($string,$for = '')
		{
			return base64_encode($string);
		}
		
		protected function curlSitecoreCall($data = array(),$function ='')
		{
			curl_setopt($curl, CURLOPT_URL, trim(SERVICE_AJAXURL.'?'.$urlstring.'&'.date('Ymdhis')));
			curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_COOKIEFILE, 'cookies.txt');
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
			curl_setopt($curl, CURLOPT_VERBOSE, TRUE);
			$result = curl_exec($curl);
			$httpResponse = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			curl_close($curl);      
		}
		
		public function updateSiteCoreData()
		{
			try {
				$params['mod'] = 'login';
				$params['method'] = 'updateSiteCoreData';
				$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
				if (isset($result['status_code']) == '200' && !empty($result['data'])) 
				{
					if(!isset($result['data'][0]))
					{
						$dataVal = $result['data'];
						unset($result['data']);
						$result['data'][0] = $dataVal;
					}
					$data  = $result['data'];
					foreach($data as $dat)
					{
						$this->addSiteCoreUser($dat,$result['mode']);
					}
				}
			} catch (Exception $e) {
				$result = 'Caught Exception:'.$e->getMessage();
			}
			return true;
		}
		
		public function getSiteCoreData()
		{
				
		}
		
		public function deleteSitecoreUser()
		{
			
		}
	/////////////////////////Site core Function ends///////////////////////////////////////////
}
