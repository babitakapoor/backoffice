<?php
/**
 * PHP version 5.
 
 * @category Classes
 
 * @package Club
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Class to handle club related functions.
 */
 /**
 * Class to handle userType related functions.
 
 * @category Classes
 
 * @package UserType
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/

 */
class club extends common
{
    /*This function used to get all the manage cycle list.*/
    /**
    * Returns an json obj of Manage Cycle List.
    * @param string $params service parameter
    *
    * @return array object object
    */    
    /*public function getManageCycle($params)
    {
        try {
            $params['mod'] = 'club';
            $params['method'] = 'getManageCycle';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart']['manageCycleList'];
    }
    */
    /* get the Members page Club Details for DROPDOWN */
    public function getClubList($params)
    {
        try {
            $params['mod'] = 'club';
            $params['method'] = 'getClubDetails';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        if (isset($result['movesmart']['clublist'])) {
            if (!isset($result['movesmart']['clublist'][0])) {
                $result['movesmart']['clublist'] = array($result['movesmart']['clublist']);
            }
        }

        return isset($result['movesmart']['clublist']) ? $result['movesmart']['clublist'] : array();
    }

    /* get the club images */
    /*public function getClubImages($params)
    {
        try {
            $params['mod'] = 'club';
            $params['method'] = 'getClubImages';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        if ($result['movesmart']['status'] == 'success' && !isset($result['movesmart']['clubImages'][0])) {
            $result['movesmart']['clubImages'] = array($result['movesmart']['clubImages']);
        }

        return $result;
    }
    */
    /* Save the club image */
    /*public function saveClubImage($params)
    {
        try {
            $params['mod'] = 'club';
            $params['method'] = 'saveClubImage';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    */
    /* Delete the club image */
    /*public function deleteClubImage($params)
    {
        try {
            $params['mod'] = 'club';
            $params['method'] = 'deleteClubImage';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    */
    /* get club list by company id */
    public function getClubListByCompany($params)
    {
        try {
            $params['mod'] = 'club';
            $params['method'] = 'getClubListByCompany';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /* get club detail by club id */
    public function getClubDetailById($params)
    {
        try {
            $params['mod'] = 'club';
            $params['method'] = 'getClubDetailById';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
	/**
    * Returns an json obj of get group
    * @param string ''
    *
    * @return array object object
    */
   /*  public function getGroup()
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getGroup';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    } */
    /* Add/Update club details */
    public function UpdateClubDetails($params)
    {
		
		
        try {
            $params['mod'] = 'club';
            $params['method'] = 'UpdateClubDetails';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /*This function used for Equipment List.*/
    /*
    public function getEquipmentList($params)
    {
        try {
            $params['mod'] = 'club';
            $params['method'] = 'getEquipmentList';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }*/

    /*This function used for Cycle Search List.*/
    /*public function getManageCycleSearch($params)
    {
        try {
            $params['mod'] = 'club';
            $params['method'] = 'getManageCycleSearch';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    */
    /* get the Members to Link the Cycle Search*/
    /*
    public function getManageLinkCycleSearch($params)
    {
        try {
            $params['mod'] = 'club';
            $params['method'] = 'getManageLinkCycleSearch';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart']['linkCycleList'];
    }
    */
    /* get the Members to Link the Cycle*/
    /*public function getManageLinkCycle($params)
    {
        try {
            $params['mod'] = 'club';
            $params['method'] = 'getManageLinkCycle';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart']['manageCycleList'];
    }
    */
    /* get the Members page Club Details for DROPDOWN */
    /*public function getCycleList($params)
    {
        try {
            $params['mod'] = 'club';
            $params['method'] = 'getCycleList';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart']['cycleList'];
    }
    */
    /* get the Members to Link the Cycle Edit/Assign* /
    public function getCycleWithMemberAddOrUpdate($params)
    {
        try {
            $params['mod'] = 'club';
            $params['method'] = 'getCycleWithMemberAddOrUpdate';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /* get the Members to Link the Cycle* /
    public function getCycleAddOrUpdate($params)
    {
        try {
            $params['mod'] = 'club';
            $params['method'] = 'getCycleAddOrUpdate';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /* check the count of Cycle already exist in any user* /
    public function getCycleCountMember($params)
    {
        try {
            $params['mod'] = 'club';
            $params['method'] = 'getCycleCountMember';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /*This function used for Cycle Available For Club.*/
    /*public function getCycleAvailableForClub($params)
    {
        try {
            $params['mod'] = 'club';
            $params['method'] = 'getCycleAvailableForClub';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        $resultSet = '';
        if ($result['availableCycles']) {
            $availableCycleLists = $result['availableCycles'];
            foreach ($availableCycleLists as $cycleList) {
                $resultSet .= "<div class='input-group'>
                                <div class='in-cell cus-check get-check'>
                                    <input type='radio'
                                            value='".$cycleList['r_equipment_id']."'
                                            name='equ".$cycleList['r_equipment_id'].'Club'.$cycleList['r_club_id']."'
                                            clubId='".$cycleList['r_club_id']."'
                                            class='customRadioCycle getcycleValue".$params['userId']." oxy'
                                            status='".$params['status']."'
                                            customselect='".$cycleList['r_equipment_id']."'
                                            customclass='getcycleValue".$params['userId']."'/>
                                    <label onclick='getValueByCheckLabel(this)'>".$cycleList['equipment_name'].'</label>
                                </div>
                            </div>';
            }
        }
        echo $resultSet;
    }
    */
    /**
     * settings::insertEmployeeClubRole()
     * Description : To insert employee club role
     * Param       : $params array
     * Fields         : $params['userTypeId'] and $params['clubId']
     * return      : Array.
     * @param $params
     * @return array object|string
     */
    public function insertEmployeeClubRole($params)
    {
        try {
            $params['mod'] = 'club';
            $params['method'] = 'insertEmployeeClubRole';

            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /**
     * settings::getEmployeeClubRoles()
     * Description : To get club user roles
     * Param       : None
     * Fields       : $params['userId'] and $params['companyId']
     * return      : Array.
     * @param $params
     * @return array object
     */
    public function getEmployeeClubRoles($params)
    {
        try {
            $params['mod'] = 'club';
            $params['method'] = 'getEmployeeClubRoles';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        if ($result['total_records'] > 0 && !isset($result['employeeClubRoles'][0])) {
            $result['employeeClubRoles'] = array($result['employeeClubRoles']);
        }

        if (isset($result['employeeClubRoles'])) {
            return $result['employeeClubRoles'];
        }
        return null;
    }

    /**
     * settings::getEmployeePrimaryClub()
     * Author : Nesarajan M
     * Description : To get user primary club
     * Fields       : $params['userId'] and $params['companyId']
     * return      : Array.
     * @param $params
     * @return array object
     */
    public function getEmployeePrimaryClub($params)
    {
        try {
            $params['mod'] = 'club';
            $params['method'] = 'getEmployeePrimaryClub';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        if ($result['total_records'] > 0 && !isset($result['employeeClubRoles'][0])) {
            $result['employeeClubRoles'] = array($result['employeeClubRoles']);
        }

        if (isset($result['employeeClubRoles'])) {
            return $result['employeeClubRoles'];
        }
        return null;
    }

    /**
     * settings::getAssociatedCentersForEmployeeWithRoles()
     * Author : Nesarajan M
     * Description : To get user primary and secondary club
     * Fields       : $params['userId'] and $params['companyId']
     * return      : Array.
     * @param $params
     * @return array object|string
     */
    public function getAssociatedCentersForEmployeeWithRoles($params)
    {
        try {
            $params['mod'] = 'club';
            $params['method'] = 'getAssociatedCentersForEmployeeWithRoles';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            if (!isset($result[0]['userType'])) {
                array($result);
            }
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /**
     * settings::getUsersClubList()
     * Description : To get club users
     * Param       : None
     * Fields       : $params['userId'] and $params['companyId'] 
     * return      : Array.
     ** /.
     public function getUsersClubList($params)
     {
     $clubList = array();
     /*Users Primary Club Id* /
     try {
     $params['mod'] = 'club';
     $params['method'] = 'getUsersClubList';
     $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
     } catch (Exception $e) {
     $result = 'Caught Exception:'.$e->getMessage();
     }
     
     if ($result['total_records'] > 0 && !isset($result['employeeClubPrimary'][0])) {
     $result['employeeClubPrimary'] = array($result['employeeClubPrimary']);
     }
     
     if (
        isset($result['employeeClubPrimary'][0]['r_club_id'])
        && $result['employeeClubPrimary'][0]['r_club_id'] != ''
     ) {
        array_push($clubList, $result['employeeClubPrimary'][0]['r_club_id']);
     }
     
     //Coach has rights to do on more clubs.
     * Members club can fall in only one
     * table that is primary record.
     * This function is not for members.
     if ($result['employeeClubPrimary'][0]['r_usertype_id'] != 3) {
     /*ALL club except Primary Club Id* /
     */
    /**
     * settings::deleteEmployeeClubRole()
     * Description : To get club user roles
     * Param       : None
     * Fields       : $params['employeeCentreId']
     * return      : Array.
     * @param $params
     * @return array object|string
     */
    public function deleteEmployeeClubRole($params)
    {
        try {
            $params['mod'] = 'club';
            $params['method'] = 'deleteEmployeeClubRole';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /* To get employee Center Details */
    public function getEmployeeCenterDetails($params)
    {
        try {
            $params['mod'] = 'club';
            $params['method'] = 'getEmployeeCenterDetails';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        if (!isset($result['movesmart']['employeeCenter'][0])) {
            $result['movesmart']['employeeCenter'] = array($result['movesmart']['employeeCenter']);
        }

        return $result['movesmart']['employeeCenter'];
    }

    /* To get club employees by club id */
    public function getCompanyEmployees($params)
    {
        try {
            $params['mod'] = 'club';
            $params['method'] = 'getCompanyEmployees';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    /* To get club employees by club id */

    /**
     * Function acts like a soft delete.
     * path format ?userId=1&action=deleteCoach&mod=club&method=deleteEmployeeCentre.
     * @param $params
     * @return array object|string
     */
    public function deleteEmployeeCentre($params)
    {
        try {
            $params['mod'] = 'club';
            $params['method'] = 'deleteEmployeeCentre';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /* Soft delete row by club id */
    public function deleteClubById($params)
    {
        try {
            $params['mod'] = 'club';
            $params['method'] = 'deleteClubById';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    /* Soft delete row by club id */
}
