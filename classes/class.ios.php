<?php
/**
 * PHP version 5.
 
 * @category Classes
 
 * @package IOS
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Class to handle mobile related functions.
 */
  /**
 * Class to handle userType related functions.
 
 * @category Classes
 
 * @package UserType
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/

 */
class ios extends common
{
    /*This function used to get all the users List.* /
    public function getMembersForTest($dataString)
    {
        $this->result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.$dataString);

        return json_decode($this->result, true);
    }

    /* This function used to change booking status * /
    public function changeBookingStatus($params)
    {
        try {
            $params['mod'] = 'ios';
            $params['method'] = 'changeBookingStatus';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /* This function used to call fitclass server to update booking attended status *
    public function changeBookingStatusInFitclass($params)
    {
        try {
            $params['mod'] = 'booking';
            $params['method'] = 'updateBooking';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH_FITCLASS.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /*This function used to Insert the Members Test Details.* /
    public function insertHeartRate($params)
    {
        try {
            $params['mod'] = 'ios';
            $params['method'] = 'insertHeartRate';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return json_encode($result, true);
    }

    /*This function used to get member heart rate details fof the latest test* /
    public function getHeartrateZoneActivity($params)
    {
        try {
            $params['userId'] = $params['userId'];
            $params['testId'] = $params['testId'];
            $params['deviceId'] = $params['deviceId'];
            $params['mod'] = 'pointsystem';
            $params['method'] = 'getPointsExternalActivity';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /*This function used to get member list based on search filter* /
    public function getMembersList($params)
    {
        try {
            $params['mod'] = 'ios';
            $params['method'] = 'getMembersList';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        if ($result['status'] == 'success' && !isset($result['members'][0])) {
            $result['members'] = array($result['members']);
        }

        return $result;
    }

    /*This function used to get member list training using Heartrate monitor id* /
    public function getMembersTrainingUsingHrm($params)
    {
        try {
            $params['mod'] = 'ios';
            $params['method'] = 'getMembersTrainingUsingHrm';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        if ($result['status'] == 'success' && !isset($result['members'][0])) {
            $result['members'] = array($result['members']);
        }

        return $result;
    }

    /*This device associated with any test* /
    public function checkDeviceAssociatedWithTest($params)
    {
        try {
            $params['mod'] = 'ios';
            $params['method'] = 'checkDeviceAssociatedWithTest';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /*This function used to get all the users List for test.* /
    public function getMemberListForTest($params)
    {
        try {
            $params['mod'] = 'ios';
            $params['method'] = 'getMemberListForTest';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /*This function used to get all the users List for training.* /
    public function getMemberListForTraining($params)
    {
        try {
            $params['mod'] = 'ios';
            $params['method'] = 'getMemberListForTraining';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /*This function used to get all the users List.* /
    public function getTestLevels($params)
    {
        $loadValue = $params['loadValue'];
        $weight = $params['weight'];
        $loadTimes = array();

        $firstPerConfig = '70%';
        $secondPerConfig = '80%';
        $thirdPerConfig = '90%';

        $firstVal = round(($firstPerConfig / 100) * $loadValue);
        $secondVal = round(($secondPerConfig / 100) * $loadValue);
        $thirdVal = round(($thirdPerConfig / 100) * $loadValue);

        $levelsArr = array();
        $levelsArr[0] = array('time' => (string) 0, 'load' => (string) $firstVal);
        $levelsArr[1] = array('time' => (string) 60, 'load' => (string) $secondVal);
        $levelsArr[2] = array('time' => (string) 120, 'load' => (string) $thirdVal);

        $inc = 5;
        $newVal = 150;
        $loadTimes[] = array('speed' => 5, 'time' => 150);
        for ($i = 5;$i <= 50;++$i) {
            $timeVal = ((200 / ($inc * 1000)) * 60 * 60);
            $timeVal += $newVal;
            $inc += 0.5;
            $loadTimes[] = array('speed' => $inc, 'time' => round($timeVal));
            $newVal = $timeVal;
        }

        $pre = 3;
        foreach ($loadTimes as $loadTime) {
            $load = round((($loadTime['speed'] * 2.98 + 4.25) * 0.9 * $weight - 320) / 11.35);
            $levelsArr[$pre] = array('time' => (string) $loadTime['time'], 'load' => (string) $load);
            ++$pre;
        }

        $result = array('result' => array('json_code' => '8',
                                          'test_machine' => '1',
                                          'levels' => $levelsArr,
                                          'error' => '0',
                                          'id' => '885129151', ));
        $result = json_encode($result, JSON_FORCE_OBJECT);

        $result = json_decode($result, true);

        return $result;

        /*try {
            $qryStr = '?path=cardiotest';
            $qryStr .= '&method=get_test_levels';
            $qryStr .= '&test_machine='.$params['test_machine'];
            $qryStr .= '&start_level='.$params['start_level'];
            $qryStr .= '&weight='.$params['weight'];		
            $userId=(isset($params['userId']) && $params['userId']<>"")?$params['userId']:"0";
            $result = parent::webServiceJsonToArray(POINT_SYSTEM_PATH.$qryStr,$userId);	
        } catch (Exception $e) {
            $result = "Caught Exception:" . $e->getMessage();
        }	
        return $result;* /
    }

    /*This function used to get all the users List.* /
    public function getTestInforForCoach($params)
    {
        try {
            $params['mod'] = 'ios';
            $params['method'] = 'getTestInforForCoach';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /*This function used update member test and test parameter data* /
    public function updateUserTestDetails($params)
    {
        try {
            $params['mod'] = 'ios';
            $params['method'] = 'updateUserTestDetails';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /* This function used for Members Email Already Exist * /
    public function checkMemberEmailExist($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'checkMemberEmailExist';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /* This function used to get insert available for Member * /
    public function insertMember($params)
    {
        try {
            $email = $params['email'];
            if ($email) {
                $param['email'] = $params['email'];
                $chkEmailExist = $this->checkMemberEmailExist($param);
                if ($chkEmailExist['movesmart']['status'] == 'Error') {
                    $result = $chkEmailExist;
                } else {
                    $params['mod'] = 'members';
                    $params['method'] = 'insertMember';
                    $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
                }
            }
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /* This function used to map heartrate device to member * /
    public function mapHrmIdForMember($params)
    {
        try {
            $params['mod'] = 'ios';
            $params['method'] = 'mapHrmIdForMember';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /* This function used to insert the Device and MemberDetails */
    /*public function insertHeartRateMonitor($params){
        try {
            $params['mod'] = "ios";
            $params['method'] = "insertHeartRateMonitor";
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            $deviceId = $result['movesmart']['device_id'];
            
            //For Already Device member mapped or not checking..
            $param['mod'] = "ios";
            $params['method'] = "getDeviceMember";
            $params['device_id'] = $deviceId;
            $result1 = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            
            if($result1['movesmart']['status'] == "success"){
                $getUser = $this->insertMember($params);
                $userId = $getUser['movesmart']['user_id'];			
                if($userId) {
                    $param['device_id'] = $deviceId;
                    $param['mod'] = "ios";
                    $param['method'] = "insertDeviceMember";
                    $param['r_user_id'] = $userId;
                    $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($param));
                }
            }else{
               $result = $result1;					
            }
        } catch(Exception $e) {
            $result = "Caught Exception:" . $e->getMessage();
        }	
        return $result;
    }*/

    /* This function used to insert heart rate device if not exist * /
    public function insertHeartRateMonitor($params)
    {
        try {
            $params['mod'] = 'ios';
            $params['method'] = 'insertHeartRateMonitor';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /* This function used to get Device Associated To Member * /
    public function getDeviceAssociatedtoMember($params)
    {
        try {
            $params['mod'] = 'ios';
            $params['method'] = 'getDeviceAssociatedtoMember';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /* This function used to puch centre Cycle Availabilty * /
    public function pushCycleAvailability($params)
    {
        try {
            $params['mod'] = 'ios';
            $params['method'] = 'pushCycleAvailability';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /* This function used to get Cycle Availabilty * /
    public function setCycleWithTablet($params)
    {
        $params['mod'] = 'ios';
        $params['method'] = 'setCycleWithTablet';
        $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));

        return $result;
    }

    /* This function used to get the Cycle Associated With Members* /
    public function getCycleAssociatedWithMembers($params)
    {
        try {
            $params['mod'] = 'ios';
            $params['method'] = 'getCycleAssociatedWithMembers';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            if ($result['membersAssociated']['membersList']) {
                if (!isset($result['membersAssociated']['membersList'][0])) {
                    $result['membersAssociated']['membersList'] = array($result['membersAssociated']['membersList']);
                }
                $result = $result['membersAssociated'];
            } else {
                $result = 0;
            }
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /* This function used to get the Cycle Associated With Members* /
    public function getMemberAssociatedWithCycle($params)
    {
        try {
            $params['mod'] = 'ios';
            $params['method'] = 'getMemberAssociatedWithCycle';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /* This function used to insert Member Test Results * /
    public function insertTestResults($params)
    {
        try {
            $params['mod'] = 'ios';
            $params['method'] = 'insertTestResults';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /* This function used to cycle ID and Status* /
    public function getCycleStatus($params)
    {
        try {
            $params['mod'] = 'ios';
            $params['method'] = 'getCycleStatus';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /* This function used to Assign Table ID* /
    public function getAssociatedWithTablet($params)
    {
        try {
            $params['mod'] = 'ios';
            $params['method'] = 'getAssociatedWithTablet';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /* This function used to assign cycle for user* /
    public function assignCycleForMember($params)
    {
        try {
            $params['mod'] = 'ios';
            $params['method'] = 'getAssociatedWithTablet';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        try {
            $params['mod'] = 'ios';
            $params['method'] = 'assignCycleForMember';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /* This function used to Get Test Levels for Member* /
    public function getTestLevelsForTablet($params)
    {
        try {
            $params['mod'] = 'ios';
            $params['method'] = 'getTestLevelsForTablet';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['loadValues'];
    }

    /* This function used to get Heart Rate for Member * /
    public function getTestHeartRateForTablet($params)
    {
        try {
            $params['mod'] = 'ios';
            $params['method'] = 'getTestHeartRateForTablet';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart']['heartRate'];
    }

    /* This function Used to get all the users Sportmed Graph * /
    public function post_sportmed_test($params)
    {
        try {
            $qryStr = '?path='.$params['path'];
            $qryStr .= '&method='.$params['method'];
            $qryStr .= '&age='.$params['age'];
            $qryStr .= '&testmachine='.$params['testmachine'];
            $qryStr .= '&weight='.$params['weight'];
            $qryStr .= '&start_level='.$params['start_level'];
            $qryStr .= '&gender='.$params['gender'];
            $qryStr .= '&samples_interval='.$params['samples_interval'];
            $qryStr .= '&heartrates='.$params['heartrates'];
            $userId = (isset($params['userId']) && $params['userId'] != '') ? $params['userId'] : '0';
            $result = parent::webServiceJsonToArray(POINT_SYSTEM_PATH.$qryStr, $userId);
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /* This function used to save SportmedTest * /
    public function saveSportmedTest($params)
    {
        try {
            $params['mod'] = 'ios';
            $params['method'] = 'saveSportmedTest';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart']['heartRate'];
    }

    /* This function used to get Cycle Availability * /
    public function getCycleAvailability($params)
    {
        try {
            $params['mod'] = 'ios';
            $params['method'] = 'getCycleAvailability';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart']['cycleList'];
    }

    public function splitHeartRateFromConsolidatedForSeconds($heartRates)
    {
        $arr = explode('|', $heartRates);
        $a = 0;
        $narr[] = $arr[0];
        foreach ($arr as $val) {
            if ($a % GET_HEART_RATE_INTERVAL == 0) {
                $narr[] = $val;
            }
            ++$a;
        }

        return implode('|', $narr);
    }

    public function reAssignDevice($params)
    {
        try {
            $params['mod'] = 'ios';
            $params['method'] = 'reAssignDevice';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart'];
    }

    /*This function used to get equipemt Id.* /
    public function getEquipmentId($params)
    {
        try {
            $params['mod'] = 'ios';
            $params['method'] = 'getEquipmentId';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart']['equipmentList']['equipment_id'];
    }

    /*This function used to get cycle test load values.* /
    public function getCycleTestlevels($params)
    {
        try {
            $params['mod'] = 'ios';
            $params['method'] = 'getCycleTestlevels';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        $count = 0;
        if (!isset($result['movesmart']['testLevels'][0])) {
            $result['movesmart']['testLevels'] = array($result['movesmart']['testLevels']);
        }
        for ($i = 0;$i < count($result['movesmart']['testLevels']);++$i) {
            count($result['movesmart']['testLevels']);

            //status If test completed
            if (isset($result['movesmart']['testLevels'][$i])
                && isset($result['movesmart']['testLevels'][$i]['status'])
                && $result['movesmart']['testLevels'][$i]['status'] == 3
            ) {
                $count = $count + 1;
            }
            //If contains all test has been completed.
            if (!isset($result['movesmart']['testLevels'][$i]['status'])) {
                $result['movesmart']['testLevels']['status']['error'] = 'No test levels found!';

                return $result['movesmart']['testLevels'];
            } elseif (isset($result['movesmart']['testLevels'][$i])
                    && $result['movesmart']['testLevels'][$i]['status'] == 3
                    && count($result['movesmart']['testLevels']) == $count
            ) {
                $result['movesmart']['testLevels']['status']['error'] = 'The test has been completed already!';
                return $result['movesmart']['testLevels'];
            } elseif (isset($result['movesmart']['testLevels'][$i])
                && $result['movesmart']['testLevels'][$i]['status'] != 3
            ) {
                return $result['movesmart']['testLevels'][$i]['load_value'];
            }

            if (count($result['movesmart']['testLevels']) == $count) {
                $result['movesmart']['testLevels']['status']['error'] = 'No test levels found!';

                return $result['movesmart']['testLevels'];
            }
        }
    }

    /*This function used to insert Rpm And HR.* /
    public function insertRpmFromCCA($params)
    {
        try {
            $params['mod'] = 'ios';
            $params['method'] = 'insertRpmFromCCA';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart'];
    }

    public function getSameAssignedDevice($params)
    {
        try {
            $params['mod'] = 'ios';
            $params['method'] = 'getSameAssignedDevice';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart'];
    }

    /**
     *  Author      : Jeyaram.A
     *  Description : this function used for insert member training points.
     * /
    public function insertMemberTrainingPoints($params)
    {
        try {
            $params['mod'] = 'ios';
            $params['method'] = 'insertMemberTrainingPoints';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart'];
    }

    /**
     *
    public function getTrainingDeviceList($params)
    {
        try {
            $params['mod'] = 'ios';
            $params['method'] = 'getTrainingDeviceList';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart']['trainingDevice'];
    }

    /*This function used to get members list for strength training * /
    public function getMemberListForStrengthTraining($params)
    {
        try {
            $params['mod'] = 'ios';
            $params['method'] = 'getMemberListForStrengthTraining';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /*This function used to get members strength test activity for strength training * /
    public function getMemberStrengthTestActivity($params)
    {
        try {
            $params['mod'] = 'ios';
            $params['method'] = 'getMemberStrengthTestActivity';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }*/
} // End Class.
