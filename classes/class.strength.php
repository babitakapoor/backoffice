<?php
/**
 * PHP version 5.
 
 * @category Classes
 
 * @package Strength
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Class to handle strength related functions.
 */
/**
 * Class to handle userType related functions.
 
 * @category Classes
 
 * @package UserType
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/

 */
class strength extends common
{
      /**
    * Returns an json obj of  calculate BMI
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function calculateBMI($params)
    {
        $height = $params['height'];
        $weight = $params['weight'];
        $gender = $params['gender'];

        /* Weight (kg) / (height(m) * height(m))  , weight in kg, height in meters */
        $bmi = $weight / ($height * $height);
        /*- when gender is male
                if  BMI    < 30 then FAT = 20 else FAT = 25
           - when gender is female
                if  BMI    < 30 then FAT = 25 else FAT = 32*/

        $bmi = ($bmi < 30) ? 20 : 25;
        if ($gender == 1) {
            $bmi = ($bmi < 30) ? 25 : 32;
        }

        return $bmi;
    }

      /**
    * Returns an json obj of  Calculate Lean based on input
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function calculateLean($params)
    {
        $weight = $params['weight'];
        $bmi = $params['bmi'];
        //Calculate Lean Lean =  (1-Fat/100) * Weight
        $lean = (1 - ($bmi / 100)) * $weight;

        return $lean;
    }
      /**
    * Returns an json obj of  Calculate strength based on input
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function calculateStrength($params)
    {
        $lean = $params['lean'];
        $coeft = $params['coeft'];

        $strength = round($lean * $coeft);

        return $strength;
    }
       /**
    * Returns an json obj of  get strength program training html
    * @param string $params service parameter
    *
    * @return string
    */
    public function getStrengthProgramTrainingHtml($params)
    {
        $settings = new settings();
        $trainingList = $settings->getStrengthTrainingList($params);
        $status = ($trainingList['status_code'] == 200) ? 1 : 0;
        $html = '';
        if ($status) {
            $trainList = $trainingList['rows'];
            foreach ($trainList as $value) {
                $html .= '<tr>
                            <td>'.$value['tweek'].'</td>
                            <td>'.$value['series'].'</td>
                            <td>'.$value['reputation'].'</td>
                            <td>'.$value['time'].'</td>
                            <td>'.$value['strengthpercentage'].'</td>
                            <td>'.$value['rest'].'</td>
                            </tr>';
            }
        } else {
            $html .= '<tr><td colspan="20">No result found</td></tr>';
        }

        return $html;
    }
      /**
    * Returns an json obj of  Add/Update strength user test
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function insertUpdateUserTest($params)
    {
        try {
            $params['mod'] = 'strength';
            $params['method'] = 'insertUpdateUserTest';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
       /**
    * Returns an json obj of  remove strength user test activities
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function removeStrengthTestActivity($params)
    {
        try {
            $params['mod'] = 'strength';
            $params['method'] = 'removeStrengthTestActivity';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of  get strength user list
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getStrengthUserList($params)
    {
        try {
            $params['mod'] = 'strength';
            $params['method'] = 'getStrengthUserList';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
      /**
    * Returns an json obj of  create training week schedule
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function insertUpdateStrengthTrainingWeek($params)
    {
        try {
            $params['mod'] = 'strength';
            $params['method'] = 'insertUpdateStrengthTrainingWeek';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

      /**
    * Returns an json obj of  Add/Update strength user test activities
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function insertUpdateUserTestActivity($params)
    {
        try {
            $params['mod'] = 'strength';
            if ((isset($params['flag'])) && ($params['flag'] == 1)) {
                $params['method'] = 'insertUserTestactivitybyaddProgram';
            } else {
                $params['method'] = 'insertUpdateUserTestActivity';
            }

            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
      /**
    * Returns an json obj of  get strength test basic information
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getStrengthTestDetail($params)
    {
        try {
            $params['mod'] = 'strength';
            $params['method'] = 'getStrengthTestDetail';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    /**
    * Returns an json obj of  get strength test list by user
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getStrengthTestList($params)
    {
        try {
            $params['mod'] = 'strength';
            $params['method'] = 'getStrengthTestList';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            if ($result['status'] == 'success' && !isset($result['rows'][0])) {
                $result['rows'] = array($result['rows']);
            }
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    /**
    * Returns an json obj of  get strength test activities 
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getStrengthTestActivities($params)
    {
        try {
            $params['mod'] = 'strength';
            $params['method'] = 'getStrengthTestActivities';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            if ($result['status'] == 'success' && !isset($result['rows'][0])) {
                $result['rows'] = array($result['rows']);
            }
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

      /**
    * Returns an json obj of  update strength activity value
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function updateActivityStrengthValue($params)
    {
        try {
            $params['mod'] = 'strength';
            $params['method'] = 'updateActivityStrengthValue';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

      /**
    * Returns an json obj of  add/update insert points to achieve
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function insertPointsToachieve($params)
    {
        try {
            $params['mod'] = 'strength';
            $params['method'] = 'insertPointsToachieve';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
        /**
    * Returns an json obj of  get test date value
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function gettestdatevalue($params)
    {
        try {
            $params['mod'] = 'strength';
            $params['method'] = 'gettestdatevalue';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /**
     * Returns an json obj of  generate start date and end date for the configured weeks
     * @param $testStartDate
     * @param $noOfweeks
     * @return array object object
     * @internal param string $params service parameter
     */
    public function getTrainingWeekDates($testStartDate, $noOfweeks)
    {
        $trainingStartDate = date('Y-m-d', strtotime($testStartDate));
        $startDate  =   '';
        for ($i = 1; $i <= 7;++$i) {
            $datem = date('Y-m-d', strtotime($trainingStartDate.'+'.$i.' days'));
            $daten = date('N', strtotime($trainingStartDate.'+'.$i.' days'));
            if ($daten == 1) {
                $startDate = date('Y-m-d', strtotime($datem));
                break;
            }
        }

        /*Note:
            --> Now only first week start date starts from training start date
            --> Other weeks from monday - sunday 
            --> Number of weeks configured in include/define file
        */

        $weeks = $noOfweeks;
        $days = 6;
        $nextDate = $startDate;
        $weekArray  =   array();
        for ($i = 1; $i <= $weeks; ++$i) {
            $endDate = date('Y-m-d', strtotime($nextDate.'+'.$days.' days'));

            //Start 1 week from training date
            if ($i == 1) {
                $nextDate = $trainingStartDate;
            }

            $weekArray[$i] = array('start_date' => $nextDate, 'end_date' => $endDate);
            $nextDate = date('Y-m-d', strtotime($endDate.'+1 days'));
        }

        return $weekArray;
    }
}
