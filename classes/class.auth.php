<?php
/**
 * PHP version 5.
 
 * @category Classes
 
 * @package Login
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Class to handle login related functions.
 */
    /**
 * Class to handle userType related functions.
 
 * @category Classes
 
 * @package UserType
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/

 */

include '../service/utility/cryptojs-aes.php';
require '../service/jwt/vendor/autoload.php';
class auth extends common
{
        /**
    * Returns an json obj of authenticate the user using password(login functionality).
    * @param string $params service parameter
    *
    * @return array object object
    */
    
    protected $authkey = 'm0v35m@r75173C0rQ4*k';
    protected $sessionkey = 'm0v9835m@rr31^1||34';
    protected $addTime =  '86400';
    
    public function __construct()
    {
		
		if(isset($_POST['register_process']) && $_POST['register_process'] == 1)
		{
			$this->registerWebUser($_POST,$_FILES);
		}
		
		if(isset($_POST['forgot_password_process']) && $_POST['forgot_password_process'] == 1)
		{
			$this->forgotpassword($_POST);
		}
	}
    
    
	protected function todayTimeStamp()
	{
		$timestring = strtotime(date('d-m-Y'));
		return $timestring;
	}
	
    protected function decrypdata($token)
    {
		$decryptToken = cryptoJsAesDecryptBase64CoveredAuth($this->sessionkey,$token);
		$data = explode("_",$decryptToken);
		$params['company_id'] = $data[0];
		$params['email'] = $data[1];
		$params['user_id'] = $data[2];
		$params['key'] = $data[3];
		$params['timestamp'] = $data[4];
		return $params;
	}
    
    protected function checkToken($param)
    {
		$token = $param['token'];
		$params  = $this->decrypdata($token);
		if($params['company_id'] != '' && $params['email'] != '' &&  $params['user_id'] != '')
		{
			if($params['timestamp'] ==  $this->todayTimeStamp())
			{
				$params['mod'] = 'auth';
				$params['method'] = 'checkUser';
				$res = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
				if($res['status_code'] = '200')
				{
					$result['code'] = 200;
					$result['data'] = $params;
				}
				else
				{
					$result['code'] = 101;
				}
			}
			else
			{
				$result['code'] = 301;
			}
		}
		else
		{
			$result['code'] = 102;
			
		}
		return $result;
		die;
	}
    
    protected function codecheme($code)
    {
		$message = '';
		switch($code)
		{
			case 103:
				$message = 'Invalid Login Details';
				break;
			case 101:
				$message = 'Account Not Present';
				break;
			case 102:
				$message = 'Details missing';
				break;
			case 301:
				$message = 'Token expires';
				break;
			case 200:
				$message = 'Success';
				break;
			case 201:
				$message = 'Company Data Not found';
				break;
			case 801:
				$message = 'Not Authorized';
				break;
			case 601:
				$message = 'Not Valid Email';
				break;
			default:
				$message = 'Not Valid Call';
				break;
		}
		return $message;
	}
    
    protected function createToken($data)
    {
		$email = $data['email'];
		$user_id = $data['user_id'];
		$company_id = $data['company_id'];
		$key = $this->authkey;
		$timestamp = $this->todayTimeStamp();
		$value = $company_id."_".$email."_".$user_id."_".$key."_".$timestamp;
		$token = cryptoJsAesEncryptBase64Covered($this->sessionkey,$value);
		return $token;
	}
    
    public function decryptlogindata($token)
    {
		$decryptToken = cryptoJsAesDecryptBase64CoveredAuthLogin($this->sessionkey,$token);
		$dat = '?'.$decryptToken;
		parse_str(parse_url($dat, PHP_URL_QUERY),$data);
		$params['username'] = $data['email'];
		$params['password'] = $data['password'];
		$params['authKey'] = $data['authKey'];
		return $params;
	}
    
    public function authenticate($param= array())
    {
		$hasheddata = $param['user_data'];
		$params = $this->decryptlogindata($hasheddata);
		if($this->authkey == trim($params['authKey']))
		{
			if (filter_var($params['username'], FILTER_VALIDATE_EMAIL)) 
			{
				//$companyCheck = $this->checkCompany($post['authKey']);
				$params['company_id'] = 1;
				$params['mod'] = 'auth';
				$params['method'] = 'authenticate';
				$res = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
				if($res['status_code'] == '200')
				{
					$getToken =  $this->createToken($res['data']);
					$result['code'] = $res['status_code'];
					$result['message'] = $this->codecheme($res['status_code']);
					$result['token'] = $getToken;
				}
				else
				{
					$result['code'] = $res['status_code'];
					$result['message'] = $this->codecheme($res['status_code']);
				}
			}
			else
			{
				$result['code'] = 601;
				$result['message'] = $this->codecheme(601);
			}
		}
		else
		{
			$result['code'] = 801;
			$result['message'] = $this->codecheme(801);
		}
		return $result;
    }
    
    public function genrateToken($param)
    {
		$token = $param['token'];
		$params  = $this->decrypdata($token);
		if($params['company_id'] != '' && $params['email'] != '' &&  $params['user_id'] != '')
		{
			$params['mod'] = 'auth';
			$params['method'] = 'checkUser';
			$res = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
			if($res['status_code'] = '200')
			{
				$token = $this->createToken($params);
				$result['code'] = $res['status_code'];
				$result['message'] = $this->codecheme($res['status_code']);
				$result['token'] = $token;
			}
			else
			{
				$result['code'] = 101;
				$result['message'] = $this->codecheme(101);
			}
		}
		else
		{
			$result['code'] = 102;
			$result['message'] = $this->codecheme(102);
		}
		return $result;
	}
    
    public function getUserData($params = array())
	{
		if(isset($params['token']))
		{
			$check = $this->checkToken($params);
			if($check['code'] == 200)
			{
				$params = $check['data'];
				$params['mod'] = 'auth';
				$params['method'] = "getUserData";
				$res = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
				if($res['status_code'] == 200)
				{
					$result['code'] = $res['status_code'];
					$result['message'] = $this->codecheme($res['status_code']);
					$result['data'] = $res['data'];
				}
				else
				{
					$result['code'] = $res['status_code'];
					$result['message'] = $this->codecheme($res['status_code']);
				}
			}
			else
			{
				$result['code'] = $check['code'];
				$result['message'] = $this->codecheme($check['code']);
			}
		}
		else
		{
			$result['code'] = 801;
			$result['message'] = $this->codecheme(801);
		}
		return $result;
	}
	
	public function validateToken($params = array())
	{
		if(isset($params['token']))
		{
			$check = $this->checkToken($params);
			if($check['code'] == 200)
			{
				$result['code'] = $check['code'];
				$result['data'] =  $this->codecheme($check['code']);
			}
			else
			{
				$result['code'] = $check['code'];
				$result['message'] = $this->codecheme($check['code']);
			}
		}
		else
		{
			$result['code'] = 801;
			$result['message'] = $this->codecheme(801);
		}
		return $result;
	}
	
	public function registerWebUser($post = array(),$file = array())
    {
		
		$url = "index.php?register_params={$post['register_params']}";
		$companyCheck = $this->checkCompany($post['register_params']);
		if($companyCheck['status_code'] == '200')
		{
			$post['associated_company_id'] = $companyCheck['company_id'];
			$check = $this->checkMemberEmailExist(array('email'=>$post['email'],'company_id'=>$post['associated_company_id']));
			if ($check['status_code'] != '200') 
			{
				$post['user_image'] = '';
				if(isset($file['user_image']))
				{
					$allowdedExt =  array('jpg','jpeg','png','gif');
					$filename = $file['user_image']['name'];
					$ext = end(explode(".",$filename));
					if(in_array($ext,$allowdedExt))
					{
						$newfilename = date('Ymdhis.').$ext;
						$tmp_name = $file['user_image']['tmp_name'];
						$dir = SERVICE_PROFILE_MEMBERURL;
						if (move_uploaded_file($tmp_name, $dir.$newfilename)) 
						{
							$post['userimage'] = $newfilename;
						}
					}
				}
				$params = $post;
				$params['mod'] = 'auth';
				$params['method'] = 'registerWebUser';
				$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
				if($result['status_code'] == '200')
				{
					
					$message ='';
					$message .= "Hi {$post['first_name']},<br/>";
					$message .= "Welcome to Movesmart. Your account is created successfully<br/>";
					$message .= "Below are your login details<br/>";
					$message .= "Username = {$post['email']}<br/>";
					$message .= "Password = {$post['password']}<br/>";
					$message .= "Thanks<br/>";
					
					$mailData['to'] = $post['email'];
					$mailData['subject'] = 'Registeration Successfull';
					$mailData['message'] = $message;
					$this->common->sendEmail($mailData);
					
					$_SESSION['flMsg']['flashMessageSuccess'] = $result['status_message'];
				}
				else
				{	
					$_SESSION['flMsg']['flashMessageError'] = $result['status_message'];
					$_SESSION['flMsg']['data'] = $post;
				}
				
			}
			else 
			{
				$_SESSION['flMsg']['flashMessageError'] =  'Email is already in use';
				$_SESSION['flMsg']['data'] = $post;
			}
		}
		else
		{
			$_SESSION['flMsg']['flashMessageError'] =  'Request is not Valid';
			$_SESSION['flMsg']['data'] = $post;
		}
		
		header('Location:'.$url);
		die;
    }
    
    protected function checkCompany($token)
    {
		$result['status_code'] = 200;
		$result['company_id'] = 1;
		return $result;
	}
    
    protected function checkMemberEmailExist($params)
    {
		
        try {
            /* Check Email Exist Status Message */
            $param['email'] = $params['email'];
            $param['company_id'] = $params['company_id'];
            $param['mod'] = 'auth';
            $param['method'] = 'checkValidEmail';
           // echo WEBSERVICE_PATH.QN.http_build_query($param);die;
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($param));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
		return $result;
    }
    
   
	
	public function forgotpassword($post)
	{
		$url = "index.php?register_params={$post['register_params']}";
		$companyCheck = $this->checkCompany($post['register_params']);
		if($companyCheck['status_code'] == '200')
		{
			$post['company_id'] = $companyCheck['company_id'];
			$check = $this->checkMemberEmailExist(array('email'=>$post['email'],'company_id'=>$post['company_id']));
			if ($check['status_code'] == '200') 
			{
				$params = $post;
				$params['mod'] = 'auth';
				$params['method'] = 'forgotpassword';
				$params['password'] = $this->generateRandomString();
				$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
				if($result['status_code'] == '200')
				{
					
					$message ='';
					$message .= "Hi {$post['first_name']},<br/>";
					$message .= "Your password is reseted successfully<br/>";
					$message .= "Below is your new password<br/>";
					$message .= "Password = {$params['password']}<br/>";
					$message .= "Thanks<br/>";
					
					$mailData['to'] = $post['email'];
					$mailData['subject'] = 'Registeration Successfull';
					$mailData['message'] = $message;
					$this->common->sendEmail($mailData);
					
					$_SESSION['flMsg']['flashMessageSuccess'] = $result['status_message'];
				}
				else
				{	
					$_SESSION['flMsg']['flashMessageError'] = $result['status_message'];
					$_SESSION['flMsg']['data'] = $post;
				}
			}
			else 
			{
				$_SESSION['flMsg']['flashMessageError'] =  'Email not present';
				$_SESSION['flMsg']['data'] = $post;
			}
		}
		else
		{
			$_SESSION['flMsg']['flashMessageError'] =  'Request is not Valid';
			$_SESSION['flMsg']['data'] = $post;
		}
		header('Location:'.$url);
		die;
	}
	
	protected function generateRandomString($length = 10) 	
	{		
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';		
		$charactersLength = strlen($characters);		
		$randomString = '';		
		for ($i = 0; $i < $length; $i++) 
		{	
			$randomString .= $characters[rand(0, $charactersLength - 1)];		
		}		
		return $randomString;	
	}	
}
