<?php
/**
 * Author        :   Senthil kumar L 
 * Date          :   14-Oct-2014
 * Modified      :    
 * Modified By   :    
 * Description   :  List club based on the company id.
 * REQUEST FORMAT will be /ios/index.php?p=getClubs&param={"companyId":"2"}
*/
 
// To list all the centers. 
$params = json_decode($_REQUEST['param'], 1);
$message = array();
$authorizedClubId = '';
$params['companyId'] = (isset($params['companyId']) && $params['companyId'] != '') ? $params['companyId'] : '';
$params['company_id'] = $params['companyId'];
$params['is_deleted'] = 0;
$params['userId']=isset($_REQUEST['userId'])? $_REQUEST['userId']: ''; 

if($params['companyId'] == ''){
	$message = array('status' => '0', "Error" => 'Provide the CompanyId');
	echo json_encode($message);
	die;
}else{
	//Primary Club For User
	
	 if($params['userId']!="") {
		 $params['authorizedClubId']= join(",",$this->club->getUsersClubList($params));
	 } else {	
		 $params['authorizedClubId'] =(isset($authorizedClubId))? $authorizedClubId:"";
	 }	 
	 $clubList = $this->club->getClubList($params);
	 $a = 1; 
	 foreach($clubList as $row){
		$rowSets[] = "club_id:".$row['club_id'].",club_name:".$row['club_name'];
		
		$clubs[] = array("club_id"   => $row['club_id'],
							 "club_name"  => $row['club_name']						
							 );
	 }	 
	 $result = array('clubList' => $clubs);
	 echo json_encode($result);	
}
 
?>