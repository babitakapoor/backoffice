<?php	
 // REQUEST FORMAT: ios/index.php?p=getStatusOfTheTest&param={"clubId":"3","companyId":"2"}
 
// Get Cycle Availablity.

/* 
	- This page shows the cycle status engaged with some test
	- Listing the cycle availability and mapped with any member or not.
	- If the cycles is mapped, shows the status of the test like "Not started","Started","Cool down" and "Test completed/Interrupted".
*/

$params = json_decode($_REQUEST['param'], 1);

//Call get cycle availability webservice
$getCycleAvailability = $this->ios->getCycleAssociatedWithMembers($params); 
//echo "<pre>"; print_r($getCycleAvailability); die;
if(isset($getCycleAvailability['membersList'])) {

	$list = $getCycleAvailability['membersList'];	 
	//Prepare return cycle available array
	$a = 1; 
		
	foreach($list as $row) {
		$clubId = $row['club_id'];
		$clubName = $row['club_name'];
		$cycleName = ($row['cycle'] != '')?$row['cycle']:0;
		if($row['currentStatusOfTest'] > 0){
		     $cycle[$a++] = array("memberId"   => $row['user_id'],
							 "firstName"  => $row['first_name'],
							 "lastName"   => $row['last_name'],
							 "email"      => $row['email'],
							 "cycle"      => $cycleName,
							 "testStatus" => $row['test_status']
							 );
		}
	}
	
	$result = array('status' => 0);	
	if(isset($clubId)) {
		$result = array("clubId"=> $clubId, "clubName"=> $clubName, "members" => $cycle);
	}
	
}
else {
	$result = array('status' => 'error','status_message'=>'No records found!');
}
echo $result=json_encode($result);
?>
 