<?php 
/**
 * Author        :   Jeyaram.A 
 * Date          :   29-Sep-2014
 * Modified      :   Nesarajan M
 * Modified By   :   17-Oct-2014
 * Description   :   Get Cycle Availability.
 */
 // REQUEST FORMAT: ios/index.php?p=pushCycleAvailability&param={"clubId":"3","companyId":"2",param={"clubId":"1","companyId":"1","cycleList":{"cycle1":"1","cycle2":"1","cycle3":"1","cycle4":"1"}}
 
// Get Cycle Availablity.

$params = json_decode($_REQUEST['param'], 1);



$message = "failure to updated";
$error = "1";

//Call get cycle availability webservice
$pushCycleAvailability = $this->ios->pushCycleAvailability($params); 

if($pushCycleAvailability['status'] == 'success') {
	$message = "successfully updated";		
	$error = "0";
}

$result = array("status Message"=>$message, "error"=>$error);

echo $result=json_encode($result);
?>
