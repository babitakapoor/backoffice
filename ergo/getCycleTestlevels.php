<?php
/**
 * Modified      :    
 * Modified By   :    
 * Description   :   get Load values with company Id, Club Id and cycle Id.
 * REQUEST FORMAT will be /ios/index.php?p=getCycleTestlevels&companyId=2&clubId=1&cycle_number=1
*/
 
$params['companyId'] = (isset($_REQUEST['companyId'])&&$_REQUEST['companyId']<>'')?$_REQUEST['companyId']: die(JSON_encode(array("status"=>"error","status_msg"=>"Required Company Id")));
$params['clubId'] = (isset($_REQUEST['clubId']) && $_REQUEST['clubId']<>'') ? $_REQUEST['clubId'] : die(JSON_encode(array("status"=>"error","status_msg"=>"Required club Id")));
$params['cycleNum'] = (isset($_REQUEST['cycle_number']) && $_REQUEST['cycle_number']<>'') ? $_REQUEST['cycle_number'] : die(JSON_encode(array("status"=>"error","status_msg"=>"Required cycleId Id")));

//Get cycle id with cycle name
$params['cycleId']=$this->ios->getEquipmentId($params);
echo ($params['cycleId']<>"")?"":die(JSON_encode(array("status"=>"error","status_msg"=>"Test level not found for the cycle")));

//Load value
$cycleTestLevels = $this->ios->getCycleTestlevels($params);

if(isset($cycleTestLevels['status']['error'])){
	echo $result=json_encode($cycleTestLevels['status']);
} else {
	$cycleTestLevels = str_replace("[", "", $cycleTestLevels);
	$cycleTestLevels = str_replace("]", "", $cycleTestLevels);
	$cycleTestLevels = str_replace(",{", "{", $cycleTestLevels);

	$cycleTestLevelsArray=explode("}",$cycleTestLevels);

	array_pop($cycleTestLevelsArray);

	echo $result = "levels:{".implode('},',
				array_map(
					function ($value, $key) { 
						return sprintf('"%s":%s', $key, $value);
					},
					$cycleTestLevelsArray,
					array_keys($cycleTestLevelsArray)
				)
			)."}}";
} 
?>