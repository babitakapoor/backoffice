<?php
/**
 * Author        : Punitha 
 * Date          : 24-Oct-2014
 * Modified by   : 
 * Modified By   :  
 * Description   : push RPM and HR
 * REQUEST FORMAT will be ?p=pushRpmAndHr&param={"clubId":"1","companyId":"2","cycles":[{"cycleId":"1","rpm":"22"}]}
*/

$parameter = json_decode($_REQUEST['param'],true);

$params['companyId'] = (isset($parameter['companyId']) && $parameter['companyId'] != '') ? $parameter['companyId'] : die("Required company ID");
$params['clubId'] = (isset($parameter['clubId']) && $parameter['clubId'] != '') ? $parameter['clubId'] : die("Required Club ID");
$params['cycles'] = (isset($parameter['cycles']) && $parameter['cycles'] != '') ? $parameter['cycles'] : die("Required Cycles");
$result = $this->ios->insertRpmFromCCA($params);

echo $result=json_encode($result,true);
?>